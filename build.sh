mkdir sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish
mkdir sager.calculo.parametro.hdp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hdp.Process/publish
mkdir sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish
mkdir sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish
mkdir sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish
mkdir sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish
mkdir sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish
mkdir sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish
mkdir sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish

cp -r sager.calculo.api.fabrica/Mapa sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish
cp -r sager.calculo.api.fabrica/Metadados sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish
cp sager.calculo.api.fabrica/Dockerfile sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish
cp sager.calculo.api.fabrica/plataforma.json sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish


mkdir  ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish

cp -r ons.saat.casobase.manter/Mapa ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish
cp -r ons.saat.casobase.manter/Metadados ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish
cp ons.saat.casobase.manter/Dockerfile ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish
cp ons.saat.casobase.manter/plataforma.json ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish

rm -rf ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/bin
rm -rf ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web/publish/*.dll


cd ons.saat.casobase.manter/Server/ONS.SAAT.CasoBase.Manter.Web
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish


python ~/ons_git/plataforma/deploy/main.py --build --platform docker --environment tst
python ~/ons_git/plataforma/deploy/main.py --register_schema --platform docker --environment tst
python ~/ons_git/plataforma/deploy/main.py --run_presentation --environment tst --platform docker





cp -r sager.calculo.parametro.hp.fabrica_1.0/Mapa sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish
cp -r sager.calculo.parametro.hp.fabrica_1.0/Metadados sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish
cp sager.calculo.parametro.hp.fabrica_1.0/Dockerfile sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish
cp sager.calculo.parametro.hp.fabrica_1.0/plataforma.json sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish

cp -r sager.calculo.taxa.teifa.acum.fabrica_1.0/Mapa sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish
cp -r sager.calculo.taxa.teifa.acum.fabrica_1.0/Metadados sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish
cp sager.calculo.taxa.teifa.acum.fabrica_1.0/Dockerfile sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish
cp sager.calculo.taxa.teifa.acum.fabrica_1.0/plataforma.json sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish

cp -r sager.calculo.taxa.teifa.fabrica_1.0/Mapa sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish
cp -r sager.calculo.taxa.teifa.fabrica_1.0/Metadados sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish
cp sager.calculo.taxa.teifa.fabrica_1.0/Dockerfile sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish
cp sager.calculo.taxa.teifa.fabrica_1.0/plataforma.json sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish

cp -r sager.calculo.taxa.teip.acum.fabrica_1.0/Mapa sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish
cp -r sager.calculo.taxa.teip.acum.fabrica_1.0/Metadados sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish
cp sager.calculo.taxa.teip.acum.fabrica_1.0/Dockerfile sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish
cp sager.calculo.taxa.teip.acum.fabrica_1.0/plataforma.json sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish

cp -r sager.calculo.taxa.teip.fabrica_1.0/Mapa sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish
cp -r sager.calculo.taxa.teip.fabrica_1.0/Metadados sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish
cp sager.calculo.taxa.teip.fabrica_1.0/Dockerfile sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish
cp sager.calculo.taxa.teip.fabrica_1.0/plataforma.json sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish

cp -r sager.calculo.uge.fabrica/Mapa sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish
cp -r sager.calculo.uge.fabrica/Metadados sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish
cp sager.calculo.uge.fabrica/Dockerfile sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish
cp sager.calculo.uge.fabrica/plataforma.json sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish

cp -r sager.calculo.usina.fabrica/Mapa sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish
cp -r sager.calculo.usina.fabrica/Metadados sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish
cp sager.calculo.usina.fabrica/Dockerfile sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish
cp sager.calculo.usina.fabrica/plataforma.json sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish

rm -rf sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/bin
rm -rf sager.calculo.parametro.hdp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hdp.Process/bin
rm -rf sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/bin
rm -rf sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/bin
rm -rf sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/bin
rm -rf sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/bin
rm -rf sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/bin
rm -rf sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/bin
rm -rf sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/bin

rm -rf sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web/publish/*.dll
rm -rf sager.calculo.parametro.hdp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hdp.Process/publish/*.dll
rm -rf sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process/publish/*.dll
rm -rf sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process/publish/*.dll
rm -rf sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process/publish/*.dll
rm -rf sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process/publish/*.dll
rm -rf sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process/publish/*.dll
rm -rf sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process/publish/*.dll
rm -rf sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process/publish/*.dll

cd sager.calculo.api.fabrica/Server/ONS.SAGER.Calculo.Api.Web
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.parametro.hdp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hdp.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.parametro.hp.fabrica_1.0/Process/ONS.SAGER.Calculo.Parametro.Hp.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.taxa.teifa.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.taxa.teifa.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.taxa.teip.acum.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Acum.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.taxa.teip.fabrica_1.0/Process/ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.uge.fabrica/Process/ONS.SAGER.Calculo.Uges.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../

cd sager.calculo.usina.fabrica/Process/ONS.SAGER.Calculo.Usinas.Process
dotnet restore --no-cache
dotnet build
dotnet publish -o publish
cd publish
python ../../../../../../deploy/main.py --build --platform docker --environment tst
python ../../../../../../deploy/main.py --register_schema --platform docker --environment tst
cd ../../../../


