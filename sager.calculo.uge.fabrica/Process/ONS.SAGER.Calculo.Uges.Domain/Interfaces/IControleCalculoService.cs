﻿using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Domain.Interfaces
{
    public interface IControleCalculoService
    {
        Task<string> IniciarControleCalculo(string usinaId, string consolidacaoMensalId);
        Task IniciarControleCalculoUnidadeGeradora(string unidadeGeradoraId, string controleCalculoId);
        Task<ResilienceResponse> VerificarConclusaoCalculoParametros(FinalizarParametroRequest request);
        Task<bool> VerificarConclusaoCalculoParametrosComSucesso(FinalizarParametroRequest request);
        Task AlterarStatusControleCalculoUnidadeGeradora(string unidadeGeradoraId, string configuracaoCenarioId, string controleCalculoId, StatusCalculo statusCalculo);
    }
}
