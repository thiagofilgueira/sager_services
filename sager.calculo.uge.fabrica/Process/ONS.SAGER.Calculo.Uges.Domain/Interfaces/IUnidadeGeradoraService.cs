using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Domain.Interfaces
{
    public interface IUnidadeGeradoraService
    {
        Task<UnidadeGeradora> ObterDadosUnidadeGeradoraAtiva(CalcularUnidadeGeradoraRequest request);
        Task<IEnumerable<NivelMontanteJusante>> ObterDadosNivelMontanteJusante(UnidadeGeradora unidadeGeradora, DateTime dataReferencia, string configuracaoCenarioId);
    }
}
