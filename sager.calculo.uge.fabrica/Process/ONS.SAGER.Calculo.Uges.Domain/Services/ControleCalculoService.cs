﻿using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.IRepositories;
using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Domain.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Domain.Services
{
    public class ControleCalculoService : IControleCalculoService
    {
        private readonly IControleCalculoRepository _controleCalculoRepository;
        private readonly IControleCalculoUnidadeGeradoraRepository _controleCalculoUnidadeGeradoraRepository;
        private readonly IControleCalculoParametroTaxaRepository _controleCalculoParametroTaxaRepository;

        public ControleCalculoService(
            IControleCalculoRepository controleCalculoRepository,
            IControleCalculoUnidadeGeradoraRepository controleCalculoUnidadeGeradoraRepository,
            IControleCalculoParametroTaxaRepository controleCalculoParametroTaxaRepository)
        {
            _controleCalculoRepository = controleCalculoRepository;
            _controleCalculoUnidadeGeradoraRepository = controleCalculoUnidadeGeradoraRepository;
            _controleCalculoParametroTaxaRepository = controleCalculoParametroTaxaRepository;
        }

        public async Task<string> IniciarControleCalculo(string usinaId, string consolidacaoMensalId)
        {
            string controleCalculoId = Guid.NewGuid().ToString();

            await _controleCalculoRepository.AddAsync(new ControleCalculo(
                controleCalculoId, 
                consolidacaoMensalId,
                usinaId,
                DateTime.Now,
                null, 
                StatusCalculo.Processando));

            return controleCalculoId;
        }

        public async Task IniciarControleCalculoUnidadeGeradora(string unidadeGeradoraId, string controleCalculoId)
        {
            await _controleCalculoUnidadeGeradoraRepository.AddAsync(new ControleCalculoUnidadeGeradora(
                controleCalculoId,
                unidadeGeradoraId,
                StatusCalculo.Processando));
        }

        public async Task<ResilienceResponse> VerificarConclusaoCalculoParametros(FinalizarParametroRequest request)
        {
            var parametrosCalculados = await _controleCalculoParametroTaxaRepository.ObterPorUnidadeGeradora(
                request.ControleCalculoId,
                request.UnidadeGeradoraId,
                request.ConfiguracaoCenarioId);

            if (parametrosCalculados.IsNullOrEmpty())
            {
                return new ResilienceResponse(ResultStatus.BadRequest, string.Format("O processamento da Unidade Geradora '{0}' ainda não foi finalizado.", request.UnidadeGeradoraId));
            }

            var tiposParametrosCalculados = parametrosCalculados.Select(p => p.TipoCalculo).Distinct();
            var todosCalculosParametros = CalculoConstants.ParametrosCalculados(request.ConsolidacaoMensal.DataReferencia);

            foreach (var tipoParametro in todosCalculosParametros)
            {
                if (tipoParametro.NotIn(tiposParametrosCalculados))
                {
                    return new ResilienceResponse(ResultStatus.BadRequest, string.Format("O processamento da Unidade Geradora '{0}' ainda não foi finalizado.", request.UnidadeGeradoraId));
                }
            }

            return new ResilienceResponse(ResultStatus.Success);
        }

        public async Task<bool> VerificarConclusaoCalculoParametrosComSucesso(FinalizarParametroRequest request)
        {
            // Retorna os parâmetros calculados por unidade geradora
            var controlesCalculoParametros = await _controleCalculoParametroTaxaRepository.ObterPorUnidadeGeradora(
                request.ControleCalculoId,
                request.UnidadeGeradoraId,
                request.ConfiguracaoCenarioId);

            return !controlesCalculoParametros.Any(p => p.StatusCalculo == StatusCalculo.Erro);
        }

        public async Task AlterarStatusControleCalculoUnidadeGeradora(
            string unidadeGeradoraId,
            string configuracaoCenarioId,
            string controleCalculoId,
            StatusCalculo statusCalculo)
        {
            if (unidadeGeradoraId.IsNullOrWhiteSpace() || controleCalculoId.IsNullOrWhiteSpace())
            {
                return;
            }

            var controleCalculoUnidadeGeradora = await _controleCalculoUnidadeGeradoraRepository.ObterPorUnidadeGeradora(
                controleCalculoId,
                unidadeGeradoraId,
                configuracaoCenarioId);

            if (controleCalculoUnidadeGeradora is null)
            {
                await _controleCalculoUnidadeGeradoraRepository.AddAsync(new ControleCalculoUnidadeGeradora(
                    controleCalculoId,
                    unidadeGeradoraId,
                    statusCalculo));

                return;
            }

            controleCalculoUnidadeGeradora.AlterarStatus(statusCalculo);

            await _controleCalculoUnidadeGeradoraRepository.UpdateAsync(controleCalculoUnidadeGeradora);
        }
    }
}
