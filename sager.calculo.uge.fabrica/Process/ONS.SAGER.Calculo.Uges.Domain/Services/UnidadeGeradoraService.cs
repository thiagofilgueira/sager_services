using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.IRepositories;
using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Domain.Services
{
    public class UnidadeGeradoraService : IUnidadeGeradoraService
    {
        private readonly IUnidadeGeradoraRepository _unidadeGeradoraRepository;
        private readonly ISuspensaoUnidadeGeradoraRepository _suspensaoUnidadeGeradoraRepository;
        private readonly IPotenciaUnidadeGeradoraRepository _potenciaUnidadeGeradoraRepository;
        private readonly IEventoMudancaEstadoOperativoRepository _eventoMudancaEstadoOperativoRepository;
        private readonly IDisponibilidadeQuedaBrutaRepository _disponibilidadeQuedaBrutaRepository;
        private readonly INivelMontanteJusanteRepository _nivelMontanteJusanteRepository;

        public UnidadeGeradoraService(
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            IEventoMudancaEstadoOperativoRepository eventoMudancaEstadoOperativoRepository,
            IDisponibilidadeQuedaBrutaRepository disponibilidadeQuedaBrutaRepository,
            INivelMontanteJusanteRepository nivelMontanteJusanteRepository)
        {
            _unidadeGeradoraRepository = unidadeGeradoraRepository;
            _suspensaoUnidadeGeradoraRepository = suspensaoUnidadeGeradoraRepository;
            _potenciaUnidadeGeradoraRepository = potenciaUnidadeGeradoraRepository;
            _eventoMudancaEstadoOperativoRepository = eventoMudancaEstadoOperativoRepository;
            _nivelMontanteJusanteRepository = nivelMontanteJusanteRepository;
            _disponibilidadeQuedaBrutaRepository = disponibilidadeQuedaBrutaRepository;
        }

        public async Task<UnidadeGeradora> ObterDadosUnidadeGeradoraAtiva(CalcularUnidadeGeradoraRequest request)
        {
            var unidadeGeradora = await _unidadeGeradoraRepository.ObterUnidadesGeradorasPorId(
                request.UnidadeGeradoraId,
                request.ConfiguracaoCenarioId);

            if (
                unidadeGeradora is null ||
                unidadeGeradora.VerificarDesativacaoDuranteMesInteiro(request.ConsolidacaoMensal.DataReferencia) ||
                unidadeGeradora.VerificarForaOperacaoComercialDuranteMesInteiro(request.ConsolidacaoMensal.DataReferencia))
            {
                return null;
            }

            return await ObterAgregadosUnidadeGeradoraAtiva(request, unidadeGeradora);
        }

        private async Task<UnidadeGeradora> ObterAgregadosUnidadeGeradoraAtiva(CalcularUnidadeGeradoraRequest request, UnidadeGeradora unidadeGeradora)
        {
            var eventos = _eventoMudancaEstadoOperativoRepository.ObterEventosUnidadeGeradoraPorPeriodo(
                request.UnidadeGeradoraId,
                request.ConsolidacaoMensal.DataReferencia,
                request.ConfiguracaoCenarioId);

            var suspensoes = _suspensaoUnidadeGeradoraRepository.ObterSuspensoesUnidadeGeradoraPeriodo(
                request.UnidadeGeradoraId,
                request.ConsolidacaoMensal.DataReferencia,
                request.ConfiguracaoCenarioId);

            var potencias = _potenciaUnidadeGeradoraRepository.ObterPotenciasUnidadeGeradoraPeriodo(
                request.UnidadeGeradoraId,
                request.ConsolidacaoMensal.DataReferencia,
                request.ConfiguracaoCenarioId);

            await Task.WhenAll(eventos, suspensoes, potencias);

            if ((eventos.Result).IsNullOrEmpty())
            {
                return null;
            }

            if ((potencias.Result).IsNullOrEmpty())
            {
                throw new InvalidOperationException($"As potências da unidade geradora '{unidadeGeradora.UnidadeGeradoraId}' devem ser informadas");
            }

            unidadeGeradora.AdicionarEventos(eventos.Result);
            unidadeGeradora.AdicionarPotencias(potencias.Result);
            unidadeGeradora.AdicionarSuspensoes(suspensoes.Result);

            if (unidadeGeradora.VerificarSuspensaoDuranteMesInteiro(request.ConsolidacaoMensal.DataReferencia))
            {
                return null;
            }

            if (unidadeGeradora.Eventos.Any(p => p.Origem == TipoClassificadorOrigem.GRH))
            {
                var quedasBrutas = await _disponibilidadeQuedaBrutaRepository.ObterPorUnidadeGeradoraId(request.UnidadeGeradoraId, request.ConfiguracaoCenarioId);
                unidadeGeradora.AdicionarDisponibilidadesQuedasBrutas(quedasBrutas);
            }

            return unidadeGeradora;
        }

        public async Task<IEnumerable<NivelMontanteJusante>> ObterDadosNivelMontanteJusante(
            UnidadeGeradora unidadeGeradora,
            DateTime dataReferencia,
            string configuracaoCenarioId)
        { 
            if (!unidadeGeradora.Eventos.Any(evento => evento.Origem == TipoClassificadorOrigem.GRH))
            {
                return new List<NivelMontanteJusante>();
            }

            var niveisMontantesJusantes = await _nivelMontanteJusanteRepository.ObterPorPeriodo(
                unidadeGeradora.UsinaId,
                dataReferencia,
                dataReferencia.MesSeguinte(),
                configuracaoCenarioId);

            if (niveisMontantesJusantes.IsNullOrEmpty())
            {
                throw new InvalidOperationException("Níveis de montante e justante não informados");
            }

            return niveisMontantesJusantes;
        }

    }
}
