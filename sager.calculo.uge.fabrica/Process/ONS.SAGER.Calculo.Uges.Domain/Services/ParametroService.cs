﻿using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Domain.Services
{
    public class ParametroService : IParametroService
    {

        private readonly IParametroRepository _parametroRepository;

        public ParametroService(IParametroRepository parametroRepository)
        {
            _parametroRepository = parametroRepository;
        }

        public async Task RealizarEstornoCalculosParametros(string controleCalculoId, string configuracaoCenarioId)
        {
            if (controleCalculoId.IsNullOrWhiteSpace())
            {
                return;
            }

            var parametrosCalculados = await _parametroRepository.ObterParametrosPorControleCalculoId(
                controleCalculoId,
                configuracaoCenarioId);

            if (parametrosCalculados.IsNullOrEmpty())
            {
                return;
            }

            await parametrosCalculados.Parallel(p => _parametroRepository.DeleteAsync(p));
        }
    }
}
