﻿using ONS.SAGER.Calculo.Util.DataAccess.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;

namespace ONS.SAGER.Calculo.Uges.Domain.IRepositories
{
    public interface IControleCalculoParametroTaxaRepository : IControleCalculoParametroTaxaRepositoryBase
    {
    }
}
