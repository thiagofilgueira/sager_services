using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Uges.Domain.Models.Requests
{
    public class FinalizarParametroRequest : Notificavel
    {
        public string ConfiguracaoCenarioId { get; }
        public string ControleCalculoId { get; }
        public string UnidadeGeradoraId { get; }
        public string UsinaId { get; }
        public TipoCalculo TipoCalculo { get; }
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; }

        public FinalizarParametroRequest(
            string configuracaoCenarioId,
            string controleCalculoId,
            string unidadeGeradoraId,
            string usinaId,
            TipoCalculo tipoCalculo,
            ConsolidacaoMensalRequest consolidacaoMensal)
        {
            ConfiguracaoCenarioId = configuracaoCenarioId;
            ControleCalculoId = controleCalculoId;
            UnidadeGeradoraId = unidadeGeradoraId;
            UsinaId = usinaId;
            TipoCalculo = tipoCalculo;
            ConsolidacaoMensal = consolidacaoMensal;
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configura��o de cen�rio deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ControleCalculoId, nameof(ControleCalculoId), "O controle de c�lculo deve ser informado")
                .VerificarSeNaoNuloOuEspacoEmBranco(UnidadeGeradoraId, nameof(UnidadeGeradoraId), "A unidade geradora deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "A usina deve ser informada"));
        }
    }
}
