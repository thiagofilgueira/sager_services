using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using System;

namespace ONS.SAGER.Calculo.Uges.Domain.Models.Requests
{
    public class CalcularUnidadeGeradoraRequest : Notificavel
    {
        public string ConfiguracaoCenarioId { get; }
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; }
        public string ControleCalculoId { get; private set; }
        public UsinaRequest Usina { get; }
        public string UnidadeGeradoraId { get; }

        public CalcularUnidadeGeradoraRequest(
            string configuracaoCenarioId,
            string consolidacaoMensalId, 
            DateTime dataReferencia, 
            string controleCalculoId,
            UsinaRequest usina,
            string unidadeGeradoraId)
        {
            ConfiguracaoCenarioId = configuracaoCenarioId;
            ConsolidacaoMensal = new ConsolidacaoMensalRequest(consolidacaoMensalId, dataReferencia);
            ControleCalculoId = controleCalculoId;
            Usina = usina;
            UnidadeGeradoraId = unidadeGeradoraId;
        }

        public void AlterarControleCalculoId(string controleCalculoId)
        {
            ControleCalculoId = controleCalculoId;
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configura��o de cen�rio deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ControleCalculoId, nameof(ControleCalculoId), "O controle de c�lculo deve ser informado")
                .VerificarSeNaoNuloOuEspacoEmBranco(Usina?.UsinaId, nameof(Usina), "A usina deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(UnidadeGeradoraId, nameof(UnidadeGeradoraId), "A unidade geradora deve ser informada"));
        }
    }
}
