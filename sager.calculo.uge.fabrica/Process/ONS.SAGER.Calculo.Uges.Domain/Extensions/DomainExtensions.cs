using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Services;

namespace ONS.SAGER.Calculo.Uges.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IUnidadeGeradoraService, UnidadeGeradoraService>();
            services.AddSingleton<IControleCalculoService, ControleCalculoService>();
            services.AddSingleton<IParametroService, ParametroService>();
        }
    }
}
