using ONS.SDK.Impl.Data;
using ONS.SAGER.Calculo.Util.Domain.Entities;

namespace ONS.SAGER.Calculo.Uges.Domain.Map
{
    public class EntitiesMap : AbstractDataMapCollection
    {
        protected override void Load()
        {
            BindMap<UnidadeGeradora>();
            BindMap<PotenciaUnidadeGeradora>();
            BindMap<EventoMudancaEstadoOperativo>();
            BindMap<SuspensaoUnidadeGeradora>();
            BindMap<NivelMontanteJusante>();
            BindMap<DisponibilidadeQuedaBruta>();
            BindMap<ParametroTaxa>();
            BindMap<ControleCalculo>();
            BindMap<ControleCalculoParametroTaxa>();
            BindMap<ControleCalculoUnidadeGeradora>();
        }
    }
}
