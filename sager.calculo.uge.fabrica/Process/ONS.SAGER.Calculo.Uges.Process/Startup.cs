using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Uges.Application.Extensions;
using ONS.SAGER.Calculo.Uges.DataAccess.Extensions;
using ONS.SAGER.Calculo.Uges.Domain.Extensions;
using ONS.SAGER.Calculo.Uges.Domain.Map;
using ONS.SAGER.Calculo.Uges.Entrypoint.Extensions;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SDK.Builder;
using ONS.SDK.Extensions.DependencyInjection;

namespace ONS.SAGER.Calculo.Uges.Process
{
    public class Startup : IStartup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.UseSDK();
            services.AddAppSettings(Configuration);
            services.AddApplicationServices();
            services.AddDomainServices();
            services.AddRepositoryServices();
            services.UseDataMap<EntitiesMap>();
            services.AddEntrypointServices();
            services.AddCalculoLogger();
            services.AddExecutionContextAbstraction();
        }

        public void Configure(IAppBuilder app)
        {
        }
    }
}
