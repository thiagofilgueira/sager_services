using ONS.SAGER.Calculo.Uges.Application.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Context;
using ONS.SDK.Services.Reproduction;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ICalcularUnidadeGeradoraApplicationService _calcularUnidadeGeradoraApplicationService;
        private readonly IFinalizarParametroApplicationService _finalizarParametroApplicationService;
        private readonly IExecutionContext _context;
        private readonly IReproductionService _repro;

        public CalculosEntrypoint(
            ICalcularUnidadeGeradoraApplicationService calcularUnidadeGeradoraApplicationService,
            IFinalizarParametroApplicationService finalizarParametroApplicationService,
            IExecutionContext context,
            IReproductionService repro
            )
        {
            _calcularUnidadeGeradoraApplicationService = calcularUnidadeGeradoraApplicationService;
            _finalizarParametroApplicationService = finalizarParametroApplicationService;
            _context = context;
            _repro = repro;
        }

        [SDKEvent(Eventos.CalcularUnidadeGeradora)]
        public async Task CalcularUnidadeGeradora(CalcularUnidadeGeradoraPayload payload)
        {
            //var result = _repro.GetReproductionStatus(_context.ExecutionParameter.MemoryEvent.Reproduction.Id);

            await _calcularUnidadeGeradoraApplicationService.Calcular(
                new CalcularUnidadeGeradoraRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.ConsolidacaoMensalId,
                    payload?.ConsolidacaoMensalPayload?.DataReferencia ?? default,
                    payload?.ControleCalculoIdPayload,
                    payload?.UsinaPayload?.MapToRequest(),
                    payload.UnidadeGeradoraId
                    ));              
        }

        [SDKEvent(Eventos.FinalizarCalculoParametro)]
        public async Task FinalizarCalculoParametro(FinalizarParametroPayload payload)
        {
            await _finalizarParametroApplicationService.Finalizar(
                new FinalizarParametroRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ControleCalculoIdPayload,
                    payload?.UnidadeGeradora?.UnidadeGeradoraId,
                    payload?.UnidadeGeradora?.UsinaId,
                    payload?.TipoCalculoPayload ?? default,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }
    }
}
