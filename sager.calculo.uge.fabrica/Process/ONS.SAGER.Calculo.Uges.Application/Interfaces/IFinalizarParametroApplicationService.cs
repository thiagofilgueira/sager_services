using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Application.Interfaces
{
    public interface IFinalizarParametroApplicationService
    {
        Task<Result> Finalizar(FinalizarParametroRequest request);
    }
}
