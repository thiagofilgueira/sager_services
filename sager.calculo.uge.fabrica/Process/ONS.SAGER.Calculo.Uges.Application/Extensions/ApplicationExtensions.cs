using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Uges.Application.Interfaces;
using ONS.SAGER.Calculo.Uges.Application.Services;

namespace ONS.SAGER.Calculo.Uges.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<IFinalizarParametroApplicationService, FinalizarParametroApplicationService>();
            services.AddSingleton<ICalcularUnidadeGeradoraApplicationService, CalcularUnidadeGeradoraApplicationService>();
        }
    }
}
