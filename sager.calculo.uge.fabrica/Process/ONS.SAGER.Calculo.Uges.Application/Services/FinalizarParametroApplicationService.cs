using ONS.SAGER.Calculo.Uges.Application.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Application.Services
{
    public class FinalizarParametroApplicationService : ApplicationServiceBase<FinalizarParametroApplicationService>, IFinalizarParametroApplicationService
    {
        private readonly IUnidadeGeradoraService _unidadeGeradoraService;
        private readonly IControleCalculoService _controleCalculoService;
        private readonly IParametroService _parametroService;
        private readonly IExecutionContextAbstraction _executionContext;

        public FinalizarParametroApplicationService(
            IUnidadeGeradoraService unidadeGeradoraService,
            IControleCalculoService controleCalculoService,
            IParametroService parametroService,
            IExecutionContextAbstraction executionContext,
            ICalculoLogger<FinalizarParametroApplicationService> logger
            ) : base(logger)
        {
            _unidadeGeradoraService = unidadeGeradoraService;
            _controleCalculoService = controleCalculoService;
            _parametroService = parametroService;
            _executionContext = executionContext;
        }


        public Task<Result> Finalizar(FinalizarParametroRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await FinalizarParametro(request));
        }

        public async Task<Result> FinalizarParametro(FinalizarParametroRequest request)
        {
            try
            {
                if (request is null)
                {
                    return new Result(ResultStatus.BadRequest, $"{nameof(FinalizarParametroRequest)} não pode ser nulo");
                }

                if (request.Invalido)
                {
                    return await TratarErroCalculoUnidadeGeradora(
                        request, 
                        ResultStatus.BadRequest, 
                        request.MensagensNotificacoesAgrupadas);
                }

                _executionContext.SetBranch(request.ConfiguracaoCenarioId);

                if (request.TipoCalculo != CalculoConstants.ParametroMandante)
                {
                    return new Result(ResultStatus.Success, "O container foi eliminado pois já existe uma instância gerenciando o calculo dos parâmetros da unidade geradora.");
                }

                // Realiza as tentativas para verificação da conclusão do cálculo
                ResilienceResponse resultadoOperacao = 
                    await Resilience.AguardarConclusaoDaFuncao(
                        10,
                        TimeSpan.FromSeconds(20),
                        async () => await _controleCalculoService.VerificarConclusaoCalculoParametros(request));

                if (resultadoOperacao.Status == ResultStatus.BadRequest)
                {
                    return await TratarErroCalculoUnidadeGeradora(
                        request, 
                        resultadoOperacao.Status,
                        resultadoOperacao.Notifications.FirstOrDefault());
                }

                var concluidoComSucesso = await _controleCalculoService.VerificarConclusaoCalculoParametrosComSucesso(request);

                var statusCalculo = concluidoComSucesso ?  StatusCalculo.Sucesso : StatusCalculo.Erro;

                string mensagem = string.Format("O cálculo da Unidade Geradora '{0}' foi finalizado com {1}.", request.UnidadeGeradoraId, statusCalculo.ToString());

                if (!concluidoComSucesso)
                {
                    return await TratarErroCalculoUnidadeGeradora(request, ResultStatus.BadRequest, $"Os parâmetros da unidade geradora {request.UnidadeGeradoraId} não foram calculados com sucesso");
                }

                await _controleCalculoService.AlterarStatusControleCalculoUnidadeGeradora(
                    request.UnidadeGeradoraId,
                    request.ConfiguracaoCenarioId,
                    request.ControleCalculoId,
                    statusCalculo);

                await EmitirEventoFinalizarCalculoUnidadeGeradora(
                    request.UsinaId,
                    request.UnidadeGeradoraId,
                    request.ControleCalculoId,
                    request.ConfiguracaoCenarioId,
                    request.ConsolidacaoMensal.ConsolidacaoMensalId,
                    request.ConsolidacaoMensal.DataReferencia,
                    mensagem);

                return new Result(ResultStatus.Success, mensagem);

            }
            catch (InvalidOperationException eio)
            {
                return await TratarErroCalculoUnidadeGeradora(request, ResultStatus.BadRequest, eio.Message);
            }
            catch (Exception e)
            {
                return await TratarErroCalculoUnidadeGeradora(request, ResultStatus.InternalError, e.Message);
            }
        }

        private async Task<Result> TratarErroCalculoUnidadeGeradora(
            FinalizarParametroRequest request,
            ResultStatus status,
            string mensagem)
        {
            try
            {
                if(request is null)
                {
                    return new Result(status, mensagem);
                }

                await _parametroService.RealizarEstornoCalculosParametros(
                    request.ControleCalculoId,
                    request.ConfiguracaoCenarioId);

                await _controleCalculoService.AlterarStatusControleCalculoUnidadeGeradora(
                    request.UnidadeGeradoraId, 
                    request.ConfiguracaoCenarioId, 
                    request.ControleCalculoId,
                    StatusCalculo.Erro);

                await EmitirEventoFinalizarCalculoUnidadeGeradora(
                    request.UsinaId,
                    request.UnidadeGeradoraId,
                    request.ControleCalculoId,
                    request.ConfiguracaoCenarioId,
                    request.ConsolidacaoMensal?.ConsolidacaoMensalId,
                    request.ConsolidacaoMensal?.DataReferencia ?? default,
                    mensagem);

                return new Result(status, mensagem);
            }
            catch (Exception e)
            {
                return new Result(ResultStatus.InternalError, e.Message);
            }

        }
 
        private async Task EmitirEventoFinalizarCalculoUnidadeGeradora(
            string usinaId,
            string unidadeGeradoraId,
            string controleCalculoId,
            string configuracaoCenarioId,
            string consolidacaoMensalId,
            DateTime dataReferencia,
            string mensagem)
        {
            if(usinaId.IsNullOrWhiteSpace() || unidadeGeradoraId.IsNullOrWhiteSpace() || controleCalculoId.IsNullOrWhiteSpace())
            {
                return;
            }
            
            await _executionContext.EmitirEvento(
                Eventos.FinalizarCalculoUge,
                new FinalizarUnidadeGeradoraPayload(
                    usinaId,
                    unidadeGeradoraId,
                    mensagem,
                    controleCalculoId,
                    configuracaoCenarioId,
                    new ConsolidacaoMensalPayload() 
                    {
                        ConsolidacaoMensalId = consolidacaoMensalId,
                        DataReferencia = dataReferencia
                    }));
        }

    }
}
