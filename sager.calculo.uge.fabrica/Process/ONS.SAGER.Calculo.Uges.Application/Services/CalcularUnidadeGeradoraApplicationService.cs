﻿using ONS.SAGER.Calculo.Uges.Application.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Interfaces;
using ONS.SAGER.Calculo.Uges.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Uges.Application.Services
{
    public class CalcularUnidadeGeradoraApplicationService : ApplicationServiceBase<CalcularUnidadeGeradoraApplicationService>, ICalcularUnidadeGeradoraApplicationService
    {
        private readonly IUnidadeGeradoraService _unidadeGeradoraService;
        private readonly IControleCalculoService _controleCalculoService;
        private readonly IExecutionContextAbstraction _executionContext;

        public CalcularUnidadeGeradoraApplicationService(
            IUnidadeGeradoraService unidadeGeradoraService,
            IControleCalculoService controleCalculoService,
            IExecutionContextAbstraction executionContext,
            ICalculoLogger<CalcularUnidadeGeradoraApplicationService> logger
            ) : base(logger)
        {
            _unidadeGeradoraService = unidadeGeradoraService;
            _controleCalculoService = controleCalculoService;
            _executionContext = executionContext;
        }

        public Task<Result> Calcular(CalcularUnidadeGeradoraRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await CalcularUnidadeGeradora(request));
        }

        private async Task<Result> CalcularUnidadeGeradora(CalcularUnidadeGeradoraRequest request)
        {
            try
            {
                if (!ValidarRequest(request))
                {
                    return new Result(ResultStatus.BadRequest, request.MensagensNotificacoesAgrupadas);
                }

                _executionContext.SetBranch(request.ConfiguracaoCenarioId);

                var unidadeGeradora = await _unidadeGeradoraService.ObterDadosUnidadeGeradoraAtiva(request);

                if (unidadeGeradora is null)
                {
                    return new Result(ResultStatus.NoContent, $"A Unidade geradora '{ request.UnidadeGeradoraId }' não está disponível para cálculo");
                }

                var niveisMontantesJusantes = await _unidadeGeradoraService.ObterDadosNivelMontanteJusante(
                    unidadeGeradora,
                    request.ConsolidacaoMensal.DataReferencia,
                    request.ConfiguracaoCenarioId);

                await IniciarControlesCalculo(request, unidadeGeradora);

                await _executionContext.EmitirEvento(
                    Eventos.CalcularParametro,
                    new CalcularParametroPayload()
                    {
                        ConfiguracaoCenarioIdPayload = request.ConfiguracaoCenarioId,
                        ControleCalculoIdPayload = request.ControleCalculoId,
                        ConsolidacaoMensalPayload = request.ConsolidacaoMensal.MapToPayload(),
                        UnidadeGeradoraPayload = unidadeGeradora.MapToPayload(),
                        NiveisMontantesJusantesPayload = niveisMontantesJusantes?.Select(n => n.MapToPayload())
                    },
                    request.ConsolidacaoMensal.DataReferencia
                 );

                return new Result(ResultStatus.Success, string.Format("O cálculo das Unidades Geradoras referente a Usina {0} foi iniciado com sucesso.", request.Usina.UsinaId));
            }
            catch (Exception ex)
            {
                return await TratarErroCalculoUnidadeGeradora(request, ex);
            }
        }

        private async Task IniciarControlesCalculo(CalcularUnidadeGeradoraRequest request, UnidadeGeradora unidadeGeradora)
        {
            if (_executionContext.IsReprocessing())
            {
                var controleCalculoId = await _controleCalculoService.IniciarControleCalculo(
                    request.Usina.UsinaId,
                    request.ConsolidacaoMensal.ConsolidacaoMensalId);

                request.AlterarControleCalculoId(controleCalculoId);
            }

            await _controleCalculoService.IniciarControleCalculoUnidadeGeradora(unidadeGeradora.UnidadeGeradoraId, request.ControleCalculoId);
        }

        private bool ValidarRequest(CalcularUnidadeGeradoraRequest request)
        {
            if (request is null)
            {
                return false;
            }

            request.Validar();


            return request.Valido;
        }


        private async Task<Result> TratarErroCalculoUnidadeGeradora(CalcularUnidadeGeradoraRequest request, Exception ex)
        {
            try
            {
                await _controleCalculoService.AlterarStatusControleCalculoUnidadeGeradora(
                    request.UnidadeGeradoraId,
                    request.ConfiguracaoCenarioId,
                    request.ControleCalculoId,
                    StatusCalculo.Erro);

                await _executionContext.EmitirEvento(
                    Eventos.FinalizarCalculoUge,
                    new FinalizarUnidadeGeradoraPayload(
                        request.Usina?.UsinaId,
                        request.UnidadeGeradoraId,
                        ex.Message,
                        request.ControleCalculoId,
                        request.ConfiguracaoCenarioId,
                        new ConsolidacaoMensalPayload()
                        {
                            ConsolidacaoMensalId = request.ConsolidacaoMensal?.ConsolidacaoMensalId,
                            DataReferencia = request.ConsolidacaoMensal?.DataReferencia ?? default
                        }));

                return new Result(ResultStatus.InternalError, ex.Message);
            }
            catch (Exception ex2)
            {
                return new Result(ResultStatus.InternalError, new List<string> { ex.Message, ex2.Message });
            }
        }
    }
}
