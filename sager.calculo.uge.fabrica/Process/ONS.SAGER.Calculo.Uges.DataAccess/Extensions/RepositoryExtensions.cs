using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Uges.DataAccess.Repositories;
using ONS.SAGER.Calculo.Uges.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Uges.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<ISuspensaoUnidadeGeradoraRepository, SuspensaoUnidadeGeradoraRepository>();
            services.AddSingleton<IEventoMudancaEstadoOperativoRepository, EventoMudancaEstadoOperativoRepository>();
            services.AddSingleton<IPotenciaUnidadeGeradoraRepository, PotenciaUnidadeGeradoraRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
            services.AddSingleton<IDisponibilidadeQuedaBrutaRepository, DisponibilidadeQuedaBrutaRepository>();
            services.AddSingleton<INivelMontanteJusanteRepository, NivelMontanteJusanteRepository>();
            services.AddSingleton<IControleCalculoRepository, ControleCalculoRepository>();
            services.AddSingleton<IControleCalculoParametroTaxaRepository, ControleCalculoParametroTaxaRepository>();
            services.AddSingleton<IControleCalculoUnidadeGeradoraRepository, ControleCalculoUnidadeGeradoraRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
        }
    }
}
