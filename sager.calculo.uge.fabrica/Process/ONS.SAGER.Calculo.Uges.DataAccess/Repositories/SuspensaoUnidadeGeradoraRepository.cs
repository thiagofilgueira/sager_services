﻿using ONS.SAGER.Calculo.Uges.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.DataAccess.Repositories;

namespace ONS.SAGER.Calculo.Uges.DataAccess.Repositories
{
    public class SuspensaoUnidadeGeradoraRepository: SuspensaoUnidadeGeradoraRepositoryBase, ISuspensaoUnidadeGeradoraRepository
    {
    }
}
