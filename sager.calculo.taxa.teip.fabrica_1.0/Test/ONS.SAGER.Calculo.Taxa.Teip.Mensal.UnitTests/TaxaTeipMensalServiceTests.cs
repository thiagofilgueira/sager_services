﻿using Moq;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.UnitTests
{
    public class TaxaTeipMensalServiceTests
    {

        [Theory]
        [InlineData(11, 4, 34, 1, 44, 4, 11, 5, 44, 7.52831594634874)]
        [InlineData(1, 1, 1, 1, 1, 1, 1, 1, 1, 2)]
        [InlineData(8.6, 1, 2, 1, 1, 3, 1, 4, 1, 0.94491040792985)]
        [InlineData(9.1, 8, 2, 1, 1, 2.2, 1, 4, 1, 1.51217798594848)]
        public async Task CalcularTaxaTeipMensalTest(
            double valorParametro1,
            double valorParametro2,
            double valorParametro3,
            double valorParametro4,
            double valorParametro5,
            double valorParametro6,
            double valorParametro7,
            double valorParametro8,
            double valorParametro9,
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", valorParametro1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", valorParametro2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", valorParametro3, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", valorParametro4, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", valorParametro5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", valorParametro6, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", valorParametro7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", valorParametro8, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", valorParametro9, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"3")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 33),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 21)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var taxaTeipMensal = await service.Calcular(calculoRequest);

            Assert.Equal(result, Math.Round(taxaTeipMensal.Value, 14));
        }

        [Theory]
        [InlineData(11, 4, 34, 1, 44, 4, 7.71541501976284)]
        public async Task DeveCalcularParaUsinaEmOperacaoDesconsiderandoUnidadeGeradoraDaUsinaNaoConsideradaNoCalculo(
            double valorParametro1,
            double valorParametro2,
            double valorParametro3,
            double valorParametro4,
            double valorParametro5,
            double valorParametro6,
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", valorParametro1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", valorParametro2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", valorParametro3, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", valorParametro4, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", valorParametro5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", valorParametro6, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","2",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2019,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2019,12,1), null, "2", 33),

            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2018,10,30))
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("2", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 10, 31));

            var taxaTeipMensal = await service.Calcular(calculoRequest);

            Assert.Equal(result, Math.Round(taxaTeipMensal.Value, 14));
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoParaUmaUgeMesmoTendoValorParametro()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 8, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 9, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1))
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoPara3UgesMesmoTendoValorParametro()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 8, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 9, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 1, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"3")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 100),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 150),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
                new SuspensaoUnidadeGeradora("2", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
                new SuspensaoUnidadeGeradora("3", "1", new DateTime(2018,1,1), new DateTime(2019,1,1))
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Theory]
        [InlineData(3.4)]
        public async Task DeveCalcularUGEqueNaoEstaSuspensaDesconsiderandoUGEsuspensaNaoConsideradaNoCalculo(
            double resultadoEsperado)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 8, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 9, "1", null, "1")
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2019,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2019,12,1), null, "2", 100),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var taxaTeipMensal = await service.Calcular(calculoRequest);

            Assert.Equal(resultadoEsperado, taxaTeipMensal);
        }

        [Theory]
        [InlineData(2.757281553398)]
        public async Task DeveCalcularTeipTendoSupensaoAntesDoInicioDoCalculo(
            double resultadoEsperado)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 2, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 2, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "2", 100),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2018,10,30)),
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var taxaTeipMensal = await service.Calcular(calculoRequest);

            Assert.Equal(resultadoEsperado, Math.Round(taxaTeipMensal.Value, 12));
        }
        [Fact]
        public void NaoDeveCalcularTaxaTeipMensalQuandoUsinaDesativada()

        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 1, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 8, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 1, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"2")
            };                                                                  

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2020,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2020,12,1), null, "2", 33),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2020,12,1), null, "3", 21)
            };


            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), new DateTime(2018, 10, 1), null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeverRetornarExceptionQuandoHpNaoTemValor()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 4, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 34, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 312)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeverRetornarExceptionQuandoHdpNaoTemValor()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 4, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 34, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 312)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeverRetornarExceptionQuandoHEDPNaoTemValor()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 4, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 34, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 312)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());
            var calculoRequest = new CalcularTaxaTeipMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));


            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }
    }
}
