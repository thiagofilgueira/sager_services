using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ITaxaApplicationService _taxaTeipMensalApplicationService;
        public CalculosEntrypoint(ITaxaApplicationService taxaTeipMensalApplicationService)
        {
            _taxaTeipMensalApplicationService = taxaTeipMensalApplicationService;
        }

        [SDKEvent(Eventos.CalcularTaxaMes)]
        public async Task CalcularTaxaTeipMensal(CalcularTaxaPayload payload)
        {
            await _taxaTeipMensalApplicationService.Calcular(
                new CalcularTaxaTeipMesRequest(
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.ControleCalculoIdPayload,
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }

    }
}
