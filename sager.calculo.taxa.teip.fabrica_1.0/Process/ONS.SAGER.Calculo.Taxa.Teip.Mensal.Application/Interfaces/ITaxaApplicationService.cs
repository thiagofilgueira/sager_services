using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Application.Interfaces
{
    public interface ITaxaApplicationService
    {
        Task<Result> Calcular(CalcularTaxaTeipMesRequest request);
    }
}
