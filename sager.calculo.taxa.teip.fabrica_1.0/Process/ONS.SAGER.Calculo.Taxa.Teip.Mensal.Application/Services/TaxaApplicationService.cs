using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Logger;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Application.Services
{
    public class TaxaApplicationService : TaxaMensalApplicationServiceBase<TaxaApplicationService>, ITaxaApplicationService
    {
        private readonly ITaxaService _taxaService;

        public TaxaApplicationService(
            ITaxaService taxaService,
            IExecutionContextAbstraction executionContext,
            IControleCalculoTaxaRepository controleRepository,
            ICalculoLogger<TaxaApplicationService> logger)
            : base(executionContext, controleRepository, logger)
        {
            _taxaService = taxaService;
        }

        public Task<Result> Calcular(CalcularTaxaTeipMesRequest request)
        {
            return CalcularTaxa(
                request,
                TipoCalculo.TEIPmes,
                () => _taxaService.Calcular(request));
        }
    }
}
