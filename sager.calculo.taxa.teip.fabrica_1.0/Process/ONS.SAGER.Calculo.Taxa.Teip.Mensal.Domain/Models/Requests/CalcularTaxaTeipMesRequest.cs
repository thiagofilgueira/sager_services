using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests
{
    public class CalcularTaxaTeipMesRequest : CalcularTaxaRequest
    {
        public CalcularTaxaTeipMesRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            string consolidacaoMensalId,
            DateTime dataReferencia)
            : base(usina, controleCalculoId, configuracaoCenarioId, new ConsolidacaoMensalRequest(consolidacaoMensalId, dataReferencia))
        {
            Validar();
        }

        public CalcularTaxaTeipMesRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal)
            : base(usina, controleCalculoId, configuracaoCenarioId, consolidacaoMensal)
        {
            Validar();
        }
    }
}
