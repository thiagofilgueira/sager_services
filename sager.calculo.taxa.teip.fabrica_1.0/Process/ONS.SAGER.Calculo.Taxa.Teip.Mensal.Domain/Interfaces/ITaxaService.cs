using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces
{
    public interface ITaxaService : ITaxaMensalServiceBase
    {
        Task<double?> Calcular(CalcularTaxaTeipMesRequest request);
    }
}
