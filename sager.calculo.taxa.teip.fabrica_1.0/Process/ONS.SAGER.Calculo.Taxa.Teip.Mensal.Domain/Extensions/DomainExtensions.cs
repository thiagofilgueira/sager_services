using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Services;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }
    }
}
