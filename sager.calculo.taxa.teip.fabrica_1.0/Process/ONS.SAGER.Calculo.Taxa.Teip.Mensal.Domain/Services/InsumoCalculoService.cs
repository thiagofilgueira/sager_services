﻿using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Services;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Services
{
    public class InsumoCalculoService : InsumoCalculoServiceBase, IInsumoCalculoService
    {
        public InsumoCalculoService(
            IExecutionContextAbstraction executionContextAbstraction, 
            IProcessMemoryServiceAbstraction processMemoryServiceAbstraction) 
            :  base(executionContextAbstraction, processMemoryServiceAbstraction)
        {

        }
    }
}
