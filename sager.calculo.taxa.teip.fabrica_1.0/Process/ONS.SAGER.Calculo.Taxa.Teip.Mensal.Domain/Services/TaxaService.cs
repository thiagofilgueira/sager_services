using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Services;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Services
{
    public class TaxaService : TaxaMensalServiceBase, ITaxaService
    {
        private readonly IInsumoCalculoService _insumoCalculoService;

        public TaxaService(
            ITaxaRepository taxaRepository,
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IParametroRepository parametroRepository,
            IInsumoCalculoService insumoCalculoService)
            : base(
                  taxaRepository,
                  unidadeGeradoraRepository,
                  potenciaUnidadeGeradoraRepository,
                  suspensaoUnidadeGeradoraRepository,
                  parametroRepository)
        {
            _insumoCalculoService = insumoCalculoService;
        }

        public async Task<double?> Calcular(CalcularTaxaTeipMesRequest request)
        {
            var taxaAjustada = await ObterTaxaAjustada(
                TipoCalculo.TEIPmesAjustada,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.Usina.UsinaId);

            if (taxaAjustada.IsNotNull())
            {
                return taxaAjustada.Valor.Value;
            }

            var unidadesGeradoras = await ObterDadosUnidadesGeradorasAtivasParaCalculo(
                request.Usina.UsinaId,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.ConsolidacaoMensal.DataReferencia,
                new TipoCalculo[]
                {
                    TipoCalculo.HP,
                    TipoCalculo.HDP,
                    TipoCalculo.HEDP
                },
                request.ConfiguracaoCenarioId);

            var valorTaxa = CalcularTeipMensal(
                unidadesGeradoras,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.ConsolidacaoMensal.DataReferencia);

            await SalvarTaxaVersionada(
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                TipoCalculo.TEIPmes,
                request.Usina.UsinaId,
                request.ConfiguracaoCenarioId,
                request.ControleCalculoId,
                valorTaxa);

            return valorTaxa;
        }

        private double CalcularTeipMensal(IEnumerable<UnidadeGeradora> unidadesGeradoras, string consolidacaoMensalId, DateTime dataReferencia)
        {
            if (unidadesGeradoras.IsNullOrEmpty())
            {
                throw new InvalidOperationException("Unidades geradoras não disponíveis para cálculo");
            }

            double numeradorTaxa = 0;
            double totalPotencia = 0;

            foreach (var unidadeGeradora in unidadesGeradoras)
            {
                var potenciaUnidadeGeradora = unidadeGeradora.ObterUltimaPotenciaDoMes(dataReferencia);

                var valorPotenciaUnidadeGeradora = potenciaUnidadeGeradora?.ValorPotencia;

                if (!valorPotenciaUnidadeGeradora.HasValue)
                {
                    throw new InvalidOperationException($"Potência não informada para { unidadeGeradora.UnidadeGeradoraId }");
                }

                var hp = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HP, consolidacaoMensalId);

                var hdp = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HDP, consolidacaoMensalId);

                var hedp = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HEDP, consolidacaoMensalId);


                if (hp is null || hdp is null || hedp is null)
                {
                    throw new InvalidOperationException("Parâmetros não calculados");
                }

                ValidarParametrosUnidadeGeradora(unidadeGeradora, hp, hdp, hedp);


                numeradorTaxa +=
                    valorPotenciaUnidadeGeradora.Value
                    .MultiplicarPor(hdp.Valor.Value + hedp.Valor.Value)
                    .DividirPor(hp.Valor.Value);

                totalPotencia += valorPotenciaUnidadeGeradora.Value;

                _insumoCalculoService.AdicionarInsumo(potenciaUnidadeGeradora);
                _insumoCalculoService.AdicionarInsumos(new[]
                {
                    hp,
                    hdp,
                    hedp
                });
            }

            if (totalPotencia == 0)
            {
                throw new InvalidOperationException($"O somatório das potências das unidades geradoras deve ser maior que 0");
            }

            return numeradorTaxa.DividirPor(totalPotencia);
        }

        private static void ValidarParametrosUnidadeGeradora(
            UnidadeGeradora unidadeGeradora,
            ParametroTaxa hp,
            ParametroTaxa hdp,
            ParametroTaxa hedp)
        {
            if (hp is null || hdp is null || hedp is null )
            {
                throw new InvalidOperationException($"Parâmetros não calculados para { unidadeGeradora.UnidadeGeradoraId } ");
            }
        }
    }
}
