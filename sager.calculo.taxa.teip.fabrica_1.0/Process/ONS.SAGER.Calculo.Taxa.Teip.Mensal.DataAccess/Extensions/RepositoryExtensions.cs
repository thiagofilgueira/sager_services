using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.DataAccess.Repositories;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IControleCalculoTaxaRepository, ControleCalculoTaxaRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IPotenciaUnidadeGeradoraRepository, PotenciaUnidadeGeradoraRepository>();
            services.AddSingleton<ISuspensaoUnidadeGeradoraRepository, SuspensaoUnidadeGeradoraRepository>();
            services.AddSingleton<ITaxaRepository, TaxaRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
        }
    }
}
