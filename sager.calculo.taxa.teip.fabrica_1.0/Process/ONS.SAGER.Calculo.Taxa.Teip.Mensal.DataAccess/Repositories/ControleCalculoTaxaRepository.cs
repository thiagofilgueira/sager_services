﻿using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.DataAccess.Repositories;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.DataAccess.Repositories
{
    public class ControleCalculoTaxaRepository : ControleCalculoTaxaRepositoryBase, IControleCalculoTaxaRepository
    {
    }
}
