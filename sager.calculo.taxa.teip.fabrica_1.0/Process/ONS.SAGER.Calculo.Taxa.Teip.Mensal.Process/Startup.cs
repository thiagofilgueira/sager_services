using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Application.Extensions;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.DataAccess.Extensions;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Extensions;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Domain.Map;
using ONS.SAGER.Calculo.Taxa.Teip.Mensal.Entrypoint.Extensions;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SDK.Builder;
using ONS.SDK.Extensions.DependencyInjection;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process
{
    public class Startup : IStartup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAppSettings(Configuration);
            services.AddApplicationServices();
            services.AddDomainServices();
            services.AddRepositoryServices();
            services.UseSDK();
            services.UseDataMap<EntitiesMap>();
            services.AddEntrypointServices();
            services.AddExecutionContextAbstraction();
            services.AddCalculoLogger();
        }

        public void Configure(IAppBuilder app)
        {
            //
        }
    }
}
