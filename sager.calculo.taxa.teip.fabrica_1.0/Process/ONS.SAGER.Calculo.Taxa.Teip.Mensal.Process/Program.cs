using ONS.SDK.Impl.Builder;

namespace ONS.SAGER.Calculo.Taxa.Teip.Mensal.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            AppBuilder.CreateDefaultBuilder(null)
                .UseStartup<Startup>()
                .RunSDK();
        }
    }
}
