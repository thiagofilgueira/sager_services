using Moq;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.UnitTests
{
    public class DomainTests
    {

        [Theory]
        [InlineData(11, 4, 34, 131, 1, 3, 1, 2, 1, 44, 4, 2, 11, 34, 11, 12, 11, 5, 44, 4, 1, 4, 151, 112, 41, 12, 53, 0.40684506802117459)]
        [InlineData(3, 5, 5, 3, 8, 5, 4, 1, 2, 6, 12, 13, 15, 20, 5, 1, 20, 45, 5, 2, 3, 5, 4, 7, 44, 4, 5, 0.78285760361232071)]
        public void CalcularTaxaTeifaMensal(
            int hp1,
            int hdp1,
            int hedp1,
            int hdf1,
            int hedf1,
            int hs1,
            int hrd1,
            int hdce1,
            int hp2,
            int hdp2,
            int hedp2,
            int hdf2,
            int hedf2,
            int hs2,
            int hrd2,
            int hdce2,
            int hp3,
            int hdp3,
            int hedp3,
            int hdf3,
            int hedf3,
            int hs3,
            int hrd3,
            int hdce3,
            int potencia1,
            int potencia2,
            int potencia3,
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();

            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", hp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", hdp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", hedp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", hdf1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", hedf1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", hs1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", hrd1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", hdce1, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", hp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", hdp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", hedp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", hdf2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", hedf2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", hs2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", hrd2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", hdce2, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", hp3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", hdp3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", hedp3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "3", hdf3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", hedf3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", hs3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "3", hrd3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", hdce3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"3")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", potencia1),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", potencia2),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", potencia3)
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var taxaTeifaMensal = service.Calcular(calculoRequest).Result;

            Assert.Equal(result, taxaTeifaMensal);
        }

        [Theory]
        [InlineData(11, 4, 34, 131, 1, 3, 1, 2, 1, 44, 4, 2, 11, 34, 11, 12, 0.8924183337009)]
        [InlineData(3, 5, 5, 3, 8, 5, 4, 1, 2, 6, 12, 13, 15, 20, 5, 1, 0.83389074693423)]
        public async Task DeveCalcularParaUsinaEmOperacaoDesconsiderandoUnidadeGeradoraDaUsinaNaoConsideradaNoCalculo(
            int hp1,
            int hdp1,
            int hedp1,
            int hdf1,
            int hedf1,
            int hs1,
            int hrd1,
            int hdce1,
            int hp2,
            int hdp2,
            int hedp2,
            int hdf2,
            int hedf2,
            int hs2,
            int hrd2,
            int hdce2,
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();

            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", hp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", hdp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", hedp1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", hdf1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", hedf1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", hs1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", hrd1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", hdce1, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", hp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", hdp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", hedp2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", hdf2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", hedf2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", hs2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", hrd2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", hdce2, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","2",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
               new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2019,12,1), null, "1", 312),
               new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2019,12,1), null, "2", 33),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2018,10,30)),
            };


            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 10, 31));

            var taxaTeifaMensal = await service.Calcular(calculoRequest);

            Assert.Equal(result, Math.Round(taxaTeifaMensal.Value, 14));
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoParaUmaUgeMesmoTendoValorParametro()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 6.4, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","2",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1))
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 01));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoPara3UgesMesmoTendoValorParametro()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 6.4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 6.4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "3", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "3", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 6.4, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 100),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 150),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                 new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
                 new SuspensaoUnidadeGeradora("2", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
                 new SuspensaoUnidadeGeradora("3", "1", new DateTime(2018,1,1), new DateTime(2019,1,1))
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 01));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Theory]
        [InlineData(0.42253521126761)]
        public async Task DeveCalcularUGEqueNaoEstaSuspensaDesconsiderandoUGEsuspensaNaoConsideradaNoCalculo(
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 6.4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 6.4, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2019,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2019,12,1), null, "2", 100),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                 new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,1,1)),
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 01));

            var taxaTeifaMensal = await service.Calcular(calculoRequest);

            Assert.Equal(result, Math.Round(taxaTeifaMensal.Value, 14));
        }

        [Theory]
        [InlineData(0.42253521126761)]
        public async Task DeveCalcularTeifaTendoSupensaoAntesDoInicioDoCalculo(
            double result)
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 6.4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 6.4, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
            };                    

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "2", 100),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                 new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2018,10,30)),
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var taxaTeifaMensal = await service.Calcular(calculoRequest);

            Assert.Equal(result, Math.Round(taxaTeifaMensal.Value, 14));
        }

        [Fact]
        public void NaoDeveCalcularQuandoUsinaDesativada()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 6.4, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 9, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 8.7, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 4.3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 6.4, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
            };                     

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2020,12,1), null, "2", 100),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                 new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2018,10,30)),
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(suspensoes);

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 01, 01), new DateTime(2019, 01, 01), null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 01));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeveRetornarExeceptionQuandoHrdEIgualAZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"2"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeveRetornarExeceptionQuandoHdfEIgualAZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);

        }

        [Fact]
        public void DeveRetornarExceptionQuandoHpIgualaZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HDF.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeveRetornarExceptionQuandoHdceIgualaZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HRDA.ToString(), TipoCalculo.HRDA.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeveRetornarExceptionQuandoHedfEZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public void DeveRetornarExceptionQuandoHedPEZero()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var unidadeGeradoraRepository = new Mock<IUnidadeGeradoraRepository>();
            var potenciaUnidadeGeradoraRepository = new Mock<IPotenciaUnidadeGeradoraRepository>();
            var suspensaoUnidadeGeradoraRepository = new Mock<ISuspensaoUnidadeGeradoraRepository>();
            var parametroRepository = new Mock<IParametroRepository>();

            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "1", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 21, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "1", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "1", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "1", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "1", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 5, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "2", 31, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "2", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "2", 45, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "2", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "2", 3, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "2", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "2", 7, "1", null, "1"),

                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HDF.ToString(), "3", 1, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "3", 2, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDF.ToString(), TipoCalculo.HEDF.ToString(), "3", 77, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDF.ToString(), TipoCalculo.HRDA.ToString(), "3", 5, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDCE.ToString(), TipoCalculo.HDCE.ToString(), "3", 6, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HSA.ToString(), TipoCalculo.HSA.ToString(), "3", 112, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "3", 3, "1", null, "1"),
            };

            var unidadesGeradoras = new List<UnidadeGeradora>()
            {
                new UnidadeGeradora("1","1","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("2","2","1",new DateTime(2000,1,1),null,null,"1"),
                new UnidadeGeradora("3","3","1",new DateTime(2000,1,1),null,null,"1")
            };

            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "1", 41),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "2", 12),
                new PotenciaUnidadeGeradora(null, new DateTime(2018,11,1), new DateTime(2018,12,1), null, "3", 53)
            };

            unidadeGeradoraRepository.Setup(p =>
            p.ObterUnidadesGeradorasPorUsina(
                It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(unidadesGeradoras);

            potenciaUnidadeGeradoraRepository.Setup(p =>
            p.ObterPotenciasUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(potencias);

            suspensaoUnidadeGeradoraRepository.Setup(p =>
            p.ObterSuspensoesUnidadesGeradoras(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
            .ReturnsAsync(new List<SuspensaoUnidadeGeradora>());

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<string>(),
                It.IsAny<IEnumerable<TipoCalculo>>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(parametros);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraRepository.Object,
                potenciaUnidadeGeradoraRepository.Object,
                suspensaoUnidadeGeradoraRepository.Object,
                parametroRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var usina = new UsinaRequest("1", new DateTime(2000, 1, 1), null, null, TipoUsina.UEE.ToString());

            var calculoRequest = new CalcularTaxaTeifaMesRequest(usina, "1", "1", "1", new DateTime(2018, 11, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);

        }
    }
}
