using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Application.Services;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaApplicationService, TaxaApplicationService>();
        }
    }
}
