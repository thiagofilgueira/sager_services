using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Application.Interfaces
{
    public interface ITaxaApplicationService
    {
        Task<Result> Calcular(CalcularTaxaTeifaMesRequest request);
    }
}
