using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Services;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }
    }
}
