using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Services;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Services
{
    public class TaxaService : TaxaMensalServiceBase, ITaxaService
    {
        private readonly IInsumoCalculoService _insumoCalculoService;

        public TaxaService(
            ITaxaRepository taxaRepository,
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IParametroRepository parametroRepository,
            IInsumoCalculoService insumoCalculoService)
            : base(
                  taxaRepository,
                  unidadeGeradoraRepository ,
                  potenciaUnidadeGeradoraRepository,
                  suspensaoUnidadeGeradoraRepository,
                  parametroRepository)
        {
            _insumoCalculoService = insumoCalculoService;
        }

        public async Task<double?> Calcular(CalcularTaxaTeifaMesRequest request)
        {
            var taxaAjustada = await ObterTaxaAjustada(
                TipoCalculo.TEIFaMesAjustada,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.Usina.UsinaId);

            if (taxaAjustada.IsNotNull())
            {
                return taxaAjustada.Valor.Value;
            }

            var unidadesGeradoras = await ObterDadosUnidadesGeradorasAtivasParaCalculo(
                request.Usina.UsinaId,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.ConsolidacaoMensal.DataReferencia,
                new TipoCalculo[]
                {
                    TipoCalculo.HDF,
                    TipoCalculo.HEDF,
                    TipoCalculo.HSA,
                    TipoCalculo.HRDA,
                    TipoCalculo.HDCE
                },
                request.ConfiguracaoCenarioId);

            var valorTaxa = CalcularTeifaMensal(
                unidadesGeradoras,
                request.ConsolidacaoMensal.ConsolidacaoMensalId, 
                request.ConsolidacaoMensal.DataReferencia);

            await SalvarTaxaVersionada(
                request.ConsolidacaoMensal.ConsolidacaoMensalId, 
                TipoCalculo.TEIFAmes, 
                request.Usina.UsinaId, 
                request.ConfiguracaoCenarioId,
                request.ControleCalculoId,
                valorTaxa);

            return valorTaxa;
        }

        private double CalcularTeifaMensal(IEnumerable<UnidadeGeradora> unidadesGeradoras, string consolidacaoMensalId, DateTime dataReferencia)
        {
            if (unidadesGeradoras.IsNullOrEmpty())
            {
                throw new InvalidOperationException("Unidades geradoras não disponíveis para cálculo");
            }

            double numeradorTaxa = 0;
            double totalPotencia = 0;

            foreach (var unidadeGeradora in unidadesGeradoras)
            {
                var potenciaUnidadeGeradora = unidadeGeradora.ObterUltimaPotenciaDoMes(dataReferencia);

                var valorPotenciaUnidadeGeradora = potenciaUnidadeGeradora?.ValorPotencia;

                if (!valorPotenciaUnidadeGeradora.HasValue)
                {
                    throw new InvalidOperationException($"Potência não informada para { unidadeGeradora.UnidadeGeradoraId }");
                }

                var hdf = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HDF, consolidacaoMensalId);

                var hedf = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HEDF, consolidacaoMensalId);

                var hsa = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HSA, consolidacaoMensalId);

                var hrda = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HRDA, consolidacaoMensalId);

                var hdce = unidadeGeradora
                    .ObterUltimoParametroCalculadoPriorizandoCenario(TipoCalculo.HDCE, consolidacaoMensalId);

                ValidarParametrosUnidadeGeradora(unidadeGeradora, hdf, hedf, hsa, hrda, hdce);

                var numerador = valorPotenciaUnidadeGeradora.Value.MultiplicarPor(hdf.Valor.Value + hedf.Valor.Value);

                var denominador = hsa.Valor.Value + hdf.Valor.Value + hrda.Valor.Value + hdce.Valor.Value;

                numeradorTaxa += denominador > 0 ? 
                    numerador.DividirPor(denominador) : 0;

                totalPotencia += valorPotenciaUnidadeGeradora.Value;

                _insumoCalculoService.AdicionarInsumo(potenciaUnidadeGeradora);
                _insumoCalculoService.AdicionarInsumos(new[] { hdf, hedf, hsa,  hrda, hdce });
            }

            if (totalPotencia == 0)
            {
                throw new InvalidOperationException($"O somatório das potências das unidades geradoras deve ser maior que 0");
            }

            return numeradorTaxa.DividirPor(totalPotencia);
        }

        private static void ValidarParametrosUnidadeGeradora(
            UnidadeGeradora unidadeGeradora, 
            ParametroTaxa hdf, 
            ParametroTaxa hedf,
            ParametroTaxa hsa, 
            ParametroTaxa hrda, 
            ParametroTaxa hdce)
        {
            if (hdf is null || hedf is null || hsa is null  || hrda is null ||  hdce is null)
            {
                throw new InvalidOperationException($"Parâmetros não calculados para { unidadeGeradora.UnidadeGeradoraId } ");
            }
        }

    }
}
