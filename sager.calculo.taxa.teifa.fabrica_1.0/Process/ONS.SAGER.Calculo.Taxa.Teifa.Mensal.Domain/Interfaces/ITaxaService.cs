using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Interfaces
{
    public interface ITaxaService : ITaxaMensalServiceBase
    {
        Task<double?> Calcular(CalcularTaxaTeifaMesRequest request);
    }
}
