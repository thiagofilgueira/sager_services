using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests
{
    public class CalcularTaxaTeifaMesRequest : CalcularTaxaRequest
    {
        public CalcularTaxaTeifaMesRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            string consolidacaoMensalId,
            DateTime dataReferencia)
            : base(usina, controleCalculoId, configuracaoCenarioId, new ConsolidacaoMensalRequest(consolidacaoMensalId, dataReferencia))
        {
            Validar();
        }
        public CalcularTaxaTeifaMesRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal)
            : base(usina, controleCalculoId, configuracaoCenarioId, consolidacaoMensal)
        {
            Validar();
        }
    }
}
