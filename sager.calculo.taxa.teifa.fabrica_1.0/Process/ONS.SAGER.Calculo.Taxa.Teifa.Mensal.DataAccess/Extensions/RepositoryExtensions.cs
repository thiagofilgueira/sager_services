using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.DataAccess.Repositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IControleCalculoTaxaRepository, ControleCalculoTaxaRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IPotenciaUnidadeGeradoraRepository, PotenciaUnidadeGeradoraRepository>();
            services.AddSingleton<ISuspensaoUnidadeGeradoraRepository, SuspensaoUnidadeGeradoraRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
            services.AddSingleton<ITaxaRepository, TaxaRepository>();
        }
    }
}
