using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Mensal.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ITaxaApplicationService _taxaTeifaMensalApplicationService;
        public CalculosEntrypoint(ITaxaApplicationService taxaTeifaMensalApplicationService)
        {
            _taxaTeifaMensalApplicationService = taxaTeifaMensalApplicationService;
        }

        [SDKEvent(Eventos.CalcularTaxaMes)]
        public async Task CalcularTaxaTeifaMensal(CalcularTaxaPayload payload)
        {
            await _taxaTeifaMensalApplicationService.Calcular(
                new CalcularTaxaTeifaMesRequest(
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.ControleCalculoIdPayload,
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }

    }
}
