using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Services;


namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IUnidadeGeradoraService, UnidadeGeradoraService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }
    }
}
