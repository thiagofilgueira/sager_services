using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Services;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Services
{
    public class TaxaService : TaxaAcumuladaServiceBase, ITaxaService
    {

        public TaxaService(
            ITaxaRepository taxaRepository,
            IUnidadeGeradoraService unidadeGeradoraService,
            IConsolidacaoMensalRepository consolidacaoMensalRepository,
            IInsumoCalculoService insumoCalculoService)
            : base(taxaRepository, unidadeGeradoraService, consolidacaoMensalRepository, insumoCalculoService)
        {
        }

        public async Task<double?> Calcular(CalcularTaxaTeifaAcumRequest request)
        {
            var taxaAjustada = await ObterTaxaAjustada(
                TipoCalculo.TEIFAacum,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.Usina.UsinaId);

            if (taxaAjustada.IsNotNull())
            {
                await SalvarTaxaVersionada(
                    request.ConsolidacaoMensal.ConsolidacaoMensalId,
                    TipoCalculo.TEIFAacum,
                    request.Usina.UsinaId,
                    request.ConfiguracaoCenarioId,
                    request.ControleCalculoId,
                    taxaAjustada.Valor.Value);

                return taxaAjustada.Valor.Value;
            }

            var valorTaxa = await CalcularTaxaAcumuladaPorTaxasMensais(
                TipoCalculo.TEIFAacum,
                request.ConsolidacaoMensal.DataReferencia,
                request.Usina.UsinaId,
                request.Usina.DataEntradaOperacao,
                request.ConfiguracaoCenarioId);

            await SalvarTaxaVersionada(
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                TipoCalculo.TEIFAacum,
                request.Usina.UsinaId,
                request.ConfiguracaoCenarioId,
                request.ControleCalculoId,
                valorTaxa);

            return valorTaxa;
        }
    }
}
