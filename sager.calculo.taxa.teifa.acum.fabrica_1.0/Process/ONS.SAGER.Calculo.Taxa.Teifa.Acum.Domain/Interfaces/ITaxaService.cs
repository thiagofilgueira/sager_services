using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Interfaces
{
    public interface ITaxaService : ITaxaAcumuladaServiceBase
    {
        Task<double?> Calcular(CalcularTaxaTeifaAcumRequest request);
    }
}
