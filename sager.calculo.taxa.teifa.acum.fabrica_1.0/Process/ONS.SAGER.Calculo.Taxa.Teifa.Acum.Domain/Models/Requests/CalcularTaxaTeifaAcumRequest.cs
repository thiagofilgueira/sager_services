using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests
{
    public class CalcularTaxaTeifaAcumRequest : CalcularTaxaRequest
    {
        public CalcularTaxaTeifaAcumRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            string consolidacaoMensalId,
            DateTime dataReferencia)
            : base(usina, controleCalculoId, configuracaoCenarioId, new ConsolidacaoMensalRequest(consolidacaoMensalId, dataReferencia))
        {
            Validar();
        }
        public CalcularTaxaTeifaAcumRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal)
            : base(usina, controleCalculoId, configuracaoCenarioId, consolidacaoMensal)
        {
            Validar();
        }
    }
}
