using ONS.SDK.Impl.Builder;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            AppBuilder.CreateDefaultBuilder(null)
                .UseStartup<Startup>()
                .RunSDK();
        }
    }
}
