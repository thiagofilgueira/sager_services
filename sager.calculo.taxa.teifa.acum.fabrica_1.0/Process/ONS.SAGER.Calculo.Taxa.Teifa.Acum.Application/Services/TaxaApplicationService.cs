using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Logger;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Services
{
    public class TaxaApplicationService : TaxaAcumuladaApplicationServiceBase<TaxaApplicationService>, ITaxaApplicationService
    {
        private readonly ITaxaService _taxaService;

        public TaxaApplicationService(
            ITaxaService taxaService,
            IExecutionContextAbstraction executionContext,
            IControleCalculoTaxaRepository controleRepository,
            ICalculoLogger<TaxaApplicationService> logger)
            : base(executionContext, controleRepository, logger)
        {
            _taxaService = taxaService;
        }

        public Task<Result> Calcular(CalcularTaxaTeifaAcumRequest request)
        {
            return CalcularTaxa(
                request,
                TipoCalculo.TEIFAacum,
                () => _taxaService.Calcular(request));
        }

    }
}
