using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Interfaces
{
    public interface ITaxaApplicationService
    {
        Task<Result> Calcular(CalcularTaxaTeifaAcumRequest request);
    }
}
