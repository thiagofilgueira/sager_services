using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Services;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaApplicationService, TaxaApplicationService>();
        }
    }
}
