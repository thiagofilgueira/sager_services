﻿using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.DataAccess.Repositories;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.DataAccess.Repositories
{
    public class ControleCalculoTaxaRepository : ControleCalculoTaxaRepositoryBase, IControleCalculoTaxaRepository
    {
    }
}
