using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ITaxaApplicationService _taxaTeifaAcumApplicationService;
        public CalculosEntrypoint(ITaxaApplicationService taxaTeifaAcumApplicationService)
        {
            _taxaTeifaAcumApplicationService = taxaTeifaAcumApplicationService;
        }

        [SDKEvent(Eventos.CalcularTaxaAcum)]
        public async Task CalcularTaxaTeifaAcum(CalcularTaxaPayload payload)
        {
            await _taxaTeifaAcumApplicationService.Calcular(
                new CalcularTaxaTeifaAcumRequest(
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.ControleCalculoIdPayload,
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));              
        }

    }
}
