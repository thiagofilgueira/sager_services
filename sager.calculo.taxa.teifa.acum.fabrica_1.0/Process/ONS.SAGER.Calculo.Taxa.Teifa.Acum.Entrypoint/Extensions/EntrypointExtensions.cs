using Microsoft.Extensions.DependencyInjection;
using ONS.SDK.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Entrypoint.Services;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.Entrypoint.Extensions
{
    public static class EntrypointExtensions
    {
        public static void AddEntrypointServices(this IServiceCollection services)
        {
            services.BindEvents<CalculosEntrypoint>();
            services.AddSingleton<CalculosEntrypoint>();
        }
    }
}
