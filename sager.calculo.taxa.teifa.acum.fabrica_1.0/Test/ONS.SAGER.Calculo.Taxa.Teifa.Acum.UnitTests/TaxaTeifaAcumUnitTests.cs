using Moq;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Taxa.Teifa.Acum.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Taxa.Teifa.Acum.UnitTests
{
    public class TaxaTeifaAcumUnitTests
    {
        [Fact]
        public async Task CalcularTaxaTeifaAcumTest()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",4,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",3,2019)
            };
            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);


            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIFAmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFAmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIFAmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIFAmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            var taxasReferencia = new List<CalculoTaxa>() 
            { 
               new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFAmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.TEIFReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFaacumAjustada,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasAjustadas);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","1","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"1")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object, 
                unidadeGeradoraService.Object, 
                consolidacaoMensalRepository.Object, 
                new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularTaxaTeifaAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2010, 01, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(95.8, taxa);
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoMesmoComParametroCalculado()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",4,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",3,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090wi",2,2019),

            };
            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);


            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIFAmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFAmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIFAmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIFAmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            var taxasReferencia = new List<CalculoTaxa>()
            {
               new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFAmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.TEIFReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFaacumAjustada,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasAjustadas);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","1","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"1")
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                new SuspensaoUnidadeGeradora("1", "1", new DateTime(2019,03,01), new DateTime(2019,07,01)),
            };
            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarSuspensoes(suspensoes.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularTaxaTeifaAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2010, 01, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);
        }

        [Fact]
        public async Task NaoDeveCalcularQuandoUsinaDesativada()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",4,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",3,2019),

            };
            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);


            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIFAmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFAmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIFAmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIFAmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIFAacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            var taxasReferencia = new List<CalculoTaxa>()
            {
               new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIFReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFAmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.TEIFReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIFaacumAjustada,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasAjustadas);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","1","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"1")
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                
            };
            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarSuspensoes(suspensoes.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularTaxaTeifaAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2017, 01, 01), new DateTime(2019,01,01), null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(95.8, taxa);
        }
    }
}
