﻿using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Process.Domain.Services
{
    public class DomainTests
    {

        [Fact]
        public void DevCalcularUnidadeGeradireUsinasValidasPorPeriodo()
        {
            //var service = new UsinaService(
            //    new Mock<IExecutionContextAbstraction>().Object,
            //    new Mock<IUsinaRepository>().Object,
            //    new Mock<AppSettings>().Object);

            //var calculoRequest = new CalcularUsinaRequest("02/01/2020", new DateTime(2020, 01, 01));

            //var usina = service.Calcular(calculoRequest);

            //Assert.NotNull(usina);
        }

        [Fact]
        public void ResultadoDeveSerVerdadeiroQuandoPassarConfiguracaoIdCenarioeDataReferencia()
        {
            //var CalcularUsinaRequest = new CalcularUsinaRequest(
            //    "02/01/2020",
            //    new DateTime(2020, 01, 01)
            //);

            //var result = CalcularUsinaRequest.Valido;

            //Assert.True(result);
        }

        //[Fact(DisplayName = "Teste para verificar se a conclusão não terminou.")]
        ////Quando unidade geradora estiver preenchida
        ////o método deve retornar verdadeiro
        //public void DeveRetornarVerdadeiroQuandoOProcessamentoDosParametrosNaoTerminou()
        //{
        //    var request = new VerificarConclusaoDeCalculoUsinasRequest(
        //            "1",
        //            "1",
        //            "1",
        //            "1",
        //            new ConsolidacaoMensalRequest(null, DateTime.Now)
        //        );

        //    var result = calcularParametroHpRequest.PossuiSuspensoes();
        //    Assert.True(result);
        //}
    }
}
