﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Application.Services;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Application
{
    public class FinalizarUnidadeGeradoraApplicationServiceTests
    {
        private readonly IControleCalculoService _controleCalculoService = new Mock<IControleCalculoService>().Object;
        private readonly ICalculoLogger<FinalizarUnidadeGeradoraApplicationService> _calculoLogger = new Mock<ICalculoLogger<FinalizarUnidadeGeradoraApplicationService>>().Object;
        private readonly IExecutionContextAbstraction _context = new Mock<IExecutionContextAbstraction>().Object;

        public FinalizarUnidadeGeradoraApplicationServiceTests()
        {
        }

        [Fact(DisplayName = "Teste para verificar se retornar Bad Request quando o resultado é igual a null")]
        public async Task DeveRetornarStatusBadRequestQuandoVerificarConclusaoDeCalculoUsinasRequestForNulo()
        {

            var usinaServiceMock = new Mock<IUsinaService>();
            var taxaServiceMock = new Mock<ITaxaService>();

            var usinaApplicationService = new FinalizarUnidadeGeradoraApplicationService(
                usinaServiceMock.Object,
                _controleCalculoService,
                taxaServiceMock.Object,
                 _context,
                 _calculoLogger);

            FinalizarUnidadeGeradoraRequest verificarConclusaoDeCalculoUsinasRequest = null;

            var result = await usinaApplicationService.Finalizar(verificarConclusaoDeCalculoUsinasRequest).ConfigureAwait(false);

            Assert.True(ResultStatus.BadRequest == result.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna Bad Request quando a requisição é inválida")]
        public async Task DeveRetornarBadRequestQuandoVerificarConclusaoDeCalculoUsinasRequestForInvalido()
        {
            var usinaServiceMock = new Mock<IUsinaService>();
            var taxaServiceMock = new Mock<ITaxaService>();

            var usinaApplicationService = new FinalizarUnidadeGeradoraApplicationService(
                usinaServiceMock.Object,
                _controleCalculoService,
                taxaServiceMock.Object,
                 _context,
                 _calculoLogger);

            FinalizarUnidadeGeradoraRequest verificarConclusaoDeCalculoUsinasRequest =
                new FinalizarUnidadeGeradoraRequest(Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(null, DateTime.MinValue));

            var result = await usinaApplicationService.Finalizar(verificarConclusaoDeCalculoUsinasRequest).ConfigureAwait(false);

            Assert.True(ResultStatus.BadRequest == result.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna sucesso quando a requisição está preenchida corretamente")]
        public async Task DeveRetornarSucessQuandoVerificarConclusaoDeCalculoUsinasRequestForValido()
        {
            var usinaServiceMock = new Mock<IUsinaService>();
            var taxaServiceMock = new Mock<ITaxaService>();

            var usinaApplicationService = new FinalizarUnidadeGeradoraApplicationService(
                usinaServiceMock.Object,
                _controleCalculoService,
                taxaServiceMock.Object,
                 _context,
                 _calculoLogger);

            FinalizarUnidadeGeradoraRequest verificarConclusaoDeCalculoUsinasRequest =
                new FinalizarUnidadeGeradoraRequest(Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.MinValue));

            var result = await usinaApplicationService.Finalizar(verificarConclusaoDeCalculoUsinasRequest).ConfigureAwait(false);

            Assert.False(ResultStatus.BadRequest == result.Status);
        }

    }
}
