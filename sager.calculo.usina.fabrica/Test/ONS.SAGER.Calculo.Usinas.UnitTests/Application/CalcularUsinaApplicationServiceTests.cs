﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Application.Services;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Application
{
    public class CalcularUsinaApplicationServiceTests
    {
        private readonly ICalculoLogger<CalcularUsinaApplicationService> _calculoLogger;
        private readonly IExecutionContextAbstraction _context = new Mock<IExecutionContextAbstraction>().Object;

        public CalcularUsinaApplicationServiceTests()
        {
            var usinaServiceMock = new Mock<IUsinaService>();

            // Métodos de Mock para cálculo de usinas
            usinaServiceMock.Setup(p => 
            p.ObterUsinaValidaParaCalculo(
                It.IsAny<string>(), 
                It.IsAny<DateTime>(), 
                It.IsAny<string>()))
                .ReturnsAsync(new Usina()
                { 
                    Id = "1",
                    UsinaId = "BAUSB",
                    DataEntradaOperacao = new DateTime(1990,01,01)
                });

            _calculoLogger = new Mock<ICalculoLogger<CalcularUsinaApplicationService>>().Object;
        }

        [Fact(DisplayName = "Teste para verificar se retornar Bad Request quando o resultado é igual a null")]
        public async Task DeveRetornarStatusBadRequestQuandoCalcularUsinaRequestForNulo()
        {
            var usinaServiceMock = new Mock<IUsinaService>();
            var controleCalculoServiceMock = new Mock<IControleCalculoService>();

            var usinaApplicationService = new CalcularUsinaApplicationService(
                usinaServiceMock.Object,
                controleCalculoServiceMock.Object,
                _context,
                _calculoLogger);

            CalcularUsinaRequest calcularUsinaRequest = null;

            var result = await usinaApplicationService.Calcular(calcularUsinaRequest).ConfigureAwait(false);

            Assert.True(ResultStatus.BadRequest == result.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna Bad Request quando a requisição é inválida")]
        public async Task DeveRetornarBadRequestQuandoCalcularUsinaRequestForInvalido()
        {
            var usinaServiceMock = new Mock<IUsinaService>();
            var controleCalculoServiceMock = new Mock<IControleCalculoService>();

            var usinaApplicationService = new CalcularUsinaApplicationService(
                usinaServiceMock.Object,
                controleCalculoServiceMock.Object,
                _context,
                _calculoLogger);

            CalcularUsinaRequest calcularUsinaRequest = new CalcularUsinaRequest(new ConsolidacaoMensalRequest(null, DateTime.MinValue), "1", "1");

            var result = await usinaApplicationService.Calcular(calcularUsinaRequest).ConfigureAwait(false);

            Assert.True(ResultStatus.BadRequest == result.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna sucesso quando a requisição está preenchida corretamente")]
        public async Task DeveRetornarSucessQuandoCalcularUsinaRequestForValido()
        {
            var usinaServiceMock = new Mock<IUsinaService>();
            var controleCalculoServiceMock = new Mock<IControleCalculoService>();

            var usinaApplicationService = new CalcularUsinaApplicationService(
                usinaServiceMock.Object,
                controleCalculoServiceMock.Object,
                _context,
                _calculoLogger);

            CalcularUsinaRequest calcularUsinaRequest = new CalcularUsinaRequest(new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.MinValue), Guid.NewGuid().ToString(), Guid.NewGuid().ToString()); ;

            var result = await usinaApplicationService.Calcular(calcularUsinaRequest).ConfigureAwait(false);

            Assert.True(ResultStatus.BadRequest == result.Status);
        }

    }
}
