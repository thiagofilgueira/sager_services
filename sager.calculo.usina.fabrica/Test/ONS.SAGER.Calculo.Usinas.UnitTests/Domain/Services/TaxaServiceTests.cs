﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Usinas.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Application
{
    public class TaxaServiceTests
    {

        [Fact]
        public void VerificarNecessidadeCalculoIndiceIndisponibilidade_DeveRetonarVerdadeiroSeUsinaPossuirDataRenovacaoConcessaoEDataReferenciaMaiorQueOutubro2014()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);


            var request = new FinalizarTaxaRequest(
                "1",
                "1",
                new UsinaRequest("1", new DateTime(1990, 1, 1), null, DateTime.Now, "UTC"),
                Util.Domain.Enums.TipoCalculo.TEIFAacum,
                new ConsolidacaoMensalRequest("1", new DateTime(2015, 01, 01)));

            var result =  taxaService.VerificarNecessidadeCalculoIndiceIndisponibilidade(request);

            Assert.True(result);
        }

        [Fact]
        public void VerificarNecessidadeCalculoIndiceIndisponibilidade_DeveRetonarFalsoSeUsinaNaoPossuirDataRenovacaoConcessao()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);


            var request = new FinalizarTaxaRequest(
                "1",
                "1",
                new UsinaRequest("1", new DateTime(1990, 1, 1), null, null, "UTC"),
                Util.Domain.Enums.TipoCalculo.TEIFAacum,
                new ConsolidacaoMensalRequest("1", new DateTime(2015, 01, 01)));

            var result = taxaService.VerificarNecessidadeCalculoIndiceIndisponibilidade(request);

            Assert.False(result);
        }

        [Fact]
        public void VerificarNecessidadeCalculoIndiceIndisponibilidade_DeveRetonarFalsoSeDataReferenciaMenorQueOutubro2014()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);

            var request = new FinalizarTaxaRequest(
                "1",
                "1",
                new UsinaRequest("1", new DateTime(1990, 1, 1), null, DateTime.Now, "UTC"),
                Util.Domain.Enums.TipoCalculo.TEIFAacum,
                new ConsolidacaoMensalRequest("1", new DateTime(2013, 01, 01)));

            var result = taxaService.VerificarNecessidadeCalculoIndiceIndisponibilidade(request);

            Assert.False(result);
        }

        [Fact]
        public void VerificarNecessidadeCalculoTaxaMensal_DeveRetonarFalsoSeDataReferenciaMaiorQueOutubro2014()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);


            var result = taxaService.VerificarNecessidadeCalculoTaxaMensal(new DateTime(2016, 01, 01));

            Assert.False(result);
        }

        [Fact]
        public void VerificarNecessidadeCalculoTaxaMensal_DeveRetonarVerdadeiroSeDataReferenciaMenorQueOutubro2014()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);


            var result = taxaService.VerificarNecessidadeCalculoTaxaMensal(new DateTime(2012, 01, 01));

            Assert.True(result);
        }


        [Fact]
        public async Task RealizarEstornoCalculosTaxas_NaoDeveRealizarEstornoSeTaxasVazias()
        {
            var taxaService = new TaxaService(new Mock<ITaxaRepository>().Object);


             await taxaService.RealizarEstornoCalculosTaxas("1","1").ConfigureAwait(false);
        }

        [Fact]
        public async Task RealizarEstornoCalculosTaxas_DeveRealizarEstorno()
        {

            var taxaRepository = new Mock<ITaxaRepository>();
            taxaRepository
                .Setup(p => p.ObterTaxasPorControleCalculoId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<CalculoTaxa>() { new CalculoTaxa() { Id = "1" } });

            var taxaService = new TaxaService(taxaRepository.Object);


            await taxaService.RealizarEstornoCalculosTaxas("1", "1").ConfigureAwait(false);
        }
    }
}
