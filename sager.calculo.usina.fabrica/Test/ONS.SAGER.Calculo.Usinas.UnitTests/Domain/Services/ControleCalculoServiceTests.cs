﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Usinas.Domain.Services;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Domain.Services
{
    public class ControleCalculoServiceTests
    {

        [Fact]
        public async Task VerificarConclusaoCalculosUnidadesGeradoras_DeveBadRequestQuandoControleCalculoForNula()
        {
            // Organização

            var consolidacaoMensal = new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now);

            var request = new FinalizarCalculoUsinaRequest(
                                                                Guid.NewGuid().ToString(),
                                                                Guid.NewGuid().ToString(),
                                                                Guid.NewGuid().ToString(),
                                                                consolidacaoMensal
                                                            );

            List<ControleCalculoUnidadeGeradora> controleCalculo = null;

            var controleCalculoUnidadeGeradoraRepository = new Mock<IControleCalculoUnidadeGeradoraRepository>();


            controleCalculoUnidadeGeradoraRepository
                .Setup(x => x.ObterPorControleCalculo(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(controleCalculo);

            var controleService = new ControleCalculoService(
                                                             new Mock<IControleCalculoRepository>().Object,
                                                             controleCalculoUnidadeGeradoraRepository.Object,
                                                            new Mock<IControleCalculoTaxaRepository>().Object );

            // Ação

            var resultado = await controleService
                .VerificarConclusaoCalculosUnidadesGeradoras(It.IsAny<string>(), It.IsAny<string>())
                .ConfigureAwait(false);

            // Assert

            Assert.Equal(ResultStatus.BadRequest, resultado.Status);
        }

        [Fact]
        public async Task VerificarConclusaoCalculoTaxas_DeveBadRequestQuandoControleCalculoForNula()
        {
            // Organização

            var consolidacaoMensal = new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now);

            var request = new FinalizarCalculoUsinaRequest(
                                                                Guid.NewGuid().ToString(),
                                                                Guid.NewGuid().ToString(),
                                                                Guid.NewGuid().ToString(),
                                                                consolidacaoMensal
                                                            );

            List<ControleCalculoTaxa> controleCalculo = null;

            var controleRepository = new Mock<IControleCalculoTaxaRepository>();


            controleRepository
                .Setup(x => x.ObterPorControleCalculo(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(controleCalculo);

            var controleService = new ControleCalculoService(
                                                             new Mock<IControleCalculoRepository>().Object,
                                                             new Mock<IControleCalculoUnidadeGeradoraRepository>().Object,
                                                             controleRepository.Object);

            // Ação

            var resultado = await controleService
                .VerificarConclusaoCalculoTaxas(It.IsAny<string>(), It.IsAny<TipoCalculo[]>(), It.IsAny<string>())
                .ConfigureAwait(false);

            // Assert

            Assert.Equal(ResultStatus.BadRequest, resultado.Status);
        }

    }
}
