﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Application
{
    public class UsinaServiceTest
    {
        private readonly Mock<ITaxaService> _taxaService;
        private readonly Mock<IParametroService> _parametroService;
        private readonly Mock<IUsinaRepository> _usinaRepository;
        private readonly Mock<IUnidadeGeradoraRepository> _unidadeGeradoraRepsitory;

        public UsinaServiceTest()
        {
           _taxaService = new Mock<ITaxaService>();
           _usinaRepository = new Mock<IUsinaRepository>();
            _unidadeGeradoraRepsitory = new Mock<IUnidadeGeradoraRepository>();
            _parametroService = new Mock<IParametroService>();

        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task ObterUsinaValidaParaCalculo_DeveRetornarNuloQuandoUsinaIdVazio(string id)
        {
            var usinaService = new UsinaService(
                _parametroService.Object,
                _taxaService.Object,
                _usinaRepository.Object,
                _unidadeGeradoraRepsitory.Object);

            var usina = await usinaService.ObterUsinaValidaParaCalculo(id, new DateTime(2014,01,01), "1").ConfigureAwait(false);

            Assert.Null(usina);
        }

        [Fact]
        public async Task ObterUsinaValidaParaCalculo_DeveRetornarNuloQuandoUsinaForaOperacao()
        {
            var usinaRepository = new Mock<IUsinaRepository>();
            usinaRepository
                .Setup(p => p.ObterUsinaPorId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new Usina()
                {
                    Id = "1",
                    UsinaId = "BAUSB",
                    DataEntradaOperacao = new DateTime(2015, 01, 01)
                });


            var usinaService = new UsinaService(
                _parametroService.Object,
                _taxaService.Object,
                usinaRepository.Object,
                _unidadeGeradoraRepsitory.Object);


            var usina = await usinaService.ObterUsinaValidaParaCalculo("BASUB", new DateTime(2014, 01, 01), "1").ConfigureAwait(false);

            Assert.Null(usina);
        }

        [Fact]
        public async Task ObterUsinaValidaParaCalculo_DeveRetornarNuloQuandoUsinaDesativada()
        {
            var usinaRepository = new Mock<IUsinaRepository>();
            usinaRepository
                .Setup(p => p.ObterUsinaPorId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new Usina()
                {
                    Id = "1",
                    UsinaId = "BAUSB",
                    DataEntradaOperacao = new DateTime(2000, 01, 01),
                    DataDesativacao = new DateTime(2013,01,01)
                });


            var usinaService = new UsinaService(
                _parametroService.Object,
                _taxaService.Object,
                usinaRepository.Object,
                _unidadeGeradoraRepsitory.Object);


            var usina = await usinaService.ObterUsinaValidaParaCalculo("BASUB", new DateTime(2014, 01, 01), "1").ConfigureAwait(false);

            Assert.Null(usina);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task ObterUsinaPorId_DeveRetornarNuloQuandoUsinaIdVazio(string id)
        {
            var usinaService = new UsinaService(
                _parametroService.Object,
                _taxaService.Object,
                _usinaRepository.Object,
                _unidadeGeradoraRepsitory.Object);

            var usina = await usinaService.ObterUsinaPorId(id, "1").ConfigureAwait(false);

            Assert.Null(usina);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task ObterUnidadesGeradorasPorUsina_DeveRetornarNuloQuandoUsinaIdVazio(string id)
        {
            var usinaService = new UsinaService(
                _parametroService.Object,
                _taxaService.Object,
                _usinaRepository.Object,
                _unidadeGeradoraRepsitory.Object);

            var usina = await usinaService.ObterUnidadesGeradorasPorUsina(id, "1").ConfigureAwait(false);

            Assert.Null(usina);
        }

    }
}
