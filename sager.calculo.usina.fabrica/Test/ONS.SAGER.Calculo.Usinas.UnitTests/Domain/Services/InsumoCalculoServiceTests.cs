﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Services;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Domain.Services
{
    public class InsumoCalculoServiceTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task ObterInsumosCalculoUsina_DeveRetornarNulloSeControleCalculoVazio(string controleCalculo)
        {

            var insumoService = new InsumoCalculoService(
                new Mock<IExecutionContextAbstraction>().Object,
                new Mock<IProcessMemoryServiceAbstraction>().Object,
                new Mock<ITaxaRepository>().Object,
                new Mock<IParametroRepository>().Object,
                new Mock<IBaseConsultaService>().Object);

            var result = await insumoService.ObterInsumosCalculoUsina(null, null, controleCalculo, null).ConfigureAwait(false);

            Assert.Null(result);
        }

        [Theory]
        [InlineData("1")]
        [InlineData("2")]
        public async Task ObterInsumosCalculoUsina_DeveRetornarNullSeParametrosTaxasNulos(string controleCalculo)
        {


            var insumoService = new InsumoCalculoService(
                new Mock<IExecutionContextAbstraction>().Object,
                new Mock<IProcessMemoryServiceAbstraction>().Object,
                new Mock<ITaxaRepository>().Object,
                new Mock<IParametroRepository>().Object,
                new Mock<IBaseConsultaService>().Object);

            var result = await insumoService.ObterInsumosCalculoUsina(null, null, controleCalculo, null).ConfigureAwait(false);

            Assert.Null(result);
        }

        [Theory]
        [InlineData("1")]
        [InlineData("2")]
        public async Task ObterInsumosCalculoUsina_NaoDeveRetornarNulloSeParametrosTaxasNaoNulos(string controleCalculo)
        {

            var taxaRepo = new Mock<ITaxaRepository>();

            taxaRepo
                .Setup(p=> p.ObterTaxasPorControleCalculoId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<CalculoTaxa>() 
                {
                    new CalculoTaxa() 
                    { 
                        Id = "1",
                        _Metadata = new SDK.Domain.Base.Metadata()
                        {
                            InstanceId = "1"
                        }
                    } 
                });

            var parametroRepository = new Mock<IParametroRepository>();
            parametroRepository
                .Setup(p => p.ObterParametrosPorControleCalculoId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<ParametroTaxa>() 
                {
                    new ParametroTaxa()
                    {
                        Id = "1",
                       _Metadata = new SDK.Domain.Base.Metadata()
                        {
                            InstanceId = "1"
                        }
                    } 
                });

            var parametroService = new ParametroService(parametroRepository.Object);


            var insumoService = new InsumoCalculoService(
                new Mock<IExecutionContextAbstraction>().Object,
                new Mock<IProcessMemoryServiceAbstraction>().Object,
                taxaRepo.Object,
                parametroRepository.Object,
                new Mock<IBaseConsultaService>().Object);


            var result = await insumoService.ObterInsumosCalculoUsina(null, null, controleCalculo, null).ConfigureAwait(false);

            Assert.NotNull(result);
        }
    }
}
