﻿using Moq;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Application
{
    public class ParametroServiceTests
    {

        [Fact]
        public async Task RealizarEstornoCalculosTaxas_NaoDeveRealizarEstornoSeTaxasVazias()
        {
            var parametroService = new ParametroService(new Mock<IParametroRepository>().Object);


             await parametroService.RealizarEstornoCalculosParametros("1","1").ConfigureAwait(false);
        }

        [Fact]
        public async Task RealizarEstornoCalculosTaxas_DeveRealizarEstorno()
        {

            var parametroRepository = new Mock<IParametroRepository>();
            parametroRepository
                .Setup(p => p.ObterParametrosPorControleCalculoId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<ParametroTaxa>() { new ParametroTaxa() { Id = "1" } });

            var parametroService = new ParametroService(parametroRepository.Object);


            await parametroService.RealizarEstornoCalculosParametros("1", "1").ConfigureAwait(false);
        }
    }
}
