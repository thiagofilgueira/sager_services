﻿using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Model.Requests
{
    public class CalcularTaxaRequestTest
    {
        [Fact(DisplayName = "ResultadoDeveSerFalsoQuandoRequestForNulo")]
        public void ResultadoDeveSerFalsoQuandoRequestForNulo()
        {
            // Organização
            CalcularTaxaUsinaRequest calcularTaxaRequest = null;

            // Assert
            Assert.Null(calcularTaxaRequest);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando configuração do cenário for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            // Organização
            var calcularTaxaRequest = new Usinas.Domain.Models.Requests.CalcularTaxaUsinaRequest(
                 new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now),
                 new UsinaRequest(Guid.NewGuid().ToString(), new DateTime(1990,01,01), null, null, "UHE"),
                 configuracaoCenarioId,
                 Guid.NewGuid().ToString());

            // Ação
            calcularTaxaRequest.Validar();
            bool resultado = calcularTaxaRequest.Valido;

            // Assert
            Assert.False(resultado);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando controle de cálculo for vazio ou nulo")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarControleCalculoIdNuloOuVazio(string controleCalculoId)
        {
            // Organização
            var calcularTaxaRequest = new Usinas.Domain.Models.Requests.CalcularTaxaUsinaRequest(
                 new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now),
                 new UsinaRequest(Guid.NewGuid().ToString(), new DateTime(1990, 01, 01), null, null, "UHE"),
                 Guid.NewGuid().ToString(),
                 controleCalculoId);

            // Ação
            calcularTaxaRequest.Validar();
            bool resultado = calcularTaxaRequest.Valido;

            // Assert
            Assert.False(resultado);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando usina for vazio ou nulo")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarUnidadeGeradoraNuloOuVazio(string usinaId)
        {
            // Organização
            var calcularTaxaRequest = new CalcularTaxaUsinaRequest(
                 new ConsolidacaoMensalRequest(
                    Guid.NewGuid().ToString(), DateTime.Now),
                 new UsinaRequest(usinaId, new DateTime(1990, 01, 01), null, null, "UHE"),
                    Guid.NewGuid().ToString(),
                    Guid.NewGuid().ToString());

            // Ação
            calcularTaxaRequest.Validar();
            bool resultado = calcularTaxaRequest.Valido;

            // Assert
            Assert.False(resultado);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando consolidação mensal for vazio ou nulo")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConsolidacaoMensalIdNuloOuVazio(string consolidacaoMensalId)
        {
            // Organização
            var calcularTaxaRequest = new CalcularTaxaUsinaRequest(
                 new ConsolidacaoMensalRequest(consolidacaoMensalId, DateTime.Now),
                 new UsinaRequest(Guid.NewGuid().ToString(), new DateTime(1990, 01, 01), null, null, "UHE"),
                 Guid.NewGuid().ToString(),
                 Guid.NewGuid().ToString());

            // Ação
            calcularTaxaRequest.Validar();
            bool resultado = calcularTaxaRequest.Valido;

            // Assert
            Assert.False(resultado);
        }
    }
}
