﻿using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Model.Requests
{
    public class VerificarConclusaoDeCalculoUsinasRequestTest
    {
        [Fact(DisplayName = "Teste para verificar se retorna falso quando a requisição for nula")]
        public void ResultadoDeveSerFalsoQuandoRequestForNulo()
        {
            // Organização
            FinalizarUnidadeGeradoraRequest verificarConclusaoDeCalculoUsinasRequest = null;

            // Assert
            Assert.Null(verificarConclusaoDeCalculoUsinasRequest);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando configuração do cenário for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            // Organização
            var verificarConclusaoDeCalculoUsinasRequest = new FinalizarUnidadeGeradoraRequest(
                configuracaoCenarioId,
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(null, DateTime.Now));

            // Ação
            verificarConclusaoDeCalculoUsinasRequest.Validar();
            var result = verificarConclusaoDeCalculoUsinasRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando controle de cálculo for vazio ou nulo")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirControleCalculo(string controleCalculoId)
        {
            // Organização
            var verificarConclusaoDeCalculoUsinasRequest = new FinalizarUnidadeGeradoraRequest(
                Guid.NewGuid().ToString(),
                controleCalculoId,
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // Ação
            verificarConclusaoDeCalculoUsinasRequest.Validar();
            var result = verificarConclusaoDeCalculoUsinasRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando unidade geradora for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirUnidadeGeradora(string unidadeGeradoraId)
        {
            // Organização
            var verificarConclusaoDeCalculoUsinasRequest = new FinalizarUnidadeGeradoraRequest(
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                unidadeGeradoraId,
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest("1", DateTime.Now));

            // Ação
            verificarConclusaoDeCalculoUsinasRequest.Validar();
            var result = verificarConclusaoDeCalculoUsinasRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando usina for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirUsina(string usinaId)
        {
            // Organização
            var verificarConclusaoDeCalculoUsinasRequest = new FinalizarUnidadeGeradoraRequest(
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                usinaId,
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // Ação
            verificarConclusaoDeCalculoUsinasRequest.Validar();
            var result = verificarConclusaoDeCalculoUsinasRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando consolidação mensal for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirConsolidacaoMensal(string consolidacaoMensalId)
        {
            // Organização
            var verificarConclusaoDeCalculoUsinasRequest = new FinalizarUnidadeGeradoraRequest(
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(consolidacaoMensalId, DateTime.Now));

            // Ação
            verificarConclusaoDeCalculoUsinasRequest.Validar();
            var result = verificarConclusaoDeCalculoUsinasRequest.Valido;

            // Assert
            Assert.False(result);
        }
    }
}
