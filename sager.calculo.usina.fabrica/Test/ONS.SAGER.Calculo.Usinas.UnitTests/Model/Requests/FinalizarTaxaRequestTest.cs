using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Model.Requests 
{ 
    public class FinalizarTaxaRequestTest
    {
        [Fact(DisplayName = "Teste para verificar se retorna falso quando a requisi��o for nula")]
        public void ResultadoDeveSerFalsoQuandoRequestForNulo()
        {
            // Organiza��o
            FinalizarTaxaRequest finalizarCalculoUsinaRequest = null;

            // Assert
            Assert.Null(finalizarCalculoUsinaRequest);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando configura��o do cen�rio for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarTaxaRequest(
                configuracaoCenarioId,
                Guid.NewGuid().ToString(),
                new UsinaRequest("1",DateTime.Now,null,null,null),
                TipoCalculo.TEIPacum,
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // A��o
            finalizarCalculoUsinaRequest.Validar();
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando controle calculo  for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarControleCalculoNuloOuVazio(string controleCalculo)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarTaxaRequest(
                Guid.NewGuid().ToString(),
                controleCalculo,
                new UsinaRequest("1", DateTime.Now, null, null, null),
                TipoCalculo.TEIPacum,
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // A��o
            finalizarCalculoUsinaRequest.Validar();
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }
    }
}
