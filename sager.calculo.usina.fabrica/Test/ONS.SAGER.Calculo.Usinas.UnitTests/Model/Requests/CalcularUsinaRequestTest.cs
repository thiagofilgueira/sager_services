﻿using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Model.Requests
{
    public class CalcularUsinaRequestTest
    {
        [Theory(DisplayName = "Teste para verificar se retorna falso quando configuração do cenário for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            // Organização
            var calcularUsinaRequest = new CalcularUsinaRequest(
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                Guid.NewGuid().ToString(),
                configuracaoCenarioId);

            // Ação
            calcularUsinaRequest.Validar();
            var result = calcularUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando consolidação mensal for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConsolidacaoMensalIdNuloOuVazio(string consolidacaoMensalId)
        {
            // Organização
            var calcularUsinaRequest = new CalcularUsinaRequest(
                new ConsolidacaoMensalRequest(consolidacaoMensalId, DateTime.Now),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString());

            // Ação
            calcularUsinaRequest.Validar();
            var result = calcularUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }
    }
}
