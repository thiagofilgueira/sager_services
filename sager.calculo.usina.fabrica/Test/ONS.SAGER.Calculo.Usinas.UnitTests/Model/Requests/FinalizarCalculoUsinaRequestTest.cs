using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Usinas.UnitTests.Model.Requests 
{ 
    public class FinalizarCalculoUsinaRequestTest
    {
        [Fact(DisplayName = "Teste para verificar se retorna falso quando a requisi��o for nula")]
        public void ResultadoDeveSerFalsoQuandoRequestForNulo()
        {
            // Organiza��o
            FinalizarCalculoUsinaRequest finalizarCalculoUsinaRequest = null;

            // Assert
            Assert.Null(finalizarCalculoUsinaRequest);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando configura��o do cen�rio for vazio ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarCalculoUsinaRequest(
                configuracaoCenarioId,
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // A��o
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando controle de c�lculo for vazio ou nulo")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarControleCalculoIdNuloOuVazio(string controleCalculoId)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarCalculoUsinaRequest(
                Guid.NewGuid().ToString(),
                controleCalculoId,
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // A��o
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando usina for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirUsina(string usinaId)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarCalculoUsinaRequest(
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                usinaId,
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now));

            // A��o
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna falso quando consolida��o mensal for vazia ou nula")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoNaoExistirConsolidacaoMensal(string consolidacaoMensalId)
        {
            // Organiza��o
            var finalizarCalculoUsinaRequest = new FinalizarCalculoUsinaRequest(
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(consolidacaoMensalId, DateTime.Now));

            // A��o
            var result = finalizarCalculoUsinaRequest.Valido;

            // Assert
            Assert.False(result);
        }
    }
}
