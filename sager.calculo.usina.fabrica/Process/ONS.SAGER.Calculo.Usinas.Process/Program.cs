using ONS.SAGER.Calculo.Usinas.Process;
using ONS.SDK.Impl.Builder;

namespace ONS.SAGER.Calculo.Usina.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            AppBuilder.CreateDefaultBuilder(null)
                .UseStartup<Startup>()
                .RunSDK();
        }
    }
}
