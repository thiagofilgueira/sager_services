using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ONS.SAGER.Calculo.Usinas.Application.Extensions;
using ONS.SAGER.Calculo.Usinas.DataAccess.Extensions;
using ONS.SAGER.Calculo.Usinas.Domain.Extensions;
using ONS.SAGER.Calculo.Usinas.Domain.Map;
using ONS.SAGER.Calculo.Usinas.Entrypoint.Extensions;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SDK.Builder;
using ONS.SDK.Extensions.DependencyInjection;
using Polly;
using System;

namespace ONS.SAGER.Calculo.Usinas.Process
{
    public class Startup : IStartup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.UseSDK();
            services.AddAppSettings(Configuration);
            services.AddApplicationServices();
            services.AddDomainServices();
            services.AddRepositoryServices();
            services.UseDataMap<EntitiesMap>();
            services.AddEntrypointServices();
            services.AddCalculoLogger();
            services.AddExecutionContextAbstraction();

            //var retryPolicy = Policy
            //   .HandleResult<Result>(x => x.Status == ResultStatus.BadRequest)
            //   .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(5),
            //       (resultadoOperacao, tempoDeEspera, tentativa, contexto) =>
            //       {
            //           resultado.AddNotification(string.Format("O processamento n�o foi conclu�do. - Erro: {0} - N�mero da Repeti��o: {1}", resultadoOperacao.Result.Notifications.FirstOrDefault(), tentativa));
            //       });

            //var builder = new HostBuilder()
            //    .ConfigureServices((hostContext, services) =>
            //    {
            //        services.AddHttpClient();
            //        services.AddTransient<MyApplication>();
            //    }).UseConsoleLifetime();
        }

        public void Configure(IAppBuilder app)
        {
        }
    }
}
