﻿using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public class FinalizarTaxaAcumuladaApplicationService : FinalizarTaxaApplicationService<FinalizarTaxaAcumuladaApplicationService>, IFinalizarTaxaAcumuladaApplicationService
    {
        private readonly ITaxaService _taxaService;

        public FinalizarTaxaAcumuladaApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
            IInsumoCalculoService insumoCalculoService,
            IExecutionContextAbstraction context,
            ITaxaService taxaService,
            ICalculoLogger<FinalizarTaxaAcumuladaApplicationService> logger)
            : base(usinaService, controleCalculoService, insumoCalculoService, context, logger)

        { 
            _taxaService = taxaService;
        }

        public Task<Result> Finalizar(FinalizarTaxaRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await FinalizarCalculoTaxa(request));
        }

        private async Task<Result> FinalizarCalculoTaxa(FinalizarTaxaRequest request)
        {
            try
            {
                if (!ValidarRequest(request))
                {
                    return new Result(ResultStatus.BadRequest, request?.Notificacoes.Select(n => n.Mensagem));
                }
               
                if (request.TipoCalculo != CalculoConstants.TaxaAcumuladaMandante)
                {
                    return new Result(ResultStatus.Success, "O container foi eliminado pois já existe uma instância gerenciando o calculo das taxas.");
                }

                Context.SetBranch(request.ConfiguracaoCenarioId);

                var todasTaxas = CalculoConstants.TaxasAcumuladasCalculadas(request.ConsolidacaoMensal.DataReferencia);

                // Realiza as tentativas para verificação da conclusão do cálculo
                ResilienceResponse resultadoOperacao = await VerificarConclusaoCalculoTaxas(request, todasTaxas);

                if (resultadoOperacao.Status == ResultStatus.BadRequest)
                {
                    return await TratarTimeOutOperacao(request);
                }

                var sucessoCalculo = await ControleCalculoService.VerificarConclusaoCalculoTaxasComSucesso(
                    request.ControleCalculoId,
                    todasTaxas,
                    request.ConfiguracaoCenarioId);

                if (!sucessoCalculo)
                {
                    return await TratarCalculoTaxaComErro(request);
                }

                if (_taxaService.VerificarNecessidadeCalculoIndiceIndisponibilidade(request))
                {
                    await Context.EmitirEvento(Eventos.CalcularTaxaIndice,
                        request.MapToCalcularTaxaPayload(), 
                        request.ConsolidacaoMensal.DataReferencia);

                    return new Result(ResultStatus.Success, string.Format("O cálculo de Taxa Acumulada foi concluido. O cálculo do índice de indiponibilidade referente a Usina '{0}' foi iniciado com sucesso.", request.Usina.UsinaId));
                }

                return await FinalizarUsinaComSucesso(request);
            }
            catch (InvalidOperationException eio)
            {
                await FinalizarUsinaComErro(request);

                return new Result(ResultStatus.BadRequest, eio.Message);
            }
            catch (Exception e)
            {
                await FinalizarUsinaComErro(request);

                return new Result(ResultStatus.InternalError, e.Message);
            }
        }

    }
}
