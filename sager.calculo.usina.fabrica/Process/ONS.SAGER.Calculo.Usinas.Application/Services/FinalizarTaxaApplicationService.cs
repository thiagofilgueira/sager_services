﻿using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public abstract class FinalizarTaxaApplicationService<T>: ApplicationServiceBase<T> where T : class
    {
        protected readonly IUsinaService UsinaService;
        protected readonly IControleCalculoService ControleCalculoService;
        protected readonly IInsumoCalculoService InsumoCalculoService;
        protected readonly IExecutionContextAbstraction Context;

        public FinalizarTaxaApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
            IInsumoCalculoService insumoCalculoService,
            IExecutionContextAbstraction context,
            ICalculoLogger<T> logger) : base(logger)
        {
            UsinaService = usinaService;
            ControleCalculoService = controleCalculoService;
            InsumoCalculoService = insumoCalculoService;
            Context = context;
        }

        protected bool ValidarRequest(FinalizarTaxaRequest request)
        {

            if (request is null)
            {
                return false;
            }

            request.Validar();

            return request.Valido;
        }

        protected async Task<ResilienceResponse> VerificarConclusaoCalculoTaxas(FinalizarTaxaRequest request, TipoCalculo[] taxasAnalisadas)
        {
            return await Resilience.AguardarConclusaoDaFuncao(
                    20,
                    TimeSpan.FromSeconds(8),
                    async () => await ControleCalculoService.VerificarConclusaoCalculoTaxas(
                        request.ControleCalculoId,
                        taxasAnalisadas,
                        request.ConfiguracaoCenarioId));
        }

        protected async Task<Result> TratarTimeOutOperacao(FinalizarTaxaRequest request)
        {
            string msgBadRequest = string.Format("O tempo para a finalização do cálculo das Taxas referentes a Usina '{0}' superou o tempo de tolerância, os cálculos foram estornados.", request.Usina.UsinaId);

            await FinalizarUsinaComErro(request);

            return new Result(ResultStatus.BadRequest, msgBadRequest);
        }

        protected async Task<Result> FinalizarUsinaComSucesso(FinalizarTaxaRequest request)
        {
            string mensagem = string.Format("O cálculo referente a Usina '{0}' foi finalizado com sucesso.", request.Usina.UsinaId);

            await ControleCalculoService.FinalizarControleCalculo(request.ControleCalculoId, StatusCalculo.Sucesso);

            //await Resilience.AguardarConclusaoDaFuncao(
            //    5,
            //    TimeSpan.FromSeconds(15),
            //    async () => await InsumoCalculoService.InserirInsumosCalculosBaseConsulta(request));

            return new Result(ResultStatus.Success, mensagem);
        }

        protected async Task<Result> TratarCalculoTaxaComErro(FinalizarTaxaRequest request)
        {
            string msgErroCalculo = string.Format("O cálculo das taxas da Usina '{0}' foi finalizado com erro, os cálculos foram estornados.", request.Usina.UsinaId);

            await FinalizarUsinaComErro(request);

            return new Result(ResultStatus.BadRequest, msgErroCalculo);
        }

        protected async Task FinalizarUsinaComErro(FinalizarTaxaRequest request)
        {
            await UsinaService.RealizarEstornoCalculosUsina(request.ControleCalculoId, request.ConfiguracaoCenarioId);
            await ControleCalculoService.FinalizarControleCalculo(request.ControleCalculoId, StatusCalculo.Erro);
        }
    }
}
