using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public class FinalizarIndiceApplicationService : FinalizarTaxaApplicationService<FinalizarIndiceApplicationService>, IFinalizarIndiceApplicationService
    {
        public FinalizarIndiceApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
             IInsumoCalculoService insumoCalculoService,
            IExecutionContextAbstraction context,
            ICalculoLogger<FinalizarIndiceApplicationService> logger)
            : base(usinaService, controleCalculoService, insumoCalculoService, context, logger)
        {
        }

        public Task<Result> Finalizar(FinalizarTaxaRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await FinalizarCalculoIndice(request));
        }

        private async Task<Result> FinalizarCalculoIndice(FinalizarTaxaRequest request)
        {
            try
            {
                if (!ValidarRequest(request))
                {
                    return new Result(ResultStatus.BadRequest, request?.Notificacoes.Select(n => n.Mensagem));
                }

                Context.SetBranch(request.ConfiguracaoCenarioId);

                var sucessoCalculo = await ControleCalculoService.VerificarConclusaoCalculoTaxaComSucesso(
                    request.ControleCalculoId,
                    TipoCalculo.IndiceIndisponibilidadeVerificada,
                    request.ConfiguracaoCenarioId);

                if (!sucessoCalculo)
                {
                    return await TratarCalculoTaxaComErro(request);
                }

                return await FinalizarUsinaComSucesso(request);

            }
            catch (InvalidOperationException eio)
            {
                await FinalizarUsinaComErro(request);
                return new Result(ResultStatus.BadRequest, eio.Message);
            }
            catch (Exception e)
            {
                await FinalizarUsinaComErro(request);
                return new Result(ResultStatus.InternalError, e.Message);
            }
        }
    }
}
