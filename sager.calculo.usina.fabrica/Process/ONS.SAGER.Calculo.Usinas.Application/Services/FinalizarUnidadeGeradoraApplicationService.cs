﻿using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Logger;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public class FinalizarUnidadeGeradoraApplicationService : ApplicationServiceBase<FinalizarUnidadeGeradoraApplicationService>, IFinalizarUnidadeGeradoraApplicationService
    {
        private readonly IUsinaService _usinaService;
        private readonly IExecutionContextAbstraction _context;
        private readonly ITaxaService _taxaService;
        private readonly IControleCalculoService _controleCalculoService;

        public FinalizarUnidadeGeradoraApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
            ITaxaService taxaService,
            IExecutionContextAbstraction context,
            ICalculoLogger<FinalizarUnidadeGeradoraApplicationService> logger) 
            : base(logger)
        {
            _usinaService = usinaService;
            _taxaService = taxaService;
            _controleCalculoService = controleCalculoService;
            _context = context;
        }


        public Task<Result> Finalizar(FinalizarUnidadeGeradoraRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await FinalizarUnidadeGeradora(request));
        }

        private async Task<Result> FinalizarUnidadeGeradora(FinalizarUnidadeGeradoraRequest request)
        {
            try
            {
                if (request is null)
                {
                    return new Result(ResultStatus.BadRequest, $"{nameof(FinalizarUnidadeGeradoraRequest)} não pode ser nulo");
                }

                request.Validar();

                if (request.Invalido)
                {
                    return new Result(ResultStatus.BadRequest, request.Notificacoes.Select(n => n.Mensagem));
                }

                _context.SetBranch(request.ConfiguracaoCenarioId);

                var unidadeGeradoraMandante = await _controleCalculoService.VerificarSeControleCalculoUnidadeGeradoraMandante(
                    request.ControleCalculoId,
                    request.UnidadeGeradoraId,
                    request.ConfiguracaoCenarioId);

                if (!unidadeGeradoraMandante)
                {
                    return new Result(ResultStatus.Success, string.Format("O container referente a Usina {0} e Unidade {1} foi eliminado pois já existe uma instância gerenciando o calculo das unidades geradoras.", request.UsinaId, request.UnidadeGeradoraId));
                }

                // Realiza as tentativas para verificação da conclusão do cálculo
                var resultadoOperacao = await Resilience.AguardarConclusaoDaFuncao(
                    10,
                    TimeSpan.FromSeconds(20),
                    async () => await _controleCalculoService.VerificarConclusaoCalculosUnidadesGeradoras(
                        request.ControleCalculoId,
                        request.ConfiguracaoCenarioId));

                if (resultadoOperacao.Status == ResultStatus.BadRequest)
                {
                    await FinalizarComErro(request);

                    return new Result(ResultStatus.BadRequest, string.Format("O tempo para a finalização do cálculo das Unidades Geradoras referentes a Usina '{0}' superou o tempo de tolerância, os cálculos foram estornados.", request.UsinaId));
                }

                var calculosComSucesso = await _controleCalculoService.VerificarConclusaoCalculosUnidadesGeradorasComSucesso(
                    request.ControleCalculoId,
                    request.ConfiguracaoCenarioId);

                if (!calculosComSucesso)
                {
                    await FinalizarComErro(request);
                    return new Result(ResultStatus.BadRequest, string.Format("O cálculo da Usina '{0}' foi finalizado com erro.", request.UsinaId));
                }

                return await IniciarCalculosTaxas(request);

            }
            catch (InvalidOperationException eio)
            {
                await FinalizarComErro(request);
                return new Result(ResultStatus.BadRequest, eio.Message);
            }
            catch (Exception e)
            {
                await FinalizarComErro(request);
                return new Result(ResultStatus.InternalError, e.Message);
            }
        }

        private async Task FinalizarComErro(FinalizarUnidadeGeradoraRequest request)
        {         
            await _usinaService.RealizarEstornoCalculosUsina(request.ControleCalculoId, request.ConfiguracaoCenarioId);
            await _controleCalculoService.FinalizarControleCalculo(request.ControleCalculoId, StatusCalculo.Erro);
        }

        private async Task<Result> IniciarCalculosTaxas(FinalizarUnidadeGeradoraRequest request)
        {
            var usina = await _usinaService.ObterUsinaPorId(request.UsinaId, request.ConfiguracaoCenarioId);

            var deveCalcularTaxaMensal = _taxaService.VerificarNecessidadeCalculoTaxaMensal(request.ConsolidacaoMensal.DataReferencia);

            string eventoCalculo = deveCalcularTaxaMensal ?  Eventos.CalcularTaxaMes : Eventos.CalcularTaxaAcum;

            await _context.EmitirEvento(
                eventoCalculo,
                new CalcularTaxaPayload()
                {
                    UsinaPayload = usina.MapToPayload(),
                    ControleCalculoIdPayload = request.ControleCalculoId,
                    ConfiguracaoCenarioIdPayload = request.ConfiguracaoCenarioId,
                    ConsolidacaoMensalPayload = request.ConsolidacaoMensal.MapToPayload(),
                },
                request.ConsolidacaoMensal.DataReferencia);

            return new Result(ResultStatus.Success, string.Format("O cálculo das Taxas da Usina '{0}' foi iniciado com sucesso.", request.UsinaId));
        }
    }
}
