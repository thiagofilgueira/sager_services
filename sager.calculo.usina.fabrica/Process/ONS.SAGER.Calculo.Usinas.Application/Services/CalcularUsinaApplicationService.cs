﻿using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public class CalcularUsinaApplicationService : ApplicationServiceBase<CalcularUsinaApplicationService>, ICalcularUsinaApplicationService
    {
        private readonly IUsinaService _usinaService;
        private readonly IExecutionContextAbstraction _context;
        private readonly IControleCalculoService _controleCalculoService;

        public CalcularUsinaApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
            IExecutionContextAbstraction context,
            ICalculoLogger<CalcularUsinaApplicationService> logger) 
            : base(logger)
        {
            _usinaService = usinaService;
            _context = context;
            _controleCalculoService = controleCalculoService;
        }

        public Task<Result> Calcular(CalcularUsinaRequest request)
        {
            return ExecutarELogar(
                request?.UsinaId,
                async () => await CalcularUsina(request));
        }

        private async Task<Result> CalcularUsina(CalcularUsinaRequest request)
        {
            var controleCalculoId = Guid.NewGuid().ToString();

            try
            {
                if (request is null)
                {
                    return new Result(ResultStatus.BadRequest, $"{typeof(CalcularUsinaRequest).Name} não pode ser nulo");
                }

                request.Validar();

                if (request.Invalido)
                {
                    return new Result(ResultStatus.BadRequest, request.Notificacoes.Select(n => n.Mensagem));
                }

                _context.SetBranch(request.ConfiguracaoCenarioId);

                var usina = await _usinaService.ObterUsinaValidaParaCalculo(
                    request.UsinaId,
                    request.ConsolidacaoMensal.DataReferencia,
                    request.ConfiguracaoCenarioId);

                if (usina is null)
                {
                    return await TratarUsinaInvalidaParaCalculo(request, controleCalculoId, StatusCalculo.Erro);
                }

                var unidadesGeradoras = await _usinaService.ObterUnidadesGeradorasAtivasPorUsina(
                    request.UsinaId, 
                    request.ConsolidacaoMensal.DataReferencia,
                    request.ConfiguracaoCenarioId);

                if (unidadesGeradoras.IsNullOrEmpty())
                {
                    return await TratarUsinaInvalidaParaCalculo(request, controleCalculoId, StatusCalculo.Inapto);
                }


                await IniciarCalculoUsina(request, controleCalculoId, usina, unidadesGeradoras);

                return new Result(ResultStatus.Success, string.Format("O cálculo da Usina '{0}' foi iniciado com sucesso.", request.UsinaId));
            }
            catch (Exception e)
            {
                await _controleCalculoService.SalvarProcessamentoControleCalculo(
                    request.UsinaId,
                    controleCalculoId,
                    request.ConsolidacaoMensal.ConsolidacaoMensalId,
                    request.ConfiguracaoCenarioId,
                    StatusCalculo.Erro,
                    DateTime.Now);

                return new Result(ResultStatus.InternalError, e.Message);
            }
        }

        private async Task IniciarCalculoUsina(
            CalcularUsinaRequest request,
            string controleCalculoId,
            Usina usina,
            IEnumerable<UnidadeGeradora> unidadesGeradoras)
        {
            await _controleCalculoService.SalvarProcessamentoControleCalculo(
                request.UsinaId,
                controleCalculoId,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.ConfiguracaoCenarioId,
                StatusCalculo.Processando);
 
            await unidadesGeradoras.Parallel(async unidadeGeradora =>
            {
                await _context.EmitirEvento(
                    Eventos.CalcularUnidadeGeradora,
                    new CalcularUnidadeGeradoraPayload()
                    {
                        UsinaPayload = usina.MapToPayload(),
                        ControleCalculoIdPayload = controleCalculoId,
                        ConfiguracaoCenarioIdPayload = request.ConfiguracaoCenarioId,
                        ConsolidacaoMensalPayload = request.ConsolidacaoMensal.MapToPayload(),
                        UnidadeGeradoraId = unidadeGeradora.UnidadeGeradoraId
                    },
                    request.ConsolidacaoMensal.DataReferencia);
            });
        }

        private async Task<Result> TratarUsinaInvalidaParaCalculo(
            CalcularUsinaRequest request,
            string controleCalculoId,
            StatusCalculo statusCalculo)
        {
            await _controleCalculoService.SalvarProcessamentoControleCalculo(
                request.UsinaId,
                controleCalculoId,
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.ConfiguracaoCenarioId,
                statusCalculo,
                DateTime.Now);

            return new Result(ResultStatus.BadRequest, $"Usina {request.UsinaId} inválida para cálculo");
        }
    }
}
