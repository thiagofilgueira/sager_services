﻿using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Services
{
    public class FinalizarTaxaMensalApplicationService : FinalizarTaxaApplicationService<FinalizarTaxaMensalApplicationService>, IFinalizarTaxaMensalApplicationService
    {

        public FinalizarTaxaMensalApplicationService(
            IUsinaService usinaService,
            IControleCalculoService controleCalculoService,
            IInsumoCalculoService insumoCalculoService,
            IExecutionContextAbstraction context,
            ICalculoLogger<FinalizarTaxaMensalApplicationService> logger)
            : base(usinaService, controleCalculoService, insumoCalculoService, context, logger)
        {
        }

        public Task<Result> Finalizar(FinalizarTaxaRequest request)
        {
            return ExecutarELogar(
                request?.ControleCalculoId,
                async () => await FinalizarCalculoTaxa(request));
        }

        private async Task<Result> FinalizarCalculoTaxa(FinalizarTaxaRequest request)
        {
            try
            {
                if (!ValidarRequest(request))
                {
                    return new Result(ResultStatus.BadRequest, request?.Notificacoes.Select(n => n.Mensagem));
                }

                if (request.TipoCalculo != CalculoConstants.TaxaMensalMandante)
                {
                    return new Result(ResultStatus.Success, "O container foi eliminado pois já existe uma instância gerenciando o calculo das taxas.");
                }

                Context.SetBranch(request.ConfiguracaoCenarioId);

                var todasTaxas = CalculoConstants.TaxasMensaisCalculadas();

                // Realiza as tentativas para verificação da conclusão do cálculo
                var resultadoOperacao = await VerificarConclusaoCalculoTaxas(request, todasTaxas);

                if (resultadoOperacao.Status == ResultStatus.BadRequest)
                {
                    return await TratarTimeOutOperacao(request);
                }

                var sucessoCalculo = await ControleCalculoService.VerificarConclusaoCalculoTaxasComSucesso(
                    request.ControleCalculoId,
                    todasTaxas,
                    request.ConfiguracaoCenarioId);

                if (!sucessoCalculo)
                {
                    return await TratarCalculoTaxaComErro(request);
                }

                if (!Context.IsReprocessing())
                {
                    await Context.EmitirEvento(
                        Eventos.CalcularTaxaAcum,
                        request.MapToCalcularTaxaPayload(),
                        request.ConsolidacaoMensal.DataReferencia);

                    return new Result(ResultStatus.Success, string.Format("O cálculo de Taxa Mensal foi concluido. O cálculo de Taxa Acumulada referente a Usina '{0}' foi iniciado com sucesso.", request.Usina.UsinaId));
                }

                return new Result(ResultStatus.Success, string.Format("O cálculo de Taxa Mensal foi concluido. O cálculo de Taxa Acumulada referente a Usina '{0}' foi iniciado com sucesso.", request.Usina.UsinaId));

            }
            catch (InvalidOperationException eio)
            {
                await FinalizarUsinaComErro(request);

                return new Result(ResultStatus.BadRequest, eio.Message);
            }
            catch (Exception e)
            {
                await FinalizarUsinaComErro(request);

                return new Result(ResultStatus.InternalError, e.Message);
            }
        }
    }
}
