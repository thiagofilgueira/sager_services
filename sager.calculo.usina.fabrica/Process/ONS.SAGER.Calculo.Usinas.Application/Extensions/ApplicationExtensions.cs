using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Application.Services;

namespace ONS.SAGER.Calculo.Usinas.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {           
            services.AddSingleton<ICalcularUsinaApplicationService, CalcularUsinaApplicationService>();
            services.AddSingleton<IFinalizarUnidadeGeradoraApplicationService, FinalizarUnidadeGeradoraApplicationService>();
            services.AddSingleton<IFinalizarTaxaMensalApplicationService, FinalizarTaxaMensalApplicationService>();
            services.AddSingleton<IFinalizarTaxaAcumuladaApplicationService, FinalizarTaxaAcumuladaApplicationService>();
            services.AddSingleton<IFinalizarIndiceApplicationService, FinalizarIndiceApplicationService>();
        }
    }
}
