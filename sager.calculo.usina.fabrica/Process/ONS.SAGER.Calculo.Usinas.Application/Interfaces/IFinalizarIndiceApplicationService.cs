using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Application.Interfaces
{
    public interface IFinalizarIndiceApplicationService 
    {
        Task<Result> Finalizar(FinalizarTaxaRequest request);
    }
}
