using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SDK.Impl.Data;

namespace ONS.SAGER.Calculo.Usinas.Domain.Map
{
    public class EntitiesMap : AbstractDataMapCollection
    {
        protected override void Load()
        {
            BindMap<Usina>();
            BindMap<ControleCalculo>();
            BindMap<ControleCalculoUnidadeGeradora>();
            BindMap<ControleCalculoTaxa>();
            BindMap<ParametroTaxa>();
            BindMap<CalculoTaxa>();
            BindMap<UnidadeGeradora>();
        }
    }
}
