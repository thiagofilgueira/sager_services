﻿using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.DataAccess.Repositories;

namespace ONS.SAGER.Calculo.Usinas.DataAccess.Repositories
{
    public class ControleCalculoRepository : ControleCalculoRepositoryBase, IControleCalculoRepository
    {
    }
}
