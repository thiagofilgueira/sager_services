using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Usinas.DataAccess.Repositories;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Usinas.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IUsinaRepository, UsinaRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
            services.AddSingleton<IControleCalculoRepository, ControleCalculoRepository>();
            services.AddSingleton<IControleCalculoUnidadeGeradoraRepository, ControleCalculoUnidadeGeradoraRepository>();
            services.AddSingleton<IControleCalculoTaxaRepository, ControleCalculoTaxaRepository>();
            services.AddSingleton<ITaxaRepository, TaxaRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
        }
    }
}
