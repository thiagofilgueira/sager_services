using Newtonsoft.Json;
using ONS.SAGER.Calculo.Usinas.Application.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Context;
using ONS.SDK.Data.Query;
using ONS.SDK.Services.ProcessMemory;
using ONS.SDK.Worker;
using System;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ICalcularUsinaApplicationService _calcularUsinaApplicationService;
        private readonly IFinalizarUnidadeGeradoraApplicationService _finalizarUnidadeGeradoraApplicationService;
        private readonly IFinalizarTaxaMensalApplicationService _finalizarTaxaMensalApplicationService;
        private readonly IFinalizarTaxaAcumuladaApplicationService _finalizarTaxaAcumuladaApplicationService;
        private readonly IFinalizarIndiceApplicationService _finalizarIndiceApplicationService;
        private readonly IProcessMemoryService _pm;
        private readonly IDataQuery _query;

        public CalculosEntrypoint(
            ICalcularUsinaApplicationService calcularUsinaApplicationService,
            IFinalizarUnidadeGeradoraApplicationService finalizarUnidadeGeradoraApplicationService,
            IFinalizarTaxaMensalApplicationService finalizarTaxaMensalApplicationService,
            IFinalizarTaxaAcumuladaApplicationService finalizarTaxaAcumuladaApplicationService,
            IFinalizarIndiceApplicationService finalizarIndiceApplicationService,
            IDataQuery query,
            IProcessMemoryService pm)
        {
            _finalizarIndiceApplicationService = finalizarIndiceApplicationService;
            _calcularUsinaApplicationService = calcularUsinaApplicationService;
            _finalizarUnidadeGeradoraApplicationService = finalizarUnidadeGeradoraApplicationService;
            _finalizarTaxaMensalApplicationService = finalizarTaxaMensalApplicationService;
            _query = query;
            _finalizarTaxaAcumuladaApplicationService = finalizarTaxaAcumuladaApplicationService;
            _pm = pm;
        }

        [SDKEvent(Eventos.CalcularUsina)]
        public async Task CalcularUsina(CalcularUsinaPayload payload)
        {
            //var result = _query.Set<Usina>().FindReproductionData("cf8e5cfe-a50f-4499-b688-cffe8e8b35e5", "byUsinaId", new { UsinaId = "AMAA" });
            await _calcularUsinaApplicationService.Calcular(
             new CalcularUsinaRequest(
                 payload?.ConsolidacaoMensalPayload?.MapToRequest(),
                 payload?.UsinaIdPayload,
                 payload?.ConfiguracaoCenarioIdPayload));
            //try
            //{
            //    var inputsUsina = _pm.GetInstanceInputs<Usina>("2c5a588c-f146-433b-aabe-573cf201c135");
            //    Console.WriteLine("Inputs de usina abaixo");
            //    Console.WriteLine(JsonConvert.SerializeObject(inputsUsina));

            //    var todosInputs = _pm.GetInstanceInputs("2c5a588c-f146-433b-aabe-573cf201c135");
            //    Console.WriteLine("Todos os inputs abaixo");
            //    Console.WriteLine(JsonConvert.SerializeObject(todosInputs));

            //    var uges = _pm.GetInstanceInputs("2c5a588c-f146-433b-aabe-573cf201c135");
            //    Console.WriteLine("Inputs de uges abaixo");
            //    var ugesD = _pm.DeserializeInputs<UnidadeGeradora>(todosInputs);
            //    Console.WriteLine(JsonConvert.SerializeObject(ugesD));
            //}
            //catch (Exception e) { Console.WriteLine(e); }
        }

        [SDKEvent("calcular.usina.error")]
        public async Task ErroCatch(ErrorPayload teste)
        {
            var result = teste.Deserialize<CalcularUsinaPayload>();
            Console.Out.WriteLine("Tratar erro", teste);
        }

        [SDKEvent(Eventos.FinalizarCalculoUge)]
        public async Task FinalizarUnidadeGeradora(FinalizarUnidadeGeradoraPayload payload)
        {
            await _finalizarUnidadeGeradoraApplicationService.Finalizar(
                new FinalizarUnidadeGeradoraRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ControleCalculoIdPayload,
                    payload?.UnidadeGeradoraIdPayload,
                    payload?.UsinaIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }

        [SDKEvent(Eventos.FinalizarCalculoTaxaMes)]
        public async Task FinalizarCalculoTaxaMes(FinalizarTaxaPayload payload)
        {
            await _finalizarTaxaMensalApplicationService.Finalizar(
                new FinalizarTaxaRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ControleCalculoIdPayload,
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.TipoCalculoPayload ?? default,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }

        [SDKEvent(Eventos.FinalizarCalculoTaxaAcum)]
        public async Task FinalizarCalculoTaxaAcum(FinalizarTaxaPayload payload)
        {
            await _finalizarTaxaAcumuladaApplicationService.Finalizar(
                new FinalizarTaxaRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ControleCalculoIdPayload,
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.TipoCalculoPayload ?? default,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }

        [SDKEvent(Eventos.FinalizarCalculoIndice)]
        public async Task FinalizarCalculoIndice(FinalizarTaxaPayload payload)
        {
            await _finalizarIndiceApplicationService.Finalizar(
                new FinalizarTaxaRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ControleCalculoIdPayload,
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.TipoCalculoPayload ?? default,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));
        }
    }
}
