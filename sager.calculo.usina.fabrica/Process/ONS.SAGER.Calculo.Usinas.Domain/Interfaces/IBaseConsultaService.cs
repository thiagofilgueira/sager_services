﻿using ONS.SAGER.Calculo.Util.Domain.Requests;
using System.Net;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface IBaseConsultaService
    {
        Task InserirDadosNaBaseDeConsultaAsync(InserirCalculosUsinaRequest dados);
    }
}
