﻿using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface IControleCalculoService
    {
        Task SalvarProcessamentoControleCalculo(string usinaId, string controleCalculoId, string consolidacaoMensalId, string configuracaoCenarioId, StatusCalculo statusCalculo,  DateTime? dataFim = null);
        Task FinalizarControleCalculo(string controleCalculoId, StatusCalculo statusCalculo);
        Task<bool> VerificarSeControleCalculoUnidadeGeradoraMandante(string controleCalculoId, string unidadeGeradoraId, string configuracaoCenarioId);
        Task<ResilienceResponse> VerificarConclusaoCalculosUnidadesGeradoras(string controleCalculoId, string configuracaoCenarioId);
        Task<bool> VerificarConclusaoCalculosUnidadesGeradorasComSucesso(string controleCalculoId, string configuracaoCenarioId);
        Task<ResilienceResponse> VerificarConclusaoCalculoTaxas(string controleCalculoId, IEnumerable<TipoCalculo> taxasVerificadas, string configuracaoCenarioId);
        Task<bool> VerificarConclusaoCalculoTaxaComSucesso(string controleCalculoId, TipoCalculo taxaVerificada, string configuracaoCenarioId);
        Task<bool> VerificarConclusaoCalculoTaxasComSucesso(string controleCalculoId, IEnumerable<TipoCalculo> taxasVerificadas, string configuracaoCenarioId);
    }
}
