using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using System;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface ITaxaService
    {

        bool VerificarNecessidadeCalculoIndiceIndisponibilidade(FinalizarTaxaRequest request);
        bool VerificarNecessidadeCalculoTaxaMensal(DateTime dataReferencia);
        Task RealizarEstornoCalculosTaxas(string controleCalculoId, string configuracaoCenarioId);
    }
}
