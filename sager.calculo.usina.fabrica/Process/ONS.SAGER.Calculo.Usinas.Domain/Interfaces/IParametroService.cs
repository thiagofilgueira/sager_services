using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface IParametroService
    {
        Task RealizarEstornoCalculosParametros(string controleCalculoId, string configuracaoCenarioId);
    }
}
