using ONS.SAGER.Calculo.Util.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface IUsinaService
    {
        Task<IEnumerable<UnidadeGeradora>> ObterUnidadesGeradorasAtivasPorUsina(string usinaId, DateTime dataReferencia,  string configuracaoCenarioId);
        Task<Usina> ObterUsinaValidaParaCalculo(string usinaId, DateTime dataReferenciaCalculo, string configuracaoCenarioId);
        Task<Usina> ObterUsinaPorId(string usinaId, string configuracaoCenarioId);
        Task<IEnumerable<UnidadeGeradora>> ObterUnidadesGeradorasPorUsina(string usinaId, string configuracaoCenarioId);
        Task RealizarEstornoCalculosUsina(string controleCalculoId, string configuracaoCenarioId);
    }
}
