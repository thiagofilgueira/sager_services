﻿using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Interfaces
{
    public interface IInsumoCalculoService : IInsumoCalculoServiceBase
    {
        Task<ResilienceResponse> InserirInsumosCalculosBaseConsulta(FinalizarTaxaRequest request);
        Task<InserirCalculosUsinaRequest> ObterInsumosCalculoUsina(UsinaRequest usina, ConsolidacaoMensalRequest consolidacaoMensal, string controleCalculoId,string configuracaoCenarioId);
    }
}
