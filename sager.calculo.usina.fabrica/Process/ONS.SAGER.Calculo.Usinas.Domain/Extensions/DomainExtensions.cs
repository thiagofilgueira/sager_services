using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.Services;

namespace ONS.SAGER.Calculo.Usinas.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IUsinaService, UsinaService>();
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IParametroService, ParametroService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
            services.AddSingleton<IControleCalculoService, ControleCalculoService>();
            services.AddSingleton<IBaseConsultaService, BaseConsultaService>();
        }
    }
}
