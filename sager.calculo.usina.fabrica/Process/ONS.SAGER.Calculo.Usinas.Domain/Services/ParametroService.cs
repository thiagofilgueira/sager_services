using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class ParametroService : IParametroService
    {
        private readonly IParametroRepository _parametroRepository;
        
        public ParametroService(
            IParametroRepository parametroRepository)
        {
            _parametroRepository = parametroRepository;
        }

        public async Task RealizarEstornoCalculosParametros(
            string controleCalculoId, 
            string configuracaoCenarioId)
        {

            if (controleCalculoId.IsNull())
            {
                return;
            }

            var parametrosCalculados = await _parametroRepository.ObterParametrosPorControleCalculoId(
                controleCalculoId,
                configuracaoCenarioId);

            if (parametrosCalculados.IsNullOrEmpty())
            {
                return;
            }

            foreach (var parametro in parametrosCalculados)
            {
                await _parametroRepository.DeleteAsync(parametro);
            }
        }
    }
}
