﻿using ONS.SDK.Utils.Http;
using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using System.Threading.Tasks;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class BaseConsultaService : IBaseConsultaService
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public BaseConsultaService(
            HttpClient client,
            IConfiguration configuration)
        {
            _client = client;
            _configuration = configuration;
        }

        public async Task InserirDadosNaBaseDeConsultaAsync(InserirCalculosUsinaRequest dados)
        {
            var url = _configuration.GetValue("URL_BASECONSULTA", "");

            string json = JsonConvert.SerializeObject(dados);

            await _client.Post(url, json);
        }
    }
}
