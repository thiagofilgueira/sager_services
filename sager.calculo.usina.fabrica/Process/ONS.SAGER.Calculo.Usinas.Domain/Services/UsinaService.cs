using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class UsinaService : IUsinaService
    {
        private readonly IParametroService _parametroService;
        private readonly ITaxaService _taxaService;
        private readonly IUsinaRepository _usinaRepository;
        private readonly IUnidadeGeradoraRepository _unidadeGeradoraRepository;

        public UsinaService(
            IParametroService parametroService,
            ITaxaService taxaService,
            IUsinaRepository usinaRepository,
            IUnidadeGeradoraRepository unidadeGeradoraRepository)
        {
            _parametroService = parametroService;
            _taxaService = taxaService;
            _usinaRepository = usinaRepository;
            _unidadeGeradoraRepository = unidadeGeradoraRepository;
        }

        public async Task<Usina> ObterUsinaValidaParaCalculo(
            string usinaId, 
            DateTime dataReferenciaCalculo, 
            string configuracaoCenarioId)
        {
            if(string.IsNullOrWhiteSpace(usinaId))
            {
                return null;
            }

            var usina = await _usinaRepository.ObterUsinaPorId(usinaId, configuracaoCenarioId);

            if(usina.EmOperacao(dataReferenciaCalculo.Year, dataReferenciaCalculo.Month))
            {
                return usina;
            }

            return null;
        }

        public async Task<Usina> ObterUsinaPorId(string usinaId, string configuracaoCenarioId)
        {
            if (usinaId.IsNullOrWhiteSpace())
            {
                return null;
            }

            return await _usinaRepository.ObterUsinaPorId(usinaId, configuracaoCenarioId);
        }

        public async Task<IEnumerable<UnidadeGeradora>> ObterUnidadesGeradorasAtivasPorUsina(
            string usinaId, 
            DateTime dataReferencia,
            string configuracaoCenarioId)
        {
            if (usinaId.IsNullOrWhiteSpace())
            {
                return null;
            }

            var unidadesGeradoras = await _unidadeGeradoraRepository.ObterUnidadesGeradorasPorUsina(usinaId, configuracaoCenarioId);

            return unidadesGeradoras?.Where(u =>
                !u.VerificarDesativacaoDuranteMesInteiro(dataReferencia) && 
                !u.VerificarForaOperacaoComercialDuranteMesInteiro(dataReferencia)) 
                ?? new List<UnidadeGeradora>();
        }

        public async Task<IEnumerable<UnidadeGeradora>> ObterUnidadesGeradorasPorUsina(string usinaId, string configuracaoCenarioId)
        {
            if (usinaId.IsNullOrWhiteSpace())
            {
                return null;
            }

            return await _unidadeGeradoraRepository.ObterUnidadesGeradorasPorUsina(usinaId, configuracaoCenarioId);
        }

        public async Task RealizarEstornoCalculosUsina(string controleCalculoId, string configuracaoCenarioId)
        {
            if (controleCalculoId.IsNullOrWhiteSpace())
            {
                return;
            }

            await _taxaService.RealizarEstornoCalculosTaxas(controleCalculoId,configuracaoCenarioId);

            await _parametroService.RealizarEstornoCalculosParametros(controleCalculoId, configuracaoCenarioId);
        }
    }
}
