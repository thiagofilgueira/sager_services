﻿using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class ControleCalculoService : IControleCalculoService
    {
        private readonly IControleCalculoRepository _controleCalculoUsinaRepository;
        private readonly IControleCalculoUnidadeGeradoraRepository _controleCalculoUnidadeGeradoraRepository;
        private readonly IControleCalculoTaxaRepository _controleCalculoTaxaRepository;

        public ControleCalculoService(
            IControleCalculoRepository controleCalculoUsinaRepository,
            IControleCalculoUnidadeGeradoraRepository controleCalculoUnidadeGeradoraRepository,
            IControleCalculoTaxaRepository controleCalculoTaxaRepository)
        {
            _controleCalculoUsinaRepository = controleCalculoUsinaRepository;
            _controleCalculoUnidadeGeradoraRepository = controleCalculoUnidadeGeradoraRepository;
            _controleCalculoTaxaRepository = controleCalculoTaxaRepository;
        }

        public async Task SalvarProcessamentoControleCalculo(
            string usinaId,
            string controleCalculoId,
            string consolidacaoMensalId,
            string configuracaoCenarioId,
            StatusCalculo statusCalculo,
            DateTime? dataFim = null)
        {
            if (usinaId.IsNullOrWhiteSpace() || controleCalculoId.IsNullOrWhiteSpace())
            {
                return;
            }

            var controleCalculoUsina = await _controleCalculoUsinaRepository.ObterPorControleCalculo(controleCalculoId, configuracaoCenarioId);

            if (controleCalculoUsina is null)
            {
                await _controleCalculoUsinaRepository.AddAsync(new ControleCalculo(
                    controleCalculoId,
                    consolidacaoMensalId,
                    usinaId,
                    DateTime.Now,
                    dataFim,
                    statusCalculo));

                return;
            }

            await AlterarStatusControleCalculo(controleCalculoUsina, statusCalculo, dataFim);

        }
        public async Task FinalizarControleCalculo(string controleCalculoId, StatusCalculo statusCalculo)
        {
            var controleCalculoUsina = await _controleCalculoUsinaRepository.ObterPorControleCalculo(controleCalculoId);

            if (controleCalculoUsina.IsNull())
            {
                throw new InvalidOperationException("Falha ao finalizar o cálculo da usina. O controle de cálculo não foi encontrado.");
            }

            await AlterarStatusControleCalculo(
                controleCalculoUsina,
                statusCalculo,
                DateTime.Now);

        }

        private async Task AlterarStatusControleCalculo(
            ControleCalculo controleCalculoUsina,
            StatusCalculo statusCalculo,
            DateTime? dataFim = null)
        {

            controleCalculoUsina.DataFim = dataFim ?? controleCalculoUsina.DataFim;
            controleCalculoUsina.AlterarStatus(statusCalculo);

            await _controleCalculoUsinaRepository.UpdateAsync(controleCalculoUsina);
        }

        public async Task<bool> VerificarSeControleCalculoUnidadeGeradoraMandante(
            string controleCalculoId,
            string unidadeGeradoraId,
            string configuracaoCenarioId)
        {
            // Retorna as unidades geradoras associadas ao controle de cálculo
            var unidadesGeradorasCalculadas = await _controleCalculoUnidadeGeradoraRepository.ObterPorControleCalculo(
                controleCalculoId,
                configuracaoCenarioId);

            // Filtra as unidades geradoras que já foram processadas
            unidadesGeradorasCalculadas = unidadesGeradorasCalculadas?.Where(x => x.StatusCalculo != StatusCalculo.Processando);

            if (unidadesGeradorasCalculadas.IsNullOrEmpty())
            {
                return false;
            }

            var controleCalculoUnidadeGeradora = unidadesGeradorasCalculadas
                .Where(x => x.GetCreatedAt.HasValue)
                .OrderBy(x => x.GetCreatedAt)
                .ThenBy(x => x.UnidadeGeradoraId)
                .FirstOrDefault();

            return controleCalculoUnidadeGeradora.UnidadeGeradoraId == unidadeGeradoraId;
        }

        public async Task<ResilienceResponse> VerificarConclusaoCalculosUnidadesGeradoras(
            string controleCalculoId,
            string configuracaoCenarioId)
        {
            var unidadesGeradorasCalculadas = await _controleCalculoUnidadeGeradoraRepository.ObterPorControleCalculo(
                controleCalculoId,
                configuracaoCenarioId);

            // Caso não exista nenhum registro calculado
            if (unidadesGeradorasCalculadas.IsNullOrEmpty())
            {
                return new ResilienceResponse(ResultStatus.BadRequest, "O cálculo ainda não terminou.");
            }

            // Filtra as unidades geradoras pendentes de processamento
            if (unidadesGeradorasCalculadas.Any(x => x.StatusCalculo == StatusCalculo.Processando))
            {
                return new ResilienceResponse(ResultStatus.BadRequest, "O cálculo ainda não terminou.");
            }

            return new ResilienceResponse(ResultStatus.Success);
        }

        public async Task<bool> VerificarConclusaoCalculosUnidadesGeradorasComSucesso(string controleCalculoId, string configuracaoCenarioId)
        {
            // Retorna os parâmetros calculados por unidade geradora
            var unidadesGeradorasCalculadas = await _controleCalculoUnidadeGeradoraRepository.ObterPorControleCalculo(
                controleCalculoId,
                configuracaoCenarioId);

            var calculoComErro = unidadesGeradorasCalculadas.Any(x => x.StatusCalculo == StatusCalculo.Erro);

            return !calculoComErro;
        }


        public async Task<ResilienceResponse> VerificarConclusaoCalculoTaxas(
            string controleCalculoId,
            IEnumerable<TipoCalculo> taxasVerificadas,
            string configuracaoCenarioId)
        {

            var taxasCalculadas = await _controleCalculoTaxaRepository.ObterPorControleCalculo(
                controleCalculoId,
                configuracaoCenarioId);

            taxasCalculadas = taxasCalculadas?
                .Where(taxa =>
                {
                    return taxa.StatusCalculo != StatusCalculo.Processando && taxa.TipoCalculo.In(taxasVerificadas);
                });

            if (taxasCalculadas.IsNullOrEmpty())
            {
                return new ResilienceResponse(ResultStatus.BadRequest, "O cálculo das taxas ainda não terminou");
            }

            foreach (var tipoTaxa in taxasVerificadas)
            {
                if (tipoTaxa.NotIn(taxasCalculadas.Select(p => p.TipoCalculo)))
                {
                    return new ResilienceResponse(ResultStatus.BadRequest, "O cálculo das taxas ainda não terminou.");
                }
            }

            return new ResilienceResponse(ResultStatus.Success);
        }

        public async Task<bool> VerificarConclusaoCalculoTaxaComSucesso(
            string controleCalculoId,
            TipoCalculo taxaVerificada,
            string configuracaoCenarioId)
        {
            return await VerificarConclusaoCalculoTaxasComSucesso(controleCalculoId, new[] { taxaVerificada }, configuracaoCenarioId);
        }

        public async Task<bool> VerificarConclusaoCalculoTaxasComSucesso(
            string controleCalculoId,
            IEnumerable<TipoCalculo> taxasVerificadas,
            string configuracaoCenarioId)
        {
            var taxasCalculadas = await _controleCalculoTaxaRepository.ObterPorControleCalculo(
                controleCalculoId,
                configuracaoCenarioId);

            var listaTaxasProcessadasComErro = taxasCalculadas
                .Where(taxa =>
                {
                    return taxa.StatusCalculo == StatusCalculo.Erro && taxa.TipoCalculo.In(taxasVerificadas);
                });

            return listaTaxasProcessadasComErro.IsNullOrEmpty();
        }
    }
}
