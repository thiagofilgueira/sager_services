﻿using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Services;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Resilience.Policies;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class InsumoCalculoService : InsumoCalculoServiceBase, IInsumoCalculoService
    {
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;
        private readonly IBaseConsultaService _baseConsultaService;

        public InsumoCalculoService(
            IExecutionContextAbstraction executionContext, 
            IProcessMemoryServiceAbstraction processMemoryService,
            ITaxaRepository taxaRepository,
            IParametroRepository parametroRepository,
            IBaseConsultaService baseConsultaService)
            : base(executionContext, processMemoryService)
        {
            _taxaRepository = taxaRepository;
            _parametroRepository = parametroRepository;
            _baseConsultaService = baseConsultaService;
        }

        public async Task<ResilienceResponse> InserirInsumosCalculosBaseConsulta(FinalizarTaxaRequest request)
        {
            try
            {
                var insumosCalculo = await ObterInsumosCalculoUsina(
                    request.Usina,
                    request.ConsolidacaoMensal,
                    request.ControleCalculoId,
                    request.ConfiguracaoCenarioId);

                if (insumosCalculo is null)
                {
                    return new ResilienceResponse(ResultStatus.BadRequest, string.Format("Não foi encontrado insumos para o cálculo {0}", request.ControleCalculoId));
                }

                await _baseConsultaService.InserirDadosNaBaseDeConsultaAsync(insumosCalculo);

                return new ResilienceResponse(ResultStatus.Success);
            }
            catch
            {
                return new ResilienceResponse(ResultStatus.BadRequest, string.Format("Não foi possível realizar a comunicação com a base de consulta"));
            }

        }

        public async Task<InserirCalculosUsinaRequest> ObterInsumosCalculoUsina(
            UsinaRequest usina, 
            ConsolidacaoMensalRequest consolidacaoMensal,
            string controleCalculoId,
            string configuracaoCenarioId)
        {
            if (controleCalculoId.IsNullOrWhiteSpace())
            {
                return null;
            }

            var parametrosCalculados = _parametroRepository.ObterParametrosPorControleCalculoId(controleCalculoId, configuracaoCenarioId);
            var taxasCalculadas = _taxaRepository.ObterTaxasPorControleCalculoId(controleCalculoId, configuracaoCenarioId);

            await Task.WhenAll(parametrosCalculados, taxasCalculadas);

            if((parametrosCalculados.Result).IsNullOrEmpty() && (taxasCalculadas.Result).IsNullOrEmpty())
            {
                return null;
            }

            var parametrosRequest = ObterParametrosRequest(parametrosCalculados.Result);
            var taxasRequest = ObterTaxasRequest(taxasCalculadas.Result);

            await Task.WhenAll(parametrosRequest, taxasRequest);

            var unidadesGeradorasRequest = ObterUnidadesGeradorasRequest(parametrosRequest.Result, taxasRequest.Result);

            return new InserirCalculosUsinaRequest()
            {
                ConfiguracaoCenarioId = configuracaoCenarioId,
                ConsolidacaoMensal = consolidacaoMensal,
                Parametros = parametrosRequest.Result,
                Taxas = taxasRequest.Result,
                UnidadesGeradoras = unidadesGeradorasRequest,
                Usina = usina
            };

        }

        private List<UnidadeGeradoraRequest> ObterUnidadesGeradorasRequest(
            IEnumerable<ParametroTaxaRequest> parametrosRequest,
            IEnumerable<CalculoTaxaRequest> taxasRequest)
        {
            var unidadesGeradoras = new List<UnidadeGeradoraRequest>();

            foreach (var parametro in parametrosRequest)
            {
                var ugesParametro = 
                    parametro
                    .InsumosCalculo?
                    .UnidadesGeradoras?
                    .Where(p=> p.UnidadeGeradoraId.NotIn(unidadesGeradoras.Select(u => u.UnidadeGeradoraId)));

                if (!ugesParametro.IsNullOrEmpty())
                {
                    unidadesGeradoras.AddRange(ugesParametro);
                }
            }

            foreach (var taxa in taxasRequest)
            {
                var ugesTaxa =
                    taxa
                    .InsumosCalculo?
                    .UnidadesGeradoras?
                    .Where(p => p.UnidadeGeradoraId.NotIn(unidadesGeradoras.Select(u => u.UnidadeGeradoraId)));

                if (!ugesTaxa.IsNullOrEmpty())
                {
                    unidadesGeradoras.AddRange(ugesTaxa);
                }
            }

            return unidadesGeradoras;
        }

        private Task<List<ParametroTaxaRequest>> ObterParametrosRequest(IEnumerable<ParametroTaxa> parametrosCalculados)
        {
            var parametrosRequest = new List<ParametroTaxaRequest>();

            foreach (var parametro in parametrosCalculados)
            {
                var insumoParametro = ObterInsumosCalculo(parametro._Metadata.InstanceId);

                var parametroRequest = new ParametroTaxaRequest(parametro);
                parametroRequest.AddInsumosCalculo(insumoParametro);

                parametrosRequest.Add(parametroRequest);

            }

            return Task.FromResult(parametrosRequest);
        }

        private Task<List<CalculoTaxaRequest>> ObterTaxasRequest(IEnumerable<CalculoTaxa> taxasCalculadas)
        {
            var taxasRequest = new List<CalculoTaxaRequest>();

            foreach (var taxa in taxasCalculadas)
            {
                var insumoParametro = ObterInsumosCalculo(taxa._Metadata.InstanceId);

                var taxaRequest = new CalculoTaxaRequest(taxa);
                taxaRequest.AddInsumosCalculo(insumoParametro);

                taxasRequest.Add(taxaRequest);

            }

            return Task.FromResult(taxasRequest);
        }


    }
}
