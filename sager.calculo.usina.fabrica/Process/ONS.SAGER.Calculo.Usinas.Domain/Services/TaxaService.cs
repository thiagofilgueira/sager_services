using ONS.SAGER.Calculo.Usinas.Domain.Interfaces;
using ONS.SAGER.Calculo.Usinas.Domain.IRepositories;
using ONS.SAGER.Calculo.Usinas.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Configuration;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Usinas.Domain.Services
{
    public class TaxaService : ITaxaService
    {
        private readonly ITaxaRepository _taxaRepository;

        public TaxaService(ITaxaRepository taxaRepository)
        {
            _taxaRepository = taxaRepository;
        }

        public bool VerificarNecessidadeCalculoIndiceIndisponibilidade(FinalizarTaxaRequest request)
        {
            return 
                request.ConsolidacaoMensal.DataReferencia >= CalculoConstants.DataInicioSegundaVersaoCalculos &&
                request.Usina.DataRenovacaoConcessao.HasValue;
        }

        public bool VerificarNecessidadeCalculoTaxaMensal(DateTime dataReferencia)
        {
            return dataReferencia < CalculoConstants.DataInicioSegundaVersaoCalculos;
        }

        public async Task RealizarEstornoCalculosTaxas(string controleCalculoId, string configuracaoCenarioId)
        {
            var listaTaxasCalculadas = await _taxaRepository.ObterTaxasPorControleCalculoId(
                controleCalculoId, 
                configuracaoCenarioId);

            if (listaTaxasCalculadas.IsNullOrEmpty())
            {
                return;
            }

            foreach (var taxa in listaTaxasCalculadas)
            {
                await _taxaRepository.DeleteAsync(taxa);
            }
        }

    }
}
