using ONS.SAGER.Calculo.Util.Domain.Requests;

namespace ONS.SAGER.Calculo.Usinas.Domain.Models.Requests
{
    public class CalcularTaxaUsinaRequest : CalcularTaxaRequest
    {
        public CalcularTaxaUsinaRequest(
            ConsolidacaoMensalRequest consolidacaoMensal, 
            UsinaRequest usina, 
            string configuracaoCenarioId,
            string controleCalculoId)
            :base (usina, controleCalculoId,configuracaoCenarioId, consolidacaoMensal)
        {
        }
    }
}
