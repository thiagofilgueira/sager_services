using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Usinas.Domain.Models.Requests
{
    public class FinalizarUnidadeGeradoraRequest : Notificavel
    {
        public string ConfiguracaoCenarioId { get; private set; }
        public string ControleCalculoId { get; private set; }
        public string UnidadeGeradoraId { get; private set; }
        public string UsinaId { get; private set; }
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; private set; }

        public FinalizarUnidadeGeradoraRequest(
            string configuracaoCenarioId,
            string controleCalculoId,
            string unidadeGeradoraId,
            string usinaId,
            ConsolidacaoMensalRequest consolidacaoMensal)
        {
            ConfiguracaoCenarioId = configuracaoCenarioId;
            ControleCalculoId = controleCalculoId;
            UnidadeGeradoraId = unidadeGeradoraId;
            UsinaId = usinaId;
            ConsolidacaoMensal = consolidacaoMensal;

        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configura��o de cen�rio deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ControleCalculoId, nameof(ControleCalculoId), "O controle de c�lculo deve ser informado")
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "A usina deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(UnidadeGeradoraId, nameof(UnidadeGeradoraId), "A unidade geradora deve ser informada"));
        }
    }
}
