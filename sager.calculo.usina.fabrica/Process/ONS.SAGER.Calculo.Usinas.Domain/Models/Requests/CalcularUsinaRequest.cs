using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Usinas.Domain.Models.Requests
{
    public class CalcularUsinaRequest : Notificavel
    {
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; private set; }
        public string UsinaId { get; private set; }
        public string ConfiguracaoCenarioId { get; private set; }

        public CalcularUsinaRequest(
            ConsolidacaoMensalRequest consolidacaoMensal,
            string usinaId, 
            string configuracaoCenarioId)
        {
            ConsolidacaoMensal = consolidacaoMensal;
            UsinaId = usinaId;
            ConfiguracaoCenarioId = configuracaoCenarioId;
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "A usina deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configuração de cenário deve ser informada"));
        }
    }
}
