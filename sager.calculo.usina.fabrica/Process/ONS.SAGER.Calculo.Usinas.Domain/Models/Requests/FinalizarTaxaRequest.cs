using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Usinas.Domain.Models.Requests
{
    public class FinalizarTaxaRequest : Notificavel
    {
        public string ConfiguracaoCenarioId { get; }
        public string ControleCalculoId { get; }
        public UsinaRequest Usina { get; }
        public TipoCalculo TipoCalculo { get; }
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; }

        public FinalizarTaxaRequest(
            string configuracaoCenarioId,
            string controleCalculoId,
            UsinaRequest usina,
            TipoCalculo tipoCalculo,
            ConsolidacaoMensalRequest consolidacaoMensal)
        {
            ConfiguracaoCenarioId = configuracaoCenarioId;
            ControleCalculoId = controleCalculoId;
            Usina = usina;
            TipoCalculo = tipoCalculo;
            ConsolidacaoMensal = consolidacaoMensal;
        }

        public CalcularTaxaPayload MapToCalcularTaxaPayload()
        {
            return new CalcularTaxaPayload()
            {
                UsinaPayload = Usina.MapToPayload(),
                ControleCalculoIdPayload = ControleCalculoId,
                ConfiguracaoCenarioIdPayload = ConfiguracaoCenarioId,
                ConsolidacaoMensalPayload = ConsolidacaoMensal.MapToPayload()
            };
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configura��o de cen�rio deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ControleCalculoId, nameof(ControleCalculoId), "O controle de c�lculo deve ser informado")
                .VerificarSeNaoNuloOuEspacoEmBranco(Usina?.UsinaId, nameof(Usina), "A usina deve ser informada"));
        }
    }
}
