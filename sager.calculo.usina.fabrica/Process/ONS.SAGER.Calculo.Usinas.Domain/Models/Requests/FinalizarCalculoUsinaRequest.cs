using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Usinas.Domain.Models.Requests
{
    public class FinalizarCalculoUsinaRequest : Notificavel
    {
        public string ConfiguracaoCenarioId { get; }
        public string ControleCalculoId { get; }
        public string UsinaId { get; }
        public ConsolidacaoMensalRequest ConsolidacaoMensal { get; }

        public FinalizarCalculoUsinaRequest(
            string configuracaoCenarioId,
            string controleCalculoId,
            string usinaId,
            ConsolidacaoMensalRequest consolidacaoMensal)
        {
            ConfiguracaoCenarioId = configuracaoCenarioId;
            ControleCalculoId = controleCalculoId;
            UsinaId = usinaId;
            ConsolidacaoMensal = consolidacaoMensal;

            Validar();
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(ConsolidacaoMensal?.ConsolidacaoMensalId, nameof(ConsolidacaoMensal), "A consolidacao mensal deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ConfiguracaoCenarioId, nameof(ConfiguracaoCenarioId), "A configura��o de cen�rio deve ser informada")
                .VerificarSeNaoNuloOuEspacoEmBranco(ControleCalculoId, nameof(ControleCalculoId), "O controle de c�lculo deve ser informado")
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "A usina deve ser informada"));
        }
    }
}
