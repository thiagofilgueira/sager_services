using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.DataAccess.Repositories;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaRepository, TaxaRepository>();
            services.AddSingleton<IConsolidacaoMensalRepository, ConsolidacaoMensalRepository>();
            services.AddSingleton<IControleCalculoTaxaRepository, ControleCalculoTaxaRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IPotenciaUnidadeGeradoraRepository, PotenciaUnidadeGeradoraRepository>();
            services.AddSingleton<ISuspensaoUnidadeGeradoraRepository, SuspensaoUnidadeGeradoraRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
            services.AddSingleton<IConsolidacaoMensalRepository, ConsolidacaoMensalRepository>();
            services.AddSingleton<IEventoMudancaEstadoOperativoRepository, EventoMudancaEstadoOperativoRepository>();
        }
    }
}
