using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Interfaces
{
    public interface ITaxaService : ITaxaAcumuladaServiceBase
    {
        Task<double?> Calcular(CalcularTaxaTeipAcumRequest request);
    }
}
