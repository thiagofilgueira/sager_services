﻿using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Services;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Services
{
    public class UnidadeGeradoraService : UnidadeGeradoraServiceBase, IUnidadeGeradoraService
    {
        public UnidadeGeradoraService(
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            IEventoMudancaEstadoOperativoRepository eventoMudancaEstadoOperativoRepository,
            IParametroRepository parametroRepository)
            : base(
                  unidadeGeradoraRepository,
                  suspensaoUnidadeGeradoraRepository,
                  potenciaUnidadeGeradoraRepository,
                  eventoMudancaEstadoOperativoRepository,
                  parametroRepository)
        {

        }
    }
}
