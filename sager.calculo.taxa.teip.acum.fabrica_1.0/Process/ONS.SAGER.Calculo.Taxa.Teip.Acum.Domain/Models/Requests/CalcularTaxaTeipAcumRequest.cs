using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Models.Requests
{
    public class CalcularTaxaTeipAcumRequest : CalcularTaxaRequest
    {
        public CalcularTaxaTeipAcumRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            string consolidacaoMensalId,
            DateTime dataReferencia)
            : base(usina, controleCalculoId, configuracaoCenarioId, new ConsolidacaoMensalRequest(consolidacaoMensalId, dataReferencia))
        {
            Validar();
        }
        public CalcularTaxaTeipAcumRequest(
            UsinaRequest usina,
            string controleCalculoId,
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal)
            : base(usina, controleCalculoId, configuracaoCenarioId, consolidacaoMensal)
        {
            Validar();
        }
    }
}
