﻿using ONS.SAGER.Calculo.Util.DataAccess.Interfaces;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.IRepositories
{
    public interface IConsolidacaoMensalRepository : IConsolidacaoMensalRepositoryBase
    {
    }
}
