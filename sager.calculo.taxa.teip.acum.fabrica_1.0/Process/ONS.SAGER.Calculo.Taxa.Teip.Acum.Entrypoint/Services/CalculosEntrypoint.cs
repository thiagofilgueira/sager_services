using ONS.SAGER.Calculo.Taxa.Teip.Acum.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly ITaxaApplicationService _taxaTeipAcumApplicationService;
        public CalculosEntrypoint(ITaxaApplicationService taxaTeipAcumApplicationService)
        {
            _taxaTeipAcumApplicationService = taxaTeipAcumApplicationService;
        }

        [SDKEvent(Eventos.CalcularTaxaAcum)]
        public async Task CalcularTaxaTeipAcum(CalcularTaxaPayload payload)
        {
           await _taxaTeipAcumApplicationService.Calcular(
                new CalcularTaxaTeipAcumRequest(
                    payload?.UsinaPayload?.MapToRequest(),
                    payload?.ControleCalculoIdPayload,
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest()
                    ));              
        }

    }
}
