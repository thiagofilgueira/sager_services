using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Application.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Application.Services;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxaApplicationService, TaxaApplicationService>();
        }
    }
}
