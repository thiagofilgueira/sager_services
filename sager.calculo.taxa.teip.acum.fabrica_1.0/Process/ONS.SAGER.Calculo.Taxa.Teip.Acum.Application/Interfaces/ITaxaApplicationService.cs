using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Application.Interfaces
{
    public interface ITaxaApplicationService
    {
        Task<Result> Calcular(CalcularTaxaTeipAcumRequest request);
    }
}
