using ONS.SDK.Impl.Data;
using ONS.SAGER.Calculo.Util.Domain.Entities;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Map
{
    public class EntitiesMap : AbstractDataMapCollection
    {
        protected override void Load()
        {
            BindMap<ParametroTaxa>();
            BindMap<CalculoTaxa>();
            BindMap<PotenciaUnidadeGeradora>();
            BindMap<ControleCalculoTaxa>();
            BindMap<ConsolidacaoMensal>();
            BindMap<SuspensaoUnidadeGeradora>();
            BindMap<UnidadeGeradora>();
            BindMap<EventoMudancaEstadoOperativo>();
            
        }
    }
}
