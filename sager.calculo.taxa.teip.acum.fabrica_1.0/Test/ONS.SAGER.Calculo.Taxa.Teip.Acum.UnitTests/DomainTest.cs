using Moq;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Interfaces;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.IRepositories;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Models.Requests;
using ONS.SAGER.Calculo.Taxa.Teip.Acum.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Taxa.Teip.Acum.UnitTests
{
    public class DomainTest
    {

        private readonly IInsumoCalculoService _insumoCalculoService = new Mock<IInsumoCalculoService>().Object;

        // Dado 6 consolida��es mensais
        // E 5 TeipMes
        // Quando executo o c�lculo da Teip
        // Ent�o eu somo as 5 TeipMes com 55 vezes a taxa de refer�ncia
        [Fact]
        public async Task DeveCalcular5MesDeTaxasMensaisMais55VezesATaxaDeReferencia()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",8,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",4,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"IPReferencia","ALUXG",1 ,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"IPReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                new[] { TipoCalculo.TEIPacumAjustada },
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);


            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);


            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2019, 03, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(95.8, taxa);
        }

        [Fact]
        public async Task DeveCalcularUGEemOperacaoEdesconsiderarUGEEmsuspensao()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",8,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",4,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"IPReferencia","ALUXG",1 ,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"IPReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                new[] { TipoCalculo.TEIPacumAjustada },
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);


            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG"),
                new UnidadeGeradora("2","ALUXG","CEUTFO",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var suspensoes = new SuspensaoUnidadeGeradora[]
            {
                  new SuspensaoUnidadeGeradora("1", "2", new DateTime(2018,1,1), new DateTime(2019,9,1))
            };

            var potencias = new PotenciaUnidadeGeradora[]
           {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "2", 312),
           };
            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarPotencias(potencias.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
                uge.AdicionarSuspensoes(suspensoes.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }
            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2019, 03, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(95.8, taxa);
        }

        [Fact]
        public void NaoDeveCalcularQuandoTemSuspensaoParaUmaUgeMesmoTendoValorParametro()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",8,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",4,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"IPReferencia","ALUXG",1 ,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"IPReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                new[] { TipoCalculo.TEIPacumAjustada },
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);


            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG")
            };
            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
             {
                  new SuspensaoUnidadeGeradora("1", "1", new DateTime(2018,1,1), new DateTime(2019,9,1))
             };
            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarPotencias(potencias.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
                uge.AdicionarSuspensoes(suspensoes.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }
            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);


            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2019, 03, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = service.Calcular(calculoRequest);

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);

        }

        [Fact]
        public void NaoDeveCalcularQuandoUsinaDesativada()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",8,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",4,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"IPReferencia","ALUXG",1 ,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"IPReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                new[] { TipoCalculo.TEIPacumAjustada },
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);


            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG")
            };
            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
            };

            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarPotencias(potencias.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }
            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);


            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2019, 03, 01), new DateTime(2019,02,01), null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = service.Calcular(calculoRequest);

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);

        }


        [Fact]
        public void DeveCalcularParaUGEvalidaDesconsiderandoUGEemSuspensao()
        {

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",8,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090df",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dg",6,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dh",5,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090di",4,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxaRepository = new Mock<ITaxaRepository>();

            var taxasMensais = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dg",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dh",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"IPReferencia","ALUXG",1 ,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090df",null,"IPReferencia","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090di",null,"TEIPacumAjustada","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                new[] { TipoCalculo.TEIPacumAjustada },
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);


            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG"),
                new UnidadeGeradora("2","CEUTFO","CEUTFO",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"CEUTFO")
            };
            var potencias = new PotenciaUnidadeGeradora[]
            {
                new PotenciaUnidadeGeradora(null, new DateTime(2017,11,1), new DateTime(2017,12,1), null, "1", 312),
            };

            var suspensoes = new SuspensaoUnidadeGeradora[]
             {
                  new SuspensaoUnidadeGeradora("2", "2", new DateTime(2018,1,1), new DateTime(2019,9,1))
             };
            foreach (var uge in unidadesGeradoras)
            {
                uge.AdicionarPotencias(potencias.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
                uge.AdicionarSuspensoes(suspensoes.FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId));
            }
            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);


            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2019, 03, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = service.Calcular(calculoRequest);

            var action = new Func<Task>(() => service.Calcular(calculoRequest));

            Assert.ThrowsAsync<InvalidOperationException>(action);

        }



        // Dado 60 taxas mensais
        // Quando executo o c�lculo
        // Ent�o o resultado do c�lculo ser� a soma das 60 taxas mensais dividido por 60

        [Fact]
        public async Task DeveCalcular60TaxasMensais()
        {
            var taxaRepository = new Mock<ITaxaRepository>();

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();

            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("a73ac08f-418f-4ac7-9f72-03928b409001",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b409002",6,2019),
                new ConsolidacaoMensal("c73ac08f-418f-4ac7-9f72-03928b409003",5,2019),
                new ConsolidacaoMensal("d73ac08f-418f-4ac7-9f72-03928b409004",4,2019),
                new ConsolidacaoMensal("e73ac08f-418f-4ac7-9f72-03928b409005",3,2019),
                new ConsolidacaoMensal("f73ac08f-418f-4ac7-9f72-03928b409006",2,2019),
                new ConsolidacaoMensal("g73ac08f-418f-4ac7-9f72-03928b409007",1,2019),
                new ConsolidacaoMensal("h73ac08f-418f-4ac7-9f72-03928b409008",12,2018),
                new ConsolidacaoMensal("i73ac08f-418f-4ac7-9f72-03928b409009",11,2018),
                new ConsolidacaoMensal("j73ac08f-418f-4ac7-9f72-03928b409010",10,2018),
                new ConsolidacaoMensal("k73ac08f-418f-4ac7-9f72-03928b409011",9,2018),
                new ConsolidacaoMensal("l73ac08f-418f-4ac7-9f72-03928b409012",8,2018),
                new ConsolidacaoMensal("m73ac08f-418f-4ac7-9f72-03928b409013",7,2018),
                new ConsolidacaoMensal("n73ac08f-418f-4ac7-9f72-03928b409014",6,2018),
                new ConsolidacaoMensal("o73ac08f-418f-4ac7-9f72-03928b409015",5,2018),
                new ConsolidacaoMensal("p73ac08f-418f-4ac7-9f72-03928b409016",4,2018),
                new ConsolidacaoMensal("q73ac08f-418f-4ac7-9f72-03928b409017",3,2018),
                new ConsolidacaoMensal("r73ac08f-418f-4ac7-9f72-03928b409018",2,2018),
                new ConsolidacaoMensal("s73ac08f-418f-4ac7-9f72-03928b409019",1,2018),
                new ConsolidacaoMensal("t73ac08f-418f-4ac7-9f72-03928b409020",12,2017),
                new ConsolidacaoMensal("u73ac08f-418f-4ac7-9f72-03928b409021",11,2017),
                new ConsolidacaoMensal("v73ac08f-418f-4ac7-9f72-03928b409022",10,2017),
                new ConsolidacaoMensal("x73ac08f-418f-4ac7-9f72-03928b409023",9,2017),
                new ConsolidacaoMensal("z73ac08f-418f-4ac7-9f72-03928b409024",8,2017),
                new ConsolidacaoMensal("w73ac08f-418f-4ac7-9f72-03928b409025",7,2017),
                new ConsolidacaoMensal("y73ac08f-418f-4ac7-9f72-03928b409026",6,2017),
                new ConsolidacaoMensal("173ac08f-418f-4ac7-9f72-03928b409027",5,2017),
                new ConsolidacaoMensal("273ac08f-418f-4ac7-9f72-03928b409028",4,2017),
                new ConsolidacaoMensal("373ac08f-418f-4ac7-9f72-03928b409029",3,2017),
                new ConsolidacaoMensal("473ac08f-418f-4ac7-9f72-03928b409030",2,2017),
                new ConsolidacaoMensal("573ac08f-418f-4ac7-9f72-03928b409031",1,2017),
                new ConsolidacaoMensal("673ac08f-418f-4ac7-9f72-03928b409032",12,2016),
                new ConsolidacaoMensal("773ac08f-418f-4ac7-9f72-03928b409033",11,2016),
                new ConsolidacaoMensal("873ac08f-418f-4ac7-9f72-03928b409034",10,2016),
                new ConsolidacaoMensal("973ac08f-418f-4ac7-9f72-03928b409035",9,2016),
                new ConsolidacaoMensal("103ac08f-418f-4ac7-9f72-03928b409036",8,2016),
                new ConsolidacaoMensal("113ac08f-418f-4ac7-9f72-03928b409037",7,2016),
                new ConsolidacaoMensal("123ac08f-418f-4ac7-9f72-03928b409038",6,2016),
                new ConsolidacaoMensal("133ac08f-418f-4ac7-9f72-03928b409039",5,2016),
                new ConsolidacaoMensal("143ac08f-418f-4ac7-9f72-03928b409040",4,2016),
                new ConsolidacaoMensal("153ac08f-418f-4ac7-9f72-03928b409041",3,2016),
                new ConsolidacaoMensal("173ac08f-418f-4ac7-9f72-03928b409043",2,2016),
                new ConsolidacaoMensal("183ac08f-418f-4ac7-9f72-03928b409044",1,2016),
                new ConsolidacaoMensal("193ac08f-418f-4ac7-9f72-03928b409045",12,2015),
                new ConsolidacaoMensal("203ac08f-418f-4ac7-9f72-03928b409046",11,2015),
                new ConsolidacaoMensal("213ac08f-418f-4ac7-9f72-03928b409047",10,2015),
                new ConsolidacaoMensal("223ac08f-418f-4ac7-9f72-03928b409048",9,2015),
                new ConsolidacaoMensal("233ac08f-418f-4ac7-9f72-03928b409049",8,2015),
                new ConsolidacaoMensal("243ac08f-418f-4ac7-9f72-03928b409050",7,2015),
                new ConsolidacaoMensal("253ac08f-418f-4ac7-9f72-03928b409051",6,2015),
                new ConsolidacaoMensal("263ac08f-418f-4ac7-9f72-03928b409052",5,2015),
                new ConsolidacaoMensal("273ac08f-418f-4ac7-9f72-03928b409053",4,2015),
                new ConsolidacaoMensal("283ac08f-418f-4ac7-9f72-03928b409054",3,2015),
                new ConsolidacaoMensal("303ac08f-418f-4ac7-9f72-03928b409056",2,2015),
                new ConsolidacaoMensal("313ac08f-418f-4ac7-9f72-03928b409057",1,2015),
                new ConsolidacaoMensal("323ac08f-418f-4ac7-9f72-03928b409058",12,2014),
                new ConsolidacaoMensal("333ac08f-418f-4ac7-9f72-03928b409059",11,2014),
                new ConsolidacaoMensal("343ac08f-418f-4ac7-9f72-03928b409060",10,2014),
                new ConsolidacaoMensal("353ac08f-418f-4ac7-9f72-03928b409061",9,2014),
                new ConsolidacaoMensal("163ac08f-418f-4ac7-9f72-03928b409042",8,2014),

            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxas = new List<CalculoTaxa>()
            {
                new CalculoTaxa("a73ac08f-418f-4ac7-9f72-03928b409001",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b409002",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("c73ac08f-418f-4ac7-9f72-03928b409003",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("d73ac08f-418f-4ac7-9f72-03928b409004",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("e73ac08f-418f-4ac7-9f72-03928b409005",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("f73ac08f-418f-4ac7-9f72-03928b409006",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("g73ac08f-418f-4ac7-9f72-03928b409007",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("h73ac08f-418f-4ac7-9f72-03928b409008",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("i73ac08f-418f-4ac7-9f72-03928b409009",null,"TEIPmes","ALUXG",13.5, "2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("j73ac08f-418f-4ac7-9f72-03928b409010",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("k73ac08f-418f-4ac7-9f72-03928b409011",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("l73ac08f-418f-4ac7-9f72-03928b409012",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("m73ac08f-418f-4ac7-9f72-03928b409013",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("n73ac08f-418f-4ac7-9f72-03928b409014",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("o73ac08f-418f-4ac7-9f72-03928b409015",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("p73ac08f-418f-4ac7-9f72-03928b409016",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("q73ac08f-418f-4ac7-9f72-03928b409017",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("r73ac08f-418f-4ac7-9f72-03928b409018",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("s73ac08f-418f-4ac7-9f72-03928b409019",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("t73ac08f-418f-4ac7-9f72-03928b409020",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("u73ac08f-418f-4ac7-9f72-03928b409021",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("v73ac08f-418f-4ac7-9f72-03928b409022",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("x73ac08f-418f-4ac7-9f72-03928b409023",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("z73ac08f-418f-4ac7-9f72-03928b409024",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("w73ac08f-418f-4ac7-9f72-03928b409025",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("y73ac08f-418f-4ac7-9f72-03928b409026",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("173ac08f-418f-4ac7-9f72-03928b409027",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("273ac08f-418f-4ac7-9f72-03928b409028",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("373ac08f-418f-4ac7-9f72-03928b409029",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("473ac08f-418f-4ac7-9f72-03928b409030",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("573ac08f-418f-4ac7-9f72-03928b409031",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("673ac08f-418f-4ac7-9f72-03928b409032",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("773ac08f-418f-4ac7-9f72-03928b409033",null,"TEIPmes","ALUXG", 12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("873ac08f-418f-4ac7-9f72-03928b409034",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("973ac08f-418f-4ac7-9f72-03928b409035",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("103ac08f-418f-4ac7-9f72-03928b409036",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("113ac08f-418f-4ac7-9f72-03928b409037",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("123ac08f-418f-4ac7-9f72-03928b409038",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("133ac08f-418f-4ac7-9f72-03928b409039",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("143ac08f-418f-4ac7-9f72-03928b409040",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("153ac08f-418f-4ac7-9f72-03928b409041",null,"TEIPmes","ALUXG", 12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("173ac08f-418f-4ac7-9f72-03928b409043",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("183ac08f-418f-4ac7-9f72-03928b409044",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("193ac08f-418f-4ac7-9f72-03928b409045",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("203ac08f-418f-4ac7-9f72-03928b409046",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("213ac08f-418f-4ac7-9f72-03928b409047",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("223ac08f-418f-4ac7-9f72-03928b409048",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("233ac08f-418f-4ac7-9f72-03928b409049",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("243ac08f-418f-4ac7-9f72-03928b409050",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("253ac08f-418f-4ac7-9f72-03928b409051",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("263ac08f-418f-4ac7-9f72-03928b409052",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("273ac08f-418f-4ac7-9f72-03928b409053",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("283ac08f-418f-4ac7-9f72-03928b409054",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("303ac08f-418f-4ac7-9f72-03928b409056",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("313ac08f-418f-4ac7-9f72-03928b409057",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("323ac08f-418f-4ac7-9f72-03928b409058",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("333ac08f-418f-4ac7-9f72-03928b409059",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("343ac08f-418f-4ac7-9f72-03928b409060",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("353ac08f-418f-4ac7-9f72-03928b409061",null,"TEIPmes","ALUXG",10,"2", null, "b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("163ac08f-418f-4ac7-9f72-03928b409042",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxas);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","ALUXG","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"ALUXG")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2010, 01, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(32.655, taxa);
        }

        // Dado 60 taxas mensais
        // E tem uma taxa acumulada ajustada no m�s de refer�ncia
        // Quando executo o c�lculo
        // Ent�o as 60 taxas mensais ser�o ignoradas e o valor da taxa acumulada ajustada ser� usada como refer�ncia no c�lculo
        [Fact]
        public async Task DeveCalcularLevandoEmConsideracaoApenasAcumuladaAjustadaParaOMesDeReferencia()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();

            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("a73ac08f-418f-4ac7-9f72-03928b409000",8,2019),
                new ConsolidacaoMensal("a73ac08f-418f-4ac7-9f72-03928b409001",7,2019),
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b409002",6,2019),
                new ConsolidacaoMensal("c73ac08f-418f-4ac7-9f72-03928b409003",5,2019),
                new ConsolidacaoMensal("d73ac08f-418f-4ac7-9f72-03928b409004",4,2019),
                new ConsolidacaoMensal("e73ac08f-418f-4ac7-9f72-03928b409005",3,2019),
                new ConsolidacaoMensal("f73ac08f-418f-4ac7-9f72-03928b409006",2,2019),
                new ConsolidacaoMensal("g73ac08f-418f-4ac7-9f72-03928b409007",1,2019),
                new ConsolidacaoMensal("h73ac08f-418f-4ac7-9f72-03928b409008",12,2018),
                new ConsolidacaoMensal("i73ac08f-418f-4ac7-9f72-03928b409009",11,2018),
                new ConsolidacaoMensal("j73ac08f-418f-4ac7-9f72-03928b409010",10,2018),
                new ConsolidacaoMensal("k73ac08f-418f-4ac7-9f72-03928b409011",9,2018),
                new ConsolidacaoMensal("l73ac08f-418f-4ac7-9f72-03928b409012",8,2018),
                new ConsolidacaoMensal("m73ac08f-418f-4ac7-9f72-03928b409013",7,2018),
                new ConsolidacaoMensal("n73ac08f-418f-4ac7-9f72-03928b409014",6,2018),
                new ConsolidacaoMensal("o73ac08f-418f-4ac7-9f72-03928b409015",5,2018),
                new ConsolidacaoMensal("p73ac08f-418f-4ac7-9f72-03928b409016",4,2018),
                new ConsolidacaoMensal("q73ac08f-418f-4ac7-9f72-03928b409017",3,2018),
                new ConsolidacaoMensal("r73ac08f-418f-4ac7-9f72-03928b409018",2,2018),
                new ConsolidacaoMensal("s73ac08f-418f-4ac7-9f72-03928b409019",1,2018),
                new ConsolidacaoMensal("t73ac08f-418f-4ac7-9f72-03928b409020",12,2017),
                new ConsolidacaoMensal("u73ac08f-418f-4ac7-9f72-03928b409021",11,2017),
                new ConsolidacaoMensal("v73ac08f-418f-4ac7-9f72-03928b409022",10,2017),
                new ConsolidacaoMensal("x73ac08f-418f-4ac7-9f72-03928b409023",9,2017),
                new ConsolidacaoMensal("z73ac08f-418f-4ac7-9f72-03928b409024",8,2017),
                new ConsolidacaoMensal("w73ac08f-418f-4ac7-9f72-03928b409025",7,2017),
                new ConsolidacaoMensal("y73ac08f-418f-4ac7-9f72-03928b409026",6,2017),
                new ConsolidacaoMensal("173ac08f-418f-4ac7-9f72-03928b409027",5,2017),
                new ConsolidacaoMensal("273ac08f-418f-4ac7-9f72-03928b409028",4,2017),
                new ConsolidacaoMensal("373ac08f-418f-4ac7-9f72-03928b409029",3,2017),
                new ConsolidacaoMensal("473ac08f-418f-4ac7-9f72-03928b409030",2,2017),
                new ConsolidacaoMensal("573ac08f-418f-4ac7-9f72-03928b409031",1,2017),
                new ConsolidacaoMensal("673ac08f-418f-4ac7-9f72-03928b409032",12,2016),
                new ConsolidacaoMensal("773ac08f-418f-4ac7-9f72-03928b409033",11,2016),
                new ConsolidacaoMensal("873ac08f-418f-4ac7-9f72-03928b409034",10,2016),
                new ConsolidacaoMensal("973ac08f-418f-4ac7-9f72-03928b409035",9,2016),
                new ConsolidacaoMensal("103ac08f-418f-4ac7-9f72-03928b409036",8,2016),
                new ConsolidacaoMensal("113ac08f-418f-4ac7-9f72-03928b409037",7,2016),
                new ConsolidacaoMensal("123ac08f-418f-4ac7-9f72-03928b409038",6,2016),
                new ConsolidacaoMensal("133ac08f-418f-4ac7-9f72-03928b409039",5,2016),
                new ConsolidacaoMensal("143ac08f-418f-4ac7-9f72-03928b409040",4,2016),
                new ConsolidacaoMensal("153ac08f-418f-4ac7-9f72-03928b409041",3,2016),
                new ConsolidacaoMensal("173ac08f-418f-4ac7-9f72-03928b409043",2,2016),
                new ConsolidacaoMensal("183ac08f-418f-4ac7-9f72-03928b409044",1,2016),
                new ConsolidacaoMensal("193ac08f-418f-4ac7-9f72-03928b409045",12,2015),
                new ConsolidacaoMensal("203ac08f-418f-4ac7-9f72-03928b409046",11,2015),
                new ConsolidacaoMensal("213ac08f-418f-4ac7-9f72-03928b409047",10,2015),
                new ConsolidacaoMensal("223ac08f-418f-4ac7-9f72-03928b409048",9,2015),
                new ConsolidacaoMensal("233ac08f-418f-4ac7-9f72-03928b409049",8,2015),
                new ConsolidacaoMensal("243ac08f-418f-4ac7-9f72-03928b409050",7,2015),
                new ConsolidacaoMensal("253ac08f-418f-4ac7-9f72-03928b409051",6,2015),
                new ConsolidacaoMensal("263ac08f-418f-4ac7-9f72-03928b409052",5,2015),
                new ConsolidacaoMensal("273ac08f-418f-4ac7-9f72-03928b409053",4,2015),
                new ConsolidacaoMensal("283ac08f-418f-4ac7-9f72-03928b409054",3,2015),
                new ConsolidacaoMensal("303ac08f-418f-4ac7-9f72-03928b409056",2,2015),
                new ConsolidacaoMensal("313ac08f-418f-4ac7-9f72-03928b409057",1,2015),
                new ConsolidacaoMensal("323ac08f-418f-4ac7-9f72-03928b409058",12,2014),
                new ConsolidacaoMensal("333ac08f-418f-4ac7-9f72-03928b409059",11,2014),
                new ConsolidacaoMensal("343ac08f-418f-4ac7-9f72-03928b409060",10,2014),
                new ConsolidacaoMensal("353ac08f-418f-4ac7-9f72-03928b409061",9,2014),
                new ConsolidacaoMensal("163ac08f-418f-4ac7-9f72-03928b409042",8,2014)

            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxas = new List<CalculoTaxa>()
            {
                new CalculoTaxa("a73ac08f-418f-4ac7-9f72-03928b409000",null,TipoCalculo.TEIPacumAjustada.ToString(),"ALUXG",27,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("a73ac08f-418f-4ac7-9f72-03928b409001",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b409002",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("c73ac08f-418f-4ac7-9f72-03928b409003",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("d73ac08f-418f-4ac7-9f72-03928b409004",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("e73ac08f-418f-4ac7-9f72-03928b409005",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("f73ac08f-418f-4ac7-9f72-03928b409006",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("g73ac08f-418f-4ac7-9f72-03928b409007",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("h73ac08f-418f-4ac7-9f72-03928b409008",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("i73ac08f-418f-4ac7-9f72-03928b409009",null,"TEIPmes","ALUXG",13.5, "2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("j73ac08f-418f-4ac7-9f72-03928b409010",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("k73ac08f-418f-4ac7-9f72-03928b409011",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("l73ac08f-418f-4ac7-9f72-03928b409012",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("m73ac08f-418f-4ac7-9f72-03928b409013",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("n73ac08f-418f-4ac7-9f72-03928b409014",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("o73ac08f-418f-4ac7-9f72-03928b409015",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("p73ac08f-418f-4ac7-9f72-03928b409016",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("q73ac08f-418f-4ac7-9f72-03928b409017",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("r73ac08f-418f-4ac7-9f72-03928b409018",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("s73ac08f-418f-4ac7-9f72-03928b409019",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("t73ac08f-418f-4ac7-9f72-03928b409020",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("u73ac08f-418f-4ac7-9f72-03928b409021",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("v73ac08f-418f-4ac7-9f72-03928b409022",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("x73ac08f-418f-4ac7-9f72-03928b409023",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("z73ac08f-418f-4ac7-9f72-03928b409024",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("w73ac08f-418f-4ac7-9f72-03928b409025",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("y73ac08f-418f-4ac7-9f72-03928b409026",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("173ac08f-418f-4ac7-9f72-03928b409027",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("273ac08f-418f-4ac7-9f72-03928b409028",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("373ac08f-418f-4ac7-9f72-03928b409029",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("473ac08f-418f-4ac7-9f72-03928b409030",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("573ac08f-418f-4ac7-9f72-03928b409031",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("673ac08f-418f-4ac7-9f72-03928b409032",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("773ac08f-418f-4ac7-9f72-03928b409033",null,"TEIPmes","ALUXG", 12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("873ac08f-418f-4ac7-9f72-03928b409034",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("973ac08f-418f-4ac7-9f72-03928b409035",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("103ac08f-418f-4ac7-9f72-03928b409036",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("113ac08f-418f-4ac7-9f72-03928b409037",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("123ac08f-418f-4ac7-9f72-03928b409038",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("133ac08f-418f-4ac7-9f72-03928b409039",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("143ac08f-418f-4ac7-9f72-03928b409040",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("153ac08f-418f-4ac7-9f72-03928b409041",null,"TEIPmes","ALUXG", 12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("173ac08f-418f-4ac7-9f72-03928b409043",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("183ac08f-418f-4ac7-9f72-03928b409044",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("193ac08f-418f-4ac7-9f72-03928b409045",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("203ac08f-418f-4ac7-9f72-03928b409046",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("213ac08f-418f-4ac7-9f72-03928b409047",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("223ac08f-418f-4ac7-9f72-03928b409048",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("233ac08f-418f-4ac7-9f72-03928b409049",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("243ac08f-418f-4ac7-9f72-03928b409050",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("253ac08f-418f-4ac7-9f72-03928b409051",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("263ac08f-418f-4ac7-9f72-03928b409052",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("273ac08f-418f-4ac7-9f72-03928b409053",null,"TEIPmes","ALUXG",3.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("283ac08f-418f-4ac7-9f72-03928b409054",null,"TEIPmes","ALUXG",23.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("303ac08f-418f-4ac7-9f72-03928b409056",null,"TEIPmes","ALUXG",103.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("313ac08f-418f-4ac7-9f72-03928b409057",null,"TEIPmes","ALUXG",12.5,"2", null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("323ac08f-418f-4ac7-9f72-03928b409058",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("333ac08f-418f-4ac7-9f72-03928b409059",null,"TEIPmes","ALUXG",14.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("343ac08f-418f-4ac7-9f72-03928b409060",null,"TEIPmes","ALUXG",12.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("353ac08f-418f-4ac7-9f72-03928b409061",null,"TEIPmes","ALUXG",10,"2", null, "b73ac08f-418f-4ac7-9f72-414111131331"),
                new CalculoTaxa("163ac08f-418f-4ac7-9f72-03928b409042",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxas);

            var taxasAjustadas = new List<CalculoTaxa>()
            {
                new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,TipoCalculo.TEIPacumAjustada.ToString(),"ALUXG",27,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextoCalculo(
                It.IsAny<string>(),
                TipoCalculo.TEIPacumAjustada,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasAjustadas);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","1","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"1")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2010, 01, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(27, taxa);
        }

        [Fact]
        public async Task DeveCalcular1MesDeTaxasMensaisMais59VezesATaxaDeReferencia()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();

            var consolidacoes = new List<ConsolidacaoMensal>()
            {
                new ConsolidacaoMensal("b73ac08f-418f-4ac7-9f72-03928b4090dd",7,2019)
            };

            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
                .ReturnsAsync(consolidacoes);

            var taxasMensais = new List<CalculoTaxa>()
            {
                 new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"TEIPmes","ALUXG",13.3,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331")
            };

            var taxasReferencia = new List<CalculoTaxa>()
            {
              new CalculoTaxa("b73ac08f-418f-4ac7-9f72-03928b4090dd",null,"IPReferencia","ALUXG",2,"2",null,"b73ac08f-418f-4ac7-9f72-414111131331"),
            };

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                TipoCalculo.TEIPmes,
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(taxasMensais);

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
              It.IsAny<IEnumerable<string>>(),
              TipoCalculo.IPReferencia,
              It.IsAny<string>(),
              It.IsAny<string>()))
                .ReturnsAsync(taxasReferencia);

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();

            var unidadesGeradoras = new List<UnidadeGeradora>
            {
                new UnidadeGeradora("1","1","ALUXG",new DateTime(2010,01,01),new DateTime(2010,01,01),null,"1")
            };

            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
                )).ReturnsAsync(unidadesGeradoras);

            var service = new TaxaService(
                taxaRepository.Object,
                unidadeGeradoraService.Object,
                consolidacaoMensalRepository.Object,
                _insumoCalculoService);

            var calculoRequest = new CalcularTaxaTeipAcumRequest(
                new UsinaRequest("ALUXG", new DateTime(2010, 01, 01), null, null, TipoUsina.UEE.ToString()),
                "b73ac08f-418f-4ac7-9f72-414111131331",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                "b73ac08f-418f-4ac7-9f72-03928b4090dd",
                new DateTime(2019, 8, 1));

            var taxa = await service.Calcular(calculoRequest);

            Assert.Equal(2.1883333333333333, taxa);
        }

    }
}

