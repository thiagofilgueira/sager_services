using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hdp.UnitTests
{
    public class CalcularParametroHdpRequestTest
    {
        // Quando configura��o de cen�rio for Nulo ou Vazio, o resultado do c�lculo do 
        // par�metro deve ser falso
        // Teste passando vazio e nulo
        [Theory(DisplayName = "Teste para verificar se retornar falso quando configura��o do cen�rio � nula ou vazio")]
        [InlineData("")]
        [InlineData(null)]
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            var calcularParametroRequest = new CalcularParametroHdpRequest(
                configuracaoCenarioId,
                new ConsolidacaoMensalRequest(null,DateTime.Now),
                new UnidadeGeradoraRequest(null, null, null, null, null, null,null,null),
                null);

            var result = calcularParametroRequest.Valido;

            Assert.False(result);
        }

        // Quando Data Refer�ncia for igual ao valor m�nimo, ou seja vazia ou nula
        // o m�todo deve retornar falso
        [Fact(DisplayName = "Teste para verificar se o resultado � falso quando data refer�ncia � nula ou vazia")]
        public void ResultadoDeveSerFalsoQuandoDataReferenciaForIgualAoValorMinimo()
        {
            var calcularParametroRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.MinValue),
                new UnidadeGeradoraRequest(null, null, null, null, null, null, null, null),
                null);

            var result = calcularParametroRequest.Valido;
            Assert.False(result);
        }

        // Quando Unidade geradora for nula,
        // o m�todo deve retornar Falso
        [Fact(DisplayName = "Teste para verificar se retornar falso quando unidade geradora n�o est� preenchida")]
        public void ResultadoDeveSerFalsoQuandoUnidadeGeradoraForNula()
        {
            var calcularParametroRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                null,
                null);

            var result = calcularParametroRequest.Valido;

            Assert.False(result);
        }

        // Quando unidadade geradora n�o for preenchida
        // o m�todo de retornar falso
        [Fact(DisplayName = "Teste para verificar se retorna false quando suspens�es n�o est�o preenchidas")]
        public void DeveRetornarFalsoQuandoSuspensoesDaUnidadeGeradoraForemNulasOuVazias()
        {
            var calcularParametroRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                null,
                null);

            var result = calcularParametroRequest.PossuiSuspensoes();
            Assert.False(result);
        }

        // Quando unidade geradora estiver preenchida
        // o m�todo deve retornar verdadeiro
        [Fact(DisplayName = "Teste para verificar se quando suspens�o preenchida retorna verdadeiro")]
        public void DeveRetornarVerdadeiroQuandoSuspensoesDaUnidadeGeradoraEstiverPreenchida()
        {
            var calcularParametroRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                new UnidadeGeradoraRequest(null,null,null,null,null,null,null,null,
                new List<SuspensaoUnidadeGeradoraRequest>()
                {
                   new SuspensaoUnidadeGeradoraRequest("1","1","1", DateTime.Now, DateTime.Now)
                },null),
                null);

            var result = calcularParametroRequest.PossuiSuspensoes();
            Assert.True(result);
        }
    }
}


