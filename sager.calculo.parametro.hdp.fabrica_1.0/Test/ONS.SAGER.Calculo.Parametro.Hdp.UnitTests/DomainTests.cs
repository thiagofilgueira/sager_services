using Moq;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hedp.UnitTests
{
    public class DomainTests
    {

        [Theory]
        [InlineData("02/01/2001", "DPR", "RPR", "GTR", "AGE", 10,                  //Evento v�lido
            "05/01/2001", "DUR", "RPR", "GAG", "AGE", 10, 720)]                   //Evento v�lido
        [InlineData(
            "03/01/2001", "DCA", "", "GUM", "AGE", 10,                         // Evento v�lido
            "29/01/2001", "DPA", "", "GAG", "AGE", 10, 696)]                 // Evento v�lido
        [InlineData(
            "03/01/2001", "DUR", "RPR", "GOO", "AGE", 10,                         // Evento inv�lido
            "20/01/2001", "DCA", "RPR", "GTR", "AGE", 10, 288)]                 // Evento v�lido
        public void DeveCalcularApenasEventosComStatusEventoValidoEstadoOperativoValidoCondicaoOperativaValidaEOrigemValida(
            string dataVerificada,
            string estadoOperativo,
            string condicaoOperativa,
            string origem,
            string statusEvento,
            int disponibilidade,
            string dataVerificada2,
            string estadoOperativo2,
            string condicaoOperativa2,
            string origem2,
            string statusEvento2,
            int disponibilidade2,
            double resultadoEsperado)
        {
            // Arrange
            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[] { };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Parse(dataVerificada),estadoOperativo, condicaoOperativa,origem,statusEvento,disponibilidade),
                  new EventoMudancaEstadoOperativoRequest("2","2",DateTime.Parse(dataVerificada2),estadoOperativo2, condicaoOperativa2,origem2,statusEvento2,disponibilidade2)

             };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2001, 01, 01), new DateTime(2001, 01, 01), null, "1", potencias, suspensoes, eventos);

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2001, 01, 02)),
                uge,
                "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(resultadoEsperado, parametroHedp);
        }

        [Theory]
        [InlineData("10/01/2001", "DUR", "RPR", "GCB", "AGE",        // Evento v�lido
            "12/01/2001", "DUR", "RPR", "GAG", "EXA",               // Evento v�lido exclu�do
            "18/01/2001", "DCA", "RPR", "GGE", "EVR",               // Evento v�lido
            528)]
        public void DeveCalcularSequenciaEventosValidosDesconsiderandoEventoExcluido(
          string dataVerificada,
          string estadoOperativo,
          string condicaoOperativa,
          string origem,
          string statusEvento,
          string dataVerificada2,
          string estadoOperativo2,
          string condicaoOperativa2,
          string origem2,
          string statusEvento2,
          string dataVerificada3,
          string estadoOperativo3,
          string condicaoOperativa3,
          string origem3,
          string statusEvento3,
          double esperado)
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {

            };
            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Parse(dataVerificada), estadoOperativo, condicaoOperativa, origem, statusEvento, 10),
                  new EventoMudancaEstadoOperativoRequest("2","2",DateTime.Parse(dataVerificada2), estadoOperativo2, condicaoOperativa2, origem2, statusEvento2, 10),
                  new EventoMudancaEstadoOperativoRequest("3","3",DateTime.Parse(dataVerificada3), estadoOperativo3, condicaoOperativa3, origem3, statusEvento3, 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2001, 01, 01), new DateTime(2001, 01, 01), null, "1", potencias, suspensoes, eventos);

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2001, 01, 01)),
                uge,
                "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(esperado, parametroHedp);
        }
        // Dado que a data entrada opera��o comercial � dia 01/01/2019
        // E que a data Evento EOC � dia 01/01/2019
        // E que eu passei eventos de desligamentos
        // Quando executo o c�lculo
        // Ent�o o resultado deve ser zero
        [Theory]
        [InlineData("LIG", "RPR", "GTR")]                              // Evento inv�lido
        [InlineData("LCS", "RPR", "GAC")]                              // Evento inv�lido
        [InlineData("LCI", "RPR", "GOT")]                              // Evento inv�lido
        [InlineData("LCC", "RPR", "GUMResultadoDoCalculoDeveSerZeroQuandoTodosOsEventosSaoInvalidos")]                              // Evento inv�lido
        public void ResultadoDeveSerZeroQuandoTodosOsEventosForemComEstadoOperativoLigado(string estadoOperativo, string condicaoOperativa, string origem)
        {
            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[] { };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 01),estadoOperativo, condicaoOperativa, origem, "AGE", 10)
             };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), null, "1", potencias, suspensoes, eventos);

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)),
                uge,
                "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;
            // Assert
            Assert.Equal(0, parametroHedp);
        }

        // Dado que eu tenho data entrada opera��o comercial
        // E que eu tenho evento EOC
        // Quando passo um evento v�lido depois de uma suspens�o
        // Ent�o o c�lculo � executado para todos os eventos v�lidos
        [Theory]
        [InlineData("DUR", "RPR", "GAC")]                           // Evento v�lido
        public void DeveCalcularEventoValidoLogoAposTerminoDeSuspensao(string estadoOperativo, string condicaoOperativa, string origem)
        {
            // Arrange
            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
                new SuspensaoUnidadeGeradoraRequest("1","1","1", DateTime.Parse("01/01/2017"), DateTime.Parse("16/01/2017")),
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Parse("16/01/2017"), estadoOperativo, condicaoOperativa, origem, "AGE",10)
             };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", DateTime.Parse("01/01/2017"), DateTime.Parse("01/01/2017"),
                null, "1", potencias, suspensoes, eventos);


            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2017, 01, 01)),
                uge,
                "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(384, parametroHedp);
        }
        // Dado que eu tenho data entrada opera��o comercial
        // E que eu tenho evento EOC
        // Quando eu tenho apenas evento com status evento exclu�do
        // Ent�o o resultado do c�lculo vai ser zero
        [Theory]
        [InlineData("DUR", "RPR", "GUM", "EXA")]                    // Evento v�lido exclu�do
        [InlineData("DPA", "RPR", "GGE", "EXI")]                    // Evento v�lido exclu�do
        [InlineData("DCA", "RPR", "GAC", "EXC")]                    // Evento v�lido exclu�do
        public void ResultadoDeveSerZeroQuandoEventoComStatusEventoExcluido(
            string estadoOperativo,
            string condicaoOperativa,
            string origem,
            string statusEvento)
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                 new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Parse("20/05/2017"), estadoOperativo, condicaoOperativa, origem, statusEvento, 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2017, 05, 01), new DateTime(2017, 05, 01), null, "1", potencias, suspensoes, eventos);


            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2017, 05, 01)),
                uge,
                "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(0, parametroHedp);
        }

        // Dado que eu tenho data entrada opera��o comercial
        // E que eu tenho data evento EOC
        // Quando tenho um evento durante uma suspens�o
        // Ent�o o resultado do c�lculo vai ser zero.
        [Theory]
        [InlineData("DUR", "RPR", "GCB")]                              // Evento v�lido
        [InlineData("DPR", "RPR", "GUM")]                              // Evento v�lido
        [InlineData("DPA", "RPR", "GOT")]                              // Evento v�lido
        [InlineData("DCA", "RPR", "GGE")]                              // Evento v�lido
        public void ResultadoDeveSerZeroQuandoEventoValidoOcorrerDuranteUmaSuspensao(string estado, string condicao, string origem)
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
           {
                new SuspensaoUnidadeGeradoraRequest("1","1","1",new DateTime(2019,01,10),new DateTime(2019,01,15)),
           };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                 new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 11), estado, condicao, origem, "AGE", 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), null, "1", potencias, suspensoes, eventos);



            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert

            Assert.Equal(0, parametroHedp);
        }

        [Theory]
        [InlineData("DPR", "RPR", "GAG")]                              // Evento v�lido
        [InlineData("DCA", "RPR", "GGE")]                              // Evento v�lido
        public void ResultadoDeveSerNullQuandoSuspensaoDurarMesReferencia(string estado, string condicao, string origem)
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
           {
               new SuspensaoUnidadeGeradoraRequest("1","1","1", new DateTime(2019, 01, 01), new DateTime(2019, 02, 01)),
           };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                 new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 11), estado, condicao, origem, "AGE", 10)
             };


            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };
            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), new DateTime(2019, 01, 02), "1", potencias, suspensoes, eventos);



            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Null(parametroHedp);
        }

        // Dado que eu tenho data entrada opera��o comercial
        // E que eu tenho data evento EOC
        // maiores que a data de refer�ncia
        // Ent�o o resultado do c�lculo vai ser null.
        [Theory]
        [InlineData("DUR", "RPR", "GAG")]                              // Evento v�lido
        [InlineData("DCA", "RPR", "GAC")]                              // Evento v�lido
        public void ResultadoDeveSerNullQuandoDataEntradaOperacaoEEventoEOCMaioresQueDataReferencia(
            string estado,
            string condicao,
            string origem)
        {
            // Arrange

            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 11), estado, condicao, origem, "AGE", 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 03, 01), new DateTime(2019, 03, 01), null, "1", potencias, suspensoes, eventos);


            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Null(parametroHedp);
        }

        [Theory]
        [InlineData("DEM", "", "GTR")]                           // Evento inv�lido
        [InlineData("DAU", "", "GAG")]                           // Evento inv�lido
        [InlineData("DCA", "", "GOU")]                           // Evento inv�lido
        [InlineData("DAP", "", "GRH")]                           // Evento inv�lido
        [InlineData("LIG", "", "GAC")]                        // Evento inv�lido
        [InlineData("DCO", "", "GUM")]                           // Evento inv�lido
        [InlineData("REP", "", "GAG")]                           // Evento inv�lido
        public void ResultadoDeveSerZeroQuandoEstadoOperativoInvalidoCondicaoVaziaOrigemValida(
            string estado,
            string condicao,
            string origem)
        {
            // Arrange

            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                  new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 01), estado, condicao, origem, "AGE", 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), null, "1", potencias, suspensoes, eventos);


            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(0, parametroHedp);
        }

        [Theory]
        [InlineData(
           "DPR", "RPR", "GOT", "EXA",                                 // Evento v�lido exclu�do
           "DUR", "RPR", "GAC", "AGE",                                 // Evento v�lido
           "DCA", "RPR", "GAG", "EVR",                                 // Evento v�lido
           88.658333333333)]
        public void DeveCalcularEventoValidoAposUmEventoExcluidoConsiderandoHorasEMinutos(
           string estadoOperativo,
           string condicaoOperativa,
           string origem,
           string statusEvento,
           string estadoOperativo2,
           string condicaoOperativa2,
           string origem2,
           string statusEvento2,
           string estadoOperativo3,
           string condicaoOperativa3,
           string origem3,
           string statusEvento3,
           double esperado)
        {
            // Arrange

            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Parse("27/01/2001 07:00:00"), estadoOperativo, condicaoOperativa, origem, statusEvento, 10),
                new EventoMudancaEstadoOperativoRequest("2","2",DateTime.Parse("28/01/2001 07:20:30"), estadoOperativo2, condicaoOperativa2, origem2, statusEvento2, 10),
                new EventoMudancaEstadoOperativoRequest("3","3",DateTime.Parse("28/01/2001 08:20:00"), estadoOperativo3, condicaoOperativa3, origem3, statusEvento3, 10)
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2001, 01, 01), new DateTime(2001, 01, 01), null, "1", potencias, suspensoes, eventos);

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2001, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(esperado, Math.Round(parametroHedp.Value, 12));
        }

        [Theory]
        [InlineData("DAU", "RFO", "GUM", "AGE")]                    // Evento inv�lido
        [InlineData("DEM", "RFO", "GAG", "AGE")]                    // Evento inv�lido
        [InlineData("DPA", "RFO", "GOU", "AGE")]                    // Evento inv�lido
        [InlineData("DPR", "NOR", "GIC", "AGE")]                    // Evento inv�lido
        [InlineData("DAU", "RPR", "GIS", "AGE")]                    // Evento inv�lido
        [InlineData("DEM", "RPR", "GVO", "AGE")]                    // Evento inv�lido
        [InlineData("RDP", "RPR", "GMT", "AGE")]                    // Evento inv�lido
        [InlineData("DAP", "RPR", "GVO", "AGE")]                    // Evento inv�lido
        [InlineData("DCO", "RPR", "GVO", "AGE")]                    // Evento inv�lido
        public void ResultadoDoCalculoDeveSerZeroQuandoTodosOsEventosSaoInvalidos(
            string estadoOperativo,
            string condicaoOperativa,
            string origem,
            string statusEvento)
        {
            // Arrange

            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
            };

            var eventos = new EventoMudancaEstadoOperativoRequest[]
             {
                new EventoMudancaEstadoOperativoRequest("1","1",new DateTime(2019, 01, 01), estadoOperativo, condicaoOperativa, origem, statusEvento, 100),
             };

            var potencias = new List<PotenciaUnidadeGeradoraRequest>
            {
                new PotenciaUnidadeGeradoraRequest("1","1",new DateTime(2000,1,1),null,150)
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2001, 01, 15), new DateTime(2001, 01, 15), null, "1", potencias, suspensoes, eventos);

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHdpRequest("1", new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)), uge, "1");

            // Act
            var parametroHedp = service.Calcular(calculoRequest).Result;

            // Assert
            // Assert
            Assert.Equal(0, parametroHedp);
        }
    }
}