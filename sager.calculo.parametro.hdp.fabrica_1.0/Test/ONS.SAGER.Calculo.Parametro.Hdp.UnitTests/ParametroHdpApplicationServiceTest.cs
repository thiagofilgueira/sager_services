using Moq;
using ONS.SAGER.Calculo.Parametro.Hdp.Application.Services;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hdp.UnitTests
{
    public class ParametroHdpApplicationServiceTest
    {
        private readonly IParametroService _parametroHdpService;
        private readonly IExecutionContextAbstraction _executionContext;
        private readonly IControleCalculoParametroRepository _controleCalculoParametroRepository;
        private readonly ICalculoLogger<ParametroApplicationService> _calculoLogger;

        public ParametroHdpApplicationServiceTest()
        {
            var serviceMock = new Mock<IParametroService>();
            serviceMock.Setup(p => p.Calcular(It.IsAny<CalcularParametroHdpRequest>())).ReturnsAsync(It.IsAny<double>());
            _parametroHdpService = serviceMock.Object;

            var contextMock = new Mock<IExecutionContextAbstraction>();
            contextMock.Setup(p => p.SetBranch(It.IsAny<string>()));
            _executionContext = contextMock.Object;

            var controleCalculoParametroRepository = new Mock<IControleCalculoParametroRepository>();
            _controleCalculoParametroRepository = controleCalculoParametroRepository.Object;

            _calculoLogger = new Mock<ICalculoLogger<ParametroApplicationService>>().Object;
        }

        // Teste para verificar se retorna InternalError quando 
        // � passado um objeto parametroRequest null 
        [Fact(DisplayName = "Teste para verificar se retornar Bad Request quando o resultado � igual a null")]
        public async Task DeveRetornarStatusBadRequestQuandoCalcularParametroHDPRequestForNulo()
        {

            var serviceMock = new Mock<IParametroService>();

            var parametro = new ParametroApplicationService(serviceMock.Object, _executionContext, _controleCalculoParametroRepository, _calculoLogger);

            CalcularParametroHdpRequest parametroRequest = null;

            var result = await parametro.Calcular(parametroRequest);

            var statusResult = new Result(ResultStatus.BadRequest);
            Assert.True(result.Status == statusResult.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna Bad Request quando a requisi��o � vazio")]
        public async Task DeveRetornarBadRequestQuandoCalcularParametroHDPRequestForInvalido()
        {
            var parametro = new ParametroApplicationService(_parametroHdpService, _executionContext, _controleCalculoParametroRepository, _calculoLogger);

            var parametroRequest = new CalcularParametroHdpRequest(null, new ConsolidacaoMensalRequest(null, DateTime.MinValue), null, "1");

            var result = await parametro.Calcular(parametroRequest);

            var statusResult = new Result(ResultStatus.BadRequest);
            Assert.True(result.Status == statusResult.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna sucesso quando a requisi��o est� preenchida corretamente")]
        public void DeveRetornarSucessQuandoCalcularParametroHDPRequestForValido()
        {
            var parametro = new ParametroApplicationService(_parametroHdpService, _executionContext, _controleCalculoParametroRepository, _calculoLogger);

            var parametroRequest = new CalcularParametroHdpRequest(
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now.ToFirstDayOfMonth()),
                new UnidadeGeradoraRequest("1", "1", "1", DateTime.Now, DateTime.Now.AddDays(1), DateTime.Now.AddDays(3), "1", new List<PotenciaUnidadeGeradoraRequest>(),
                new List<SuspensaoUnidadeGeradoraRequest>()
                {
                    new SuspensaoUnidadeGeradoraRequest("1","1","1", DateTime.Now, DateTime.Now.AddDays(2))
                },
                new List<EventoMudancaEstadoOperativoRequest>()
                {
                    new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Now,"LIG","RPR","GCI","",10)
                }),
                "1"
                );

            var result = parametro.Calcular(parametroRequest).Result;

            Assert.Equal(ResultStatus.Success, result.Status);
        }
    }
}


