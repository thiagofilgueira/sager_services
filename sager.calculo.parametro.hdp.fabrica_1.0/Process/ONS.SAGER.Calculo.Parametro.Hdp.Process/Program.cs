﻿using ONS.SDK.Impl.Builder;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            AppBuilder.CreateDefaultBuilder(null)
                .UseStartup<Startup>()
                .RunSDK();
        }
    }
}
