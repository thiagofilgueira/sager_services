﻿using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Application.Interfaces
{
    public interface IParametroApplicationService
    {
        Task<Result> Calcular(CalcularParametroHdpRequest request);
    }
}
