﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hdp.Application.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Application.Services;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroApplicationService, ParametroApplicationService>();
        }
    }
}
