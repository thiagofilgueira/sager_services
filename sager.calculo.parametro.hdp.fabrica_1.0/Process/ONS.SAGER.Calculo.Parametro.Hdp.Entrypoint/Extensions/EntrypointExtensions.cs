﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SDK.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hdp.Entrypoint.Services;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Entrypoint.Extensions
{
    public static class EntrypointExtensions
    {
        public static void AddEntrypointServices(this IServiceCollection services)
        {
            services.BindEvents<CalculosEntrypoint>();
            services.AddSingleton<CalculosEntrypoint>();
        }
    }
}
