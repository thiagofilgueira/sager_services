﻿using ONS.SAGER.Calculo.Parametro.Hdp.Application.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly IParametroApplicationService _calculoHdpApplicationService;
        public CalculosEntrypoint(IParametroApplicationService calculoHdpApplicationService)
        {
            _calculoHdpApplicationService = calculoHdpApplicationService;
        }

        [SDKEvent(Eventos.CalcularParametro)]
        public async Task CalcularParameroHdp(CalcularParametroPayload payload)
        {
           await _calculoHdpApplicationService.Calcular(
               new CalcularParametroHdpRequest(
                    payload?.ConfiguracaoCenarioIdPayload,
                    payload?.ConsolidacaoMensalPayload?.MapToRequest(),
                    payload?.UnidadeGeradoraPayload?.MapToRequest(),
                    payload?.ControleCalculoIdPayload)
               );          
        }

    }
}
