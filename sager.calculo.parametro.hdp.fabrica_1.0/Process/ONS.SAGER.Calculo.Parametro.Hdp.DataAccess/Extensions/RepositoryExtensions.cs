﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hdp.DataAccess.Repositories;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Parametro.Hdp.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IControleCalculoParametroRepository, ControleCalculoParametroRepository>();
        }
    }
}
