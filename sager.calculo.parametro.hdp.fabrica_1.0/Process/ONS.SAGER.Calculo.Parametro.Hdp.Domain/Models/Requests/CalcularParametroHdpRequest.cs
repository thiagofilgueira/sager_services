﻿using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests
{
    public class CalcularParametroHdpRequest : CalcularParametroRequest
    {
        public CalcularParametroHdpRequest(
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal,
            UnidadeGeradoraRequest unidadeGeradora,
            string controleCalculoId)
        : base(
              unidadeGeradora,
              controleCalculoId,
              configuracaoCenarioId,
              consolidacaoMensal)
        {
        }
    }
}
