﻿using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Domain.Services
{
    public class ParametroService : ParametroServiceBase, IParametroService
    {
        public ParametroService(
            IParametroRepository parametroRepository,
            IInsumoCalculoService insumoCalculoService)
            : base(parametroRepository, insumoCalculoService)
        {
        }

        public async Task<double?> Calcular(CalcularParametroHdpRequest request)
        {
            double? valorParametro = await CalcularParametro(
                request.UnidadeGeradora.MapToEntity(),
                request.ConsolidacaoMensal.DataReferencia,
                ObterRestricoesHdp());

            if (!valorParametro.HasValue)
            {
                return null;
            }

            await SalvarParametroVersionado(
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.UnidadeGeradora.UnidadeGeradoraId,
                request.ConfiguracaoCenarioId,
                request.ControleCalculoId,
                TipoCalculo.HDP,
                valorParametro.Value);

            return valorParametro;
        }

        private List<RestricoesCalculoParametro> ObterRestricoesHdp()
        {
            return new List<RestricoesCalculoParametro>()
            {
                new RestricoesCalculoParametro(
                    EstrategiaCalculoParametro.HorasDeEvento,
                    new TipoClassificadorOrigem[]
                    {
                        TipoClassificadorOrigem.GUM,
                        TipoClassificadorOrigem.GGE,
                        TipoClassificadorOrigem.GTR,
                        TipoClassificadorOrigem.GOT,
                        TipoClassificadorOrigem.GAC,
                        TipoClassificadorOrigem.GAG,
                        TipoClassificadorOrigem.GCB
                    },
                    new TipoEstadoOperativo[]
                    {
                        TipoEstadoOperativo.DPR,
                        TipoEstadoOperativo.DUR,
                        TipoEstadoOperativo.DPA,
                        TipoEstadoOperativo.DCA
                    },
                    null)
            };
        }
    }
}
