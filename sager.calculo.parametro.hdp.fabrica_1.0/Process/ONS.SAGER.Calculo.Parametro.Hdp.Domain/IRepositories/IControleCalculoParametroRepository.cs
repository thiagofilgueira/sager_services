﻿using ONS.SAGER.Calculo.Util.DataAccess.Interfaces;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Domain.IRepositories
{
    public interface IControleCalculoParametroRepository : IControleCalculoParametroTaxaRepositoryBase
    {
    }
}
