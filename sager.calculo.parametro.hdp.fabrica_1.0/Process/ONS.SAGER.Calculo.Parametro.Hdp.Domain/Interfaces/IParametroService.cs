﻿using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Domain.Interfaces
{
    public interface IParametroService : IParametroServiceBase
    {
        Task<double?> Calcular(CalcularParametroHdpRequest request);
    }
}
