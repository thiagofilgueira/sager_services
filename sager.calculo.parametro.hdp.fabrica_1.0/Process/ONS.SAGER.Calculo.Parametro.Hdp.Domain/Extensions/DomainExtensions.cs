﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hdp.Domain.Services;

namespace ONS.SAGER.Calculo.Parametro.Hdp.Domain.Extensions
{
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroService, ParametroService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }
    }
}
