Remove-Item $Env:mcalculo_deploy\publish -Recurse -ErrorAction Ignore

dotnet restore 
dotnet publish -o $Env:mcalculo_deploy\publish
copy Mapa $Env:mcalculo_deploy\publish
copy Mapa/*.yaml $Env:mcalculo_deploy\publish\Mapa
copy Metadados $Env:mcalculo_deploy\publish
copy Metadados/*.yaml $Env:mcalculo_deploy\publish\Metadados
copy Dockerfile $Env:mcalculo_deploy\publish
copy plataforma.json $Env:mcalculo_deploy\publish

pipenv run powershell.exe -File $Env:mcalculo_deploy\deploy_1.ps1;