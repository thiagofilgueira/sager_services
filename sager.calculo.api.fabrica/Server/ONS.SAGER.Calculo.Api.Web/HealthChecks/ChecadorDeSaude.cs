﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using ONS.SAGER.Calculo.Api.Web.Configurations;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.HealthChecks
{
    /// <summary>
    /// Checador de saúde para chamadas Http
    /// </summary>
    public class HttpChecadorDeSaude : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var response = await client.GetAsync(ConfiguracoesAplicacao.ChecadorDeSaudeHttpDev);

                    if (!response.IsSuccessStatusCode)
                        return HealthCheckResult.Unhealthy();
                }
                catch (Exception ex)
                {
                    var e = ex.Message;
                    return HealthCheckResult.Unhealthy();
                }
            }

            return HealthCheckResult.Healthy();
        }
    }
}
