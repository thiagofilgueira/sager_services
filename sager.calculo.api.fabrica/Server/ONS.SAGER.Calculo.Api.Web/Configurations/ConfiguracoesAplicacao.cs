﻿namespace ONS.SAGER.Calculo.Api.Web.Configurations
{
    public static class ConfiguracoesAplicacao
    {
        public static  readonly string ChecadorDeSaudeHttpDev = @"http://localhost:54583/api/v1/teste";
        public static readonly string UrlStatusAplicacao = @"http://localhost:54583/saude-aplicacao";
    }
}
