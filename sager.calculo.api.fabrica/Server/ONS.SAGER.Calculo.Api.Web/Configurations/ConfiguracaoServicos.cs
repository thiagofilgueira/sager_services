﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.Application.Extensions;
using ONS.SAGER.Calculo.Api.DataAccess.Extensions;
using ONS.SAGER.Calculo.Api.Domain.Extensions;
using ONS.SAGER.Calculo.Api.Domain.Map;
using ONS.SAGER.Calculo.Api.Exportador.Extensions;
using ONS.SAGER.Calculo.Api.Web.Entrypoint;
using ONS.SAGER.Calculo.Api.Web.HealthChecks;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Extensions;
using ONS.SAGER.Calculo.Util.Swagger;
using ONS.SDK.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Reflection;

namespace ONS.SAGER.Calculo.Api.Web.Configurations
{
    public static class ConfiguracaoServicos
    {
        public static void ResolverDependencias(this IServiceCollection services)
        {
            //TODO: REMOVER APÓS TESTES DE REPROCESSAMENTO
            services.BindEvents<CalculoEntrypoint>();
            services.AddSingleton<CalculoEntrypoint>();

            services.UseSDK();
            services.AddExportadorServices();
            services.AddApplicationServices();
            services.AddDomainServices();
            services.AddRepositoryServices();
            services.UseDataMap<EntitiesMap>();
            services.AddExecutionContextAbstraction();
        }
        
        public static void ConfigurarGDPR(this IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        }

        public static void InserirCors(this IServiceCollection services) => services.AddCors();

        public static void RemoverValoresNulos(this IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(opcoes =>
            {
                opcoes.SerializerSettings.NullValueHandling =
                    Newtonsoft.Json.NullValueHandling.Ignore;
            });
        }

        public static void InserirCacheDeResponse(this IServiceCollection services) => services.AddResponseCaching();

        public static void InserirCacheDeMemoria(this IServiceCollection services) => services.AddMemoryCache();

        public static void InserirCompressaoDeRequest(this IServiceCollection services)
        {
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.EnableForHttps = true;
            });
        }

        public static void ConfigurarSaudeDaAplicacao(this IServiceCollection services)
        {
            services.AddHealthChecksUI();

            services.AddHealthChecks().AddCheck<HttpChecadorDeSaude>("http");
        }

        public static void HabilitarSwagger(this IServiceCollection services) => services.AddSwaggerDocs((options) => 
        {
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            options.IncludeXmlComments(xmlPath);
        });
    }
}
