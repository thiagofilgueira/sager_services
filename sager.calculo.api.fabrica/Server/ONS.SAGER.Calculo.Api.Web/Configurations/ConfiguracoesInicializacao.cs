﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using ONS.SAGER.Calculo.Util.Swagger;
using System;
using System.Linq;
using System.Net.Mime;

namespace ONS.SAGER.Calculo.Api.Web.Configurations
{
    public static class ConfiguracoesInicializacao
    {
        public static void InicializarCors(this IApplicationBuilder app)
        {
            app.UseCors(builder => builder
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader()
             .AllowCredentials());

            app.UseCors("*");
        }

        public static void InicializarCompressaoECache(this IApplicationBuilder app)
        {
            app.UseResponseCompression();
            app.UseResponseCaching();
        }

        public static void InicializarHSTS(this IApplicationBuilder app) => app.UseHsts();

        public static void InicializarSwagger(this IApplicationBuilder app)
        {
            app.UseSwaggerDocs();
        }

        public static void InicializarRedicionamentoAutomaticoDeHTTPS(this IApplicationBuilder app) => app.UseHttpsRedirection();

        public static void InicializarMVC(this IApplicationBuilder app) => app.UseMvc();

        public static void InicializarPaginaDeErroDeDesenvolvimento(this IApplicationBuilder app) => app.UseDeveloperExceptionPage();

        public static void InicializarPoliticasDeCookie(this IApplicationBuilder app) => app.UseCookiePolicy();

        public static void InicializarSaudeDaAplicacao(this IApplicationBuilder app)
        {
            app.UseHealthChecks("/status",
              new HealthCheckOptions()
              {
                  ResponseWriter = async (context, report) =>
                  {
                      var result = JsonConvert.SerializeObject(
                          new
                          {
                              statusApplication = report.Status.ToString(),
                              healthChecks = report.Entries.Select(e => new
                              {
                                  ItemChecado = e.Key,
                                  MensagemDeErro = e.Value.Exception?.Message,
                                  Status = Enum.GetName(typeof(HealthStatus), e.Value.Status)
                              })
                          });
                      context.Response.ContentType = MediaTypeNames.Application.Json;
                      await context.Response.WriteAsync(result);
                  }
              });

            //healthchecks-data-ui
            // Gera o endpoint que retornará os dados utilizados no dashboard
            app.UseHealthChecks("/healthchecks-data-ui", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            // Ativa o dashboard para a visualização da situação de cada Health Check
            app.UseHealthChecksUI(config =>
            {
                config.UIPath = "/status-aplicacao";
                config.AddCustomStylesheet("dotnet.css");
            });
        }

        public static void InicializarArquivosEstaticos(this IApplicationBuilder app)
            => app.UseStaticFiles();
    }
}
