using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.Web.Configurations;
using ONS.SAGER.Calculo.Util.Configuration;

namespace ONS.SAGER.Calculo.Api.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAppSettings(Configuration);
            services.ResolverDependencias();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.ConfigurarGDPR();
            services.InserirCors();
            services.RemoverValoresNulos();
            services.InserirCacheDeResponse();
            services.InserirCacheDeMemoria();
            services.InserirCompressaoDeRequest();
            //services.ConfigurarSaudeDaAplicacao();
            services.HabilitarSwagger();


        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.InicializarHSTS();
            }

            app.InicializarCors();
            app.InicializarCompressaoECache();
            app.InicializarSwagger();
            app.InicializarRedicionamentoAutomaticoDeHTTPS();
            app.InicializarPoliticasDeCookie();
            //app.InicializarSaudeDaAplicacao();
            app.InicializarArquivosEstaticos();
            app.InicializarMVC();
        }
    }
}