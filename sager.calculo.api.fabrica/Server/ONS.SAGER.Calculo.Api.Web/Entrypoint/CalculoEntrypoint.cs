﻿using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models;
using ONS.SAGER.Calculo.Api.Domain.Models.Payload;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SDK.Context;
using ONS.SDK.Data;
using ONS.SDK.Domain.Base;
using ONS.SDK.Worker;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.Entrypoint
{
    public class CalculoEntrypoint
    {
        private readonly IEventoMudancaEstadoOperativoRepository _eventoMudancaEstadoOperativoRepository;
        private readonly IUnidadeGeradoraRepository _unidadeGeradoraRepository;
        private readonly IExecutionContextAbstraction _executionContextAbstraction;
        private readonly IExecutionContext _executionContext;
        private readonly IDataTracker _dataTracker;

        public CalculoEntrypoint(
            IEventoMudancaEstadoOperativoRepository eventoMudancaEstadoOperativoRepository,
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            IExecutionContextAbstraction executionContextAbstraction,
            IExecutionContext executionContext,
            IDataTracker dataTracker)
        {
            _eventoMudancaEstadoOperativoRepository = eventoMudancaEstadoOperativoRepository;
            _unidadeGeradoraRepository = unidadeGeradoraRepository;
            _executionContextAbstraction = executionContextAbstraction;
            _executionContext = executionContext;
            _dataTracker = dataTracker;
        }

        [SDKEvent("alterarevento.request")]
        public async Task AlterarEvento(AlterarEventoPayload payload)
        {
            try
            {
                if (_executionContext.IsReprocessing)
                {
                    return;
                }

                _executionContextAbstraction.SetBranch("master");
                var eventos = await _eventoMudancaEstadoOperativoRepository.ObterEventosUnidadeGeradora("BAUSB-0UG6", new[] { "77a5585e-7386-47d0-a117-f67231df2b79" });

                //foreach (var evento in eventos)
                //{
                //    evento.NumeroOns = "teste" + DateTime.Now.ToString();
                //}

                var teste = eventos.ElementAt(1);

                teste.NumeroOns = "teste" + DateTime.Now.ToString(); ;

                await _eventoMudancaEstadoOperativoRepository.UpdateAsync(teste);

                //var teste2 = eventos.ElementAt(2);

                //teste2.NumeroOns = "teste" + DateTime.Now.ToString(); ;

                //await _eventoMudancaEstadoOperativoRepository.UpdateAsync(teste2);


                //var unidadeGeradora = new UnidadeGeradora()
                //{
                //    DataDesativacao = null,
                //    DataEntradaOperacao = DateTime.Now,
                //    DataEventoEOC = DateTime.Now,
                //    IdoOns = "teste",
                //    UnidadeGeradoraId = "teste",
                //    UsinaId = "BAUSB"
                //};

                //await _unidadeGeradoraRepository.AddAsync(unidadeGeradora);



                //if (_executionContext?.IsReprocessing ?? true)
                //{
                //    return;
                //}
                //_executionContextAbstraction.SetBranch("master");

                //var eventoId = "b8581875-9467-4066-b028-70eded05c8fb";
                //var consolidacaoMensalId = "26124b82-6a08-4985-9f7e-fd390a418789";
                //var ugeId = "BAUSB-0UG6";


                //var ugeModificada = uges.FirstOrDefault(p => p.UnidadeGeradoraId == ugeId);
                //ugeModificada.IdoOns = "teste" + DateTime.Now.ToString();

                //await _unidadeGeradoraRepository.UpdateAsync(ugeModificada);



                //_dataContextBuilderAbstraction.TrackEntities(eventos);

                //var evento = eventos.FirstOrDefault();

                //evento.NumeroOns = "teste" + DateTime.Now.ToString();

                //await _eventoMudancaEstadoOperativoRepository.UpdateAsync(evento);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
