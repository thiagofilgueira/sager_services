using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ONS.SDK.Extensions.Builder;

namespace ONS.SAGER.Calculo.Api.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var envport = System.Environment.GetEnvironmentVariable("PORT");

            if (args.Length > 0)
            {
                envport = args[0];
            }

            var URL_REDIRECT_DEBUG = Environment.GetEnvironmentVariable("URL_REDIRECT_DEBUG");
            var INSTANCE_ID = Environment.GetEnvironmentVariable("INSTANCE_ID");
            if (!string.IsNullOrEmpty(URL_REDIRECT_DEBUG) && !string.IsNullOrEmpty(INSTANCE_ID))
            {
                HttpResponseMessage response = null;
                try
                {
                    URL_REDIRECT_DEBUG = URL_REDIRECT_DEBUG.Replace("{instanceId}", INSTANCE_ID);
                    Console.WriteLine($"DEBUG MODE -- CONNECTION TOOLS TO DEBUG! URL_REDIRECT_DEBUG={URL_REDIRECT_DEBUG}");
                    var client = new HttpClient();
                    var request = client.GetAsync(URL_REDIRECT_DEBUG);
                    response = request.Result;

                    if (response.IsSuccessStatusCode)
                    {
                        return;
                    }
                    else
                    {
                        Console.WriteLine($"Error {response.StatusCode}: {response.ReasonPhrase}.");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex);
                }

                return;
            }

            var webHost = CreateWebHostBuilder(args);

            if (!string.IsNullOrEmpty(envport))
            {
                webHost.UseUrls("http://*:" + envport);
            }

            webHost.RunSDK();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
