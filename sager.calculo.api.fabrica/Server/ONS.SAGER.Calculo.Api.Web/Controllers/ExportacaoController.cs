﻿using Microsoft.AspNetCore.Mvc;
using ONS.SAGER.Calculo.Api.Application.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using System;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.Controllers
{
    /// <summary>
    /// Controller que contém os endpoint para recuperação de taxas
    /// </summary>
    [Produces("application/json")]
    [ResponseCache(Duration = 50)]
    [Route("[controller]")]
    [ApiController]
    public class ExportacaoController : ControllerBase
    {
        private readonly IExportacaoTaxaApplicationService _exportacaoTaxaApplicationService;

        /// <summary>
        /// Construtor do Controller de taxas
        /// </summary>
        /// <param name="exportacaoTaxaApplicationService"></param>
        public ExportacaoController(IExportacaoTaxaApplicationService exportacaoTaxaApplicationService)
        {
           _exportacaoTaxaApplicationService = exportacaoTaxaApplicationService;
        }

        [HttpPost]
        [Route("v1/taxa")]
        [ProducesResponseType(200, Type = typeof(FileContentResult))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> ExportarPlanilhaTaxa([FromBody] ExportacaoTaxaRequest request)
        {
            try
            {
                var arquivo = await _exportacaoTaxaApplicationService.ObterArquivoExportacaoTaxa(request);

                return File(arquivo.Conteudo, arquivo.Extensao, arquivo.Nome);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
