﻿using Microsoft.AspNetCore.Mvc;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Payload;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SDK.Services.ProcessMemory;
using ONS.SDK.Worker;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.Controllers
{
    /// <summary>
    /// Controller de calculo de usina
    /// </summary>
    [Produces("application/json")]
    [ResponseCache(Duration = 50)]
    [Route("[controller]")]
    [ApiController]
    public class PeriodoApuracaoController : ControllerBase
    {
        private readonly string _branchMaster = "master";
        private readonly IUsinaRepository _usinaRepository;
        private readonly IUnidadeGeradoraRepository _unidadeGeradoraRepository;
        private readonly ISuspensaoUnidadeGeradoraRepository _suspensaoUnidadeGeradoraRepository;
        private readonly IPotenciaUnidadeGeradoraRepository _potenciaUnidadeGeradoraRepository;
        private readonly IEventoMudancaEstadoOperativoRepository _eventoMudancaEstadoOperativoRepository;
        private readonly IConsolidacaoMensalRepository _consolidacaoMensalRepository;
        private readonly IExecutionContextAbstraction _executionContextAbstraction;
        private readonly IInsumoCalculoService _insumoCalculoService;
        private readonly ISDKWorker _sdkWorker;

        /// <summary>
        /// Construtor do Controller de calculo de usina
        /// </summary>
        /// <param name="usinaRepository"></param>
        /// <param name="consolidacaoMensalRepository"></param>
        /// <param name="executionContextAbstraction"></param>
        public PeriodoApuracaoController(
            IUsinaRepository usinaRepository,
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            IEventoMudancaEstadoOperativoRepository eventoMudancaEstadoOperativoRepository,
            IConsolidacaoMensalRepository consolidacaoMensalRepository,
            IExecutionContextAbstraction executionContextAbstraction,
            IInsumoCalculoService insumoCalculoService,
            ISDKWorker sdkWorker)
        {
            _usinaRepository = usinaRepository;
            _suspensaoUnidadeGeradoraRepository = suspensaoUnidadeGeradoraRepository;
            _potenciaUnidadeGeradoraRepository = potenciaUnidadeGeradoraRepository;
            _eventoMudancaEstadoOperativoRepository = eventoMudancaEstadoOperativoRepository;
            _unidadeGeradoraRepository = unidadeGeradoraRepository;
            _consolidacaoMensalRepository = consolidacaoMensalRepository;
            _executionContextAbstraction = executionContextAbstraction;
            _insumoCalculoService = insumoCalculoService;
            _sdkWorker = sdkWorker;
        }

        /// <summary>
        /// Calculo de usina
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [Route("calcularusina")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CalcularUsina([FromBody] PeriodoApuracaoRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest();
                }

                request.Validar();

                if (request.Invalido)
                {
                    return BadRequest(request.MensagensNotificacoesAgrupadas);
                }

                var consolidacaoMensal = await _consolidacaoMensalRepository
                    .ObterConsolidacaoPorAnoMes(request.DataReferencia.Year, request.DataReferencia.Month, _branchMaster);

                if (consolidacaoMensal is null)
                {
                    return BadRequest("Data de cálculo inválida.");
                }


                var usina = await _usinaRepository.ObterUsinaPorId("BAUSB", _branchMaster);

                if(usina is null)
                {
                    return BadRequest("Usina inválida.");
                }

                var uges = await _unidadeGeradoraRepository.ObterUnidadesGeradorasPorUsina("BAUSB");

                foreach(var uge in uges)
                {

                    var calcularUgePayload = new CalcularUnidadeGeradoraPayload()
                    {
                        ConfiguracaoCenarioIdPayload = _branchMaster,
                        ConsolidacaoMensalPayload = consolidacaoMensal.MapToPayload(),
                        ControleCalculoIdPayload = Guid.NewGuid().ToString(),
                        UsinaPayload = usina.MapToPayload(),
                        UnidadeGeradoraId = uge.UnidadeGeradoraId
                    };

                    await _executionContextAbstraction.EmitirEvento(Eventos.CalcularUnidadeGeradora, calcularUgePayload);
                }


                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("calcularperiodo")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CalcularPeriodo([FromBody] PeriodoApuracaoRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest();
                }

                var consolidacaoMensal = await _consolidacaoMensalRepository
                    .ObterConsolidacaoPorAnoMes(request.DataReferencia.Year, request.DataReferencia.Month, _branchMaster);

                if (consolidacaoMensal is null)
                {
                    return BadRequest("Data de cálculo inválida.");
                }

                var payload = new CalcularUsinaPayload()
                {
                    ConfiguracaoCenarioIdPayload = _branchMaster,
                    ConsolidacaoMensalPayload = consolidacaoMensal.MapToPayload(),
                    //UsinaIdPayload = "RJSBRA"
                    UsinaIdPayload = "ROUHJI"
                };

                await _executionContextAbstraction.EmitirEvento(Eventos.CalcularUsina, payload);

                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("calculartaxamensal")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CalcularTaxaMensal([FromBody] PeriodoApuracaoRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest();
                }

                var consolidacaoMensal = await _consolidacaoMensalRepository
                    .ObterConsolidacaoPorAnoMes(request.DataReferencia.Year, request.DataReferencia.Month, _branchMaster);

                if (consolidacaoMensal is null)
                {
                    return BadRequest("Data de cálculo inválida.");
                }

                var usina = await _usinaRepository.ObterUsinaPorId("BAUSB");

                var payload = new CalcularTaxaPayload()
                {
                    ConfiguracaoCenarioIdPayload = _branchMaster,
                    ConsolidacaoMensalPayload = consolidacaoMensal.MapToPayload(),
                    UsinaPayload = usina.MapToPayload(),
                    ControleCalculoIdPayload = DateTime.Now.ToString()
                };

                await _executionContextAbstraction.EmitirEvento(Eventos.CalcularTaxaMes, payload, consolidacaoMensal.DataReferencia);

                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("calculartaxaacumulada")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CalcularTaxaAcumulada([FromBody] PeriodoApuracaoRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest();
                }

                var consolidacaoMensal = await _consolidacaoMensalRepository
                    .ObterConsolidacaoPorAnoMes(request.DataReferencia.Year, request.DataReferencia.Month, _branchMaster);

                if (consolidacaoMensal is null)
                {
                    return BadRequest("Data de cálculo inválida.");
                }

                var usina = await _usinaRepository.ObterUsinaPorId("BAUSB");




                var payload = new CalcularTaxaPayload()
                {
                    ConfiguracaoCenarioIdPayload = _branchMaster,
                    ConsolidacaoMensalPayload = consolidacaoMensal.MapToPayload(),
                    UsinaPayload = usina.MapToPayload(),
                    ControleCalculoIdPayload = DateTime.Now.ToString()
                };

                await _executionContextAbstraction.EmitirEvento(Eventos.CalcularTaxaAcum, payload, consolidacaoMensal.DataReferencia);

                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("calcularparametros")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CalcularParametros([FromBody] PeriodoApuracaoRequest request)
        {
            try
            {
                if (request is null)
                {
                    return BadRequest();
                }

                var consolidacaoMensal = await _consolidacaoMensalRepository
                    .ObterConsolidacaoPorAnoMes(request.DataReferencia.Year, request.DataReferencia.Month, _branchMaster);

                if (consolidacaoMensal is null)
                {
                    return BadRequest("Data de cálculo inválida.");
                }

                var usina = await _usinaRepository.ObterUsinaPorId("BAUSB");
                if(usina is null)
                {
                    return BadRequest("Usina invalida inválida.");
                }

                var unidadesGeradoras = await _unidadeGeradoraRepository.ObterUnidadesGeradorasPorUsina(usina.UsinaId, _branchMaster);

                var uges = new[] { unidadesGeradoras.FirstOrDefault() };

                var eventos = await _eventoMudancaEstadoOperativoRepository.ObterEventosUnidadesGeradoras(
                    uges.Select(uge => uge.UnidadeGeradoraId),
                    new[] { consolidacaoMensal.ConsolidacaoMensalId },
                    _branchMaster);

                var potencias = (await _potenciaUnidadeGeradoraRepository.ObterPotenciasUnidadesGeradoras(uges.Select(uge => uge.UnidadeGeradoraId)))
                    .FiltrarPotenciasPorPeriodo(consolidacaoMensal.DataReferencia, consolidacaoMensal.DataReferencia.MesSeguinte());

                var suspensoes = (await _suspensaoUnidadeGeradoraRepository.ObterSuspensoesUnidadesGeradoras(uges.Select(uge => uge.UnidadeGeradoraId)))
                    .FiltrarSuspensoesPorPeriodo(consolidacaoMensal.DataReferencia, consolidacaoMensal.DataReferencia.MesSeguinte());

                
                foreach (var uge in uges)
                {
                    var eventosUge = eventos
                        .FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId);

                    var potenciasUge = potencias
                        .FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId);

                    var suspensoesUge = suspensoes
                        .FiltrarPorUnidadeGeradora(uge.UnidadeGeradoraId);

                    uge.AdicionarEventos(eventosUge);
                    uge.AdicionarPotencias(potenciasUge);
                    uge.AdicionarSuspensoes(suspensoesUge);

                              
                    await _executionContextAbstraction.EmitirEvento(
                        Eventos.CalcularParametro,
                        new CalcularParametroPayload()
                        {
                            ConfiguracaoCenarioIdPayload = _branchMaster,
                            ControleCalculoIdPayload = "teste" + DateTime.Now,
                            ConsolidacaoMensalPayload = consolidacaoMensal.MapToPayload(),
                            UnidadeGeradoraPayload = uge.MapToPayload(),
                            NiveisMontantesJusantesPayload = null
                        },
                        consolidacaoMensal.DataReferencia
                     );
                }

                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
           
            
            }
        }

        [HttpGet]
        [Route("consultarinsumo/{instanceId}")]
        public async Task<IActionResult> ConsultarInsumo(string instanceId)
        {
            try
            {
                var insumos = _insumoCalculoService.ObterInsumosCalculo(instanceId);
                return Ok(insumos);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);


            }
        }

        [HttpGet]
        [Route("alterarevento")]
        [ProducesResponseType(200, Type = typeof(void))]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AlterarEvento()
        {
            try
            {
                var eventoId = "943b2f11-cef4-43b7-9ca3-22d531c3fca0";

                var payload = new AlterarEventoPayload()
                {
                    EventoId = eventoId
                };

                _sdkWorker.Run(
                    payload,
                    "alterarevento.request");

                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
