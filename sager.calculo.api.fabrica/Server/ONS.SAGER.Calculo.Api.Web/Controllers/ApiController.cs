﻿using Microsoft.AspNetCore.Mvc;
using ONS.SAGER.Calculo.Api.Web.Configurations;
using System;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.Controllers
{
    /// <summary>
    /// Controller para testes da api
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        /// <summary>
        /// Endpoint para teste de api
        /// </summary>
        /// <returns>Retorna texto  caso api esteja funcionando</returns>
        [HttpGet]
        [Route("api/v1/teste")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(404)]
        public IActionResult TestarApi() => Ok("Api funcional");

        /// <summary>
        /// Endpoint para verficação da versão da api
        /// </summary>
        /// <returns>Retorna texto  contendo a versão da api e data</returns>
        [HttpGet]
        [Route("api/v1/versao")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(404)]
        public IActionResult RetornarVersao() => Ok($"Versão 1.0 {DateTime.UtcNow}");

        /// <summary>
        /// Endpoint para verificar saúde da aplicação
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v1/saude")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(404)]
        public IActionResult VerificarSaudeDaAplicacao()
        {
            return   Redirect(ConfiguracoesAplicacao.UrlStatusAplicacao);
        }
    }
}