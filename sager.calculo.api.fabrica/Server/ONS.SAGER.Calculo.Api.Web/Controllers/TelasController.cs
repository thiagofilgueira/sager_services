﻿using Microsoft.AspNetCore.Mvc;
using ONS.SAGER.Calculo.Api.Application.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.Models.ViewModels;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Web.Controllers
{
    /// <summary>
    /// Controller que contém os endpoint para recuperação de taxas
    /// </summary>
    [Produces("application/json")]
    [ResponseCache(Duration = 50)]
    [Route("[controller]")]
    [ApiController]
    public class TelasController : ControllerBase
    {
        private readonly IUsinaRepository _usinaRepository;
        private readonly IUnidadeGeradoraRepository _unidadesGeradorasRepository;
        private readonly IConsolidacaoMensalRepository _consolidacaoMensalRepository;


        public TelasController(
            IUsinaRepository usinaRepository,
            IUnidadeGeradoraRepository unidadesGeradorasRepository,
            IConsolidacaoMensalRepository consolidacaoMensalRepository)
        {
            _usinaRepository = usinaRepository;
            _unidadesGeradorasRepository = unidadesGeradorasRepository;
            _consolidacaoMensalRepository = consolidacaoMensalRepository;
        }

        [HttpGet]
        [Route("filtro")]
        public async Task<IActionResult> ObterDadosFiltro()
        {
            try
            {
                var consolicoesMensais = await _consolidacaoMensalRepository.ObterConsolidacoesAteDataReferencia(DateTime.Now);


                var usinas = await _usinaRepository.GetAllAsync();
                var usinasIds = usinas.Select(p => p.UsinaId);

                var unidadesGeradoras = await _unidadesGeradorasRepository.GetAllAsync();

                var agentesId = unidadesGeradoras.Select(p => p.AgenteProprietarioId).Distinct();

                var usinasViewModel = new List<UsinaViewModel>();

                var tiposTaxas = new TipoCalculo[]
                {
                    TipoCalculo.TEIPacum,
                    TipoCalculo.TEIFAacum,
                    TipoCalculo.TEIPmes,
                    TipoCalculo.TEIFAmes
                };


                foreach(var usina in usinas)
                {
                    var ugesUsina = unidadesGeradoras.Where(p => p.UsinaId == usina.UsinaId);
                    var agentesUsina = ugesUsina.Select(p => p.AgenteProprietarioId).Distinct();

                    var usinaViewModel = new UsinaViewModel()
                    {
                        UsinaId = usina.UsinaId,
                        DataDesativacao = usina.DataDesativacao,
                        DataEntradaOperacao = usina.DataEntradaOperacao.Value,
                        NomeCurtoUsina = usina.UsinaId,
                        TipoUsinaId = usina.Tipo,
                        AgentesProprietariosIds = agentesUsina
                    };

                    usinasViewModel.Add(usinaViewModel);
                }

                var filtroViewModel = new FiltroViewModel()
                {
                    Usinas = usinasViewModel,
                    AgentesProprietariosIds = agentesId,
                    DataMinima = consolicoesMensais.Min(p => p.DataReferencia),
                    TiposTaxasIds = tiposTaxas.Select(p => p.ToString()),
                    TiposUsinasIds = usinas.Select(p => p.Tipo).Distinct()
                };


                return Ok(filtroViewModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
