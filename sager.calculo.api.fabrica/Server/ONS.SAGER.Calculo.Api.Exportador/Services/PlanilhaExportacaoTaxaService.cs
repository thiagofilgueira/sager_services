﻿using Aspose.Cells;
using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.Enums.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Api.Exportador.Interfaces;
using ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa;
using ONS.SAGER.Calculo.Api.Exportador.ValueObjects.PlanilhaExportacaoTaxa;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Exportador.Services
{
    public class PlanilhaExportacaoTaxaService : ExportacaoServiceBase, IPlanilhaExportacaoTaxaService
    {
        public async Task<Workbook> ObterPlanilhaExportacaoTaxa(DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            if (dadosExportacaoTaxa is null)
            {
                throw new ArgumentNullException("Dados de Exportação de Taxa não encontrados.");
            }

            if (dadosExportacaoTaxa.Invalido)
            {
                throw new ArgumentException(dadosExportacaoTaxa.MensagensNotificacoesAgrupadas);
            }

            var configuracaoPlanilha = ObterConfiguracaoPlanilha(dadosExportacaoTaxa);

            var workbook = new Workbook();

            await Task.WhenAll(
                InserirAbaTaxas(workbook, configuracaoPlanilha, dadosExportacaoTaxa),
                InserirAbaEventos(workbook, configuracaoPlanilha, dadosExportacaoTaxa));

            return workbook;
        }

        private ConfiguracaoPlanilha ObterConfiguracaoPlanilha(DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(
                dadosExportacaoTaxa.TaxaExportacao.TipoCalculo,
                dadosExportacaoTaxa.DataReferencia);

            var titulosAbaTaxas = strategy.ObterTitulosAbaTaxas();
            var titulosAbaEventos = strategy.ObterTitulosAbaEventos();
            var colunaMesAno = strategy.ObterColunaMesAno();
            var colunaTaxaMensal = strategy.ObterColunaTaxaMensal();
            var colunaDadosUnidadeGeradoraTaxas = strategy.ObterColunaDadosUnidadeGeradoraTaxas();
            var colunaDadosUnidadeGeradoraEventos = strategy.ObterColunaDadosUnidadeGeradoraEventos();
            var colunaParametrosTeip = strategy.ObterColunaParametrosTeip();
            var colunaParametrosTeifa = strategy.ObterColunaParametrosTeifa();
            var colunaTeipReferencia = strategy.ObterColunaTeipReferencia();
            var colunaHorasTeipReferencia = strategy.ObterColunaHorasTeipReferencia();
            var colunaTeifaReferencia = strategy.ObterColunaTeifaReferencia();
            var colunaHorasMes = strategy.ObterColunaHorasMes();
            var tipoTaxaMensal = strategy.ObterTipoTaxaMensal();
            var tipoTeipAjustada = strategy.ObterTipoTeipAjustada();
            var tipoTeifaAjustada = strategy.ObterTipoTeifaAjustada();
            var sequenciaParametrosTeip = strategy.ObterSequenciaParametrosTeip();
            var sequenciaParametrosTeifa = strategy.ObterSequenciaParametrosTeifa();
            var sequenciaParametrosEventos = strategy.ObterSequenciaParametrosEventos();
            var utilizaIdentificadorReferencia = strategy.ObterUtilizacaoIdentificadorReferencia();

            return new ConfiguracaoPlanilha(
                titulosAbaTaxas,
                titulosAbaEventos,
                colunaMesAno,
                colunaTaxaMensal,
                colunaDadosUnidadeGeradoraTaxas,
                colunaDadosUnidadeGeradoraEventos,
                colunaParametrosTeip,
                colunaParametrosTeifa,
                colunaTeipReferencia,
                colunaHorasTeipReferencia,
                colunaTeifaReferencia,
                colunaHorasMes,
                tipoTaxaMensal,
                tipoTeipAjustada,
                tipoTeifaAjustada,
                sequenciaParametrosTeip,
                sequenciaParametrosTeifa,
                sequenciaParametrosEventos,
                utilizaIdentificadorReferencia);
        }

        #region Aba Taxas

        private async Task InserirAbaTaxas(
            Workbook workbook,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var worksheet = workbook.Worksheets[0];

            worksheet.Name = GerarNomeAbaTaxas(
                dadosExportacaoTaxa.TaxaExportacao.TipoCalculo, 
                dadosExportacaoTaxa.DataReferencia);

            await Task.WhenAll(
                InserirCabecalho(worksheet, dadosExportacaoTaxa),
                InserirTitulosCorpoTaxa(worksheet, configuracaoPlanilha, dadosExportacaoTaxa),
                InserirCorpoTaxa(worksheet, configuracaoPlanilha, dadosExportacaoTaxa));

        }

        private async Task InserirTitulosCorpoTaxa(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var linha = dadosExportacaoTaxa.ObterQuantidadeTaxasAuxiliares()*2 + 3;
            var titulos = configuracaoPlanilha.TitulosAbaTaxas;

           await InserirTitulosCorpo(worksheet, titulos, linha);
        }

        private async Task InserirCorpoTaxa(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var tasks = new List<Task>();

            var linha = dadosExportacaoTaxa.ObterQuantidadeTaxasAuxiliares()*2 + 4;

            foreach (var dadosPeriodos in dadosExportacaoTaxa.DadosPeriodos)
            {
                int totalLinhas = dadosPeriodos.DadosUnidadesGeradoras.Count;

                tasks.Add(InserirDadosPeriodoTaxa(worksheet, configuracaoPlanilha, dadosPeriodos, dadosExportacaoTaxa.TaxasReferencia, linha, totalLinhas));
                tasks.Add(InserirDadosPorUnidadeGeradoraTaxa(worksheet, configuracaoPlanilha, linha, dadosPeriodos, dadosExportacaoTaxa.TaxasReferencia));

                linha += totalLinhas;
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirDadosPeriodoTaxa(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosPeriodoUsina dadosPeriodos,
            IEnumerable<CalculoTaxa> taxasReferencia,
            int linha,
            int totalLinhas)
        {
            var dadosMesAno = InserirDadoMesAno(
                worksheet, 
                configuracaoPlanilha, 
                dadosPeriodos, 
                linha, 
                totalLinhas);

            var taxaReferenciaInserida = InserirDadosTaxasReferencia(
                worksheet, 
                configuracaoPlanilha, 
                dadosPeriodos, 
                taxasReferencia, 
                linha,
                totalLinhas);

            await Task.WhenAll(dadosMesAno, taxaReferenciaInserida);

            if (taxaReferenciaInserida.Result)
            {
                await InserirDadosHoraMes(worksheet, configuracaoPlanilha, dadosPeriodos, linha, totalLinhas);
                return;
            }

            await InserirDadosTaxaMensal(worksheet, configuracaoPlanilha, dadosPeriodos, linha, totalLinhas);
        }

        private async Task InserirDadosHoraMes(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosPeriodoUsina dadosPeriodos,
            int linha,
            int totalLinhas)
        {
            if (configuracaoPlanilha.ColunaHorasMes == ColunaPlanilha.Indefinido)
            {
                return;
            }

            var posicaoHoraMes = ObterPosicao(configuracaoPlanilha.ColunaHorasMes, linha);

            await Task.WhenAll(
                InserirValorCelula(worksheet, dadosPeriodos.DataReferencia.TotalHorasDoMes(), posicaoHoraMes),
                RealizarMergeCelulas(worksheet, linha, totalLinhas, posicaoHoraMes));
        }

        private async Task InserirDadosTaxaMensal(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosPeriodoUsina dadosPeriodos,
            int linha,
            int totalLinhas)
        {
            if (configuracaoPlanilha.ColunaTaxaMensal == ColunaPlanilha.Indefinido)
            {
                return;
            }

            var taxaMensal = dadosPeriodos.Taxas.FirstOrDefault(p => p.TipoCalculo == configuracaoPlanilha.TipoTaxaMensal);
            var posicoes = await ObterPosicoesOrdenadasPorColunaInicial(configuracaoPlanilha.ColunaTaxaMensal, linha, 2);

            await Task.WhenAll(
                InserirValorCelula(worksheet, taxaMensal?.Valor, posicoes.ElementAt(0)),
                InserirValorCelula(worksheet, taxaMensal?.Versao, posicoes.ElementAt(1)),
                RealizarMergeCelulas(worksheet, linha, totalLinhas, posicoes)
                );
        }

        private async Task<bool> InserirDadosTaxasReferencia(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosPeriodoUsina dadosPeriodos,
            IEnumerable<CalculoTaxa> taxasReferencia,
            int linha,
            int totalLinhas)
        {
            var tasks = new List<Task<bool>>();

            //Inserir TEIP 
            tasks.Add(InserirDadosTaxasReferencia(
                worksheet,
                linha,
                totalLinhas,
                await ObterTaxaRefAjustada(
                    dadosPeriodos,
                    taxasReferencia,
                    configuracaoPlanilha.TipoTeipAjustada,
                    TipoCalculo.IPReferencia),
                configuracaoPlanilha.ColunaTeipReferencia,
                configuracaoPlanilha.UtilizaIdentificadorReferencia));

            //Inserir TEIFA
            tasks.Add(InserirDadosTaxasReferencia(
                worksheet,
                linha,
                totalLinhas,
                await ObterTaxaRefAjustada(
                    dadosPeriodos,
                    taxasReferencia,
                    configuracaoPlanilha.TipoTeifaAjustada,
                    TipoCalculo.TEIFReferencia),
                configuracaoPlanilha.ColunaTeifaReferencia,
                configuracaoPlanilha.UtilizaIdentificadorReferencia));

           var operacoesRealizadas = await Task.WhenAll(tasks);

            return operacoesRealizadas.Any(p => p);
        }

        private async Task<bool> InserirDadosTaxasReferencia(
            Worksheet worksheet,
            int linha,
            int totalLinhas,
            CalculoTaxa taxa,
            ColunaPlanilha colunaTaxaReferencia,
            bool utilizaIdentificador)
        {
            if (colunaTaxaReferencia == ColunaPlanilha.Indefinido || taxa?.Valor is null)
            {
                return false;
            }

            int quantidadePosicoes = utilizaIdentificador ? 2 : 1;

            var posicoes = await ObterPosicoesOrdenadasPorColunaInicial(colunaTaxaReferencia, linha, quantidadePosicoes);

            await InserirValorCelula(worksheet, taxa.Valor, posicoes.ElementAt(0));

            if (utilizaIdentificador)
            {
                var identificadorTaxa = await ObterIdentificadorTaxaRefAjustada(taxa);
                await InserirValorCelula(worksheet, identificadorTaxa, posicoes.ElementAt(1));
            }

            await RealizarMergeCelulas(worksheet, linha, totalLinhas, posicoes);

            return true;
        }

        private Task<CalculoTaxa> ObterTaxaRefAjustada(
            DadosPeriodoUsina dadosPeriodos,
            IEnumerable<CalculoTaxa> taxasReferencia,
            TipoCalculo tipoTaxaAjustada,
            TipoCalculo tipoTaxaReferencia)
        {
            if (dadosPeriodos.UsinaEmOperacao)
            {
                return Task.FromResult(
                    dadosPeriodos.Taxas.FirstOrDefault(p => p.TipoCalculo == tipoTaxaAjustada)
                    );
            }
            return Task.FromResult(
                taxasReferencia.FirstOrDefault(p => p.TipoCalculo == tipoTaxaReferencia));
        }

        private Task<string> ObterIdentificadorTaxaRefAjustada(CalculoTaxa taxa)
        {
            if (taxa.TipoCalculo.In(new[] { TipoCalculo.IPReferencia, TipoCalculo.TEIFReferencia }))
            {
                return Task.FromResult(IdentificadorTaxa.Referencia.GetDescription());
            }

            return Task.FromResult(IdentificadorTaxa.Acumulada.GetDescription());
        }

        private async Task InserirDadosPorUnidadeGeradoraTaxa(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosPeriodoUsina dadosPeriodoUsina,
            IEnumerable<CalculoTaxa> taxasReferencia)
        {
            var tasks = new List<Task>();
            foreach (var dadosUnidadeGeradora in dadosPeriodoUsina.DadosUnidadesGeradoras)
            {
                tasks.Add(InserirDadosUnidadeGeradora(worksheet, configuracaoPlanilha.ColunaDadosUnidadeGeradoraTaxas, linha, dadosUnidadeGeradora));
                tasks.Add(InserirDadosParametrosTeip(worksheet, configuracaoPlanilha, linha, dadosUnidadeGeradora));
                tasks.Add(InserirDadosParametrosTeifa(worksheet, configuracaoPlanilha, linha, dadosUnidadeGeradora));
                tasks.Add(InserirDadosHoraTeipReferencia(worksheet, configuracaoPlanilha, linha, dadosPeriodoUsina, taxasReferencia, dadosUnidadeGeradora));

                linha++;
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirDadosHoraTeipReferencia(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosPeriodoUsina dadosPeriodoUsina,
            IEnumerable<CalculoTaxa> taxasReferencia,
            DadosUnidadeGeradora dadosUnidadeGeradora)
        {
            if (configuracaoPlanilha.ColunaHorasTeipReferencia == ColunaPlanilha.Indefinido)
            {
                return;
            }

            var valorHp = dadosPeriodoUsina.UsinaEmOperacao ?
                dadosUnidadeGeradora.ObterValorUltimoParametroVersionado(TipoCalculo.HP) :
                dadosPeriodoUsina.DataReferencia.TotalHorasDoMes();

            var teip = await ObterTaxaRefAjustada(
                dadosPeriodoUsina,
                taxasReferencia,
                configuracaoPlanilha.TipoTeipAjustada,
                TipoCalculo.IPReferencia);

            if (teip?.Valor is null || valorHp is null)
            {
                return;
            }

            var valorHorasTeip = teip.Valor.Value.MultiplicarPor(valorHp.Value);
            var posicao =  ObterPosicao(configuracaoPlanilha.ColunaHorasTeipReferencia, linha);

            await InserirValorCelula(worksheet, valorHorasTeip, posicao);
        }

        private async Task InserirDadosParametrosTeifa(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosUnidadeGeradora dadosUnidadeGeradora,
            bool incluirVersao = true)
        {
            if (configuracaoPlanilha.ColunaParametrosTeifa == ColunaPlanilha.Indefinido)
            {
                return;
            }

            await InserirDadosParametros(
                worksheet,
                configuracaoPlanilha.ColunaParametrosTeifa,
                linha,
                dadosUnidadeGeradora,
                configuracaoPlanilha.SequenciaParametrosTeifa,
                incluirVersao);
        }

        private async Task InserirDadosParametrosTeip(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosUnidadeGeradora dadosUnidadeGeradora,
            bool incluirVersao = true)
        {
            if (configuracaoPlanilha.ColunaParametrosTeip == ColunaPlanilha.Indefinido)
            {
                return;
            }

            await InserirDadosParametros(
                worksheet,
                configuracaoPlanilha.ColunaParametrosTeip,
                linha,
                dadosUnidadeGeradora,
                configuracaoPlanilha.SequenciaParametrosTeip,
                incluirVersao);
        }


        private async Task InserirDadosParametros(
            Worksheet worksheet,
            ColunaPlanilha colunaInicial,
            int linha,
            DadosUnidadeGeradora dadosUnidadeGeradora,
            IEnumerable<TipoCalculo> sequenciaParametros,
            bool incluirVersao)
        {
            if (sequenciaParametros.IsNullOrEmpty())
            {
                return;
            }

            int multiplicador = incluirVersao ? 2 : 1;

            int quantidadeColunas = sequenciaParametros.Count() * multiplicador;

            var posicoes = await ObterPosicoesOrdenadasPorColunaInicial(colunaInicial, linha, quantidadeColunas);

            var tasks = new List<Task>();

            int i = 0;
            foreach (var parametro in sequenciaParametros)
            {
                tasks.Add(InserirValorCelula(
                    worksheet, 
                    dadosUnidadeGeradora.ObterValorUltimoParametroVersionado(parametro), 
                    posicoes.ElementAt(i)));

                tasks.Add(InserirValorCelula(
                    worksheet,
                    dadosUnidadeGeradora.ObterVersaoUltimoParametroVersionado(parametro), 
                    posicoes.ElementAt(i + 1)));

                i += multiplicador;
            }

            await Task.WhenAll(tasks);
        }

        #endregion

        #region Aba Eventos

        private async Task InserirAbaEventos(
            Workbook workbook,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            workbook.Worksheets.Add(GerarNomeAbaEventos());

            var worksheet = workbook.Worksheets[1];

            await Task.WhenAll(
                InserirCabecalho(worksheet, dadosExportacaoTaxa),
                InserirTitulosCorpo(worksheet, configuracaoPlanilha.TitulosAbaEventos, 1),
                InserirCorpoEventos(worksheet, configuracaoPlanilha, dadosExportacaoTaxa));
        }


        private async Task InserirCorpoEventos(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var tasks = new List<Task>();

            int linha = 2;

            foreach (var dadosPeriodos in dadosExportacaoTaxa.DadosPeriodos)
            {
                int totalLinhas = dadosPeriodos.ObterTotalEventosPeriodo();

                tasks.Add(InserirDadoMesAno(worksheet, configuracaoPlanilha, dadosPeriodos, linha, totalLinhas));

                tasks.Add(InserirDadosPorUnidadeGeradoraEventos(worksheet, configuracaoPlanilha, linha, dadosPeriodos));

                linha += totalLinhas;
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirDadosPorUnidadeGeradoraEventos(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosPeriodoUsina dadosPeriodoUsina)
        {
            var tasks = new List<Task>();
            foreach (var dadosUnidadeGeradora in dadosPeriodoUsina.DadosUnidadesGeradoras)
            {
                tasks.Add(InserirDadosUnidadeGeradora(
                    worksheet,
                    configuracaoPlanilha.ColunaDadosUnidadeGeradoraEventos, 
                    linha, 
                    dadosUnidadeGeradora, 
                    true));

                tasks.Add(InserirDadosEventos(worksheet, configuracaoPlanilha, linha, dadosUnidadeGeradora));

                linha += dadosUnidadeGeradora.ObterTotalEventos();
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirDadosEventos(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            int linha,
            DadosUnidadeGeradora dadosUnidadeGeradora)
        {
            var tasks = new List<Task>();

            foreach (var evento in dadosUnidadeGeradora.Eventos)
            {
                var parametros = configuracaoPlanilha.SequenciaParametrosEventos;

                int colunas = 8 + parametros.Count();

                var posicoes = await ObterPosicoesOrdenadasPorColunaInicial(ColunaPlanilha.D, linha, colunas);

                tasks.Add(
                    InserirValorCelula(
                        worksheet,
                        evento.NumeroOns,
                        posicoes.ElementAt(0)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet,
                        evento.Versao,
                        posicoes.ElementAt(1)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet,
                        evento.ObterDataHoraFormatados(),
                        posicoes.ElementAt(2)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet,
                        evento.EstadoOperativo,
                        posicoes.ElementAt(3)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet, 
                        evento.CondicaoOperativa,
                        posicoes.ElementAt(4)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet,
                        evento.Origem, 
                        posicoes.ElementAt(5)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet, 
                        evento.Disponibilidade, 
                        posicoes.ElementAt(6)));

                tasks.Add(
                    InserirValorCelula(
                        worksheet, 
                        evento.Duracao,
                        posicoes.ElementAt(7)));

                int contadorParametros = 1;
                foreach (var parametro in parametros)
                {
                    tasks.Add(
                        InserirValorCelula(
                            worksheet,
                            evento.ObterCaracterMarcacao(parametro),
                            posicoes.ElementAt(7 + contadorParametros)));

                    contadorParametros++;
                }

                linha++;
            }

            await Task.WhenAll(tasks);
        }

        #endregion

        private async Task InserirDadosUnidadeGeradora(
            Worksheet worksheet,
            ColunaPlanilha coluna,
            int linha,
            DadosUnidadeGeradora dadosUnidadeGeradora,
            bool realizarMergePorEventos = false)
        {
            if (coluna == ColunaPlanilha.Indefinido)
            {
                return;
            }

            var posicoesUnidadeGeradora = await ObterPosicoesOrdenadasPorColunaInicial(
                coluna,
                linha,
                2);

            var tasks = new List<Task>();

            tasks.Add(
            InserirValorCelula(
                worksheet,
                dadosUnidadeGeradora.UnidadeGeradoraId,
                posicoesUnidadeGeradora.ElementAt(0)));

            tasks.Add(
                InserirValorCelula(
                    worksheet,
                    dadosUnidadeGeradora.Potencia,
                    posicoesUnidadeGeradora.ElementAt(1)));

            int totalLinhas = dadosUnidadeGeradora.ObterTotalEventos();
            if (realizarMergePorEventos && totalLinhas > 1)
            {
                tasks.Add(
                    RealizarMergeCelulas(
                        worksheet,
                        linha, 
                        totalLinhas, 
                        posicoesUnidadeGeradora));
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirCabecalho(Worksheet worksheet, DadosExportacaoTaxa dadosExportacaoTaxa)
        {
            var tasks = new List<Task>();

            tasks.Add(InserirTaxaCabecalho(
                worksheet,
                dadosExportacaoTaxa.TaxaExportacao.TipoCalculo,
                dadosExportacaoTaxa.TaxaExportacao.Valor.Value,
                dadosExportacaoTaxa.TaxaExportacao.Versao,
                ColunaPlanilha.A,
                1));

            tasks.Add(
            InserirValorCelula(
                worksheet,
                TituloPlanilha.DataReferenciaTaxa.GetDescription(),
                ObterPosicao(ColunaPlanilha.C, 1),
                TipoEstilo.Titulo));

            tasks.Add(
            InserirValorCelula(
                worksheet,
                dadosExportacaoTaxa.ObterDataReferenciaExportacaoFormatada(),
                ObterPosicao(ColunaPlanilha.C, 2)));

            if (!dadosExportacaoTaxa.PossuiTaxasAuxiliares())
            {
                 await Task.WhenAll(tasks);
                return;
            }

            //Insere taxas auxiliares
            int linha = 3;
            foreach (var taxa in dadosExportacaoTaxa.TaxasAuxiliares)
            {
                tasks.Add(InserirTaxaCabecalho(
                    worksheet,
                    taxa.TipoCalculo,
                    taxa.Valor.Value,
                    taxa.Versao,
                    ColunaPlanilha.A,
                    linha));

                linha+= 2;
            }

            await Task.WhenAll(tasks);
        }
        private async Task InserirTaxaCabecalho(
            Worksheet worksheet,
            TipoCalculo tipoCalculo,
            double valorTaxa,
            string versao,
            ColunaPlanilha coluna,
            int linha)
        {
            var colunaSeguinte = await ObterColunaSeguinte(coluna);

            var tasks = new List<Task>();

            tasks.Add(
            InserirValorCelula(
                worksheet,
                ObterTituloTaxaCabecalho(tipoCalculo),
                ObterPosicao(coluna, linha),
                TipoEstilo.Titulo));

            tasks.Add(
            InserirValorCelula(
                worksheet,
                TituloPlanilha.VersaoTaxa.GetDescription(),
                ObterPosicao(coluna, linha + 1),
                TipoEstilo.Titulo));

            tasks.Add(
            InserirValorCelula(
                worksheet,
                valorTaxa,
                ObterPosicao(colunaSeguinte, linha)));

            tasks.Add(
            InserirValorCelula(
                worksheet,
                versao,
                ObterPosicao(colunaSeguinte, linha + 1)));

            await Task.WhenAll(tasks);
        }

        private string ObterTituloTaxaCabecalho(TipoCalculo tipoCalculo)
        {
            switch (tipoCalculo)
            {
                case TipoCalculo.IndiceIndisponibilidadeVerificada:
                    return "Índice de Indisponibilidade";
                case TipoCalculo.TEIPIndiceIndisponibilidade:
                    return "TEIP Índice de Indisponibilidade";
                case TipoCalculo.TEIFaIndiceIndisponibilidade:
                    return "TEIFa Índice de Indisponibilidade";
                case TipoCalculo.TEIPacum:
                    return "TEIP Acum";
                case TipoCalculo.TEIFAacum:
                    return "TEIFa Acum";
                default:
                    return tipoCalculo.ToString();
            }
        }

        private async Task InserirTitulosCorpo(
            Worksheet worksheet,
            IEnumerable<TituloPlanilha> titulos,
            int linha)
        {
            var posicoesOrdenadas = await ObterPosicoesOrdenadasPorColunaInicial(
                ColunaPlanilha.A,
                linha,
                titulos.Count());

            var tasks = new List<Task>();

            int index = 0;
            foreach (var titulo in titulos)
            {
                string posicao = posicoesOrdenadas.ElementAt(index);

                tasks.Add(
                    InserirValorCelula(worksheet, titulo.GetDescription(), posicao, TipoEstilo.Titulo));

                worksheet.Cells.SetColumnWidth(index, 25);

                index++;
            }

            await Task.WhenAll(tasks);
        }

        private async Task InserirDadoMesAno(
            Worksheet worksheet,
            ConfiguracaoPlanilha configuracaoPlanilha,
            DadosPeriodoUsina dadosPeriodos,
            int linha,
            int totalLinhas)
        {
            if (configuracaoPlanilha.ColunaMesAno == ColunaPlanilha.Indefinido)
            {
                return;
            }

            var posicao = ObterPosicao(configuracaoPlanilha.ColunaMesAno, linha);

            await Task.WhenAll(
                InserirValorCelula(worksheet, dadosPeriodos.ObterMesAnoPeriodoFormatado(), posicao),
                RealizarMergeCelulas(worksheet, linha, totalLinhas, posicao));
        }

        private string GerarNomeAbaTaxas(TipoCalculo tipoCalculo, DateTime dataReferencia)
        {
            var nomeCalculo = ObterNomeCalculoSimplificado(tipoCalculo);

            var ano = dataReferencia.Year.ToString().Substring(2, 2);
            var mes = dataReferencia.ToString("MMM");
            string dataReferenciaFormatada =string.Concat(mes,"_",ano);

            if (nomeCalculo.Length > 15)
            {
                nomeCalculo = nomeCalculo.Substring(0, 15);
            }

            return string.Concat(nomeCalculo,"_", dataReferenciaFormatada, ".xlsx");
        }

        private string ObterNomeCalculoSimplificado(TipoCalculo tipoCalculo)
        {
            switch (tipoCalculo)
            {
                case TipoCalculo.IndiceIndisponibilidadeVerificada:
                    return "IndiceIndis";
                case TipoCalculo.TEIPacum:
                    return "TEIPacum";
                case TipoCalculo.TEIFAacum:
                    return "TEIFaAcum";
                default:
                    return tipoCalculo.ToString();
            }
        }

        private string GerarNomeAbaEventos()
        {
            return "Eventos.xlsx";
        }

    }
}