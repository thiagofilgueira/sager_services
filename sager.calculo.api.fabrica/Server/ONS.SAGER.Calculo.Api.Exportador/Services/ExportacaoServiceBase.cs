﻿using Aspose.Cells;
using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace ONS.SAGER.Calculo.Api.Exportador.Services
{
    public abstract class ExportacaoServiceBase
    {
        protected Task<List<string>> ObterColunasPlanilhaOrdenadas()
        {
            return Task.FromResult(ColunaPlanilha.A.ToList());
        }

        protected async Task<ColunaPlanilha> ObterColunaSeguinte(ColunaPlanilha coluna)
        {
            if(coluna == ColunaPlanilha.Indefinido)
            {
                return coluna;
            }

            var todasColunas = await ObterColunasPlanilhaOrdenadas();
            int indexColuna = todasColunas.FindIndex(p => p == coluna.ToString());

            if(indexColuna + 1 == todasColunas.Count)
            {
                throw new InvalidOperationException("Número de colunas insuficiente.");
            }

            return todasColunas.ElementAt(indexColuna + 1).ToEnum<ColunaPlanilha>();
        }

        protected async Task<List<string>> ObterPosicoesOrdenadasPorColunaInicial(
            ColunaPlanilha colunaInicial, 
            int linha,
            int quantidadeColunas)
        {
            var todasColunas = await ObterColunasPlanilhaOrdenadas();
            int indexColuna = todasColunas.FindIndex(p => p == colunaInicial.ToString());
            var posicoes = new List<string>();

            for (int i = 0; i < quantidadeColunas; i++)
            {
                var coluna = todasColunas.ElementAt(indexColuna);
                var posicao =  ObterPosicao(coluna, linha);
                posicoes.Add(posicao);

                indexColuna++;
            }

            return posicoes;
        }

        protected string ObterPosicao(ColunaPlanilha coluna, int linha)
        {
            return string.Join(string.Empty, coluna.GetDescription(), linha.ToString());
        }

        protected string ObterPosicao(string coluna, int linha)
        {
            return string.Join(string.Empty, coluna, linha.ToString());
        }

        protected async Task RealizarMergeCelulas(Worksheet worksheet, int linha, int totalLinhas, IEnumerable<string> posicoes)
        {
            var tasks = new List<Task>();

            foreach(var posicao in posicoes)
            {
                tasks.Add(RealizarMergeCelulas(worksheet, linha, totalLinhas, posicao));
            }

            await Task.WhenAll(tasks);
        }

        protected Task RealizarMergeCelulas(Worksheet worksheet, int linha, int totalLinhas, string posicao)
        {
            if (totalLinhas <= 0)
            {
                return Task.CompletedTask;
            }

            worksheet.Cells.Merge(linha - 1, worksheet.Cells[posicao].Column, totalLinhas, 1);

            return Task.CompletedTask;
        }


        protected async Task InserirValorCelula(
            Worksheet worksheet,
            object valor,
            string posicao,
            TipoEstilo tipoEstilo = TipoEstilo.Centralizado)
        {
            if(worksheet is null || valor is null || posicao.IsNullOrEmpty())
            {
                return;
            }

            worksheet.Cells[posicao].PutValue(valor);

            if (tipoEstilo == TipoEstilo.Centralizado)
            {
                 await CentralizarCelula(worksheet.Cells[posicao]);
            }

            else if(tipoEstilo == TipoEstilo.Titulo)
            {
                await EstilizarTitulo(worksheet.Cells[posicao]);
            }
        }

        protected Task CentralizarCelula(Cell celula)
        {
            if (celula is null)
            {
                return Task.CompletedTask;
            }

            var estilo = new CellsFactory().CreateStyle();

            estilo.HorizontalAlignment = TextAlignmentType.Center;
            estilo.VerticalAlignment = TextAlignmentType.Center;

            celula.SetStyle(estilo);

            return Task.CompletedTask;
        }

        protected Task EstilizarTitulo(Cell celula)
        {
            if (celula is null)
            {
                return Task.CompletedTask;
            }

            var estilo = new CellsFactory().CreateStyle();
            estilo.Pattern = BackgroundType.Solid;
            estilo.ForegroundColor = Color.Gray;
            estilo.Font.Color = Color.White;
            estilo.Font.Size = 12;
            estilo.Font.IsBold = true;
            estilo.SetBorder(BorderType.BottomBorder, CellBorderType.Thin, Color.Black);
            estilo.SetBorder(BorderType.LeftBorder, CellBorderType.Thin, Color.Black);
            estilo.SetBorder(BorderType.RightBorder, CellBorderType.Thin, Color.Black);
            estilo.SetBorder(BorderType.TopBorder, CellBorderType.Thin, Color.Black);
            estilo.VerticalAlignment = TextAlignmentType.Center;
            estilo.HorizontalAlignment = TextAlignmentType.Center;

            celula.SetStyle(estilo);

            return Task.CompletedTask;
        }

    }
}
