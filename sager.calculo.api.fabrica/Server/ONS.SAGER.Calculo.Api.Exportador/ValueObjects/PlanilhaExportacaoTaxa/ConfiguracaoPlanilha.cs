﻿using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Exportador.ValueObjects.PlanilhaExportacaoTaxa
{
    public class ConfiguracaoPlanilha
    {
        public IEnumerable<TituloPlanilha> TitulosAbaTaxas { get; }
        public IEnumerable<TituloPlanilha> TitulosAbaEventos { get;}
        public ColunaPlanilha ColunaMesAno { get; }
        public ColunaPlanilha ColunaTaxaMensal { get; }
        public ColunaPlanilha ColunaDadosUnidadeGeradoraTaxas { get; }
        public ColunaPlanilha ColunaDadosUnidadeGeradoraEventos{ get; }
        public ColunaPlanilha ColunaParametrosTeip { get; }
        public ColunaPlanilha ColunaParametrosTeifa { get; }
        public ColunaPlanilha ColunaTeipReferencia { get; }
        public ColunaPlanilha ColunaHorasTeipReferencia { get; }
        public ColunaPlanilha ColunaTeifaReferencia { get; }
        public ColunaPlanilha ColunaHorasMes { get; }
        public TipoCalculo TipoTaxaMensal { get; }
        public TipoCalculo TipoTeipAjustada { get; }
        public TipoCalculo TipoTeifaAjustada { get; }
        public IEnumerable<TipoCalculo> SequenciaParametrosTeip { get; }
        public IEnumerable<TipoCalculo> SequenciaParametrosTeifa { get; }
        public IEnumerable<TipoCalculo> SequenciaParametrosEventos { get; }
        public bool UtilizaIdentificadorReferencia { get; }

        public ConfiguracaoPlanilha(
            IEnumerable<TituloPlanilha> titulosAbaTaxas,
            IEnumerable<TituloPlanilha> titulosAbaEventos,
            ColunaPlanilha colunaMesAno,
            ColunaPlanilha colunaTaxaMensal,
            ColunaPlanilha colunaDadosUnidadeGeradoraTaxas,
            ColunaPlanilha colunaDadosUnidadeGeradoraEventos,
            ColunaPlanilha colunaParametrosTeip,
            ColunaPlanilha colunaParametrosTeifa,
            ColunaPlanilha colunaTeipReferencia,
            ColunaPlanilha colunaHorasTeipReferencia,
            ColunaPlanilha colunaTeifaReferencia,
            ColunaPlanilha colunaHorasMes,
            TipoCalculo tipoTaxaMensal,
            TipoCalculo tipoTeipAjustada,
            TipoCalculo tipoTeifaAjustada,
            IEnumerable<TipoCalculo> sequenciaParametrosTeip,
            IEnumerable<TipoCalculo> sequenciaParametrosTeifa,
            IEnumerable<TipoCalculo> sequenciaParametrosEventos,
            bool utilizaIdentificadorReferencia)
        {
            TitulosAbaTaxas = titulosAbaTaxas ?? new List<TituloPlanilha>();
            TitulosAbaEventos = titulosAbaEventos ?? new List<TituloPlanilha>();
            ColunaMesAno = colunaMesAno;
            ColunaTaxaMensal = colunaTaxaMensal;
            ColunaDadosUnidadeGeradoraTaxas = colunaDadosUnidadeGeradoraTaxas;
            ColunaDadosUnidadeGeradoraEventos = colunaDadosUnidadeGeradoraEventos;
            ColunaParametrosTeip = colunaParametrosTeip;
            ColunaParametrosTeifa = colunaParametrosTeifa;
            ColunaTeipReferencia = colunaTeipReferencia;
            ColunaHorasTeipReferencia = colunaHorasTeipReferencia;
            ColunaTeifaReferencia = colunaTeifaReferencia;
            ColunaHorasMes = colunaHorasMes;
            TipoTaxaMensal = tipoTaxaMensal;
            TipoTeipAjustada = tipoTeipAjustada;
            TipoTeifaAjustada = tipoTeifaAjustada;
            SequenciaParametrosTeip = sequenciaParametrosTeip ?? new List<TipoCalculo>();
            SequenciaParametrosTeifa = sequenciaParametrosTeifa ?? new List<TipoCalculo>();
            SequenciaParametrosEventos = sequenciaParametrosEventos ?? new List<TipoCalculo>();
            UtilizaIdentificadorReferencia = utilizaIdentificadorReferencia;
        }
    }
}
