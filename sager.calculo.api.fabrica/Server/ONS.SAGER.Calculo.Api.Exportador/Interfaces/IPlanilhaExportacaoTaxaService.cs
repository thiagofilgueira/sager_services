﻿using Aspose.Cells;
using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Exportador.Interfaces
{
    public  interface IPlanilhaExportacaoTaxaService
    {
        Task<Workbook> ObterPlanilhaExportacaoTaxa(DadosExportacaoTaxa dadosCalculoTaxaUsina);
    }
}