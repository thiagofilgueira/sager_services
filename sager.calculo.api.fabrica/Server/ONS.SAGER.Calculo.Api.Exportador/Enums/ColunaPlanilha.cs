﻿using System.ComponentModel;

namespace ONS.SAGER.Calculo.Api.Exportador.Enums
{
    public enum ColunaPlanilha
    {
        Indefinido = 0,

        [Description("A")]
        A = 1,

        [Description("B")]
        B = 2,

        [Description("C")]
        C = 3,

        [Description("D")]
        D = 4,

        [Description("E")]
        E = 5,

        [Description("F")]
        F = 6,

        [Description("G")]
        G = 7,

        [Description("H")]
        H = 8,

        [Description("I")]
        I = 9,

        [Description("J")]
        J = 10,

        [Description("K")]
        K = 11,

        [Description("L")]
        L = 12,

        [Description("M")]
        M = 13,

        [Description("N")]
        N = 14,

        [Description("O")]
        O = 15,

        [Description("P")]
        P = 16,

        [Description("Q")]
        Q = 17,

        [Description("R")]
        R = 18,

        [Description("S")]
        S = 19,

        [Description("T")]
        T = 20,

        [Description("U")]
        U = 21,

        [Description("V")]
        V = 22,

        [Description("W")]
        W = 23,

        [Description("X")]
        X = 24,

        [Description("Y")]
        Y = 25,

        [Description("Z")]
        Z = 26
    }
}
