﻿using System.ComponentModel;

namespace ONS.SAGER.Calculo.Taxa.Exportador.Enums
{
    public enum TituloPlanilha
    {
        [Description("Taxa acumulada")]
        TaxaAcumulada,

        [Description("Versão da taxa")]
        VersaoTaxa,

        [Description("Índice Indisponibilidade")]
        IndiceIndisponibilidade,

        [Description("Mês de referência")]
        DataReferenciaTaxa,

        [Description("Mês")]
        Mes,

        [Description("Taxa Mensal")]
        TaxaMensal,

        [Description("Versão")]
        Versao,

        [Description("DataHora")]
        DataHora,

        [Description("EO")]
        EO,

        [Description("CO")]
        CO,

        [Description("OR")]
        OR,

        [Description("Disp")]
        Disp,

        [Description("Duração")]
        Duracao,

        [Description("UGEs")]
        UGES,

        [Description("Potência")]
        Potencia,

        [Description("Num ONS")]
        NumeroOns,

        [Description("HP")]
        HP,

        [Description("HDP")]
        HDP,

        [Description("HEDP")]
        HEDP,

        [Description("Tx Ref.(R) ou Ajust.(A)")]
        TaxaReferenciaOuAjustada,

        [Description("TEIP Ref.(R) ou Ajust.(A)")]
        TeipReferenciaOuAjustada,

        [Description("TEIFa Ref.(R) ou Ajust.(A)")]
        TeifaReferenciaOuAjustada,

        [Description("(R/A)")]
        RA,

        [Description("HEDF")]
        HEDF,

        [Description("HS")]
        HS,

        [Description("HDF")]
        HDF,

        [Description("HDCE")]
        HDCE,

        [Description("HRD")]
        HRD,

        [Description("TEIP Índice de Indisponibilidade")]
        TeipIndiceIndisponibilidade,

        [Description("TEIFa Índice de Indisponibilidade")]
        TeifaIndiceIndisponibilidade,

        [Description("Horas Mês")]
        HorasMes,

        [Description("TEIP Ref/Ajus em horas")]
        HorasTeipReferenciaAjustada,

        [Description("TEIP Ref.")]
        TeipReferencia,

        [Description("TEIFA Ref.")]
        TeifaReferencia

    }
}