﻿namespace ONS.SAGER.Calculo.Api.Exportador.Enums
{
    public enum TipoEstilo
    {
        Indefinido,
        Centralizado,
        Titulo
    }
}
