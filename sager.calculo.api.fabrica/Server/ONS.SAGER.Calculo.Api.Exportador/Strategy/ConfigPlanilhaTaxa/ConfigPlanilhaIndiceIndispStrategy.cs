﻿using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public class ConfigPlanilhaIndiceIndispStrategy : IConfigPlanilhaTaxaStrategy
    {
        public IEnumerable<TituloPlanilha> ObterTitulosAbaTaxas()
        {
            return new[]
            {
                TituloPlanilha.Mes,
                TituloPlanilha.UGES,
                TituloPlanilha.Potencia,
                TituloPlanilha.HP,
                TituloPlanilha.Versao,
                TituloPlanilha.HDP,
                TituloPlanilha.Versao,
                TituloPlanilha.HEDP,
                TituloPlanilha.Versao,
                TituloPlanilha.TeipReferencia,
                TituloPlanilha.HS,
                TituloPlanilha.Versao,
                TituloPlanilha.HDF,
                TituloPlanilha.Versao,
                TituloPlanilha.HEDF,
                TituloPlanilha.Versao,
                TituloPlanilha.HRD,
                TituloPlanilha.Versao,
                TituloPlanilha.HDCE,
                TituloPlanilha.Versao,
                TituloPlanilha.TeifaReferencia,
                TituloPlanilha.HorasMes,
            };
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeifa()
        {
            return new[]
            {
                TipoCalculo.HS,
                TipoCalculo.HDF,
                TipoCalculo.HEDF,
                TipoCalculo.HRD,
                TipoCalculo.HDCE
            };
        }
        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraTaxas()
        {
            return ColunaPlanilha.B;
        }

        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraEventos()
        {
            return ColunaPlanilha.B;
        }

        public ColunaPlanilha ObterColunaParametrosTeifa()
        {
            return ColunaPlanilha.K;
        }

        public ColunaPlanilha ObterColunaParametrosTeip()
        {
            return ColunaPlanilha.D;
        }

        public ColunaPlanilha ObterColunaTaxaMensal()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaTeifaReferencia()
        {
            return ColunaPlanilha.U;
        }

        public ColunaPlanilha ObterColunaTeipReferencia()
        {
            return ColunaPlanilha.J;
        }

        public ColunaPlanilha ObterColunaHorasTeipReferencia()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaHorasMes()
        {
            return ColunaPlanilha.V;
        }

        public TipoCalculo ObterTipoTaxaMensal()
        {
            return TipoCalculo.Indefinido;
        }

        public TipoCalculo ObterTipoTeipAjustada()
        {
            return TipoCalculo.TEIPacumAjustada;
        }

        public TipoCalculo ObterTipoTeifaAjustada()
        {
            return TipoCalculo.TEIFaacumAjustada;
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeip()
        {
            return new[]
            {
                TipoCalculo.HP,
                TipoCalculo.HDP,
                TipoCalculo.HEDP
            };
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosEventos()
        {
            return new[]
            {
                TipoCalculo.HP,
                TipoCalculo.HDP,
                TipoCalculo.HEDP,
                TipoCalculo.HDF,
                TipoCalculo.HEDF,
                TipoCalculo.HS,
                TipoCalculo.HDCE,
                TipoCalculo.HRD
            };
        }

        public IEnumerable<TituloPlanilha> ObterTitulosAbaEventos()
        {
            return new[]
            {
                TituloPlanilha.Mes,
                TituloPlanilha.UGES,
                TituloPlanilha.Potencia,
                TituloPlanilha.NumeroOns,
                TituloPlanilha.Versao,
                TituloPlanilha.DataHora,
                TituloPlanilha.EO,
                TituloPlanilha.CO,
                TituloPlanilha.OR,
                TituloPlanilha.Disp,
                TituloPlanilha.Duracao,
                TituloPlanilha.HP,
                TituloPlanilha.HDP,
                TituloPlanilha.HEDP,
                TituloPlanilha.HDF,
                TituloPlanilha.HEDF,
                TituloPlanilha.HS,
                TituloPlanilha.HDCE,
                TituloPlanilha.HRD
            };
        }

        public ColunaPlanilha ObterColunaMesAno()
        {
            return ColunaPlanilha.A;
        }

        public bool ObterUtilizacaoIdentificadorReferencia()
        {
            return false;
        }
    }
}