﻿using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public class ConfigPlanilhaTeifaStrategy : IConfigPlanilhaTaxaStrategy
    {
        private readonly bool _primeiraVersaoTaxa;

        public ConfigPlanilhaTeifaStrategy(DateTime dataReferencia)
        {
            _primeiraVersaoTaxa = dataReferencia < new DateTime(2014, 10, 1);
        }

        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraTaxas()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.D;
            }

            return ColunaPlanilha.B;
        }
        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraEventos()
        {
            return ColunaPlanilha.B;
        }

        public ColunaPlanilha ObterColunaHorasMes()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.Indefinido;
            }

            return ColunaPlanilha.P;
        }

        public ColunaPlanilha ObterColunaHorasTeipReferencia()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaParametrosTeifa()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.F;
            }

            return ColunaPlanilha.D;
        }

        public ColunaPlanilha ObterColunaParametrosTeip()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaTaxaMensal()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.B;
            }

            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaTeifaReferencia()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.P;
            }

            return ColunaPlanilha.N;
        }

        public ColunaPlanilha ObterColunaTeipReferencia()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.Indefinido;
            }

            return ColunaPlanilha.Q;
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosEventos()
        {
            return ObterSequenciaParametrosTeifa();
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeifa()
        {
            if (_primeiraVersaoTaxa)
            {
                return new[]
                {
                    TipoCalculo.HSA,
                    TipoCalculo.HDF,
                    TipoCalculo.HEDF,
                    TipoCalculo.HRDA,
                    TipoCalculo.HDCE
                };
            }

            return new[]
            {
                TipoCalculo.HS,
                TipoCalculo.HDF,
                TipoCalculo.HEDF,
                TipoCalculo.HRD,
                TipoCalculo.HDCE
            };
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeip()
        {
            return new List<TipoCalculo>();
        }

        public TipoCalculo ObterTipoTaxaMensal()
        {
            return TipoCalculo.TEIFAmes;
        }

        public TipoCalculo ObterTipoTeifaAjustada()
        {
            if (_primeiraVersaoTaxa)
            {
                return TipoCalculo.TEIFaMesAjustada;
            }

            return TipoCalculo.TEIFaacumAjustada;
        }

        public TipoCalculo ObterTipoTeipAjustada()
        {
            return TipoCalculo.Indefinido;
        }

        public IEnumerable<TituloPlanilha> ObterTitulosAbaTaxas()
        {
            if (_primeiraVersaoTaxa)
            {
                return new[]
                {
                    TituloPlanilha.Mes,
                    TituloPlanilha.TaxaMensal,
                    TituloPlanilha.Versao,
                    TituloPlanilha.UGES,
                    TituloPlanilha.Potencia,
                    TituloPlanilha.HS,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDF,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HEDF,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HRD,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDCE,
                    TituloPlanilha.Versao,
                    TituloPlanilha.TeifaReferenciaOuAjustada,
                    TituloPlanilha.RA
                };
            }

            return new[]
            {
                    TituloPlanilha.Mes,
                    TituloPlanilha.UGES,
                    TituloPlanilha.Potencia,
                    TituloPlanilha.HS,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDF,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HEDF,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HRD,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDCE,
                    TituloPlanilha.Versao,
                    TituloPlanilha.TeifaReferenciaOuAjustada,
                    TituloPlanilha.RA,
                    TituloPlanilha.HorasMes,
                    TituloPlanilha.TeipReferencia
            };
        }


        public IEnumerable<TituloPlanilha> ObterTitulosAbaEventos()
        {
            return new[]
            {
                TituloPlanilha.Mes,
                TituloPlanilha.UGES,
                TituloPlanilha.Potencia,
                TituloPlanilha.NumeroOns,
                TituloPlanilha.Versao,
                TituloPlanilha.DataHora,
                TituloPlanilha.EO,
                TituloPlanilha.CO,
                TituloPlanilha.OR,
                TituloPlanilha.Disp,
                TituloPlanilha.Duracao,
                TituloPlanilha.HS,
                TituloPlanilha.HDF,
                TituloPlanilha.HEDF,
                TituloPlanilha.HRD,
                TituloPlanilha.HDCE
            };
        }

        public ColunaPlanilha ObterColunaMesAno()
        {
            return ColunaPlanilha.A;
        }
        public bool ObterUtilizacaoIdentificadorReferencia()
        {
            return true;
        }
    }
}