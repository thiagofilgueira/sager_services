﻿using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public interface IConfigPlanilhaTaxaStrategy
    {
        IEnumerable<TituloPlanilha> ObterTitulosAbaTaxas();
        IEnumerable<TituloPlanilha> ObterTitulosAbaEventos();
        ColunaPlanilha ObterColunaMesAno();
        ColunaPlanilha ObterColunaTaxaMensal();
        ColunaPlanilha ObterColunaDadosUnidadeGeradoraTaxas();
        ColunaPlanilha ObterColunaDadosUnidadeGeradoraEventos();
        ColunaPlanilha ObterColunaParametrosTeip();
        ColunaPlanilha ObterColunaParametrosTeifa();
        ColunaPlanilha ObterColunaTeipReferencia();
        ColunaPlanilha ObterColunaHorasTeipReferencia();
        ColunaPlanilha ObterColunaTeifaReferencia();
        ColunaPlanilha ObterColunaHorasMes();
        TipoCalculo ObterTipoTaxaMensal();
        TipoCalculo ObterTipoTeipAjustada();
        TipoCalculo ObterTipoTeifaAjustada();
        IEnumerable<TipoCalculo> ObterSequenciaParametrosTeip();
        IEnumerable<TipoCalculo> ObterSequenciaParametrosTeifa();
        IEnumerable<TipoCalculo> ObterSequenciaParametrosEventos();
        bool  ObterUtilizacaoIdentificadorReferencia();
    }
}
