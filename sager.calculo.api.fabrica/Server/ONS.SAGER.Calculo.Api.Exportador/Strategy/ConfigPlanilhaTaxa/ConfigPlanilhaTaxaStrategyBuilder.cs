﻿using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;

namespace ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public static class ConfigPlanilhaTaxaStrategyBuilder
    {
        public static IConfigPlanilhaTaxaStrategy Build(TipoCalculo tipoTaxaExportacao, DateTime dataReferencia)
        {
            switch (tipoTaxaExportacao)
            {
                case TipoCalculo.TEIPacum:
                    {
                        return new ConfigPlanilhaTeipStrategy(dataReferencia);
                    }

                case TipoCalculo.TEIFAacum:
                    {
                        return new ConfigPlanilhaTeifaStrategy(dataReferencia);
                    }

                case TipoCalculo.IndiceIndisponibilidadeVerificada:
                    {
                        return new ConfigPlanilhaIndiceIndispStrategy();
                    }
            }

            throw new ArgumentException("Estrategia para o tipo de cálculo não definido.");
        }
    }
}
