﻿using ONS.SAGER.Calculo.Api.Exportador.Enums;
using ONS.SAGER.Calculo.Taxa.Exportador.Enums;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public class ConfigPlanilhaTeipStrategy : IConfigPlanilhaTaxaStrategy
    {
        private readonly bool _primeiraVersaoTaxa;

        public ConfigPlanilhaTeipStrategy(DateTime dataReferencia)
        {
            _primeiraVersaoTaxa = dataReferencia < new DateTime(2014, 10, 1);
        }


        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraTaxas()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.D;
            }

            return ColunaPlanilha.B;
        }
        public ColunaPlanilha ObterColunaDadosUnidadeGeradoraEventos()
        {
            return ColunaPlanilha.B;
        }

        public ColunaPlanilha ObterColunaHorasMes()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.Indefinido;
            }

            return ColunaPlanilha.L;
        }

        public ColunaPlanilha ObterColunaHorasTeipReferencia()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.Indefinido;
            }

            return ColunaPlanilha.M;
        }

        public ColunaPlanilha ObterColunaMesAno()
        {
            return ColunaPlanilha.A;
        }

        public ColunaPlanilha ObterColunaParametrosTeifa()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaParametrosTeip()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.F;
            }

            return ColunaPlanilha.D;
        }

        public ColunaPlanilha ObterColunaTaxaMensal()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.B;
            }

            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaTeifaReferencia()
        {
            return ColunaPlanilha.Indefinido;
        }

        public ColunaPlanilha ObterColunaTeipReferencia()
        {
            if (_primeiraVersaoTaxa)
            {
                return ColunaPlanilha.L;
            }

            return ColunaPlanilha.J;
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosEventos()
        {
            return ObterSequenciaParametrosTeip();
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeifa()
        {
            return new List<TipoCalculo>();
        }

        public IEnumerable<TipoCalculo> ObterSequenciaParametrosTeip()
        {
            return new[] 
            {
                TipoCalculo.HP,
                TipoCalculo.HDP,
                TipoCalculo.HEDP
            };
        }

        public TipoCalculo ObterTipoTaxaMensal()
        {
            return TipoCalculo.TEIPmes;
        }

        public TipoCalculo ObterTipoTeifaAjustada()
        {
            return TipoCalculo.Indefinido;
        }

        public TipoCalculo ObterTipoTeipAjustada()
        {
            if (_primeiraVersaoTaxa)
            {
                return TipoCalculo.TEIPmesAjustada;
            }

            return TipoCalculo.TEIPacumAjustada;
        }

        public IEnumerable<TituloPlanilha> ObterTitulosAbaTaxas()
        {
            if(_primeiraVersaoTaxa)
            {
                return new[]
                {
                    TituloPlanilha.Mes,
                    TituloPlanilha.TaxaMensal,
                    TituloPlanilha.Versao,
                    TituloPlanilha.UGES,
                    TituloPlanilha.Potencia,
                    TituloPlanilha.HP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HEDP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.TeipReferenciaOuAjustada,
                    TituloPlanilha.RA
                };
            }

            return new[]
            {
                    TituloPlanilha.Mes,
                    TituloPlanilha.UGES,
                    TituloPlanilha.Potencia,
                    TituloPlanilha.HP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HDP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.HEDP,
                    TituloPlanilha.Versao,
                    TituloPlanilha.TeipReferenciaOuAjustada,
                    TituloPlanilha.RA,
                    TituloPlanilha.HorasMes,
                    TituloPlanilha.HorasTeipReferenciaAjustada
            };
        }

        public IEnumerable<TituloPlanilha> ObterTitulosAbaEventos()
        {
            return new[]
            {
                TituloPlanilha.Mes,
                TituloPlanilha.UGES,
                TituloPlanilha.Potencia,
                TituloPlanilha.NumeroOns,
                TituloPlanilha.Versao,
                TituloPlanilha.DataHora,
                TituloPlanilha.EO,
                TituloPlanilha.CO,
                TituloPlanilha.OR,
                TituloPlanilha.Disp,
                TituloPlanilha.Duracao,
                TituloPlanilha.HP,
                TituloPlanilha.HDP,
                TituloPlanilha.HEDP
            };
        }
        public bool ObterUtilizacaoIdentificadorReferencia()
        {
            return true;
        }
    }
}