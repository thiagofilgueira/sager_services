﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.Exportador.Interfaces;
using ONS.SAGER.Calculo.Api.Exportador.Services;

namespace ONS.SAGER.Calculo.Api.Exportador.Extensions
{
    public static class ExportadorExtensions
    {
        public static void AddExportadorServices(this IServiceCollection services)
        {
            services.AddScoped<IPlanilhaExportacaoTaxaService, PlanilhaExportacaoTaxaService>();
        }
    }
}
