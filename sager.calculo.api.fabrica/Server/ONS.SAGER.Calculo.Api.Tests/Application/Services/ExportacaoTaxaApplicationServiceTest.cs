﻿using Aspose.Cells;
using Moq;
using ONS.SAGER.Calculo.Api.Application.Services;
using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Exportador.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Application.Services
{
    public class ExportacaoTaxaApplicationServiceTest
    {
        private readonly IExportacaoTaxaService _exportacaoTaxaService;
        private readonly IPlanilhaExportacaoTaxaService _planilhaExportacaoTaxaService;

        public ExportacaoTaxaApplicationServiceTest()
        {
            _exportacaoTaxaService = new Mock<IExportacaoTaxaService>().Object;
            _planilhaExportacaoTaxaService = new Mock<IPlanilhaExportacaoTaxaService>().Object;
        }

        [Fact]
        public async Task DeveRetornarArgumentNullExceptionQuandoRequestForNulo()
        {
            var service = new ExportacaoTaxaApplicationService(_exportacaoTaxaService, _planilhaExportacaoTaxaService);

            var @action = new Func<Task>(() => service.ObterArquivoExportacaoTaxa(null));

            await Assert.ThrowsAsync<ArgumentNullException>(@action);
        }

        [Fact]
        public async Task DeveRetornarInvalidOperationExceptionQuandoRequestForInvalido()
        {

            var service = new ExportacaoTaxaApplicationService(_exportacaoTaxaService, _planilhaExportacaoTaxaService);

            var request = new ExportacaoTaxaRequest();

            var @action = new Func<Task>(() => service.ObterArquivoExportacaoTaxa(request));

            await Assert.ThrowsAsync<InvalidOperationException>(@action);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIFaacumAjustada)]
        [InlineData(TipoCalculo.TEIPacumAjustada)]
        [InlineData(TipoCalculo.TEIPmes)]
        [InlineData(TipoCalculo.TEIFAmes)]
        public async Task DeveRetornarInvalidOperationExceptionQuandoTipoCalculoInvalido(TipoCalculo tipoCalculo)
        {

            var service = new ExportacaoTaxaApplicationService(_exportacaoTaxaService, _planilhaExportacaoTaxaService);

            var request = new ExportacaoTaxaRequest()
            {
                ConfiguracaoCenarioId = "master",
                DataReferencia = new DateTime(2018, 1, 1),
                TipoTaxa = tipoCalculo,
                UsinaId = "BAUSB",
                VersaoTaxa = null
            };

            var @action = new Func<Task>(() => service.ObterArquivoExportacaoTaxa(request));

            await Assert.ThrowsAsync<InvalidOperationException>(@action);
        }

        [Fact]
        public async Task DeveRetornarInvalidOperationExceptionQuandoIndiceIndispEDataReferenciaAnteriorOutubro2014()
        {

            var service = new ExportacaoTaxaApplicationService(_exportacaoTaxaService, _planilhaExportacaoTaxaService);

            var request = new ExportacaoTaxaRequest()
            {
                ConfiguracaoCenarioId = "master",
                DataReferencia = new DateTime(2010, 1, 1),
                TipoTaxa = TipoCalculo.IndiceIndisponibilidadeVerificada,
                UsinaId = "BAUSB",
                VersaoTaxa = null
            };

            var @action = new Func<Task>(() => service.ObterArquivoExportacaoTaxa(request));

            await Assert.ThrowsAsync<InvalidOperationException>(@action);
        }

        [Theory]
        [InlineData("planilha_1")]
        [InlineData("planilha_2")]
        [InlineData("planilha_3")]
        public async Task DeveRetornarArquivoExportacaoQuandoRequestValido(string nomePlanilha)
        {
            var exportacaoTaxaService = new Mock<IExportacaoTaxaService>();

            exportacaoTaxaService.Setup(p => p.ObterDadosPlanilha(It.IsAny<ExportacaoTaxaRequest>()))
                .ReturnsAsync(new DadosExportacaoTaxa("1",null,new DateTime(),null,null,null));

            var planilhaExportacaoTaxaService = new Mock<IPlanilhaExportacaoTaxaService>();

            var workbook = new Workbook();
            workbook.Worksheets[0].Name = nomePlanilha;

            planilhaExportacaoTaxaService.Setup(p => p.ObterPlanilhaExportacaoTaxa(It.IsAny<DadosExportacaoTaxa>()))
                .ReturnsAsync(workbook);

            var service = new ExportacaoTaxaApplicationService(exportacaoTaxaService.Object, planilhaExportacaoTaxaService.Object);

            var request = new ExportacaoTaxaRequest()
            {
                ConfiguracaoCenarioId = "master",
                DataReferencia = new DateTime(2018, 1, 1),
                TipoTaxa = TipoCalculo.TEIPacum,
                UsinaId = "BAUSB",
                VersaoTaxa = null
            };

            var retorno = await service.ObterArquivoExportacaoTaxa(request);

            Assert.True(retorno.Nome == nomePlanilha);
        }
    }
}
