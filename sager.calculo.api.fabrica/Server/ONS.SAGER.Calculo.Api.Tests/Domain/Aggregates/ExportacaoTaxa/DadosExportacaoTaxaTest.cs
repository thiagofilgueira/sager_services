﻿using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Aggregates.ExportacaoTaxa
{
    public class DadosExportacaoTaxaTest
    {
        [Fact]
        public void EntidadeInvalidaQuandoUsinaForNula()
        {
            string usinaId = null;

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa>{ taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                usinaId,
                taxaExportacao,
                DateTime.Now,
                taxasCollection,
                taxasCollection,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaQuandoTaxaExportacaoForNula()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                null,
                DateTime.Now,
                taxasCollection,
                taxasCollection,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }


        [Fact]
        public void EntidadeInvalidaQuandoDataReferenciaInvalida()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                default,
                taxasCollection,
                taxasCollection,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaQuandoTaxasReferenciasNulasOuVazias()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                taxasCollection,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaQuandoTaxasAuxiliaresNulasOuVaziasETaxaExporacaoIndiceIndisp()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                taxasCollection,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeValidaQuandoTaxasAuxiliaresNulasOuVaziasETaxaExporacaoDiferenteDeIndiceIndisp()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                taxasCollection,
                null,
                ObterDadosPeriodoUsina());

            dadosExportacaoTaxa.Validar();


            var teste = dadosExportacaoTaxa.MensagensNotificacoesAgrupadas;

            Assert.True(dadosExportacaoTaxa.Valido);
        }

        [Fact]
        public void EntidadeInvalidaQuandoDadosPeriodosNulo()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                taxasCollection,
                taxasCollection,
                null);

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaQuandoQuantidadeDadosPeriodosMenorQue60()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                taxasCollection,
                taxasCollection,
                ObterDadosPeriodoUsina(10));

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Invalido);
        }

        [Fact]
        public void EntidadeValidaQuandoQuantidadeDadosPeriodosIgual60()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                taxasCollection,
                taxasCollection,
                ObterDadosPeriodoUsina(60));

            dadosExportacaoTaxa.Validar();

            Assert.True(dadosExportacaoTaxa.Valido);
        }

        [Fact]
        public void AdicionarTaxasReferencia_DeveAdicionarTaxasSeValoresDiferentesNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasReferencia(taxasCollection);

            Assert.False(dadosExportacaoTaxa.TaxasReferencia.IsNullOrEmpty());
        }

        [Fact]
        public void AdicionarTaxasReferencia_NaoDeveAdicionarTaxasSeValoresIguaisNuloOuVazio()
        {
            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");


            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasReferencia(null);

            Assert.True(dadosExportacaoTaxa.TaxasReferencia.IsNullOrEmpty());
        }

        [Fact]
        public void AdicionarTaxasAuxiliares_DeveAdicionarTaxasSeValoresDiferentesNuloOuVazio()
        {
            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasAuxiliares(taxasCollection);

            Assert.False(dadosExportacaoTaxa.TaxasAuxiliares.IsNullOrEmpty());
        }

        [Fact]
        public void AdicionarTaxasAuxiliares_NaoDeveAdicionarTaxasSeValoresIguaisNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasAuxiliares(null);

            Assert.True(dadosExportacaoTaxa.TaxasAuxiliares.IsNullOrEmpty());
        }

        [Fact]
        public void AdicionarDadosPeriodos_DeveAdicionarPeriodosSeValoresDiferentesNuloOuVazio()
        {
            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarDadosPeriodos(ObterDadosPeriodoUsina());

            Assert.False(dadosExportacaoTaxa.DadosPeriodos.IsNullOrEmpty());
        }

        [Fact]
        public void AdicionarDadosPeriodos_NaoDeveAdicionarPeriodosSeValoresIguaisNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarDadosPeriodos(null);

            Assert.True(dadosExportacaoTaxa.DadosPeriodos.IsNullOrEmpty());
        }

        [Fact]
        public void PossuiTaxasAuxiliares_DeveRetornarVerdadeiroSeTaxasAuxiliaresDiferenteNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasAuxiliares(taxasCollection);

            Assert.True(dadosExportacaoTaxa.PossuiTaxasAuxiliares());
        }

        [Fact]
        public void PossuiTaxasAuxiliares_DeveRetornarFalsoSeTaxasAuxiliaresIgualNuloOuVazio()
        {
            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            Assert.False(dadosExportacaoTaxa.PossuiTaxasAuxiliares());
        }

        [Fact]
        public void ObterQuantidadeTaxasAuxiliares_DeveRetornarMaiorQue0SeTaxasAuxiliaresDiferenteNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");

            var taxasCollection = new List<CalculoTaxa> { taxaExportacao };

            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            dadosExportacaoTaxa.AdicionarTaxasAuxiliares(taxasCollection);

            Assert.Equal(1, dadosExportacaoTaxa.ObterQuantidadeTaxasAuxiliares());
        }

        [Fact]
        public void ObterQuantidadeTaxasAuxiliares_DeveRetornar0SeTaxasAuxiliaresIgualNuloOuVazio()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");


            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            Assert.Equal(0, dadosExportacaoTaxa.ObterQuantidadeTaxasAuxiliares());
        }

        [Fact]
        public void ObterDataReferenciaExportacaoFormatada_DeveRetornarVazioSeDataRefereciaDefault()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");


            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                default,
                null,
                null,
                null);

            Assert.False(dadosExportacaoTaxa.ObterDataReferenciaExportacaoFormatada().HasValue());
        }

        [Fact]
        public void ObterDataReferenciaExportacaoFormatada_DeveRetornarDataFormatadaSeDataRefereciaNaoDefault()
        {

            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1",
                1.0,
                "1",
                null,
                "1");


            var dadosExportacaoTaxa = new DadosExportacaoTaxa(
                "1",
                taxaExportacao,
                DateTime.Now,
                null,
                null,
                null);

            Assert.True(dadosExportacaoTaxa.ObterDataReferenciaExportacaoFormatada().HasValue());
        }

        private List<DadosPeriodoUsina> ObterDadosPeriodoUsina(int quantidade = 60)
        {
            var dadosPeriodoUsina = new List<DadosPeriodoUsina>();

            for (int i = 0; i < quantidade; i++)
            {
                dadosPeriodoUsina.Add(new DadosPeriodoUsina(DateTime.Now, true, null, null));
            }

            return dadosPeriodoUsina;
        }

    }
}
