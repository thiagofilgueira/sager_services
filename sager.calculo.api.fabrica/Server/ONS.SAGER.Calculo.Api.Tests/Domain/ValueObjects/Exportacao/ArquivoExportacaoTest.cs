﻿using ONS.SAGER.Calculo.Api.Domain.ValueObjects.Exportacao;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.ValueObjects.Exportacao
{
    public class ArquivoExportacaoTest
    {

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void EntidadeInvalidaSeNomeNuloOuVazio(string nome)
        {
            var request = new ArquivoExportacao(nome, ".pdf", new byte[41]);

            Assert.True(request.Invalido);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void EntidadeInvalidaSeExtensaoNulaOuVazia(string extensao)
        {
            var request = new ArquivoExportacao("arquivo", extensao, new byte[41]);

            Assert.True(request.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeConteudoIgualNulo()
        {
            var request = new ArquivoExportacao("arquivo", ".pdf", null);

            Assert.True(request.Invalido);
        }
    }
}
