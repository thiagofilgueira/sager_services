﻿using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.ValueObjects.ExportacaoTaxa
{
    public class DadosPeriodoUsinaTest
    {
        [Fact]
        public void EntidadeInvalidaSeDataReferenciaDefault()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(default, true, null, null);

            Assert.True(dadosPeriodoUsina.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeDataReferenciaInvalida()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018,01,5), true, null, null);

            Assert.True(dadosPeriodoUsina.Invalido);
        }

        [Fact]
        public void AdicionarTaxas_NaoDeveAdicionarTaxasSeParametroNulo()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            dadosPeriodoUsina.AdicionarTaxas(null);

            Assert.Empty(dadosPeriodoUsina.Taxas);
        }

        [Fact]
        public void AdicionarTaxas_NaoDeveAdicionarTaxasSeParametroVazio()
        {
            var taxas = new List<CalculoTaxa>
            {
                new CalculoTaxa(
                "1",
                TipoCalculo.TEIFAacum.ToString(),
                TipoCalculo.TEIFAacum.ToString(),
                "1",
                125.3625,
                "1",
                null,
                "1")
            };

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, taxas, null);

            dadosPeriodoUsina.AdicionarTaxas(new List<CalculoTaxa>());

            Assert.Single(dadosPeriodoUsina.Taxas);
        }

        [Fact]
        public void AdicionarTaxas_DeveAdicionarTaxasSeParametroNaoVazio()
        {

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            var taxas = new List<CalculoTaxa>
            {
                new CalculoTaxa(
                "1",
                TipoCalculo.TEIFAacum.ToString(),
                TipoCalculo.TEIFAacum.ToString(),
                "1",
                125.3625,
                "1",
                null,
                "1")
            };

            dadosPeriodoUsina.AdicionarTaxas(taxas);

            Assert.Single(dadosPeriodoUsina.Taxas);
        }

        [Fact]
        public void AdicionarDadosUnidadeGeradoras_NaoDeveAdicionarSeParametroNulo()
        {
            var dadosUnidadesGeradoras = new List<DadosUnidadeGeradora>
            {
                new DadosUnidadeGeradora("1", 1, null , null)
            };

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, dadosUnidadesGeradoras);

            dadosPeriodoUsina.AdicionarDadosUnidadeGeradoras(null);

            Assert.Single(dadosPeriodoUsina.DadosUnidadesGeradoras);
        }

        [Fact]
        public void AdicionarDadosUnidadeGeradoras_NaoDeveAdicionarSeParametroVazio()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            dadosPeriodoUsina.AdicionarDadosUnidadeGeradoras(new List<DadosUnidadeGeradora>());

            Assert.Empty(dadosPeriodoUsina.DadosUnidadesGeradoras);
        }

        [Fact]
        public void AdicionarDadosUnidadeGeradoras_DeveAdicionarSeParametroNaoVazio()
        {

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            var dadosUnidadesGeradoras = new List<DadosUnidadeGeradora>
            {
                new DadosUnidadeGeradora("1", 1, null , null)
            };

            dadosPeriodoUsina.AdicionarDadosUnidadeGeradoras(dadosUnidadesGeradoras);

            Assert.Single(dadosPeriodoUsina.DadosUnidadesGeradoras);
        }

        [Fact]
        public void ObterTotalEventosPeriodo_DeveObter0SeDadosUnidadesGeradorasNulo()
        {
            var request = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            Assert.Equal(0, request.ObterTotalEventosPeriodo());
        }

        [Fact]
        public void ObterTotalEventosPeriodo_DeveObter0SeNaoPossuirEventos()
        {
            var dadosUnidadesGeradoras = new List<DadosUnidadeGeradora>
            {
                new DadosUnidadeGeradora("1", 1, null , null)
            };

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, dadosUnidadesGeradoras);

            Assert.Equal(0, dadosPeriodoUsina.ObterTotalEventosPeriodo());
        }

        [Fact]
        public void ObterTotalEventosPeriodo_DeveObterTotalEventos()
        {
            var dadosUnidadesGeradoras = new List<DadosUnidadeGeradora>
            {
                new DadosUnidadeGeradora(
                    "1",
                    1, 
                    null,
                    new List<EventoUnidadeGeradora>
                    {
                    new EventoUnidadeGeradora(null,null,default,null,null,null,1, new TimeSpan())
                    })
            };

            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, dadosUnidadesGeradoras);

            Assert.Equal(1, dadosPeriodoUsina.ObterTotalEventosPeriodo());
        }

        [Fact]
        public void ObterMesAnoPeriodoFormatado_DeveObterVazioSeDataReferenciaDefaut()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(default, true, null, null);

            Assert.Empty(dadosPeriodoUsina.ObterMesAnoPeriodoFormatado());
        }

        [Fact]
        public void ObterMesAnoPeriodoFormatado_DeveObterDataFormatadaSeDataReferenciaNaoDefaut()
        {
            var dadosPeriodoUsina = new DadosPeriodoUsina(new DateTime(2018, 01, 1), true, null, null);

            Assert.NotEmpty(dadosPeriodoUsina.ObterMesAnoPeriodoFormatado());
        }
    }
}
