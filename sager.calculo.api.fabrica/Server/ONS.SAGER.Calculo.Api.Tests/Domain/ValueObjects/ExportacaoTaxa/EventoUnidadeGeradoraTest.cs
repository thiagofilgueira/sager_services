﻿using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Value_Objects;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.ValueObjects.ExportacaoTaxa
{
    public class EventoUnidadeGeradoraTest
    {
        [Fact]
        public void EntidadeInvalidaSeDataHoraDefault()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null,null,default,"1","1","1",1,new TimeSpan());

            Assert.True(dadosPeriodoUsina.Invalido);
        }

        [Fact]
        public void EntidadeValidaSeDataHoraValida()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            Assert.True(dadosPeriodoUsina.Valido);
        }

        [Fact]
        public void EntidadeInvalidaSeEstadoOperativoECondicaoOperativaEOrigemNulos()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, null, null, null, 1, new TimeSpan());

            Assert.True(dadosPeriodoUsina.Invalido);
        }

        [Fact]
        public void ObterDataHoraFormatados_RetornaVazioSeDataHoraDefault()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, default, "1", "1", "1", 1, new TimeSpan());

            Assert.Empty(dadosPeriodoUsina.ObterDataHoraFormatados());
        }

        [Fact]
        public void ObterDataHoraFormatados_RetornaDataHoraFormatadaSeDataHoraNaoDefault()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            Assert.NotEmpty(dadosPeriodoUsina.ObterDataHoraFormatados());
        }

        [Fact]
        public void AdicionarParametrosInfluenciados_NaoAdicionaParametrosSeNulo()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(null);

            Assert.Empty(dadosPeriodoUsina.ParametrosInfluenciados);
        }

        [Fact]
        public void AdicionarParametrosInfluenciados_NaoAdicionaParametrosSeVazio()
        {
            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(new List<TipoCalculo>());

            Assert.Empty(dadosPeriodoUsina.ParametrosInfluenciados);
        }

        [Fact]
        public void AdicionarParametrosInfluenciados_AdicionarParametrosSeParametroNaoExisteNaLista()
        {
            var parametros = new List<TipoCalculo>()
            {
                TipoCalculo.HP
            };

            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(parametros);

            Assert.Single(dadosPeriodoUsina.ParametrosInfluenciados);
        }

        [Fact]
        public void AdicionarParametrosInfluenciados_NaoAdicionaParametrosSeParametroJaExisteNaLista()
        {
            var parametrosAnteriores = new List<TipoCalculo>()
            {
                TipoCalculo.HP
            };

            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(parametrosAnteriores);

            var parametrosNovos = new List<TipoCalculo>()
            {
                TipoCalculo.HP
            };

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(parametrosNovos);

            Assert.Single(dadosPeriodoUsina.ParametrosInfluenciados);
        }

        [Fact]
        public void ObterCaracterMarcacao_ObterCaracterSeParametroInfluenciado()
        {
            var parametros = new List<TipoCalculo>()
            {
                TipoCalculo.HP
            };

            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(parametros);

            Assert.NotEmpty(dadosPeriodoUsina.ObterCaracterMarcacao(TipoCalculo.HP));
        }

        [Fact]
        public void ObterCaracterMarcacao_RetornaVazioSeParametroNaoInfluenciado()
        {
            var parametros = new List<TipoCalculo>()
            {
                TipoCalculo.HP
            };

            var dadosPeriodoUsina = new EventoUnidadeGeradora(null, null, DateTime.Now, "1", "1", "1", 1, new TimeSpan());

            dadosPeriodoUsina.AdicionarParametrosInfluenciados(parametros);

            Assert.Empty(dadosPeriodoUsina.ObterCaracterMarcacao(TipoCalculo.HEDP));
        }
    }
}
