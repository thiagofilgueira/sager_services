﻿using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.ValueObjects.ExportacaoTaxa
{
    public class DadosUnidadeGeradoraTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void EntidadeInvalidaSeUnidadeGeradoraIdNuloOuVazio(string unidadeGeradoraId)
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora(unidadeGeradoraId, 1, null, null);

            Assert.True(dadosUnidadeGeradora.Invalido);
        }

        [Fact]
        public void EntidadeValidaSeUnidadeGeradoraIdPossuiValor()
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, null);

            Assert.True(dadosUnidadeGeradora.Valido);
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterUltimoParametroVersionado_RetornaNuloSeNaoPossuiParametros(TipoCalculo tipoCalculo)
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, null);

            Assert.Null(dadosUnidadeGeradora.ObterUltimoParametroVersionado(tipoCalculo));
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterUltimoParametroVersionado_RetornaParametroSePossuiParametros(TipoCalculo tipoCalculo)
        {
            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 41, "1", null, "1")
            };

            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, parametros, null);

            Assert.NotNull(dadosUnidadeGeradora.ObterUltimoParametroVersionado(tipoCalculo));
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterVersaoUltimoParametroVersionado_RetornaNuloSeNaoPossuiParametros(TipoCalculo tipoCalculo)
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, null);

            Assert.Null(dadosUnidadeGeradora.ObterVersaoUltimoParametroVersionado(tipoCalculo));
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterVersaoUltimoParametroVersionado_RetornaVersaoSePossuiParametros(TipoCalculo tipoCalculo)
        {
            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 41, "1", null, "1")
            };

            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, parametros, null);

            Assert.NotNull(dadosUnidadeGeradora.ObterVersaoUltimoParametroVersionado(tipoCalculo));
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterValorUltimoParametroVersionado_RetornaNuloSeNaoPossuiParametros(TipoCalculo tipoCalculo)
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, null);

            Assert.Null(dadosUnidadeGeradora.ObterValorUltimoParametroVersionado(tipoCalculo));
        }

        [Theory]
        [InlineData(TipoCalculo.HP)]
        [InlineData(TipoCalculo.HDP)]
        [InlineData(TipoCalculo.HEDP)]
        public void ObterValorUltimoParametroVersionado_RetornaValorSePossuiParametros(TipoCalculo tipoCalculo)
        {
            var parametros = new List<ParametroTaxa>
            {
                new ParametroTaxa("1", TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString(), "1", 41, "1", null, "1"),
                new ParametroTaxa("1", TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString(), "1", 41, "1", null, "1")
            };

            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, parametros, null);

            Assert.NotNull(dadosUnidadeGeradora.ObterValorUltimoParametroVersionado(tipoCalculo));
        }

        [Fact]
        public void ObterTotalEventos_Retorna0SeNaoPossuiEventos()
        {
            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, null);

            Assert.Equal(0,dadosUnidadeGeradora.ObterTotalEventos());
        }

        [Fact]
        public void ObterTotalEventos_RetornaTotalEventosSePossuiEventos()
        {
            var eventos = new List<EventoUnidadeGeradora>
            {
                new EventoUnidadeGeradora(null,null,default,null,null,null,1, new TimeSpan())
            };

            var dadosUnidadeGeradora = new DadosUnidadeGeradora("1", 1, null, eventos);

            Assert.Equal(1, dadosUnidadeGeradora.ObterTotalEventos());
        }
    }
}
