﻿using Moq;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Services;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Services
{
    public class TaxaServiceTest
    {
        [Fact]
        public void DeveRetornarTaxaServiceSeRepositorioEAppSettingsNaoNulos()
        {
            var service = new TaxaService(new Mock<ITaxaRepository>().Object);

            Assert.NotNull(service);
        }
    }
}
