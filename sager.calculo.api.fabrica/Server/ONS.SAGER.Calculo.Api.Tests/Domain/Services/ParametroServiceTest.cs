﻿using Moq;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Services;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Services
{
    public class ParametroServiceTest
    {
        [Fact]
        public void DeveRetornarParametroServiceSeRepositorioEAppSettingsNaoNulos()
        {
            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            Assert.NotNull(service);
        }
    }
}
