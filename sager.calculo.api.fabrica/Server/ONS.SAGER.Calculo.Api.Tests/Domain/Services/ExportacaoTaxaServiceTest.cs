﻿using Moq;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.Services;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SDK.Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Services
{
    public class ExportacaoTaxaServiceTest
    {
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;
        private readonly IUsinaRepository _usinaRepository;
        private readonly IParametroService _parametroService;
        private readonly IUnidadeGeradoraService _unidadeGeradoraService;
        private readonly IConsolidacaoMensalRepository _consolidacaoMensalRepository;
        private readonly AppSettings _appSettings;

        public ExportacaoTaxaServiceTest()
        {
            var usinaRepository = new Mock<IUsinaRepository>();
            usinaRepository.Setup(p => p.ObterUsinaPorId(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(ObterUsina());
            _usinaRepository = usinaRepository.Object;

            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();
            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(ObterUnidadesGeradoras());
            _unidadeGeradoraService = unidadeGeradoraService.Object;

            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(It.IsAny<DateTime>(), It.IsAny<string>()))
                .ReturnsAsync(ObterDatasConsolidacoesMensais());
            _consolidacaoMensalRepository = consolidacaoMensalRepository.Object;

            var appSettings = new Mock<AppSettings>();
            appSettings.Setup(p => p.BranchMaster).Returns("master");
            _appSettings = appSettings.Object;

            var taxaRepository = new Mock<ITaxaRepository>();

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterTaxas());

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterTaxas());

            taxaRepository.Setup(p => p.ObterTaxasPorContextoCalculo(
                It.IsAny<string>(),
                It.IsAny<TipoCalculo>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterTaxas());

            _taxaRepository = taxaRepository.Object;

            var parametroRepository = new Mock<IParametroRepository>();

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterParametros().AsEnumerable());

            _parametroRepository = parametroRepository.Object;


            _parametroService = new Mock<IParametroService>().Object;
        }


        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentNullExceptionQuandoRequestForNulo()
        {
            var service = new ExportacaoTaxaService(null, null, null, null, null, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(null));

            await Assert.ThrowsAsync<ArgumentNullException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentExceptionQuandoUsinaForNula()
        {

            var usinRepository = new Mock<IUsinaRepository>();
            usinRepository.Setup(p => p.ObterUsinaPorId(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult<Usina>(null));

            var service = new ExportacaoTaxaService(
                _taxaRepository,
                _parametroRepository,
                usinRepository.Object,
                _parametroService,
                _unidadeGeradoraService,
                _consolidacaoMensalRepository,
                _appSettings);

            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(request));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentExceptionQuandoUnidadesGeradorasForemNulas()
        {
            var unidadeGeradoraService = new Mock<IUnidadeGeradoraService>();
            unidadeGeradoraService.Setup(p => p.ObterUnidadesGeradoras(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(Task.FromResult<List<UnidadeGeradora>>(null));

            var service = new ExportacaoTaxaService(
                _taxaRepository,
                _parametroRepository,
                _usinaRepository,
                _parametroService,
                unidadeGeradoraService.Object,
                _consolidacaoMensalRepository,
                _appSettings);

            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(request));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentExceptionQuandoConsolidacoesMensaisForemNulas()
        {
            var consolidacaoMensalRepository = new Mock<IConsolidacaoMensalRepository>();
            consolidacaoMensalRepository.Setup(p => p.ObterConsolidacoesAteDataReferencia(It.IsAny<DateTime>(), It.IsAny<string>()))
                .Returns(Task.FromResult<IEnumerable<ConsolidacaoMensal>>(null));

            var service = new ExportacaoTaxaService(
                _taxaRepository,
                _parametroRepository,
                _usinaRepository,
                _parametroService,
                _unidadeGeradoraService,
                consolidacaoMensalRepository.Object,
                _appSettings);

            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(request));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentExceptionQuandoTaxaExportacaoNaoExistir()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .Returns(Task.FromResult<IEnumerable<CalculoTaxa>>(null));

            var service = new ExportacaoTaxaService(
                taxaRepository.Object,
                _parametroRepository,
                _usinaRepository,
                _parametroService,
                _unidadeGeradoraService,
                _consolidacaoMensalRepository,
                _appSettings);

            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(request));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarArgumentExceptionQuandoUsinaNaoPossuiParametros()
        {
            var parametroRepository = new Mock<IParametroRepository>();
            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .Returns(Task.FromResult<IEnumerable<ParametroTaxa>>(null));

            var service = new ExportacaoTaxaService(
                _taxaRepository,
                parametroRepository.Object,
                _usinaRepository,
                _parametroService,
                _unidadeGeradoraService,
                _consolidacaoMensalRepository,
                _appSettings);

            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var @action = new Func<Task>(() => service.ObterDadosPlanilha(request));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterDadosPlanilha_DeveRetornarDadosExportacaoTaxaQuandoPossuiTodosInsumos()
        {
            var service = new ExportacaoTaxaService(
                _taxaRepository,
                _parametroRepository,
                _usinaRepository,
                _parametroService,
                _unidadeGeradoraService,
                _consolidacaoMensalRepository,
                _appSettings);


            var request = new ExportacaoTaxaRequest("1", new DateTime(2019, 12, 1), TipoCalculo.TEIPacum, null, null);

            var dadosPlanilha = await service.ObterDadosPlanilha(request);

            Assert.NotNull(dadosPlanilha);
        }

        #region Métodos Privados

        private Usina ObterUsina()
        {
            return new Usina()
            {
                DataDesativacao = null,
                DataEntradaOperacao = new DateTime(2000,1,1),
                DataRenovacaoConcessao = new DateTime(2012,1,1),
                Id = Guid.NewGuid().ToString(),
                UsinaId = "1",
                _Metadata = ObterMetadata()
            };
        }

        private List<UnidadeGeradora> ObterUnidadesGeradoras()
        {
            var unidadeGeradora = new UnidadeGeradora()
            {
                UsinaId = "1",
                Id = Guid.NewGuid().ToString(),
                DataDesativacao = null,
                DataEntradaOperacao = new DateTime(2000, 1, 1),
                DataEventoEOC = new DateTime(2000, 1, 1),
                UnidadeGeradoraId = "1",
                IdoOns = "1",
                EquipamentoId = "1",
                _Metadata = ObterMetadata()
            };


            unidadeGeradora.AdicionarEventos(ObterEventosUnidadeGeradora());
            unidadeGeradora.AdicionarPotencias(ObterPotenciasUnidadeGeradora());

            return new List<UnidadeGeradora>
            {
                unidadeGeradora
            };
        }


        private IEnumerable<PotenciaUnidadeGeradora> ObterPotenciasUnidadeGeradora()
        {
            return new List<PotenciaUnidadeGeradora>()
            {
                new PotenciaUnidadeGeradora(new DateTime(2000,1,1), null, 120)
            };
        }

        private IEnumerable<EventoMudancaEstadoOperativo> ObterEventosUnidadeGeradora()
        {
            var eventos = new List<EventoMudancaEstadoOperativo>();

            for (DateTime data = new DateTime(2000,1,1); data < new DateTime(2020, 1, 1); data = data.AddMonths(1))
            {
                eventos.Add(new EventoMudancaEstadoOperativo(
                    data,
                    TipoEstadoOperativo.DAP.ToString(),
                    TipoCondicaoOperativa.RFO.ToString(),
                    TipoClassificadorOrigem.GCB.ToString(),
                    null,
                    100));
            }

            return eventos;
        }
        private List<ConsolidacaoMensal> ObterDatasConsolidacoesMensais()
        {
            var datasConsolidacoes = new List<ConsolidacaoMensal>();

            int id = 1;
            for (DateTime data = new DateTime(1960, 1, 1); data < new DateTime(2020, 1, 1); data = data.AddMonths(1))
            {
                datasConsolidacoes.Add(new ConsolidacaoMensal(id.ToString(), data.Month, data.Year));

                id++;
            }

            return datasConsolidacoes;
        }

        private Metadata ObterMetadata()
        {
            return new Metadata()
            {
                Branch = "master",
                CreatedAt = new DateTime(2000, 1, 1)
            };
        }

        private List<CalculoTaxa> ObterTaxas()
        {
            var taxas = new List<CalculoTaxa>();

            var consolidacoes = ObterDatasConsolidacoesMensais();

            foreach (var consolidacao in consolidacoes)
            {
                taxas.Add(new CalculoTaxa(
                    consolidacao.ConsolidacaoMensalId, 
                    TipoCalculo.TEIPacum.ToString(),
                    TipoCalculo.TEIPacum.ToString(),
                    "1", 
                    40, 
                    "1",
                    null,
                    "1"));



                taxas.Add(new CalculoTaxa(
                    consolidacao.ConsolidacaoMensalId, 
                    TipoCalculo.IPReferencia.ToString(), 
                    TipoCalculo.IPReferencia.ToString(), 
                    "1",
                    40, 
                    "1",
                    null,
                    "1"));
            }

            taxas.ForEach(p => p._Metadata = ObterMetadata());

            return taxas;
        }

        private CalculoTaxa ObterTaxaReferencia()
        {
            var consolidacoes = ObterDatasConsolidacoesMensais();

            var consolidacaoId = consolidacoes.FirstOrDefault(p => p.DataReferencia == new DateTime(2020, 1, 1))?.ConsolidacaoMensalId;

            return new CalculoTaxa(consolidacaoId, TipoCalculo.IPReferencia.ToString(), TipoCalculo.IPReferencia.ToString(), "1", 40, "1", null, "1");
        }

        private List<ParametroTaxa> ObterParametros()
        {
            var parametros = new List<ParametroTaxa>();

            var consolidacoes = ObterDatasConsolidacoesMensais();

            foreach(var consolidacao in consolidacoes)
            {
                parametros.AddRange(new List<ParametroTaxa>
                {
                new ParametroTaxa(consolidacao.ConsolidacaoMensalId, TipoCalculo.HP.ToString(), TipoCalculo.HP.ToString() , "1", 40, "1", null, "1"),
                new ParametroTaxa(consolidacao.ConsolidacaoMensalId, TipoCalculo.HDP.ToString(), TipoCalculo.HDP.ToString() , "1", 20, "1", null, "1"),
                new ParametroTaxa(consolidacao.ConsolidacaoMensalId, TipoCalculo.HEDP.ToString(), TipoCalculo.HEDP.ToString() , "1", 15, "1", null, "1")
                });
            }

            parametros.ForEach(p => p._Metadata = ObterMetadata());

            return parametros;
        }

        #endregion
    }
}
