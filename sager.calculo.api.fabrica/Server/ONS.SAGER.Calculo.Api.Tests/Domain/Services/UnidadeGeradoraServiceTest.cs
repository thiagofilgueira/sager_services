﻿using Moq;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Services;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Services
{
    public class UnidadeGeradoraServiceTest
    {
        [Fact]
        public void DeveRetornarUnidadeGeradoraServiceSeParametrosNaoNulos()
        {
            var service = new UnidadeGeradoraService(
                new Mock<IUnidadeGeradoraRepository>().Object,
                new Mock<ISuspensaoUnidadeGeradoraRepository>().Object,
                new Mock<IPotenciaUnidadeGeradoraRepository>().Object,
                new Mock<IEventoMudancaEstadoOperativoRepository>().Object,
                new Mock<IParametroRepository>().Object);

            Assert.NotNull(service);
        }
    }
}
