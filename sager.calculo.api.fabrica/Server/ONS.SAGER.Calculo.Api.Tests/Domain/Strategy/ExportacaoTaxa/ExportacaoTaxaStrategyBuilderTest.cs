﻿using Moq;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Strategy.ExportacaoTaxa
{
    public class ExportacaoTaxaStrategyBuilderTest
    {
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;


        public ExportacaoTaxaStrategyBuilderTest()
        {
            var taxaRepository = new Mock<ITaxaRepository>();
            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterTaxas());

            taxaRepository.Setup(p => p.ObterTaxasPorContextosCalculo(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterTaxas());

            _taxaRepository = taxaRepository.Object;

            var parametroRepository = new Mock<IParametroRepository>();

            parametroRepository.Setup(p => p.ObterParametrosPorContextosCalculos(
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<TipoCalculo[]>(),
                It.IsAny<IEnumerable<string>>(),
                It.IsAny<string>()))
                .ReturnsAsync(ObterParametros().AsEnumerable());

            _parametroRepository = parametroRepository.Object;

        }

        [Fact]
        public void DeveRetornarArgumentNullExceptionQuandoRequestForNulo()
        {
            var @action = new Action(() => ExportacaoTaxaStrategyBuilder.Build(null, _taxaRepository, _parametroRepository));

             Assert.Throws<ArgumentNullException>(@action);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacumAjustada)]
        [InlineData(TipoCalculo.TEIPIndiceIndisponibilidade)]
        [InlineData(TipoCalculo.TEIPmes)]
        [InlineData(TipoCalculo.TEIPmesAjustada)]
        [InlineData(TipoCalculo.TEIP_FC)]
        [InlineData(TipoCalculo.TEIP_OP)]
        [InlineData(TipoCalculo.TEIFaacumAjustada)]
        [InlineData(TipoCalculo.TEIFaIndiceIndisponibilidade)]
        [InlineData(TipoCalculo.TEIFAmes)]
        [InlineData(TipoCalculo.TEIFaMesAjustada)]
        [InlineData(TipoCalculo.TEIFA_FC)]
        [InlineData(TipoCalculo.TEIFA_OP)]
        public void DeveRetornarArgumentExceptionQuandoTipoTaxaNaoDefinidaNaEstrategia(TipoCalculo tipoTaxa)
        {
            var exportacaoRequest = new ExportacaoTaxaRequest()
            {
                DataReferencia = DateTime.Now,
                TipoTaxa = tipoTaxa
            };

            var @action = new Action(() => ExportacaoTaxaStrategyBuilder.Build(exportacaoRequest, _taxaRepository, _parametroRepository));

            Assert.Throws<ArgumentException>(@action);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacum, true)]
        [InlineData(TipoCalculo.TEIPacum, false)]
        [InlineData(TipoCalculo.TEIFAacum, true)]
        [InlineData(TipoCalculo.TEIFAacum, false)]
        [InlineData(TipoCalculo.IndiceIndisponibilidadeVerificada, false)]
        public void DeveRetornarEstrategiaDeCalculoQuandoTipoTaxaDefinidaNaEstrategia(TipoCalculo tipoTaxa, bool primeiraVersao)
        {
            var exportacaoRequest = new ExportacaoTaxaRequest()
            {
                DataReferencia = ObterDataPorVersao(primeiraVersao),
                TipoTaxa = tipoTaxa
            };

            var retorno = ExportacaoTaxaStrategyBuilder.Build(exportacaoRequest, _taxaRepository, _parametroRepository);

            Assert.NotNull(retorno);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacum, true)]
        [InlineData(TipoCalculo.TEIPacum, false)]
        [InlineData(TipoCalculo.TEIFAacum, true)]
        [InlineData(TipoCalculo.TEIFAacum, false)]
        [InlineData(TipoCalculo.IndiceIndisponibilidadeVerificada, false)]
        public void ObterDataEntradaOperacaoUsina_DeveRetornarDataValida(TipoCalculo tipoTaxa, bool primeiraVersao)
        {
            var exportacaoRequest = new ExportacaoTaxaRequest()
            {
                DataReferencia = ObterDataPorVersao(primeiraVersao),
                TipoTaxa = tipoTaxa
            };

            var usina = new Usina()
            {
                UsinaId = "1",
                DataEntradaOperacao = DateTime.Now,
                DataRenovacaoConcessao = DateTime.Now
            };

            var strategy = ExportacaoTaxaStrategyBuilder.Build(exportacaoRequest, _taxaRepository, _parametroRepository);

            Assert.False(strategy.ObterDataEntradaOperacaoUsina(usina) == default);
        }

        private List<ParametroTaxa> ObterParametros()
        {
            return new List<ParametroTaxa>
            {
                new ParametroTaxa("1",TipoCalculo.HP.ToString(),TipoCalculo.HP.ToString(),"1", 40, "1", null, "1"),
                new ParametroTaxa("1",TipoCalculo.HDP.ToString(),TipoCalculo.HDP.ToString(),"1", 20, "1", null, "1"),
                new ParametroTaxa("1",TipoCalculo.HEDP.ToString(),TipoCalculo.HEDP.ToString(),"1", 15, "1", null, "1")
            };
        }

        private List<CalculoTaxa> ObterTaxas()
        {
            return new List<CalculoTaxa>
            {
                new CalculoTaxa("1", TipoCalculo.TEIPIndiceIndisponibilidade.ToString(), TipoCalculo.TEIPIndiceIndisponibilidade.ToString(),"1",40,  "1", null, "1"),
                new CalculoTaxa("1", TipoCalculo.TEIFaIndiceIndisponibilidade.ToString(), TipoCalculo.TEIFaIndiceIndisponibilidade.ToString(),"1",25, "1", null, "1")
            };

        }

        private DateTime ObterDataPorVersao(bool primeiraVersao)
        {
            if (primeiraVersao)
            {
                return new DateTime(2010, 1, 1);
            }

            return new DateTime(2018, 1, 1);
        }


    }
}
