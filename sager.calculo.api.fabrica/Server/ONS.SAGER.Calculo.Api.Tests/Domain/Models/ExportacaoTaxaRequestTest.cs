﻿using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Domain.Models
{
    public class ExportacaoTaxaRequestTest
    {
        [Fact]
        public void EntidadeInvalidaSeEntidadeVazia()
        {
            var request = new ExportacaoTaxaRequest();

            request.Validar();

            Assert.True(request.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeUsinaForNula()
        {
            var request = new ExportacaoTaxaRequest(
                null,
                new DateTime(2014, 01, 01),
                TipoCalculo.TEIFAacum,
                null,
                null);

            Assert.True(request.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeDataReferenciaDefault()
        {
            var request = new ExportacaoTaxaRequest()
            {
                UsinaId = "1",
                DataReferencia = default,
                TipoTaxa = TipoCalculo.TEIFAacum,
                ConfiguracaoCenarioId = null,
                VersaoTaxa = null
            };

            request.Validar();

            Assert.True(request.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeDataReferenciaInvalida()
        {
            var request = new ExportacaoTaxaRequest(
                "1",
                new DateTime(2014, 01, 25),
                TipoCalculo.TEIFAacum,
                null,
                null);

            Assert.True(request.Invalido);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIFaacumAjustada)]
        [InlineData(TipoCalculo.TEIPacumAjustada)]
        [InlineData(TipoCalculo.TEIPmes)]
        [InlineData(TipoCalculo.TEIFAmes)]
        public void EntidadeInvalidaSeTipoTaxaInvalida(TipoCalculo tipoTaxa)
        {
            var request = new ExportacaoTaxaRequest()
            {
                UsinaId = "1",
                DataReferencia = new DateTime(2014, 01, 1),
                TipoTaxa = tipoTaxa,
                ConfiguracaoCenarioId = null,
                VersaoTaxa = null
            };

            request.Validar();

            Assert.True(request.Invalido);
        }

        [Fact]
        public void EntidadeInvalidaSeTipoTaxaIgualIndiceIndispEDataMenorQueOutubro2014()
        {
            var request = new ExportacaoTaxaRequest(
                "1",
                new DateTime(2014, 01, 1),
                TipoCalculo.IndiceIndisponibilidadeVerificada,
                null,
                null);

            Assert.True(request.Invalido);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacum)]
        [InlineData(TipoCalculo.TEIFAacum)]
        public void EntidadeValidaSeTipoTaxaDiferentelIndiceIndispEDataMenorQueOutubro2014(TipoCalculo tipoTaxa)
        {
            var request = new ExportacaoTaxaRequest()
            {
                UsinaId = "1",
                DataReferencia = new DateTime(2014, 01, 1),
                TipoTaxa = tipoTaxa,
                ConfiguracaoCenarioId = null,
                VersaoTaxa = null
            };

            request.Validar();

            Assert.True(request.Valido);
        }

        [Fact]
        public void EntidadeValidaSeTipoTaxaIgualIndiceIndispEDataMaiorQueOutubro2014()
        {
            var request = new ExportacaoTaxaRequest(
                "1",
                new DateTime(2015, 01, 1),
                TipoCalculo.IndiceIndisponibilidadeVerificada,
                null,
                null);

            Assert.True(request.Valido);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacum)]
        [InlineData(TipoCalculo.TEIFAacum)]
        public void EntidadeValidaSeTipoTaxaDiferentelIndiceIndispEDataMaiorQueOutubro2014(TipoCalculo tipoTaxa)
        {
            var request = new ExportacaoTaxaRequest()
            {
                UsinaId = "1",
                DataReferencia = new DateTime(2018, 01, 1),
                TipoTaxa = tipoTaxa,
                ConfiguracaoCenarioId = null,
                VersaoTaxa = null
            };

            request.Validar();

            Assert.True(request.Valido);
        }

    }
}
