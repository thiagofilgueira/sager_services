﻿using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Exportador.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Exportador.Services
{
    public class PlanilhaExportacaoTaxaServiceTest
    {
        [Fact]
        public async Task ObterPlanilhaExportacaoTaxa_DeveRetornarArgumentNullExceptionQuandoPlanilhaDadosExportacaoNulo()
        {
            var service = new PlanilhaExportacaoTaxaService();

            var @action = new Func<Task>(() => service.ObterPlanilhaExportacaoTaxa(null));

            await Assert.ThrowsAsync<ArgumentNullException>(@action);
        }

        [Fact]
        public async Task ObterPlanilhaExportacaoTaxa_DeveRetornarArgumentNullExceptionQuandoPlanilhaDadosExportacaoInvalida()
        {
            var service = new PlanilhaExportacaoTaxaService();

            var dadosExportacaoTaxa = new DadosExportacaoTaxa("1", null, new DateTime(), null, null, null);

            var @action = new Func<Task>(() => service.ObterPlanilhaExportacaoTaxa(dadosExportacaoTaxa));

            await Assert.ThrowsAsync<ArgumentException>(@action);
        }

        [Fact]
        public async Task ObterPlanilhaExportacaoTaxa_DeveRetornarPlanilhaExportacaoQuandoDadosExportacaoTaxaValido()
        {
            var service = new PlanilhaExportacaoTaxaService();
            var taxaExportacao = ObterTaxaExportacao();
            var taxasReferencia = ObtertaxasReferencia();
            var taxasAuxiliares = new List<CalculoTaxa>();

            var dadosPeriodos = ObterdadosPeriodoUsina();
            var dadosExportacaoTaxa = new DadosExportacaoTaxa("1",
                                                              taxaExportacao,
                                                              new DateTime(2014, 01, 01),
                                                              taxasReferencia,
                                                              taxasAuxiliares,
                                                              dadosPeriodos);

            var retorno = await service.ObterPlanilhaExportacaoTaxa(dadosExportacaoTaxa);

            Assert.NotNull(retorno);
        }

        private IEnumerable<EventoUnidadeGeradora> ObterEventos()
        {
            var eventoUnidadeGeradora = new EventoUnidadeGeradora(null, null, new DateTime(2014, 01, 01), null, null, null, 1, new TimeSpan());

            return new List<EventoUnidadeGeradora>
            {
                eventoUnidadeGeradora
            };
        }
      
        private IEnumerable<DadosUnidadeGeradora> ObterDadosUnidadeGeradora()
        {
            var dadosUnidadeGeradoras = new List<DadosUnidadeGeradora>();

            dadosUnidadeGeradoras.Add(new DadosUnidadeGeradora("1", 125.369, ObterParametros(), ObterEventos()));

            return dadosUnidadeGeradoras;
        }

        private List<DadosPeriodoUsina> ObterdadosPeriodoUsina()
        {
            var dadosPeriodoUsinas = new List<DadosPeriodoUsina>();

            for (DateTime data = new DateTime(2010, 1, 1); data < new DateTime(2015, 1, 1); data = data.AddMonths(1))
            {
                dadosPeriodoUsinas.Add(new DadosPeriodoUsina(
                   data, true, ObterTaxas(), ObterDadosUnidadeGeradora()));
            }

            return dadosPeriodoUsinas;
        }

        private IEnumerable<EventoMudancaEstadoOperativo> ObterEventosUnidadeGeradora()
        {
            var eventos = new List<EventoMudancaEstadoOperativo>();

            for (DateTime data = new DateTime(2000, 1, 1); data < new DateTime(2020, 1, 1); data = data.AddMonths(1))
            {
                eventos.Add(new EventoMudancaEstadoOperativo(
                    data,
                    TipoEstadoOperativo.DAP.ToString(),
                    TipoCondicaoOperativa.RFO.ToString(),
                    TipoClassificadorOrigem.GCB.ToString(),
                    null,
                    100));
            }

            return eventos;
        }

        private IEnumerable<ConsolidacaoMensal> ObterDatasConsolidacoesMensais()
        {
            var datasConsolidacoes = new List<ConsolidacaoMensal>();

            int id = 1;
            for (DateTime data = new DateTime(1960, 1, 1); data < new DateTime(2020, 1, 1); data = data.AddMonths(1))
            {
                datasConsolidacoes.Add(new ConsolidacaoMensal(id.ToString(), data.Month, data.Year));

                id++;
            }

            return datasConsolidacoes;
        }

        private List<CalculoTaxa> ObterTaxas()
        {
            var taxas = new List<CalculoTaxa>();

            var consolidacoes = ObterDatasConsolidacoesMensais();

            foreach (var consolidacao in consolidacoes)
            {
                taxas.Add(new CalculoTaxa(consolidacao.ConsolidacaoMensalId, TipoCalculo.TEIPacum.ToString(), TipoCalculo.TEIPacum.ToString(), "1", 40, "1", null, "1"));
                taxas.Add(new CalculoTaxa(consolidacao.ConsolidacaoMensalId, TipoCalculo.IPReferencia.ToString(), TipoCalculo.IPReferencia.ToString(), "1", 40, "1", null, "1"));
            }

            return taxas;
        }

        private List<ParametroTaxa> ObterParametros()
        {
            var parametros = new List<ParametroTaxa>();

            var consolidacoes = ObterDatasConsolidacoesMensais();

            foreach (var consolidacao in consolidacoes)
            {
                var parametrosPeriodo = new List<ParametroTaxa>
                {
                    new ParametroTaxa(consolidacao.ConsolidacaoMensalId,TipoCalculo.HP.ToString(),TipoCalculo.HP.ToString() ,"1" ,40 , "1" ,null,"1"),
                    new ParametroTaxa(consolidacao.ConsolidacaoMensalId,TipoCalculo.HDP.ToString(),TipoCalculo.HDP.ToString() ,"1" , 20 ,"1" ,null,"1"),
                    new ParametroTaxa(consolidacao.ConsolidacaoMensalId,TipoCalculo.HEDP.ToString(),TipoCalculo.HEDP.ToString() ,"1" , 15 , "1" ,null,"1"),
                };

                parametros.AddRange(parametrosPeriodo);
            }

            return parametros;
        }

        private CalculoTaxa ObterTaxaExportacao()
        {
            var taxaExportacao = new CalculoTaxa(
                "1",
                TipoCalculo.TEIPacum.ToString(),
                TipoCalculo.TEIPacum.ToString(),
                "1", 
                125.3625,
                "1",
                null,
                "1");


            return taxaExportacao;
        }

        private List<CalculoTaxa> ObtertaxasReferencia()
        {
            var taxasReferencia = new List<CalculoTaxa>();

            taxasReferencia.Add(new CalculoTaxa(
                "1",
                TipoCalculo.IPReferencia.ToString(),
                TipoCalculo.IPReferencia.ToString(),
                "1",
                125.3625,
                "1", 
                null,
                "1"));

            return taxasReferencia;
        }
    }
}
