﻿using ONS.SAGER.Calculo.Api.Exportador.Strategy.ConfigPlanilhaTaxa;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Api.Tests.Exportador.Strategy.ConfigPlanilhaTaxa
{
    public class ConfigPlanilhaTaxaStrategyBuilderTest
    {
        [Fact]
        public void DeveRetornarArgumentExceptionQuandoRequestForInvalido()
        {
            var @action = new Action(() => ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.HDCE, new DateTime(2014, 01, 01)));

            Assert.Throws<ArgumentException>(@action);
        }

        [Fact]
        public void DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var @action = new Action(() => ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01)));

            Assert.NotNull(@action);
        }

        [Fact]
        public void ObterSequenciaParametrosEventos_DeveRetornarParametros()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterSequenciaParametrosEventos();

            Assert.NotEmpty(result);
        }

        [Fact]
        public void ObterColunaDadosUnidadeGeradoraTaxas_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaDadosUnidadeGeradoraTaxas();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaDadosUnidadeGeradoraEventos_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaDadosUnidadeGeradoraEventos();

            Assert.NotEmpty(result.ToString());
        }


        [Fact]
        public void ObterColunaHorasMes_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaHorasMes();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaHorasTeipReferencias_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaHorasTeipReferencia();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaParametrosTeifa_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaParametrosTeifa();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaParametrosTeip_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaParametrosTeip();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaTaxaMensal_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaTaxaMensal();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaTeifaReferencia_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIFAacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaTeifaReferencia();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaTeipReferencia_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaTeipReferencia();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterTipoTaxaMensala_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterTipoTaxaMensal();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterTipoTeifaAjustada_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterTipoTeifaAjustada();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterTipoTeipAjustada_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterTipoTeipAjustada();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterColunaMesAno_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterColunaMesAno();

            Assert.NotEmpty(result.ToString());
        }

        [Fact]
        public void ObterUtilizacaoIdentificadorReferencia_DeveRetornarConfigPlanilhaTaxaStrategy()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            var result = strategy.ObterUtilizacaoIdentificadorReferencia();

            Assert.True(result);
        }

        [Theory]
        [InlineData(TipoCalculo.TEIPacumAjustada)]
        [InlineData(TipoCalculo.TEIPIndiceIndisponibilidade)]
        [InlineData(TipoCalculo.TEIPmes)]
        [InlineData(TipoCalculo.TEIPmesAjustada)]
        [InlineData(TipoCalculo.TEIP_FC)]
        [InlineData(TipoCalculo.TEIP_OP)]
        [InlineData(TipoCalculo.TEIFaacumAjustada)]
        [InlineData(TipoCalculo.TEIFaIndiceIndisponibilidade)]
        [InlineData(TipoCalculo.TEIFAmes)]
        [InlineData(TipoCalculo.TEIFaMesAjustada)]
        [InlineData(TipoCalculo.TEIFA_FC)]
        [InlineData(TipoCalculo.TEIFA_OP)]
        public void DeveRetornarArgumentExceptionQuandoTipCalculoNaoEncontrado(TipoCalculo tipoTaxa)
        {
            var dataReferencia = DateTime.Now;

            var @action = new Action(() => ConfigPlanilhaTaxaStrategyBuilder.Build(tipoTaxa, dataReferencia));

            Assert.Throws<ArgumentException>(@action);
        }

        [Fact]
        public void  ObterTitulosAbaTaxas_DeveRetornarParametros()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterTitulosAbaTaxas());
        }

        [Fact]
        public void ObterTitulosAbaEventos_DeveRetornarParametros()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterTitulosAbaEventos());
        }

        [Fact]
        public void ObterSequenciaParametrosTeip_DeveRetornarParametros()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterSequenciaParametrosTeip());
        }

        [Fact]
        public void ObterSequenciaParametrosTeip_DeveRetornarIEnumerableTituloPlanilhaTeip()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.TEIPacum, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterTitulosAbaTaxas());
        }

        [Fact]
        public void ObterTitulosAbaTaxas_DeveRetornarIEnumerableTituloPlanilhaIndiceIndisp()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.IndiceIndisponibilidadeVerificada, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterTitulosAbaTaxas());
        }

        [Fact]
        public void ObterSequenciaParametrosTeifa_DeveRetornarIEnumerableParametroIndiceIndisp()
        {
            var strategy = ConfigPlanilhaTaxaStrategyBuilder.Build(TipoCalculo.IndiceIndisponibilidadeVerificada, new DateTime(2014, 01, 01));

            Assert.NotEmpty(strategy.ObterSequenciaParametrosTeifa());
        }

    }
}
