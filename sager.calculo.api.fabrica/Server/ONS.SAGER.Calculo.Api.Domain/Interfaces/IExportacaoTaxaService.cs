using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Domain.Interfaces
{
    public interface IExportacaoTaxaService
    {
        Task<DadosExportacaoTaxa> ObterDadosPlanilha(ExportacaoTaxaRequest exportacaoTaxaRequest);
    }
}
