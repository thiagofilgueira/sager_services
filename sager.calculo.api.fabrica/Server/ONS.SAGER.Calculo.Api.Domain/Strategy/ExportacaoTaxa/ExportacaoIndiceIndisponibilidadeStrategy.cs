﻿using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa
{
    public class ExportacaoIndiceIndisponibilidadeStrategy : IExportacaoTaxaStrategy
    {
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;

        public ExportacaoIndiceIndisponibilidadeStrategy(
            ITaxaRepository taxaRepository,
            IParametroRepository parametroRepository)
        {
            _taxaRepository = taxaRepository;
            _parametroRepository = parametroRepository;
        }

        public DateTime ObterDataEntradaOperacaoUsina(Usina usina)
        {
            if (!usina.DataRenovacaoConcessao.HasValue)
            {
                throw new InvalidOperationException($"Data de renovação de concessão não informada para {usina.UsinaId}.");
            }

            return usina.DataRenovacaoConcessao.Value;
        }

        public async Task<IEnumerable<CalculoTaxa>> ObterTaxasReferenciaCalculo(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            string usinaId)
        {
            var tiposTaxasReferencia = new[]
            {
                TipoCalculo.IPReferencia,
                TipoCalculo.TEIFReferencia
            };

            var taxasReferencia = await _taxaRepository.ObterTaxasPorContextosCalculo(
                consolidacoesMensais.Select(p => p.ConsolidacaoMensalId),
                tiposTaxasReferencia,
                usinaId);

            if (taxasReferencia.IsNullOrEmpty())
            {
                return new List<CalculoTaxa>();
            }

            return tiposTaxasReferencia.Select(tipoTaxa =>
            {
                var taxasPorTipo = taxasReferencia.FiltrarPorTipoTaxa(tipoTaxa);

                return taxasReferencia
                .Join
                (
                    consolidacoesMensais,
                    t => t.ConsolidacaoMensalId,
                    c => c.ConsolidacaoMensalId,
                (taxa, consolidacaoMensal) => new { taxa, consolidacaoMensal })
                .OrderByDescending(c => c.consolidacaoMensal.DataReferencia)
                .FirstOrDefault(t => t.taxa.Valor.HasValue)?
                .taxa;
            });
        }

        public async Task<IEnumerable<CalculoTaxa>> ObterTaxasAuxiliares(
            ConsolidacaoMensal consolidacaoMensal,
            string usinaId,
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            var taxasIndisp = await _taxaRepository.ObterTaxasPorContextosCalculo(
                new[] { consolidacaoMensal.ConsolidacaoMensalId },
                new[]
                {
                    TipoCalculo.TEIPIndiceIndisponibilidade,
                    TipoCalculo.TEIFaIndiceIndisponibilidade
                },
                usinaId,
                configuracaoCenarioId);

            taxasIndisp = taxasIndisp?.Where(p => p.CompararBranch(configuracaoCenarioId) && p.GetCreatedAt <= dataCalculoTaxaExportacao);

            if (taxasIndisp.IsNullOrEmpty())
            {
                return new List<CalculoTaxa>();
            }

            var teipIndisp = taxasIndisp
                .ObterUltimaTaxaCalculadaPriorizandoCenario(TipoCalculo.TEIPIndiceIndisponibilidade, consolidacaoMensal.ConsolidacaoMensalId);

            var teifaIndisp = taxasIndisp?
                .ObterUltimaTaxaCalculadaPriorizandoCenario(TipoCalculo.TEIFaIndiceIndisponibilidade, consolidacaoMensal.ConsolidacaoMensalId);

            return new List<CalculoTaxa>
            {
                teipIndisp,
                teifaIndisp
            };
        }

        public async Task<IEnumerable<CalculoTaxa>> ObterTaxasPeriodos(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            string usinaId,
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            var taxas = await _taxaRepository.ObterTaxasPorContextosCalculo(
                consolidacoesMensais.Select(p=> p.ConsolidacaoMensalId),
                new[]
                {
                    TipoCalculo.TEIFaacumAjustada,
                    TipoCalculo.TEIPacumAjustada
                },
                usinaId);

            return taxas ?? new List<CalculoTaxa>();
        }

        public async Task<IEnumerable<ParametroTaxa>> ObterParametrosPeriodos(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais, 
            IEnumerable<string> unidadesGeradorasIds, 
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            var parametros = await _parametroRepository
                .ObterParametrosPorContextosCalculos(
                consolidacoesMensais.Select(p => p.ConsolidacaoMensalId),
                new[]
                {
                    TipoCalculo.HP,
                    TipoCalculo.HDP,
                    TipoCalculo.HEDP,
                    TipoCalculo.HS,
                    TipoCalculo.HDF,
                    TipoCalculo.HRD,
                    TipoCalculo.HDCE,
                    TipoCalculo.HEDF
                },
                unidadesGeradorasIds,
                configuracaoCenarioId);

            return parametros?.Where(p => p.Valor.HasValue && p.GetCreatedAt.HasValue && p.GetCreatedAt.Value <= dataCalculoTaxaExportacao) ?? new List<ParametroTaxa>();
        }
    }
}