﻿using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa
{
    public class ExportacaoTeipStrategy : IExportacaoTaxaStrategy
    {
        private readonly DateTime _dataReferencia;
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;

        public ExportacaoTeipStrategy(
            DateTime dataReferencia,
            ITaxaRepository taxaRepository,
            IParametroRepository parametroRepository)
        {
            _dataReferencia = dataReferencia;
            _taxaRepository = taxaRepository;
            _parametroRepository = parametroRepository;
        }

        public DateTime ObterDataEntradaOperacaoUsina(Usina usina)
        {
            if (!usina.DataEntradaOperacao.HasValue)
            {
                throw new InvalidOperationException($"Data de entrada em operação não informada para {usina.UsinaId}.");
            }

            return usina.DataEntradaOperacao.Value;
        }

        public async Task<IEnumerable<CalculoTaxa>> ObterTaxasReferenciaCalculo(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            string usinaId)
        {
            var taxasReferencia = await _taxaRepository.ObterTaxasPorContextosCalculo(
                consolidacoesMensais.Select(p => p.ConsolidacaoMensalId),
                TipoCalculo.IPReferencia,
                usinaId);

            var taxaReferencia = taxasReferencia
            .Join
            (
                consolidacoesMensais,
                t => t.ConsolidacaoMensalId,
                c => c.ConsolidacaoMensalId,
            (taxa, consolidacaoMensal) => new { taxa, consolidacaoMensal })
            .OrderByDescending(c => c.consolidacaoMensal.DataReferencia)
            .FirstOrDefault(t => t.taxa.Valor.HasValue)?
            .taxa;

            return new List<CalculoTaxa> { taxaReferencia };
        }

        public Task<IEnumerable<CalculoTaxa>> ObterTaxasAuxiliares(
            ConsolidacaoMensal consolidacaoMensal,
            string usinaId,
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            return Task.FromResult(new List<CalculoTaxa>().AsEnumerable());
        }

        public async Task<IEnumerable<CalculoTaxa>> ObterTaxasPeriodos(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            string usinaId,
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            var tiposTaxas = ObterTiposTaxasPeriodos();

            var taxas = await _taxaRepository.ObterTaxasPorContextosCalculo(
                consolidacoesMensais.Select(p => p.ConsolidacaoMensalId),
                tiposTaxas,
                usinaId,
                configuracaoCenarioId);

            return taxas?
                .Where(p => p.Valor.HasValue && p.GetCreatedAt.HasValue && p.GetCreatedAt <= dataCalculoTaxaExportacao) ?? new List<CalculoTaxa>();

        }

        private TipoCalculo[] ObterTiposTaxasPeriodos()
        {
            if (_dataReferencia < new DateTime(2014, 10, 01))
            {
                return new[]
                {
                    TipoCalculo.TEIPmesAjustada,
                    TipoCalculo.TEIPmes
                };
            }

            return new[] { TipoCalculo.TEIPacumAjustada };
        }

        public async Task<IEnumerable<ParametroTaxa>> ObterParametrosPeriodos(
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            IEnumerable<string> unidadesGeradorasIds,
            string configuracaoCenarioId,
            DateTime dataCalculoTaxaExportacao)
        {
            var parametros = await _parametroRepository
                .ObterParametrosPorContextosCalculos(
                consolidacoesMensais.Select(p=> p.ConsolidacaoMensalId),
                new[]
                {
                    TipoCalculo.HP,
                    TipoCalculo.HDP,
                    TipoCalculo.HEDP
                },
                unidadesGeradorasIds,
                configuracaoCenarioId);

            return parametros?.Where(p => p.Valor.HasValue && p.GetCreatedAt.HasValue && p.GetCreatedAt.Value <= dataCalculoTaxaExportacao) ?? new List<ParametroTaxa>();
        }
    }
}