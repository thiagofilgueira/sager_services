﻿using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using System;

namespace ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa
{
    public static class ExportacaoTaxaStrategyBuilder
    {
        public static IExportacaoTaxaStrategy Build(
            ExportacaoTaxaRequest exportacaoTaxaRequest, 
            ITaxaRepository taxaRepository,
            IParametroRepository parametroRepository)
        {
            if(exportacaoTaxaRequest is null)
            {
                throw new ArgumentNullException("Argumentos inválidos.");
            }

            switch (exportacaoTaxaRequest.TipoTaxa)
            {
                case TipoCalculo.TEIPacum:
                    {
                        return new ExportacaoTeipStrategy(exportacaoTaxaRequest.DataReferencia, taxaRepository, parametroRepository);
                    }

                case TipoCalculo.TEIFAacum:
                    {
                        return new ExportacaoTeifaStrategy(exportacaoTaxaRequest.DataReferencia, taxaRepository, parametroRepository);
                    }

                case TipoCalculo.IndiceIndisponibilidadeVerificada:
                    {
                        return new ExportacaoIndiceIndisponibilidadeStrategy(taxaRepository, parametroRepository);
                    }
            }

            throw new ArgumentException("Argumentos inválidos.");
        }
    }
}