﻿using ONS.SAGER.Calculo.Util.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa
{
    public interface IExportacaoTaxaStrategy
    {
        DateTime ObterDataEntradaOperacaoUsina(Usina usina);
        Task<IEnumerable<CalculoTaxa>> ObterTaxasReferenciaCalculo(IEnumerable<ConsolidacaoMensal> consolidacoesMensais, string usinaId);
        Task<IEnumerable<CalculoTaxa>> ObterTaxasAuxiliares(ConsolidacaoMensal consolidacaoMensal, string usinaId, string configuracaoCenarioId, DateTime dataCalculoTaxaExportacao);
        Task<IEnumerable<CalculoTaxa>> ObterTaxasPeriodos(IEnumerable<ConsolidacaoMensal> consolidacoesMensais, string usinaId, string configuracaoCenarioId, DateTime dataCalculoTaxaExportacao);
        Task<IEnumerable<ParametroTaxa>> ObterParametrosPeriodos(IEnumerable<ConsolidacaoMensal> consolidacoesMensais, IEnumerable<string> unidadesGeradorasIds, string configuracaoCenarioId, DateTime dataCalculoTaxaExportacao);
    }
}