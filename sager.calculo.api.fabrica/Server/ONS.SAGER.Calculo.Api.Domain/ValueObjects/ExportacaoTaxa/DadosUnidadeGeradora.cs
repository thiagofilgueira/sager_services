﻿using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa
{
    public class DadosUnidadeGeradora : Notificavel
    {
        public string UnidadeGeradoraId { get; private set; }
        public double Potencia { get; private set; }
        public IEnumerable<ParametroTaxa> Parametros { get; private set; }
        public IEnumerable<EventoUnidadeGeradora> Eventos { get; private set; }

        public DadosUnidadeGeradora(
            string unidadeGeradoraId,
            double potencia,
            IEnumerable<ParametroTaxa> dadosParametros,
            IEnumerable<EventoUnidadeGeradora> eventos)
        {
            UnidadeGeradoraId = unidadeGeradoraId;
            Potencia = potencia;
            Parametros = dadosParametros ?? new List<ParametroTaxa>();
            Eventos = eventos ?? new List<EventoUnidadeGeradora>();

            Validar();
        }

        public ParametroTaxa ObterUltimoParametroVersionado(TipoCalculo tipoCalculo)
        {
            if (Parametros.IsNullOrEmpty())
            {
                return null;
            }

            var parametrosCenario = Parametros.FiltrarPorTipoParametroVersao(tipoCalculo, TipoVersao.Cenario);

            var parametrosOficial = Parametros.FiltrarPorTipoParametroVersao(tipoCalculo, TipoVersao.Oficial);

            if (parametrosCenario.IsNullOrEmpty())
            {
                return parametrosOficial.OrderByDescending(p => p.VersaoConvertida).FirstOrDefault();
            }

            return parametrosCenario.OrderByDescending(p => p.VersaoConvertida).FirstOrDefault();
        }

        public double? ObterValorUltimoParametroVersionado(TipoCalculo tipoCalculo)
        {
            return ObterUltimoParametroVersionado(tipoCalculo)?.Valor;
        }

        public string ObterVersaoUltimoParametroVersionado(TipoCalculo tipoCalculo)
        {
            return ObterUltimoParametroVersionado(tipoCalculo)?.Versao;
        }

        public int ObterTotalEventos()
        {
            return Eventos?.Count() ?? 0;
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(UnidadeGeradoraId, nameof(UnidadeGeradoraId), "Unidade Geradora Inválida."));
        }
    }
}
