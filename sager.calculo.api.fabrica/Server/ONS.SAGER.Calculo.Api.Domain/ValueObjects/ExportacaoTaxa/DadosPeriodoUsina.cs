﻿using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa
{
    public class DadosPeriodoUsina : Notificavel
    {
        public DadosPeriodoUsina(
            DateTime dataReferencia,
            bool usinaEmOperacao, 
            IEnumerable<CalculoTaxa> taxas,
            IEnumerable<DadosUnidadeGeradora> dadosUnidadesGeradoras)
        {
            DataReferencia = dataReferencia;
            UsinaEmOperacao = usinaEmOperacao;
            AdicionarTaxas(taxas);
            AdicionarDadosUnidadeGeradoras(dadosUnidadesGeradoras);

            Validar();
        }

        public DateTime DataReferencia { get; private set; }
        public bool UsinaEmOperacao { get; private set; }
        public List<CalculoTaxa> Taxas { get; private set; } = new List<CalculoTaxa>();
        public List<DadosUnidadeGeradora> DadosUnidadesGeradoras { get; private set; } = new List<DadosUnidadeGeradora>();

        public void AdicionarTaxas(IEnumerable<CalculoTaxa> taxas)
        {
            if (taxas.IsNullOrEmpty())
            {
                return;
            }

            Taxas.AddRange(taxas);

            Validar();
        }

        public void AdicionarDadosUnidadeGeradoras(IEnumerable<DadosUnidadeGeradora> dadosUnidadesGeradoras)
        {
            if (dadosUnidadesGeradoras.IsNullOrEmpty())
            {
                return;
            }

            DadosUnidadesGeradoras.AddRange(dadosUnidadesGeradoras);

            Validar();
        }

        public string ObterMesAnoPeriodoFormatado()
        {
            if(DataReferencia == default)
            {
                return string.Empty;
            }

            return DataReferencia.ToString("MM/yyyy");
        }

        public int ObterTotalEventosPeriodo()
        {
            return DadosUnidadesGeradoras?.Sum(p => p.ObterTotalEventos()) ?? 0;
        }

        public override void Validar()
        {
            RemoverNotificacoes();

            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoDefault(DataReferencia, nameof(DataReferencia), "A data de referência é obrigatória")
                .VerificarSeVerdadeiro(DataReferencia.Day == 1, nameof(DataReferencia), "A data de referência está inválida"));
        }
    }

}
