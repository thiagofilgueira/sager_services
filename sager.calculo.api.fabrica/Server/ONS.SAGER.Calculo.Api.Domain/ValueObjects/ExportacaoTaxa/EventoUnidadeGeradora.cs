﻿using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa
{
    public class EventoUnidadeGeradora : Notificavel
    {
        public string NumeroOns { get; private set; }
        public string Versao { get; private set; }
        public DateTime DataHora { get; private set; }
        public string EstadoOperativo { get; private set; }
        public string CondicaoOperativa { get; private set; }
        public string Origem { get; private set; }
        public double Disponibilidade { get; private set; }
        public TimeSpan Duracao { get; private set; }
        public List<TipoCalculo> ParametrosInfluenciados { get; private set; } = new List<TipoCalculo>();

        public EventoUnidadeGeradora(
            string numeroOns,
            string versao,
            DateTime dataHora,
            string estadoOperativo,
            string condicaoOperativa,
            string origem,
            double disponibilidade,
            TimeSpan duracao)
        {
            NumeroOns = numeroOns;
            Versao = versao;
            DataHora = dataHora;
            EstadoOperativo = estadoOperativo;
            CondicaoOperativa = condicaoOperativa;
            Origem = origem;
            Disponibilidade = disponibilidade;
            Duracao = duracao;

            Validar();
        }

        public void AdicionarParametrosInfluenciados(IEnumerable<TipoCalculo> parametros)
        {
            if (parametros.IsNullOrEmpty())
            {
                return;
            }

            var parametrosNovos = parametros.Where(p => p.NotIn(ParametrosInfluenciados));

            if (parametrosNovos.IsNullOrEmpty())
            {
                return;
            }

            ParametrosInfluenciados.AddRange(parametrosNovos);
        }

        public string ObterCaracterMarcacao(TipoCalculo tipoCalculo)
        {
            if (tipoCalculo.In(ParametrosInfluenciados))
            {
                return "x";
            }

            return string.Empty;
        }

        public string ObterDataHoraFormatados()
        {
            if(DataHora == default)
            {
                return string.Empty;
            }

            return DataHora.ToString("dd/MM/yyyy HH:mm");
        }

        public override void Validar()
        {
            RemoverNotificacoes();

            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeFalso(EstadoOperativo.IsNullOrWhiteSpace() && CondicaoOperativa.IsNullOrWhiteSpace() && Origem.IsNullOrWhiteSpace(), "EstadOp/Origem/CondOp", "Estado Operativo/Origem/Codição operativa inválidos")
                .VerificarSeNaoDefault(DataHora, nameof(DataHora), "Data de verificação do evento inválida"));
        }
    }
}
