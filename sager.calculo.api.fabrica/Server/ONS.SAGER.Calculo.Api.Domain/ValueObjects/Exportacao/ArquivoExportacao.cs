﻿using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;

namespace ONS.SAGER.Calculo.Api.Domain.ValueObjects.Exportacao
{
    public class ArquivoExportacao : Notificavel
    {
        public string Nome { get; private set; }
        public string Extensao { get; private set; }
        public byte[] Conteudo { get; private set; }

        public ArquivoExportacao(string nome, string extensao, byte[] conteudo)
        {
            Nome = nome;
            Extensao = extensao;
            Conteudo = conteudo;

            Validar();
        }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(Nome, nameof(Nome), "Nome deve ser preenchido")
                .VerificarSeNaoNuloOuEspacoEmBranco(Extensao, nameof(Extensao), "Extensao deve ser preenchido")
                .VerificarSeNaoEstaNulo(Conteudo, nameof(Conteudo), "Extensao deve ser preenchido"));
        }
    }
}
