using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Services;

namespace ONS.SAGER.Calculo.Api.Domain.Extensions
{
    public static class DomainExtensions
    {
        //TODO: REMOVER SINGLETON AP�S TESTES
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroService, ParametroService>();
            services.AddSingleton<IExportacaoTaxaService, ExportacaoTaxaService>();
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IUnidadeGeradoraService, UnidadeGeradoraService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }     
    }
}
