﻿using ONS.SAGER.Calculo.Util.DataAccess.Interfaces;

namespace ONS.SAGER.Calculo.Api.Domain.IRepositories
{
    public interface ISuspensaoUnidadeGeradoraRepository : ISuspensaoUnidadeGeradoraRepositoryBase
    {
    }
}
