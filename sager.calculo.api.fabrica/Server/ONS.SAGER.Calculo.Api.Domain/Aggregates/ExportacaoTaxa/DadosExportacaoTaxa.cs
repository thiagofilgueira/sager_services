﻿using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;

namespace ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa
{
    public class DadosExportacaoTaxa : Notificavel
    {
        public DadosExportacaoTaxa(
            string usinaId,
            CalculoTaxa taxaExportacao,
            DateTime dataReferencia,
            IEnumerable<CalculoTaxa> taxasReferencia,
            IEnumerable<CalculoTaxa> taxasAuxiliares,
            List<DadosPeriodoUsina> dadosPeriodos)
        {
            UsinaId = usinaId;
            TaxaExportacao = taxaExportacao;
            DataReferencia = dataReferencia;
            AdicionarTaxasReferencia(taxasReferencia);
            AdicionarTaxasAuxiliares(taxasAuxiliares);
            AdicionarDadosPeriodos(dadosPeriodos);

            Validar();
        }

        public string UsinaId { get; private set; }
        public CalculoTaxa TaxaExportacao { get; private set; }
        public DateTime DataReferencia { get; private set; }
        public List<CalculoTaxa> TaxasReferencia { get; private set; } = new List<CalculoTaxa>();
        public List<CalculoTaxa> TaxasAuxiliares { get; private set; } = new List<CalculoTaxa>();
        public List<DadosPeriodoUsina> DadosPeriodos { get; private set; } = new List<DadosPeriodoUsina>();

        public void AdicionarTaxasReferencia(IEnumerable<CalculoTaxa> taxasReferencia)
        {
            if (taxasReferencia.IsNullOrEmpty())
            {
                return;
            }

            TaxasReferencia.AddRange(taxasReferencia);

            Validar();
        }

        public void AdicionarTaxasAuxiliares(IEnumerable<CalculoTaxa> taxasAuxiliares)
        {
            if (taxasAuxiliares.IsNullOrEmpty())
            {
                return;
            }

            TaxasAuxiliares.AddRange(taxasAuxiliares);

            Validar();
        }

        public void AdicionarDadosPeriodos(IEnumerable<DadosPeriodoUsina> dadosPeriodos)
        {
            if (dadosPeriodos.IsNullOrEmpty())
            {
                return;
            }

            DadosPeriodos.AddRange(dadosPeriodos);

            Validar();
        }

        public bool PossuiTaxasAuxiliares()
        {
            return ObterQuantidadeTaxasAuxiliares() > 0;
        }

        public int ObterQuantidadeTaxasAuxiliares()
        {
            return TaxasAuxiliares?.Count ?? 0;
        }

        public string ObterDataReferenciaExportacaoFormatada()
        {
            if(DataReferencia == default)
            {
                return string.Empty;
            }

            var ano = DataReferencia.Year.ToString().Substring(2, 2);
            var mes = DataReferencia.ToString("MMM");

            return $"{mes}/{ano}";
        }

        public override void Validar()
        {
            RemoverNotificacoes();

            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "Usina inválida.")
                .VerificarSeNaoEstaNulo(TaxaExportacao, nameof(TaxaExportacao), "Taxa de exportação inválida.")
                .VerificarSeNaoDefault(DataReferencia, nameof(DataReferencia), "Data de referência não informada.")
                .VerificarSeFalso(TaxasReferencia.IsNullOrEmpty(), nameof(TaxasReferencia), "Taxas de referência não informadas.")
                .VerificarSeVerdadeiro(TaxaExportacao?.TipoCalculo != TipoCalculo.IndiceIndisponibilidadeVerificada || !TaxasAuxiliares.IsNullOrEmpty(), nameof(TaxasAuxiliares), "Taxas de auxiliares não informadas.")
                .VerificarSeFalso(DadosPeriodos.IsNullOrEmpty(), nameof(DadosPeriodos), "Dados periodos não informados.")
                .VerificarSeVerdadeiro(DadosPeriodos.Count == 60, nameof(DadosPeriodos), "Dados dos 60 meses não informados."));
        }
    }
}