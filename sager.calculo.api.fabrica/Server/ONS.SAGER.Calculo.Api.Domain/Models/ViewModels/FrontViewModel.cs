﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ONS.SAGER.Calculo.Api.Domain.Models.ViewModels
{
    public class FiltroViewModel
    {
        public DateTime DataMinima { get; set; }
        public IEnumerable<string> AgentesProprietariosIds { get; set; }
        public IEnumerable<string> TiposUsinasIds { get; set; }
        public IEnumerable<UsinaViewModel> Usinas { get; set; }
        public IEnumerable<string> TiposTaxasIds { get; set; }
    }

    public class ResultadoViewModel
    {
        public IEnumerable<string> UsinasIds { get; set; }
        public string UsinaSelecionadaId { get; set; }
        public IEnumerable<DadosPeriodoTaxasViewModel> DadosPeriodosUsina { get; set; }
    }

    public class DadosPeriodoTaxasViewModel
    {
        public DateTime DataReferencia { get; set; }
        public IEnumerable<TaxaViewModel> Taxas { get; set; }

    }

    public class TaxaViewModel
    {
        public string TipoTaxa { get; set; }
        public IEnumerable<DetalhesTaxaViewModel> DadosTaxas { get; set; }
    }

    public class DetalhesTaxaViewModel
    {
        public CenarioViewModel Cenario { get; set; }
        public double Valor { get; set; }
        public int Versao { get; set; }
    }

    public class CenarioViewModel
    {
        public string CenarioId { get; set; }
        public string TipoCenario { get; set; }
        public string NomeCenario { get; set; }
    }

    public class PeriodoApuracaoViewModel
    {
        public string PeriodoApuracaoId { get; set; }
        public DateTime DataReferencia { get; set; }
    }

    public class UsinaViewModel
    {
        public string UsinaId { get; set; }
        public string NomeCurtoUsina { get; set; }
        public DateTime DataEntradaOperacao { get; set; }
        public DateTime? DataDesativacao { get; set; }
        public IEnumerable<string> AgentesProprietariosIds { get; set; }
        public string TipoUsinaId { get; set; }
    }

}
