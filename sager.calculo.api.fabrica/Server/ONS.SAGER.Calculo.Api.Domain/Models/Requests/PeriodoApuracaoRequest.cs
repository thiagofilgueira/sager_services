﻿using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using System;

namespace ONS.SAGER.Calculo.Api.Domain.Models.Requests
{
    public class PeriodoApuracaoRequest : Notificavel
    {
        public PeriodoApuracaoRequest(
            DateTime dataReferencia)
        {
            DataReferencia = dataReferencia;
        }

        public DateTime DataReferencia { get; set; }

        public override void Validar()
        {
            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoDefault(DataReferencia, nameof(DataReferencia), "A data de referência é obrigatória")
                .VerificarSeVerdadeiro(DataReferencia.Day == 1, nameof(DataReferencia), "A data de referência está inválida"));
        }
    }
}