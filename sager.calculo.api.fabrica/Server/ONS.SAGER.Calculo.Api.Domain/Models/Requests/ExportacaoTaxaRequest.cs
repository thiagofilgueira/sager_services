﻿using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Validations.Contracts;
using ONS.SAGER.Calculo.Util.Domain.Validations.Notifications;
using ONS.SAGER.Calculo.Util.Extensions;
using System;

namespace ONS.SAGER.Calculo.Api.Domain.Models.Requests
{
    public class ExportacaoTaxaRequest : Notificavel
    {
        public ExportacaoTaxaRequest()
        {

        }

        public ExportacaoTaxaRequest(
            string usinaId,
            DateTime dataReferencia,
            TipoCalculo tipoTaxa,
            string configuracaoCenarioId,
            string versaoTaxa)
        {
            UsinaId = usinaId;
            DataReferencia = dataReferencia;
            TipoTaxa = tipoTaxa;
            ConfiguracaoCenarioId = configuracaoCenarioId;
            VersaoTaxa = versaoTaxa;

            Validar();
        }

        public DateTime DataReferencia { get; set; }
        public string ConfiguracaoCenarioId { get; set; }
        public string UsinaId { get; set; }
        public TipoCalculo TipoTaxa { get; set; }
        public string VersaoTaxa { get; set; }

        public override void Validar()
        {
            RemoverNotificacoes();

            InserirNotificacoes(new Contrato()
                .Requer()
                .VerificarSeNaoNuloOuEspacoEmBranco(UsinaId, nameof(UsinaId), "Id da Usina é obrigatorio")
                .VerificarSeVerdadeiro(TipoTaxa.In(new TipoCalculo[] {
                    TipoCalculo.TEIFAacum, 
                    TipoCalculo.TEIPacum,
                    TipoCalculo.IndiceIndisponibilidadeVerificada
                }), nameof(TipoTaxa), $"Tipo da taxa é inválido: {TipoTaxa.GetName()} ")
                .VerificarSeVerdadeiro(TipoTaxa != TipoCalculo.IndiceIndisponibilidadeVerificada||
                (TipoTaxa == TipoCalculo.IndiceIndisponibilidadeVerificada && DataReferencia >= new DateTime(2014,10,1)), nameof(TipoTaxa), $"A data de referência é inválida para o tipo de taxa informado")
                .VerificarSeNaoDefault(DataReferencia, nameof(DataReferencia), "A data de referência é obrigatória")
                .VerificarSeVerdadeiro(DataReferencia.Day == 1, nameof(DataReferencia), "A data de referência está inválida"));
        }
    }
}