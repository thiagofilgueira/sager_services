﻿using ONS.SDK.Context;

namespace ONS.SAGER.Calculo.Api.Domain.Models.Payload
{
    public class AlterarEventoPayload : IPayload
    {
        public string EventoId { get; set; }
    }
}
