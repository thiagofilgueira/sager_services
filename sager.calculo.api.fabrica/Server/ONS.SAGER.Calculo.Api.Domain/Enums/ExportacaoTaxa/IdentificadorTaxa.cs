﻿using System.ComponentModel;

namespace ONS.SAGER.Calculo.Api.Domain.Enums.ExportacaoTaxa
{
    public enum IdentificadorTaxa
    {
        [Description("")]
        Indefinido,

        [Description("A")]
        Acumulada,

        [Description("R")]
        Referencia
    }
}
