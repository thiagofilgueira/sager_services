﻿using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Services;

namespace ONS.SAGER.Calculo.Api.Domain.Services
{
    public class ParametroService : ParametroServiceBase, IParametroService
    {
        public ParametroService(IParametroRepository parametroRepository, IInsumoCalculoService insumoService)
            : base(parametroRepository, insumoService)
        {
        }

    }
}
