using ONS.SAGER.Calculo.Api.Domain.Aggregates.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.Strategy.ExportacaoTaxa;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.ExportacaoTaxa;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Domain.Services
{
    public class ExportacaoTaxaService : IExportacaoTaxaService
    {
        private readonly ITaxaRepository _taxaRepository;
        private readonly IParametroRepository _parametroRepository;
        private readonly IUsinaRepository _usinaRepository;
        private readonly IParametroService _parametroService;
        private readonly IUnidadeGeradoraService _unidadeGeradoraService;
        private readonly IConsolidacaoMensalRepository _consolidacaoMensalRepository;
        private readonly AppSettings _appSettings;

        public ExportacaoTaxaService(
            ITaxaRepository taxaRepository,
            IParametroRepository parametroRepository,
            IUsinaRepository usinaRepository,
            IParametroService parametroService,
            IUnidadeGeradoraService unidadeGeradoraService,
            IConsolidacaoMensalRepository consolidacaoMensalRepository,
            AppSettings appSettings)
        {
            _taxaRepository = taxaRepository;
            _parametroRepository = parametroRepository;
            _usinaRepository = usinaRepository;
            _parametroService = parametroService;
            _unidadeGeradoraService = unidadeGeradoraService;
            _consolidacaoMensalRepository = consolidacaoMensalRepository;
            _appSettings = appSettings;
        }

        public async Task<DadosExportacaoTaxa> ObterDadosPlanilha(ExportacaoTaxaRequest request)
        {
            if(request is null)
            {
                throw new ArgumentNullException("Requisi��o inv�lida.");
            }

            if (request.ConfiguracaoCenarioId.IsNullOrEmpty())
            {
                request.ConfiguracaoCenarioId = _appSettings.BranchMaster;
            }

            var usina = _usinaRepository
                .ObterUsinaPorId(request.UsinaId, request.ConfiguracaoCenarioId);

            var unidadesGeradoras = _unidadeGeradoraService
                .ObterUnidadesGeradoras(request.UsinaId, request.ConfiguracaoCenarioId, true, true, true);

            var consolidacoes = _consolidacaoMensalRepository
                .ObterConsolidacoesAteDataReferencia(request.DataReferencia, request.ConfiguracaoCenarioId);

            await Task.WhenAll(usina, unidadesGeradoras, consolidacoes);

            if (usina.Result is null)
            {
                throw new ArgumentException("N�o foi poss�vel encontrar usina com os dados informados.");
            }

            if (unidadesGeradoras.Result.IsNullOrEmpty())
            {
                throw new ArgumentException("N�o foi poss�vel encontrar unidade geradora com os dados informados.");
            }

            if (consolidacoes.Result.IsNullOrEmpty())
            {
                throw new ArgumentException("N�o foi poss�vel encontrar consolida��o com os dados informados.");
            }

            var unidadesGeradorasIds = unidadesGeradoras.Result.Select(p => p.UnidadeGeradoraId);

            var taxaExportacao = await ObterTaxaExportacaoSelecionada(request, consolidacoes.Result);

            if (taxaExportacao?.GetCreatedAt is null )
            {
                throw new ArgumentException("N�o foi poss�vel encontrar nenhuma taxa com os dados informados.");
            }

            return await ObterDadosExportacaoTaxa(
                request,
                usina.Result,
                unidadesGeradoras.Result,
                consolidacoes.Result,
                taxaExportacao);
        }

        private async Task<CalculoTaxa> ObterTaxaExportacaoSelecionada(
            ExportacaoTaxaRequest exportacaoTaxaRequest,
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais)
        {
            var consolidacaoMensal = consolidacoesMensais
                .FirstOrDefault(p => p.DataReferencia == exportacaoTaxaRequest.DataReferencia);

            var taxasExportacao = await _taxaRepository.ObterTaxasPorContextoCalculo(
                consolidacaoMensal.ConsolidacaoMensalId,
                exportacaoTaxaRequest.TipoTaxa,
                exportacaoTaxaRequest.UsinaId,
                exportacaoTaxaRequest.ConfiguracaoCenarioId);

            if (taxasExportacao is null || taxasExportacao.IsNullOrEmpty())
            {
                throw new ArgumentException("N�o foi poss�vel encontrar nenhuma taxa com os dados informados.");
            }

            if (exportacaoTaxaRequest.VersaoTaxa.HasValue())
            {
                taxasExportacao = taxasExportacao.Where(p => p.Versao == exportacaoTaxaRequest.VersaoTaxa);
            }

            return taxasExportacao
                .Where(p => p.GetBranch == exportacaoTaxaRequest.ConfiguracaoCenarioId)
                .ObterUltimaTaxaCalculadaPriorizandoCenario(exportacaoTaxaRequest.TipoTaxa, consolidacaoMensal.ConsolidacaoMensalId);
        }

        private async Task<DadosExportacaoTaxa> ObterDadosExportacaoTaxa(
            ExportacaoTaxaRequest exportacaoTaxaRequest,
            Usina usina,
            IEnumerable<UnidadeGeradora> unidadesGeradoras,
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            CalculoTaxa taxaSelecionada)
        {
            var strategy = ExportacaoTaxaStrategyBuilder.Build(exportacaoTaxaRequest, _taxaRepository, _parametroRepository);

            var taxasReferencia = strategy.ObterTaxasReferenciaCalculo(
                consolidacoesMensais,
                usina.UsinaId);

            var taxasAuxiliares = strategy.ObterTaxasAuxiliares(
                consolidacoesMensais.FirstOrDefault(p=> p.DataReferencia == exportacaoTaxaRequest.DataReferencia), 
                usina.UsinaId,
                exportacaoTaxaRequest.ConfiguracaoCenarioId,
                taxaSelecionada.GetCreatedAt.Value);

            var taxasPeriodos = strategy.ObterTaxasPeriodos(
                consolidacoesMensais,
                usina.UsinaId,
                exportacaoTaxaRequest.ConfiguracaoCenarioId,
                taxaSelecionada.GetCreatedAt.Value);

            var parametrosPeriodos = strategy.ObterParametrosPeriodos(
                consolidacoesMensais,
                unidadesGeradoras.Select(p => p.UnidadeGeradoraId),
                exportacaoTaxaRequest.ConfiguracaoCenarioId,
                taxaSelecionada.GetCreatedAt.Value);

            var dataEntradaOperacao = strategy.ObterDataEntradaOperacaoUsina(usina);

            await Task.WhenAll(taxasReferencia,taxasAuxiliares, taxasPeriodos, parametrosPeriodos);

            if(parametrosPeriodos.Result.IsNullOrEmpty())
            {
                throw new ArgumentException("A usina n�o possui par�metros calculados.");
            }

            var dadosPeriodos = ObterDadosPeriodos(
                unidadesGeradoras,
                consolidacoesMensais,
                exportacaoTaxaRequest.DataReferencia,
                taxasPeriodos.Result,
                parametrosPeriodos.Result,
                dataEntradaOperacao);

            var dadosExportacao = new DadosExportacaoTaxa(
                usina.UsinaId,
                taxaSelecionada,
                exportacaoTaxaRequest.DataReferencia,
                taxasReferencia.Result,
                taxasAuxiliares.Result,
                dadosPeriodos);

            dadosExportacao.Validar();

            if (dadosExportacao.Invalido)
            {
                throw new ArgumentException(dadosExportacao.MensagensNotificacoesAgrupadas);
            }

            return dadosExportacao;
        }

        private List<DadosPeriodoUsina> ObterDadosPeriodos(
            IEnumerable<UnidadeGeradora> unidadesGeradoras,
            IEnumerable<ConsolidacaoMensal> consolidacoesMensais,
            DateTime dataReferencia,
            IEnumerable<CalculoTaxa> taxasPeriodos,
            IEnumerable<ParametroTaxa> parametrosPeriodos,
            DateTime dataEntradaOperacao)
        {
            var dadosPeriodos = new List<DadosPeriodoUsina>();

            var dataReferenciaCalculo = dataReferencia;

            while (dadosPeriodos.Count < 60)
            {
                var consolidacaoMensal = consolidacoesMensais
                    .FirstOrDefault(p => p.DataReferencia == dataReferenciaCalculo);

                if (consolidacaoMensal is null)
                {
                    dataReferenciaCalculo = dataReferenciaCalculo.AddMonths(-1);
                    continue;
                }

                bool usinaEmOperacao = dataEntradaOperacao < dataReferenciaCalculo;

                var parametrosDataCalculo = parametrosPeriodos.Where(p=> p.ConsolidacaoMensalId == consolidacaoMensal.ConsolidacaoMensalId);

                if (usinaEmOperacao && parametrosDataCalculo.IsNullOrEmpty())
                {
                    dataReferenciaCalculo = dataReferenciaCalculo.AddMonths(-1);
                    continue;
                }

                var dadosUnidadesGeradoras = ObterDadosUnidadesGeradoras(
                    consolidacaoMensal,
                    unidadesGeradoras,
                    parametrosDataCalculo,
                    usinaEmOperacao);

                var taxasDataCalculo = taxasPeriodos.Where(p => p.ConsolidacaoMensalId == consolidacaoMensal.ConsolidacaoMensalId);

                var dadosTaxas = ObterUltimasTaxasCalculadas(
                    taxasDataCalculo,
                    consolidacaoMensal,
                    usinaEmOperacao);

               var dadoPeriodo = new DadosPeriodoUsina(
                   dataReferenciaCalculo, 
                   usinaEmOperacao,
                   dadosTaxas,
                   dadosUnidadesGeradoras);

                dadosPeriodos.Add(dadoPeriodo);

                dataReferenciaCalculo = dataReferenciaCalculo.AddMonths(-1);
            }
            return dadosPeriodos;
        }

        private IEnumerable<DadosUnidadeGeradora> ObterDadosUnidadesGeradoras(
            ConsolidacaoMensal consolidacaoMensal,
            IEnumerable<UnidadeGeradora> unidadesGeradoras,
            IEnumerable<ParametroTaxa> parametrosDataCalculo,
            bool usinaEmOperacao)
        {
            var dadosUnidadesGeradoras = new List<DadosUnidadeGeradora>();

            foreach (var unidadeGeradora in unidadesGeradoras.OrderBy(p => p.UnidadeGeradoraId))
            {
                var parametrosUnidadeGeradora = parametrosDataCalculo.FiltrarPorUnidadeGeradora(unidadeGeradora.UnidadeGeradoraId);

                if (usinaEmOperacao && parametrosUnidadeGeradora.IsNullOrEmpty())
                {
                    continue;
                }

                var parametros = ObterUltimosParametrosCalculados(parametrosUnidadeGeradora, consolidacaoMensal, usinaEmOperacao);

                var potencia = usinaEmOperacao ?
                unidadeGeradora.ObterUltimaPotenciaDoMes(consolidacaoMensal.DataReferencia) :
                unidadeGeradora.ObterPrimeiraPotencia();

                var eventos = ObterEventosUnidadeGeradora(
                    unidadeGeradora.Eventos,
                    parametros,
                    consolidacaoMensal.DataReferencia);

                dadosUnidadesGeradoras.Add(new DadosUnidadeGeradora(
                    unidadeGeradora.UnidadeGeradoraId,
                    potencia.ValorPotencia.Value,
                    parametros,
                    eventos));
            }

            return dadosUnidadesGeradoras.Where(p=> p != null);
        }

        private IEnumerable<EventoUnidadeGeradora> ObterEventosUnidadeGeradora(
            IEnumerable<EventoMudancaEstadoOperativo> eventosUnidadeGeradora, 
            IEnumerable<ParametroTaxa> parametrosUnidadeGeradora,
            DateTime dataReferencia)
        {
            var eventosPeriodo = eventosUnidadeGeradora
                    .Where(e => e.DataVerificada.EstaNoMesmoMesAno(dataReferencia))
                    .OrderBy(p => p.DataVerificada)
                    .ToLinkedList();

            var retorno = new List<EventoUnidadeGeradora>();

            for (var evento = eventosPeriodo.First; evento != null; evento = evento.Next)
            {
                var inicioEvento = evento.Value.DataVerificada;
                var fimEvento = evento.Next is null ? dataReferencia.MesSeguinte() : evento.Next.Value.DataVerificada;
                var duracao = fimEvento.Subtract(inicioEvento);

                var eventoUge = new EventoUnidadeGeradora(
                    evento.Value.NumeroOns,
                    evento.Value.VersaoOficial ?? evento.Value.VersaoCenario,
                    evento.Value.DataVerificada,
                    evento.Value.EstadoOperativoId,
                    evento.Value.CondicaoOperativaId,
                    evento.Value.OrigemId,
                    evento.Value.Disponibilidade,
                    duracao);

                var parametrosInfluenciados = ObterParametrosInfluenciadosPorEvento(
                    evento.Value,
                    parametrosUnidadeGeradora);

                eventoUge.AdicionarParametrosInfluenciados(parametrosInfluenciados);

                retorno.Add(eventoUge);
            }

            return retorno;
        }

        private IEnumerable<TipoCalculo> ObterParametrosInfluenciadosPorEvento(
            EventoMudancaEstadoOperativo evento,
            IEnumerable<ParametroTaxa> parametros)
        {
            var parametrosInfluenciados = new List<TipoCalculo>();

            foreach(var parametro in parametros)
            {
                var restricoes = _parametroService.ObterRestricoesCalculo(parametro.TipoCalculo);

                if (restricoes.IsNullOrEmpty() || evento.ValidoParaRestricoes(restricoes))
                {
                    parametrosInfluenciados.Add(parametro.TipoCalculo);
                }
            }

            return parametrosInfluenciados.Distinct();
        }


        private IEnumerable<ParametroTaxa> ObterUltimosParametrosCalculados(
            IEnumerable<ParametroTaxa> parametros,
            ConsolidacaoMensal consolidacaoMensal,
            bool usinaEmOperacao)
        {
            if (!usinaEmOperacao || parametros.IsNullOrEmpty())
            {
                return new List<ParametroTaxa>();
            }

            var tiposCalculo = parametros.Select(p => p.TipoCalculo).Distinct();

            return tiposCalculo.Select(tipoCalculo =>
            {
                return parametros.ObterUltimoParametroCalculadoPriorizandoCenario(tipoCalculo, consolidacaoMensal.ConsolidacaoMensalId);
            });
        }

        private IEnumerable<CalculoTaxa> ObterUltimasTaxasCalculadas(
        IEnumerable<CalculoTaxa> taxas,
        ConsolidacaoMensal consolidacaoMensal,
        bool usinaEmOperacao)
        {
            if (!usinaEmOperacao || taxas.IsNullOrEmpty())
            {
                return new List<CalculoTaxa>();
            }

            var tiposCalculo = taxas.Select(p => p.TipoCalculo).Distinct();

            return tiposCalculo.Select(tipoCalculo =>
            {
                return taxas.ObterUltimaTaxaCalculadaPriorizandoCenario(tipoCalculo, consolidacaoMensal.ConsolidacaoMensalId);
            });
        }
    }
}
