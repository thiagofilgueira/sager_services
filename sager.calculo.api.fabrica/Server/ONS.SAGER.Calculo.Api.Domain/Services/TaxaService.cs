﻿using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Services;

namespace ONS.SAGER.Calculo.Api.Domain.Services
{
    public class TaxaService : TaxaServiceBase, ITaxaService
    {
        public TaxaService( ITaxaRepository taxaRepository) 
            : base(taxaRepository)
        {
        }
    }
}
