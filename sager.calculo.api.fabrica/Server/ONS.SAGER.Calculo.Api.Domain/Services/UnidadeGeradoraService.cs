﻿using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;
using ONS.SAGER.Calculo.Util.Domain.Services;

namespace ONS.SAGER.Calculo.Api.Domain.Services
{
    public class UnidadeGeradoraService : UnidadeGeradoraServiceBase, IUnidadeGeradoraService
    {
        public UnidadeGeradoraService(
            IUnidadeGeradoraRepository unidadeGeradoraRepository,
            ISuspensaoUnidadeGeradoraRepository suspensaoUnidadeGeradoraRepository,
            IPotenciaUnidadeGeradoraRepository potenciaUnidadeGeradoraRepository,
            IEventoMudancaEstadoOperativoRepository eventoMudancaEstadoOperativoRepository,
            IParametroRepository parametroRepository)
            : base(
                  unidadeGeradoraRepository,
                  suspensaoUnidadeGeradoraRepository,
                  potenciaUnidadeGeradoraRepository,
                  eventoMudancaEstadoOperativoRepository,
                  parametroRepository)
        {

        }
    }
}
