using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.Exportacao;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Api.Application.Interfaces
{
    public interface IExportacaoTaxaApplicationService
    {
        Task<ArquivoExportacao> ObterArquivoExportacaoTaxa(ExportacaoTaxaRequest exportacaoTaxaRequest);
    }
}
