using Aspose.Cells;
using ONS.SAGER.Calculo.Api.Application.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Models.Requests;
using ONS.SAGER.Calculo.Api.Domain.ValueObjects.Exportacao;
using ONS.SAGER.Calculo.Api.Exportador.Configurations;
using ONS.SAGER.Calculo.Api.Exportador.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.IO;

namespace ONS.SAGER.Calculo.Api.Application.Services
{
    public class ExportacaoTaxaApplicationService : IExportacaoTaxaApplicationService
    {
        private readonly IExportacaoTaxaService _taxaExportacaoMensalService;
        private readonly IPlanilhaExportacaoTaxaService _exportadorPlanilhaService; 

        public ExportacaoTaxaApplicationService(
            IExportacaoTaxaService taxaExportacaoMensalService,
            IPlanilhaExportacaoTaxaService exportadorPlanilhaService )
        {
            _taxaExportacaoMensalService = taxaExportacaoMensalService;
            _exportadorPlanilhaService = exportadorPlanilhaService;
        }

        public async Task<ArquivoExportacao> ObterArquivoExportacaoTaxa(ExportacaoTaxaRequest exportacaoTaxaRequest)
        {
            if (exportacaoTaxaRequest is null)
            {
                throw new ArgumentNullException("Requisi��o inv�lida.");
            }

            exportacaoTaxaRequest.Validar();

            if (exportacaoTaxaRequest.Invalido)
            {
                throw new InvalidOperationException(exportacaoTaxaRequest.MensagensNotificacoesAgrupadas);
            }

            var dadoCalculoTaxaUsina = await _taxaExportacaoMensalService.ObterDadosPlanilha(exportacaoTaxaRequest);

            var planilha = await _exportadorPlanilhaService.ObterPlanilhaExportacaoTaxa(dadoCalculoTaxaUsina);

            using (var ms = new MemoryStream())
            {
                planilha.Save(ms, SaveFormat.Xlsx);

                return new ArquivoExportacao(
                    planilha.Worksheets[0].Name,
                    ConfiguracoesExportacao.RetornarExtensaoArquivoExcel(),
                     ms.ToArray());
            }
        }
    }
}
