using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.Application.Interfaces;
using ONS.SAGER.Calculo.Api.Application.Services;
using ONS.SAGER.Calculo.Api.Domain.Interfaces;
using ONS.SAGER.Calculo.Api.Domain.Services;

namespace ONS.SAGER.Calculo.Api.Application.Extensions
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<IExportacaoTaxaApplicationService, ExportacaoTaxaApplicationService>();
            services.AddSingleton<ITaxaService, TaxaService>();
            services.AddSingleton<IUnidadeGeradoraService, UnidadeGeradoraService>();
        }
    }
}
