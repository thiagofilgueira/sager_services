using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Api.DataAccess.Repositories;
using ONS.SAGER.Calculo.Api.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Api.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IConsolidacaoMensalRepository, ConsolidacaoMensalRepository>();
            services.AddSingleton<IEventoMudancaEstadoOperativoRepository, EventoMudancaEstadoOperativoRepository>();
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IPotenciaUnidadeGeradoraRepository, PotenciaUnidadeGeradoraRepository>();          
            services.AddSingleton<ITaxaRepository, TaxaRepository>();
            services.AddSingleton<IUnidadeGeradoraRepository, UnidadeGeradoraRepository>();
            services.AddSingleton<IUsinaRepository, UsinaRepository>();
            services.AddSingleton<ISuspensaoUnidadeGeradoraRepository, SuspensaoUnidadeGeradoraRepository>();
        }
    }
}
