using ONS.SDK.Impl.Data;
using ONS.SAGER.Calculo.Util.Domain.Entities;

namespace ONS.SAGER.Calculo.Api.Domain.Map
{
    public class EntitiesMap : AbstractDataMapCollection
    {
        protected override void Load()
        {
            BindMap<PotenciaUnidadeGeradora>();
            BindMap<UnidadeGeradora>();
            BindMap<ConsolidacaoMensal>();
            BindMap<CalculoTaxa>();
            BindMap<ParametroTaxa>();
            BindMap<Usina>();
            BindMap<EventoMudancaEstadoOperativo>();
            BindMap<SuspensaoUnidadeGeradora>();
        }
    }
}