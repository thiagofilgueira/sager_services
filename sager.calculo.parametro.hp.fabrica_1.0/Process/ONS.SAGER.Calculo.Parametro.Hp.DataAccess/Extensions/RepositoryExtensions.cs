﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hp.DataAccess.Repositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;

namespace ONS.SAGER.Calculo.Parametro.Hp.DataAccess.Extensions
{
    public static class RepositorExtensions
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroRepository, ParametroRepository>();
            services.AddSingleton<IControleCalculoParametroRepository, ControleCalculoParametroRepository>();
        }
}
}
