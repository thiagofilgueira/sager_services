﻿using ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Interfaces;
using ONS.SDK.Context;

namespace ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Services
{
    public class ExecutionContextAbstraction : IExecutionContextAbstraction
    {
        protected readonly IExecutionContext _executionContext;

        public ExecutionContextAbstraction(IExecutionContext executionContext)
        {
            _executionContext = executionContext;
            _executionContext.ExecutionParameter.Context.Memory.Commit = true;
            _executionContext.ExecutionParameter.SynchronousPersistence = true;
        }

        public void SetBranch(string branchId)
        {
            _executionContext.ExecutionParameter.MemoryEvent.Branch = branchId;
        }
    }
}
