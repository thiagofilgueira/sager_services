﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Services;

namespace ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Extensions
{
    public static class ExecutionContextAbstractionExtensions
    {
        public static void AddExecutionContextAbstraction(this IServiceCollection services)
        {
            services.AddSingleton<IExecutionContextAbstraction, ExecutionContextAbstraction>();
        }
    }
}
