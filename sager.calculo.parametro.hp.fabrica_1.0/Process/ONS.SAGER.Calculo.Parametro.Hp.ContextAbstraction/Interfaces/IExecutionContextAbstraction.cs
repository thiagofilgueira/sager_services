﻿namespace ONS.SAGER.Calculo.Parametro.Hp.ContextAbstraction.Interfaces
{
    public interface IExecutionContextAbstraction
    {
        void SetBranch(string branchId);
    }
}
