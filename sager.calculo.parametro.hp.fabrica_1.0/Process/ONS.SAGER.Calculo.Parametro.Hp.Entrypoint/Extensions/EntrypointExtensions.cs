﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SDK.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hp.Entrypoint.Services;

namespace ONS.SAGER.Calculo.Parametro.Hp.Entrypoint.Extensions
{
    public static class EntrypointExtensions
    {
        public static void AddEntrypointServices(this IServiceCollection services)
        {
            services.BindEvents<CalculosEntrypoint>();
            services.AddSingleton<CalculosEntrypoint>();
        }
    }
}
