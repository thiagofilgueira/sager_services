﻿using ONS.SAGER.Calculo.Parametro.Hp.Application.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Events;
using ONS.SAGER.Calculo.Util.Domain.Events.Payloads;
using ONS.SDK.Worker;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hp.Entrypoint.Services
{
    public class CalculosEntrypoint
    {
        private readonly IParametroApplicationService _calculoHpApplicationService;
        public CalculosEntrypoint(IParametroApplicationService calculoHpApplicationService)
        {
            _calculoHpApplicationService = calculoHpApplicationService;
        }

        [SDKEvent(Eventos.CalcularParametro)]
        public async Task CalcularParameroHp(CalcularParametroPayload payload)
        {
            await _calculoHpApplicationService.Calcular(
                new CalcularParametroHpRequest(
                     payload?.ConfiguracaoCenarioIdPayload,
                     payload?.ConsolidacaoMensalPayload?.MapToRequest(),
                     payload?.UnidadeGeradoraPayload?.MapToRequest(),
                     payload?.ControleCalculoIdPayload)
                );
        }

    }
}
