﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hp.Application.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Application.Services;
using System.Diagnostics.CodeAnalysis;

namespace ONS.SAGER.Calculo.Parametro.Hp.Application.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroApplicationService, ParametroApplicationService>();
        }
    }
}
