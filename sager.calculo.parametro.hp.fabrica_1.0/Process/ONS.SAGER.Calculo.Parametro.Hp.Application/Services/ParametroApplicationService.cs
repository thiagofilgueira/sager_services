﻿using ONS.SAGER.Calculo.Parametro.Hp.Application.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.Application;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Logger;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hp.Application.Services
{
    public class ParametroApplicationService : ParametroApplicationServiceBase<ParametroApplicationService>, IParametroApplicationService
    {
        private readonly IParametroService _parametroService;

        public ParametroApplicationService(
            IParametroService parametroService,
            IExecutionContextAbstraction executionContext,
            IControleCalculoParametroRepository controleCalculoParametroRepository,
            ICalculoLogger<ParametroApplicationService> logger)
                : base(executionContext, controleCalculoParametroRepository, logger)
        {
            _parametroService = parametroService;
        }

        public Task<Result> Calcular(CalcularParametroHpRequest request)
        {
            return CalcularParametro(
                request,
                TipoCalculo.HP,
                () => _parametroService.Calcular(request));
        }
    }
}
