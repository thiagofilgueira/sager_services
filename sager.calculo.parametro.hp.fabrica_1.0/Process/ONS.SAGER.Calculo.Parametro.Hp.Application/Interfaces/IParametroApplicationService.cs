﻿using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hp.Application.Interfaces
{
    public interface IParametroApplicationService 
    {
        Task<Result> Calcular(CalcularParametroHpRequest request);

    }
}
