﻿using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SDK.Impl.Data;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.Map
{
    public class EntitiesMap : AbstractDataMapCollection
    {
        protected override void Load()
        {
            BindMap<ParametroTaxa>();
            BindMap<ControleCalculoParametroTaxa>();
        }
    }
}
