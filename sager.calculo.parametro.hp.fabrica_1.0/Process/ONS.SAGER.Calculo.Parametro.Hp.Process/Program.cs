﻿using ONS.SDK.Impl.Builder;

namespace ONS.SAGER.Calculo.Parametro.Hp.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            var teste = AppBuilder.CreateDefaultBuilder(null)
                .UseStartup<Startup>();
                teste.RunSDK();
        }
    }
}
