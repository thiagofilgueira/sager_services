﻿using ONS.SAGER.Calculo.Util.Domain.Requests;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests
{
    public class CalcularParametroHpRequest : CalcularParametroRequest
    {
        public CalcularParametroHpRequest(
            string configuracaoCenarioId,
            ConsolidacaoMensalRequest consolidacaoMensal,
            UnidadeGeradoraRequest unidadeGeradora,
            string controleCalculoId)
        : base(
              unidadeGeradora,
              controleCalculoId,
              configuracaoCenarioId,
              consolidacaoMensal)
        {
        }
}
}
