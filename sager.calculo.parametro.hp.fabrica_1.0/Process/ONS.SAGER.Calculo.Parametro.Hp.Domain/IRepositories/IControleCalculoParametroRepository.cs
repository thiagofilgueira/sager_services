﻿using ONS.SAGER.Calculo.Util.DataAccess.Interfaces;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories
{
    public interface IControleCalculoParametroRepository : IControleCalculoParametroTaxaRepositoryBase
    {
    }
}
