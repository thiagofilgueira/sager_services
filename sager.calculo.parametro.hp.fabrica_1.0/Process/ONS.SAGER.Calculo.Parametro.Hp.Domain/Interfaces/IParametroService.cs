﻿using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Interfaces.Services;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces
{
    public interface IParametroService : IParametroServiceBase
    {
        Task<double?> Calcular(CalcularParametroHpRequest request);
    }
}
