﻿using Microsoft.Extensions.DependencyInjection;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Services;
using System.Diagnostics.CodeAnalysis;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class DomainExtensions
    {
        public static void AddDomainServices(this IServiceCollection services)
        {
            services.AddSingleton<IParametroService, ParametroService>();
            services.AddSingleton<IInsumoCalculoService, InsumoCalculoService>();
        }
    }
}
