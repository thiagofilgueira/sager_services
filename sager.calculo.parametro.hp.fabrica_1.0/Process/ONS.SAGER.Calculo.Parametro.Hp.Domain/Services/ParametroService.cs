﻿using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Enums;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Services;
using ONS.SAGER.Calculo.Util.Extensions;
using System.Threading.Tasks;

namespace ONS.SAGER.Calculo.Parametro.Hp.Domain.Services
{
    public class ParametroService : ParametroServiceBase, IParametroService
    {
        public ParametroService(
            IParametroRepository parametroHpRepository,
            IInsumoCalculoService insumoCalculoService)
            : base(parametroHpRepository, insumoCalculoService)
        {
        }

        public async Task<double?> Calcular(CalcularParametroHpRequest request)
        {
            var unidadeGeradora = request.UnidadeGeradora.MapToEntity();
            var dataReferencia = request.ConsolidacaoMensal.DataReferencia;

            var dataEntradaComercial = unidadeGeradora.CalcularEntradaOperacaoComercial();

            var dataInicio = ObterDataInicioCalculoParametro(dataReferencia, dataEntradaComercial);
            var dataFim = ObterDataFimCalculoParametro(dataReferencia, unidadeGeradora.DataDesativacao);

            var horasMes = dataFim.ObterDuracaoPeriodoEmHoras(dataInicio);

            var horasSuspensao = unidadeGeradora.Suspensoes.CalcularDuracaoSuspensoesPorPeriodo(dataInicio, dataFim);

            double valorParametro = horasMes > horasSuspensao ? horasMes - horasSuspensao : 0;

            SalvarInsumoCalculo(unidadeGeradora);

            await SalvarParametroVersionado(
                request.ConsolidacaoMensal.ConsolidacaoMensalId,
                request.UnidadeGeradora.UnidadeGeradoraId,
                request.ConfiguracaoCenarioId,
                request.ControleCalculoId,
                TipoCalculo.HP,
                valorParametro);

            return valorParametro;
        }

        private void SalvarInsumoCalculo(UnidadeGeradora unidadeGeradora)
        {

            InsumoCalculoService.AdicionarInsumos(unidadeGeradora.Eventos);
            InsumoCalculoService.AdicionarInsumos(unidadeGeradora.Suspensoes);
            InsumoCalculoService.AdicionarInsumos(unidadeGeradora.Potencias);
        }
    }
}
