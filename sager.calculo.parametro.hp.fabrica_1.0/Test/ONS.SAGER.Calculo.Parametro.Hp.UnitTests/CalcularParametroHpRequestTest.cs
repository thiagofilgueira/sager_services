using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hp.UnitTests
{
    public class CalcularParametroHpRequestTest
    {
        [Theory(DisplayName = "Teste para verificar se retornar falso quando configura��o do cen�rio � nula ou vazio")]
        [InlineData("")]
        [InlineData(null)]
        //Quando configura��o de cen�rio for Nulo ou Vazio, o resultado do c�lculo do 
        //par�metro deve ser falso
        //Teste passando vazio e nulo
        public void ResultadoDeveSerFalsoQuandoPassarConfiguracaoIdCenarioNuloOuVazio(string configuracaoCenarioId)
        {
            var calcularParametroHpRequest = new CalcularParametroHpRequest(
                configuracaoCenarioId,
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                new UnidadeGeradoraRequest(null, null, null, null, null, null,null,null),
                null);

            var result = calcularParametroHpRequest.Valido;

            Assert.False(result);
        }
        [Fact(DisplayName = "Teste para verificar se o resultado � falso quando data refer�ncia � nula ou vazia")]
        //Quando Data Refer�ncia for igual ao valor m�nimo, ou seja vazia ou nula
        //o m�todo deve retornar falso
        public void ResultadoDeveSerFalsoQuandoDataReferenciaForIgualAoValorMinimo()
        {
            var calcularParametroHpRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.MinValue),
                new UnidadeGeradoraRequest(null, null, null, null, null, null, null, null),
                null);

            var result = calcularParametroHpRequest.Valido;
            Assert.False(result);
        }

        [Fact(DisplayName = "Teste para verificar se retornar falso quando unidade geradora n�o est� preenchida")]
        //Quando Unidade geradora for nula,
        //o m�todo deve retornar Falso
        public void ResultadoDeveSerFalsoQuandoUnidadeGeradoraForNula()
        {
            var calcularParametroHpRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest(null,DateTime.Now),
                null,
                null);

            var result = calcularParametroHpRequest.Valido;

            Assert.False(result);
        }

        [Fact(DisplayName = "Teste para verificar se retorna false quando suspens�es n�o est�o preenchidas")]
        //Quando unidadade geradora n�o for preenchida
        //o m�todo de retornar falso
        public void DeveRetornarFalsoQuandoSuspensoesDaUnidadeGeradoraForemNulasOuVazias()
        {
            var calcularParametroHpRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                null,
                null);

            var result = calcularParametroHpRequest.PossuiSuspensoes();
            Assert.False(result);
        }


        [Fact(DisplayName = "Teste para verificar se quando suspens�o preenchida retorna verdadeiro")]
        //Quando unidade geradora estiver preenchida
        //o m�todo deve retornar verdadeiro
        public void DeveRetornarVerdadeiroQuandoSuspensoesDaUnidadeGeradoraEstiverPreenchida()
        {
            var calcularParametroHpRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest(null, DateTime.Now),
                new UnidadeGeradoraRequest(null,null,null,null,null,null,null,null, new List<SuspensaoUnidadeGeradoraRequest>()
                {
                   new SuspensaoUnidadeGeradoraRequest("1","1","1", DateTime.Now, DateTime.Now)
                }, 
                null),
                null);

            var result = calcularParametroHpRequest.PossuiSuspensoes();
            Assert.True(result);
        }
    }
}


