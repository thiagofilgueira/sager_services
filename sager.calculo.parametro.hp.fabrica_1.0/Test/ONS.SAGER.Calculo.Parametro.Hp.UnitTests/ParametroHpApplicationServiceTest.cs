using Moq;
using ONS.SAGER.Calculo.Parametro.Hp.Application.Services;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Util;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using ONS.SAGER.Calculo.Util.Extensions;
using ONS.SAGER.Calculo.Util.Logger;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hp.UnitTests
{
    public class ParametroHpApplicationServiceTest
    {
        private readonly IParametroService _parametroHpService;
        private readonly IExecutionContextAbstraction _executionContext;
        private readonly IControleCalculoParametroRepository _controleRepository;
        private readonly ICalculoLogger<ParametroApplicationService> _calculoLogger;

        public ParametroHpApplicationServiceTest()
        {
        var serviceMock = new Mock<IParametroService>();
            serviceMock.Setup(p => p.Calcular(It.IsAny<CalcularParametroHpRequest>())).ReturnsAsync(It.IsAny<double>());
            _parametroHpService = serviceMock.Object;

            var contextMock = new Mock<IExecutionContextAbstraction>();
            contextMock.Setup(p => p.SetBranch(It.IsAny<string>()));

            _executionContext = contextMock.Object;

            var controleRepository = new Mock<IControleCalculoParametroRepository>();
            _controleRepository = controleRepository.Object;

            _calculoLogger = new Mock<ICalculoLogger<ParametroApplicationService>>().Object;
        }

        [Fact(DisplayName = "Teste para verificar se retornar Bad Request quando o resultado � igual a null")]
        //Teste para verificar se retorna InternalError quando 
        //� passado um objeto parametroRequest null 
        public async Task DeveRetornarStatusBadRequestQuandoCalcularParametroHPRequestForNulo()
        {

            var serviceMock = new Mock<IParametroService>();

            var parametro = new ParametroApplicationService(serviceMock.Object, _executionContext, _controleRepository, _calculoLogger);

            CalcularParametroHpRequest parametroRequest = null;

            var result = await parametro.Calcular(parametroRequest);

            var statusResult = new Result(ResultStatus.BadRequest);
            Assert.True(result.Status == statusResult.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna Bad Request quando a requisi��o � vazio")]
        public async Task DeveRetornarBadRequestQuandoCalcularParametroHPRequestForInvalido()
        {
            var parametro = new ParametroApplicationService(_parametroHpService, _executionContext, _controleRepository, _calculoLogger);

            var parametroRequest = new CalcularParametroHpRequest(null, new ConsolidacaoMensalRequest(null, DateTime.MinValue), null, "1");

            var result = await parametro.Calcular(parametroRequest);

            var statusResult = new Result(ResultStatus.BadRequest);
            Assert.True(result.Status == statusResult.Status);
        }

        [Fact(DisplayName = "Teste para verificar se retorna sucesso quando a requisi��o est� preenchida corretamente")]
        public void DeveRetornarSucessQuandoCalcularParametroHPRequestForValido()
        {
            var parametro = new ParametroApplicationService(_parametroHpService, _executionContext, _controleRepository, _calculoLogger);

            var parametroRequest = new CalcularParametroHpRequest(
                Guid.NewGuid().ToString(),
                new ConsolidacaoMensalRequest(Guid.NewGuid().ToString(), DateTime.Now.ToFirstDayOfMonth()),
                new UnidadeGeradoraRequest("1", "1", "1", DateTime.Now, DateTime.Now.AddDays(1), DateTime.Now.AddDays(3), "1", new List<PotenciaUnidadeGeradoraRequest>(),
                new List<SuspensaoUnidadeGeradoraRequest>()
                {
                    new SuspensaoUnidadeGeradoraRequest("1","1","1", DateTime.Now, DateTime.Now.AddDays(2))
                },
                new List<EventoMudancaEstadoOperativoRequest>()
                {
                    new EventoMudancaEstadoOperativoRequest("1","1",DateTime.Now,"LIG","RPR","GCI","",10)
                }),
                "1"
                );

            var result = parametro.Calcular(parametroRequest).Result;

            Assert.Equal(ResultStatus.Success, result.Status);
        }
    }
}


