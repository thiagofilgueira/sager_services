using Moq;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Services;
using ONS.SAGER.Calculo.Util.Configuration;
using ONS.SAGER.Calculo.Util.ContextAbstraction.Interfaces;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hp.UnitTests
{
    public class DomainTests
    {
        [Fact]
        public void CalcularParametroHpTest()
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
                new SuspensaoUnidadeGeradoraRequest("1","1","1",new DateTime(2018,12,10),new DateTime(2019,01,02)),
                new SuspensaoUnidadeGeradoraRequest("2","2","1",new DateTime(2019,01,10),new DateTime(2019,01,15)),
                new SuspensaoUnidadeGeradoraRequest("3","3","1",new DateTime(2019, 01, 17),new DateTime(2019, 02, 10))
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), null, "1", null, suspensoes, null);


            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);

            var calculoRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)),
                uge,
                "1");

            // Act
            var parametroHp = service.Calcular(calculoRequest).Result;

            // Assert
            Assert.Equal(240, parametroHp);
        }

    }
}
