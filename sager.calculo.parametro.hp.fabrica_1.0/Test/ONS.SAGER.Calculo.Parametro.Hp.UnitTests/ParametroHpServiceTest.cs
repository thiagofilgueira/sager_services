using Moq;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Interfaces;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.IRepositories;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Models.Requests;
using ONS.SAGER.Calculo.Parametro.Hp.Domain.Services;
using ONS.SAGER.Calculo.Util.Domain.Entities;
using ONS.SAGER.Calculo.Util.Domain.Extensions;
using ONS.SAGER.Calculo.Util.Domain.Requests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hp.UnitTests
{
    public class ParametroHpServiceTest
    {
        private readonly ParametroService _service;

        public ParametroHpServiceTest()
        {
            _service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);
        }

        [Theory(DisplayName = "Teste para verificar se o resultado do c�lculo de dura��o das supens�o � zero quando as suspens�es s�o nulas")]
        [InlineData("01/02/2015", "01/02/2015")]
        [InlineData("31/03/2016", "31/03/2016")]
        [InlineData("28/02/2018", "28/02/2018")]
        [InlineData("05/04/2018", "05/04/2018")]
        public void DeveRetornarZeroQuandoSuspensoesNulas(string dataInicio, string dataFim)
        {
            List<SuspensaoUnidadeGeradora> listaDeSuspensoes = null;

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicio), DateTime.Parse(dataFim));

            Assert.Equal(0, result);
        }

        [Theory(DisplayName = "Teste para verificar se o resultado do c�lculo de suspens�es � igual a zero quando lista de suspens�es � vazias")]
        [InlineData("02/01/2014", "02/03/2014")]
        [InlineData("01/03/2014", "01/05/2018")]
        [InlineData("01/03/2015", "01/03/2014")]
        public void DeveRetornarZeroQuandoPassoListaDeSuspensoesVazias(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>();

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicio), DateTime.Parse(dataFim));

            Assert.Equal(0, result);
        }

        [Theory(DisplayName = "Teste para verificar se retorna zero quando n�o existe suspens�es e o sistema tenta c�lcular as suspens�es")]
        [InlineData("11/02/2015", "11/02/2015", "01/02/2018", "03/02/2018")]
        [InlineData("11/02/2015", "11/02/2015", "11/02/2015", "11/02/2015")]

        public void DeveRetornarZeroQuandoSuspensoesPeriodoForemNulas(
            string dataInicioSuspensao,
            string dataFimSuspensao, 
            string dataInicioCalculo,
            string dataFimCalculo)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicioSuspensao), DateTime.Parse(dataFimSuspensao))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicioCalculo),
               DateTime.Parse(dataFimCalculo));

            Assert.Equal(0, result);
        }

        [Theory(DisplayName = "Teste para verificar se retornar o c�lculo de 2 dias de suspens�o dentro de um intervalo")]
        [InlineData("04/03/2015", "08/03/2015", "04/03/2015", "06/03/2015")]
        [InlineData("25/02/2018", "02/03/2018", "28/02/2018", "02/03/2018")]

        public void DeveRetornarDiferenteDeZeroQuandoSuspensoesMaioresQueZero(
            string dataInicioSuspensao,
            string dataFimSuspensao, 
            string dataInicioCalculo,
            string dataFimCalculo)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicioSuspensao), DateTime.Parse(dataFimSuspensao))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicioCalculo), DateTime.Parse(dataFimCalculo));

            int number = 48;
            double correctNumber = Convert.ToDouble(number);

            if (result < 1)
            {
                Console.WriteLine("");
            }

            Assert.Equal(correctNumber, result);
        }

        [Theory]
        [InlineData("01/03/2015", "04/03/2015")]

        public void DeveRetornarDiferenteDeZeroQuandoSuspensoesMaioresQueZero2(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataFim))
            };

            var service = new ParametroService(new Mock<IParametroRepository>().Object, new Mock<IInsumoCalculoService>().Object);
            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(new DateTime(2015, 3, 1), new DateTime(2015, 3, 3));

            Assert.Equal(48, result);
        }

        [Theory]
        [InlineData("01/03/2015", "31/03/2015")]

        public void DeveRetornarDiferenteDeZeroQuandoSuspensoesMaioresQueZero3(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataFim))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(new DateTime(2015, 3, 1), new DateTime(2015, 3, 31));

            Assert.Equal(720, result);
        }

        [Fact]
        public void DeveRetornarDataEntradaOperacaoComercialQuandoDataOperacaoComercialForMaiorQueDataInicio()
        {
            var dataInicio = DateTime.Now;
            var dataEntradaOperacaoComercial = DateTime.Now.AddDays(1);

            var result = _service.ObterDataInicioCalculoParametro(dataInicio, dataEntradaOperacaoComercial);

            Assert.True(result > dataInicio);
        }

        [Theory]
        [InlineData("01/03/2015", "31/03/2015")]

        public void DeveRetornarDiferenteDeZeroQuandoSuspensoesMaioresQueZero4(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataFim))
            };
            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(new DateTime(2015, 3, 1), new DateTime(2015, 3, 2));

            Assert.Equal(24, result);
        }

        [Theory]
        [InlineData("01/03/2015", "30/04/2015")]

        public void DeveRetornarDiferenteDeZeroQuandoSuspensoesDoMesDeMarcoAteMesDeAbril(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataFim))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(new DateTime(2015, 3, 1), new DateTime(2015, 4, 30));

            Assert.Equal(1440, result);
        }

        [Fact]
        public void DeveRetornarDataInicioQuandoDataInicioForMaiorQueDataEntradaOperacaoComercial()
        {
            var dataEntradaOperacaoComercial = DateTime.Now;

            var dataInicio = DateTime.Now.AddDays(2);

            var result =  _service.ObterDataInicioCalculoParametro(dataInicio,dataEntradaOperacaoComercial);

            Assert.True(result > dataEntradaOperacaoComercial);
        }


        [Theory]
        [InlineData("03/03/2018", "04/03/2018")]

        public void DeveRetornarDataDeDesativacaoQuandoValorEstiverEntreDataInicioEMesSeguinte(string dataInicio, string dataDesativacao)

        {
            var mesSeguinte = DateTime.Parse(dataInicio).AddMonths(1);

            var result =  _service.ObterDataFimCalculoParametro(DateTime.Parse(dataInicio), DateTime.Parse(dataDesativacao));

            Assert.True(result >= DateTime.Parse(dataInicio) && DateTime.Parse(dataInicio) <= mesSeguinte);
        }

        [Theory]
        [InlineData("01/01/2015")]

        public void DeveRetornarMesSeguinteQuandoDataDesativacaoENula(string dataInicio)
        {
            var mesSeguinte = DateTime.Parse(dataInicio).AddMonths(1);

            DateTime? dataDesativacao = null;

            var result =  _service.ObterDataFimCalculoParametro(DateTime.Parse(dataInicio), dataDesativacao);

            Assert.True(result == mesSeguinte);
        }

        [Theory]
        [InlineData("01/03/2018", "03/03/2018", "01/03/2018")]
        public void DeveRetornarTotalDeHorasIgualAZeroQuandoTotalDeSuspensoesForMenorQueZero(string dataInicio, string dataFim, string dataDesativacao)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataDesativacao))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicio), DateTime.Parse(dataFim));

            Assert.Equal(0, result);
        }
        [Theory]
        [InlineData("03/03/2018", "01/04/2018", "04/03/2018")]
        public void DeveRetornarTotalDeHorasDaSubtracaoDaDataFimEDataInicioDaSuspensaoQuandoADataFimDaSuspensaoTiverValorEForMenorOuIgualADataFim(string dataInicio, string dataFim, string dataDesativacao)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataDesativacao))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicio), DateTime.Parse(dataFim));

            Assert.Equal(24, result);
        }
        [Theory]
        [InlineData("03/03/2018", "01/04/2018")]
        public void DeveRetornarTotalDeHorasDaSubtracaoDeDataFimMenorDataInicioQuandoDataInicioMenorQueDataFim(string dataInicio, string dataFim)
        {
            var listaDeSuspensoes = new List<SuspensaoUnidadeGeradora>()
            {
                new SuspensaoUnidadeGeradora("1", DateTime.Parse(dataInicio), DateTime.Parse(dataFim))
            };

            var result = listaDeSuspensoes.CalcularDuracaoSuspensoesPorPeriodo(DateTime.Parse(dataInicio), DateTime.Parse(dataFim));

            Assert.True(result == (DateTime.Parse(dataFim) - DateTime.Parse(dataInicio)).TotalHours);
        }

        [Fact]
        public async Task CalcularParametroHpComSuspensoesEmSerie()
        {
            // Arrange
            var suspensoes = new SuspensaoUnidadeGeradoraRequest[]
            {
                new SuspensaoUnidadeGeradoraRequest("1","1","1",new DateTime(2019,01,10),new DateTime(2019,01,15)),
                new SuspensaoUnidadeGeradoraRequest("2","2","1",new DateTime(2019, 01, 17),new DateTime(2019, 01, 28))
            };

            var uge = new UnidadeGeradoraRequest("1", "1", "1", new DateTime(2019, 01, 01), new DateTime(2019, 01, 01), null, "1", null,suspensoes, null);

            var calculoRequest = new CalcularParametroHpRequest(
                "1",
                new ConsolidacaoMensalRequest("1", new DateTime(2019, 01, 01)),
                uge,
                "1");

            // Act
            var parametroHp = await _service.Calcular(calculoRequest);

            // Assert
            Assert.Equal(360, parametroHp);
        }


    }
}