using ONS.SAGER.Calculo.Util.Domain.Entities;
using System;
using Xunit;

namespace ONS.SAGER.Calculo.Parametro.Hp.UnitTests
{
    public class UnidadeGeradoraTest
    {
        [Fact]
        public void DeveRetornarDataEmEntradaOperacaoQuandoDataEventoEOCForNulo()
        {
            var unidadeGeradora = new UnidadeGeradora()
            {
                DataEventoEOC = null,
                DataEntradaOperacao = DateTime.Now
            };

            var result = unidadeGeradora.CalcularEntradaOperacaoComercial();

            Assert.True(result == unidadeGeradora.DataEntradaOperacao);
        }

        [Fact]
        public void DeveRetornarDataEventoEOCQuandoDataEventoEOCNaoForNula()
        {
            var unidadeGeradora = new UnidadeGeradora()
            {
                DataEventoEOC = DateTime.Now,
                DataEntradaOperacao = null,
            };

            var result = unidadeGeradora.CalcularEntradaOperacaoComercial();

            Assert.True(result == unidadeGeradora.DataEventoEOC);
        }
    }
}


